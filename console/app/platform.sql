-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 08 月 02 日 14:19
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `platform`
--
CREATE DATABASE IF NOT EXISTS `platform` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `platform`;

-- --------------------------------------------------------

--
-- 表的结构 `ec_platform_setting`
--

CREATE TABLE IF NOT EXISTS `ec_platform_setting` (
  `keyname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ec_platform_setting`
--

INSERT INTO `ec_platform_setting` (`keyname`, `value`) VALUES
('client_qr_code', 'qr_code'),
('client_android', 'http://www.baidu.com/1111.exe'),
('client_from_ksyun', '1'),
('client_ipad', '333'),
('client_iphone', 'http://www.baidu.com/1111.exe'),
('client_mac', 'http://www.baidu.com/1111.exe'),
('client_windows', '222333444'),
('client_win_sync', 'ddf32'),
('display_offline_message', '''1'''),
('fromname', '金山企业云服务'),
('helpurl', ''),
('logo', ''),
('mailer', 'sendmail'),
('mailfrom', 'huangqianyuan@kinsoft.com'),
('offline', '0'),
('offline_image', ''),
('offline_message', '网站在维护中。<br /> 请稍候访问。'),
('sendmail', 'smtp88'),
('servicetel', '33123'),
('sitename', '浪潮云盘'),
('smtpauth', '1'),
('smtphost', '12122'),
('smtppass', '333'),
('smtpport', '25'),
('smtpsecure', '222'),
('smtpuser', '122');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
