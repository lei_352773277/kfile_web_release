<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author      Dwayne Charrington
 * @copyright  2014 Dwayne Charrington and Github contributors
 * @link           http://ilikekillnerds.com
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @version     2.0
 */
/**
 * Theme URL
 *
 * A helper function for getting the current theme URL
 * in a web friendly format.
 *
 * @param string $location
 * @return mixed
 */
function theme_url($location = '')
{
    $CI =& get_instance();
    return $CI->parser->theme_url($location);
}
/**
 * CSS
 *
 * A helper function for getting the current theme CSS embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('css') )
{
    function css($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->css($file, $attributes);
    }
}
/**
 * JS
 *
 * A helper function for getting the current theme JS embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('js') )
{
    function js($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->js($file, $attributes);
    }
}
/**
 * IMG
 *
 * A helper function for getting the current theme IMG embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('themes_img') )
{
    function themes_img($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->img($file, $attributes);
    }
}
/**
 * Session
 *
 * A helper function for getting a session variable alias of $this->session->userdata($name)
 *
 * @param $name
 */
if ( ! function_exists('userdata') )
{
    function userdata($name)
    {
        $CI =& get_instance();
        return $CI->session->userdata($name);
    }
}
/**
 * Uri segment
 *
 * A helper function for getting a uri segment alias of $this->uri->segment(n)
 *
 * @param $segnum
 */
if ( ! function_exists('uriseg') )
{
    function uriseg($segnum)
    {
        $CI =& get_instance();
        return $CI->uri->segment($segnum);
    }
}
/**
 * Flashdata
 *
 * A helper function for getting a flash message alias of $this->session->flashdata($name)
 *
 * @param $name
 */
if ( ! function_exists('flashdata') )
{
    function flashdata($name)
    {
        $CI =& get_instance();
        return $CI->session->flashdata($name);
    }
}

 // 十六进制转二进制
if(!function_exists("hex2bin")){
    function hex2bin($h) {
        if (! is_string ( $h )) {
            return null;
        }
        $r = '';
        for($a = 0; $a < strlen ( $h ); $a += 2) {
            $r .= chr ( hexdec ( $h {$a} . $h {($a + 1)} ) );
        }
        return $r;
    }
}

if(!function_exists("rc4")){
    function rc4 ($pwd, $data)//$pwd密钥　$data需加密字符串
    {
            $key[] ="";
            $box[] ="";
         
            $pwd_length = strlen($pwd);
            $data_length = strlen($data);
         
            for ($i = 0; $i < 256; $i++)
            {
                $key[$i] = ord($pwd[$i % $pwd_length]);
                $box[$i] = $i;
            }
         
            for ($j = $i = 0; $i < 256; $i++)
            {
                $j = ($j + $box[$i] + $key[$i]) % 256;
                $tmp = $box[$i];
                $box[$i] = $box[$j];
                $box[$j] = $tmp;
            }
            $cipher = "";
            for ($a = $j = $i = 0; $i < $data_length; $i++)
            {
                $a = ($a + 1) % 256;
                $j = ($j + $box[$a]) % 256;
         
                $tmp = $box[$a];
                $box[$a] = $box[$j];
                $box[$j] = $tmp;
         
                $k = $box[(($box[$a] + $box[$j]) % 256)];
                $cipher .= chr(ord($data[$i]) ^ $k);
            }
             
            return $cipher;
    }
}

if(!function_exists("checkPermission")) {
    //检测用户权限
    function checkPermission($user, $right) {
        if(2==PRODUCTMODEL) {
            if(in_array($right, array('backup_index','monitor_index','space_index'))) {
                return false;
            }
        }

        if($user['email'] =='wangpan@kingsoft.com') {
            return true;
        }
        if(empty($user)) {
            return false;
        } else {
            $right_map = array(
                //代理商
                "role_1"=>array("operate_info", "operate_dashboard","apply_agentrequest",
                    "operate_applypay","operate_listneedmepay"
                ),
                //销售
                "role_2"=>array("operate_dashboard","operate_info", "operate_listneedmepay",
                    "operate_listdomains","apply_applycheck","operate_applypay",
                    "operate_willend","apply_salesrequest",
                    "apply_balance","apply_auditlog",
                    "apply_listown"),
                //财务
                "role_3"=>array("operate_dashboard","apply_financecheck",
                    "apply_applycheck", "apply_auditlog","operate_alipay"),
                //管理员
                "role_4"=>array("operate_dashboard","operate_info", "operate_listdomains",
                    "operate_alipay", "apply_applycheck",
                    "operate_toupkingfile", "operate_postpone","operate_oneuser",
                    "operate_report","operate_willend","apply_auditlog",
                    "operate_users","apply_admincheck","operate_feedback",
                    "operate_linkcheck"),
                //数据查看者
                "role_5"=>array("operate_dashboard","operate_info", "operate_listdomains",
                    "operate_alipay", 
                    "operate_report"),
                //技术支持
                "role_6"=>array("operate_dashboard","operate_info", "operate_listdomains",
                    "operate_alipay", "operate_alipaynopayed"),
                //产品负责人
                "role_7"=>array("operate_dashboard","operate_info", "operate_listdomains",
                    "operate_alipay", "apply_applycheck",
                    "operate_toupkingfile", "operate_postpone","operate_oneuser",
                    "operate_report","operate_willend","apply_auditlog",
                    "operate_users","apply_admincheck","operate_feedback",
                    "operate_linkcheck")
            );
            $user_type = $user['user_type'];
            if(!isset($right_map["role_".$user_type])) {
                return false;
            } 

            if(!in_array($right, $right_map["role_".$user_type])) {
                return false;
            }

            return true;
        }
    }
}


if(!function_exists("getLoginedUser")) {
    //获取申请状态
    function getLoginedUser() {
        $CI =& get_instance();
        return $CI->account_info;
    }
}

if(!function_exists("formatUserType")) {
    function formatUserType($type) {
        $map = array("type1"=>"代理商","type2"=>"销售","type3"=>"财务","type4"=>'管理员','type5'=>'数据查看者','type6'=>'技术支持','type0'=>'未定义','type7'=>'产品负责人',); 
        return $map["type".$type]; 
    }
}


if(!function_exists("checkPortIsVaild")) {
    /*端口检测*/
    function checkPortIsVaild($host, $port) {
        $connection = @fsockopen($host, $port);
        if (is_resource($connection)) {
            fclose($connection);
            return false;
        } else {
            return true;
        }
    }
}