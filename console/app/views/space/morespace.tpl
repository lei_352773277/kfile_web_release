<div style="width:650px;margin-left: 20px;">
	<form action="post" id="morespaceform">
	<table  class="table table-striped table-bordered table-hover backupsettable" >
	    <tr>
	        <th width="150">当前服务器角色</th>
	        <td>
	            <select id="serverrole">
	            	<option value="master">主服务器</option>
	            	<!--<option>备服务器</option>-->
	            	<option value="node">数据节点</option>
	            </select>
	        </td>
	    </tr>
	    
	   	<tr id="masterip" style="display:none;">
	    	<th>主服务器IP：</th>
	    	<td><input type="text" style="width:400px;" value="" /></td>
	    </tr>
	    <tr id="alaisname" style="display:none;">
	    	<th>当前服务器别名：</th>
	    	<td><input type="text" style="width:400px;" value="" /></td>
	    </tr>
		
	   	<tr>
	    	<th></th>
	    	<td>
	    		<input type="submit"  class="btn btn-primary"  value="保存" />
	    	</td>
	    </tr>
	</div>
	</table>
	</form>
</div>
{literal}
    <script type="text/javascript">
        $(function () {
        	$("#serverrole").change(function(){
        		var v = $(this).val();
        		if ('node' == v) {
        			$("#masterip").show();
        			$("#alaisname").show();
        		} else {
        		    $("#masterip").hide();
        			$("#alaisname").hide();	
        		}
        	})

        	$("#morespaceform").submit(function(){
               $.ajax({
                    url:"../space/saveconfig", 
                    data:$("#morespaceform").serialize(), 
                    dataType:'json',
                    success:function (ret) {
	                    showTips({
	                        "command":true,
	                        "payload":"保存成功"
	                    });
                    },
                    error:function(){
                        showTips({
	                        "command":true,
	                        "payload":"保存失败"
	                    });
                    }
                });
        		return false;
        	})

        })
    </script>
{/literal}