<!DOCTYPE html>
<html lang="zh" ng-controller="homeController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/highcharts/highcharts.js"></script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
</head>
<body>
{include file="../common/nav.tpl"}
<div class="container-fluid" ui-view>
    {include file="../common/siderbar.tpl" title="空间情况"}
    <div class="toolbar ">
        <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
            <li role="presentation" {if $option=='space'}class="active"{/if}><a href="?option=space" >空间信息</a></li>
            <li role="presentation" class="hide" {if $option=='morespace'}class="active"{/if}><a href="?option=morespace" >扩容设置</a></li>
        </ul>
    </div>
    <div class="table-wrap" style="background:#fff;">

        <div>
            <div class="tab-content">
                {if $option=='space'}
                <div role="tabpanel" class="tab-pane active" id="space"><br/>{include file="./space.tpl" title="空间信息"}
                </div>
                {/if}
                {if $option=='morespace'}
                <div role="tabpanel" class="tab-pane active" id="space"><br/>{include file="./morespace.tpl" title="空间信息"}
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
</body>
</html>
{literal}
    <script type="text/javascript">
        $(function () {
            $.get("/platform/getspace", function (ret) {
                showPie(ret.data);
            }, 'json');


            function showPie(data) {
                data.total = data.total.toFixed(2);
                data.used = data.used.toFixed(2);

                $('#diskspace').highcharts({
                    chart: {
                        plotBackgroundColor:null,
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    legend: {
                        verticalAlign: 'top'
                    },
                    colors: [
                        '#ededed',
                        '#6bd79f'
                    ],
                    title: {
                        text: '存储集群空间容量 ( 共' + data.total + 'G )',
                        style:{
                            fontSize: '14px',
                            margin:'20px 50px 20px 20px'
                        },
                        align:'left'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            showInLegend:true,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name} {point.num}G'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '占总空间比例',
                        data: [
                            {
                                name: '已使用容量',
                                num: (data.total - data.used).toFixed(2),
                                y: (data.total - data.used) * 100 / data.total
                            },
                            {
                                name: '剩余容量',
                                num: data.used,
                                y: data.used * 100 / data.total,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
                });
            }


        })
    </script>
{/literal}