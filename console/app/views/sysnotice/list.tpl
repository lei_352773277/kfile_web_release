<div style="margin-top:10px;margin-left:10px;">
                <table class="table table-responsive table-striped table-bordered table-hover">
                    <tr>
                        <th style="width:5%">ID</th>
                        <th style="width:120px;">日期</th>
                        <th>内容</th>
                        <td style="width:7%">操作</td>
                    </tr>

                     {foreach from=$notice item="item"}
                    <tr>
                        <td>{$item['announce_id']}</td>
                        <td>{date("Y-m-d H:i", $item['ctime'])}</td>
                        <td><span style="font-weight:bold;"><a title="点击打开新窗口查看" target="_blank" href="show?id={$item['announce_id']}&sign={md5($item['announce_id'])}">{$item['title']|escape}</a></span> </td>
                        <td><a href="javascript:if(confirm('确定要删除吗？')){
                            location.href='del?id={$item['announce_id']}&sign={md5($item['announce_id'])}';
                        }">删除</a></td>
                    </tr>
                    {foreachelse}
                    <tr>
                        <td colspan="4">暂未找到系统公告</td>
                    </tr>
                     {/foreach}

                </table>
            </div>