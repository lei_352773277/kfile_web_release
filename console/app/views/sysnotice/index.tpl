<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">


    <link rel="stylesheet" type="text/css" href="{$common}/../simd/styles/simditor.css" />
    <link rel="stylesheet" type="text/css" href="{$common}/../simd/styles/simditor-html.css" />

    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    {*<script src="{$common}/jquery.validate/jquery.validate.min.js"></script>*}


    <script type="text/javascript" src="{$common}/../simd/scripts/module.js"></script>
    <script type="text/javascript" src="{$common}/../simd/scripts/hotkeys.js"></script>
    <script type="text/javascript" src="{$common}/../simd/scripts/simditor.js"></script>
    <script type="text/javascript" src="{$common}/../simd/scripts/beautify-html.js"></script>
    <script type="text/javascript" src="{$common}/../simd/scripts/simditor-html.js"></script>



    
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>

<script type="text/javascript">
    var app = angular.module("myApp", []);
    app.controller("myCtrl",function($scope, $http){



        $scope.saveTxt="发布";
        $scope.saveing=false;

        $scope.submit_update=function(form_name){
            $scope.saveTxt="发布中";
            $scope.saveing=true;
        }

    });

</script>
{literal}
    <style>
      .ipt-wrap {width:70%;}
    </style>
{/literal}
</head>
<body>
{include file="../common/nav.tpl"}

<div class="container-fluid" ng-app="myApp" ng-controller="myCtrl">

    {include file="../common/siderbar.tpl" title="公告管理"}
    <div class="toolbar ">
        <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
            <li role="presentation" class="{if $option=='all'}active{/if}">
                <a href="?option=all" >公告列表</a>
            </li>
            <li role="presentation" class="{if $option=='create'}active{/if}">
                <a href="?option=create" >发布新公告</a>
            </li>
        </ul>
    </div>
    <div class="table-wrap">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane {if $option=='all'}active{/if}" id="all">
                    <br/>
                    {include file="./list.tpl" notice=$notice['data']}</div>
                <div  class="tab-pane {if $option=='create'}active{/if}" id="create">
                    <br/>{include file="./form.tpl" title="发布窗口"}

                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
