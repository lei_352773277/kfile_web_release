<form class="form-horizontal" action="create" method="post" name="create">
    <fieldset>

        <div class="form-group">
            <label for="sitename" class="control-label">标题<span class="glyphicon glyphicon-info-sign" title="系统公告标题"></span>
            </label>
            <div class="ipt-wrap">
                <input type="text" class="form-control" name="title" required >
            </div>
        </div>

        <div class="form-group">
            <label for="helpurl" class="control-label">内容<span class="glyphicon glyphicon-info-sign"
                                                                   title="系统内容"></span></label>
            <div class="ipt-wrap">

                    <textarea id="content" name="content" placeholder="" autofocus></textarea>

                    <script type="text/javascript">
                        var editor = new Simditor({
                          textarea: $('#content'),
                          //optional options
                          toolbar:[
                              'title',
                              'bold',
                              'italic',
                              'underline',
                              'strikethrough',
                              'fontScale',
                              'color',
                              'ol',//ordered list
                              'ul',//unordered list
                              'blockquote',
                              'code',//code block
                              'table',
                              'link',
                              'image',
                              'hr',//horizontal ruler
                              'indent',
                              'outdent',
                              'alignment',
                              'html'
                            ],
                            allowedTags:['span','div','link','table','tr','td','br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre', 'code', 'h1', 'h2', 'h3', 'h4', 'hr','iframe'],
                            allowedAttributes:{
                                div:['class','style'],
                                link:['href','rel'],
                                img: ['src', 'alt', 'width', 'height', 'data-non-image'],
                                a: ['href', 'target','style'],
                                font: ['color'],
                                code: ['class'],
                                span:['class','style'],
                                i:['class','style'],
                                iframe:['src','height','width']
                            },
                            allowedStyles:{
                                span: ['color', 'font-size'],
                                b: ['color'],
                                i: ['color'],
                                strong: ['color'],
                                strike: ['color'],
                                u: ['color'],
                                p: ['margin-left', 'text-align'],
                                h1: ['margin-left', 'text-align'],
                                h2: ['margin-left', 'text-align'],
                                h3: ['margin-left', 'text-align'],
                                h4: ['margin-left', 'text-align']
                            }
                          
                        });

                        if(editor.getValue() == ''){
                                editor.setValue(document.getElementById("content").innerHTML)
                            }else{
                                document.getElementById("content").innerHTML = editor.getValue();
                            }
                            editor.on("valuechanged",function(){
                                console.info(editor.getValue());
                                document.getElementById("content").innerHTML = editor.getValue();
                            })

                    </script>

            </div>
        </div>

        <div class="form-group">
            <label  class="control-label">&nbsp;</label>
            <div class="ipt-wrap">
                <button type="submit" class="btn btn-primary" ng-disabled="savewhitelistform.$invalid||saveing" >{literal}{{saveTxt}}{/literal}</button>
                <a type="button"  class="btn btn-default" ng-disabled="saveing" ng-hide="true" href="?option=create" >取消</a>
            </div>
        </div>


        {if "savepok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}

    </fieldset>
</form>