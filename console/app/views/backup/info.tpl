
<div style="margin:0px 10px;">

{if $config['checkssh'] =='-1' || $config['backupnow']=='-1'}
    <a href="javascript:;" ng-disabled="true" class="btn btn-primary" >开启备份</a>
{else}
    {if $config['backupstatus']=='1'}
    <a href="?option=backupset" class="btn btn-primary" >关闭备份</a>
    {else}
    <a href="?option=backupset" class="btn btn-primary" >开启备份</a>
    {/if}
{/if}
{if $config['checkssh'] =='-1' || $config['backupnow']=='-1'}
    {if $config['checkssh'] =='-1'}
    <a href="javascript:;" ng-disabled="true" class="btn btn-default" >连通检测……</a>
    {else}
    <a href="javascript:;" ng-disabled="true" class="btn btn-default" >正在备份……</a>
    {/if}
{else}
    <a href="./invokebackup"  class="btn btn-default" >立即备份一次</a>
{/if}
<br/>
<br/>
{$str}
<table  class="table ng-table-responsive table-striped table-bordered table-hover" >
    <tr>
        <th>备份名称</th>
        <th>备份时间</th>
        <th>耗时</th>
        <th>备份方式</th>
        <th>备份文件大小</th>
        <th>备份到服务器</th>
        <th>备份状态</th>
    </tr>

    {foreach from=$loglist item=item}
    <tr>
        <td>
            {$item['directory']}
        </td>
        <td>
            {date("Y-m-d H:i:s",strtotime($item['directory']))}
        </td>
        <td>{$item['time']}</td>
        <td>
            {if $item['level']==0}
            全备
            {else}
            增备
            {/if}
        </td>
        <td>
             {$item['size']}
        </td>
        <td>
           {$item['comment']}
        </td>
        <td>
            {if 'Backup succeeded' == $item['status']}
            <span class='green'>{$item['status']}</span>
            {else}
            <span class='orange'>{$item['status']}</span>
            {/if}
        </td>
    </tr>
    {/foreach}
</table>
</div>
