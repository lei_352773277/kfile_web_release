<div style="margin:0px 10px;">
<table  class="table table-striped table-bordered table-hover backupsettable" >
    <tr>
        <th>状态：</th>
        <td>
            {if $config['checkssh'] =='-1'}
                <span class="orange">联通性检测中……</span>
            {else}
                {if empty($sshresult['fail']) && !empty($sshresult['succ'])}
                    {if $config['backupstatus']=='1'}
                        <span class="green">运行中</span>
                    {else}
                        <span class="green">连通性正常</span>
                    {/if}
                {else}
                    {if !empty($sshresult['fail']) }
                        <span class="orange">连通性失败[ {implode(",",$sshresult['fail'])} ]，请检查配置</span>
                    {else}
                        {if $config['backupstatus']=='0'}
                            <span class="orange">停止备份</span>
                        {else}
                            <span class="green">运行中</span>
                        {/if}
                    {/if}
                {/if}
            {/if}
        </td>
    </tr>
    <tr>
        <th width="150">设置备份起始时间：</th>
        <td>
        	<select ng-model="backuptime"  {if empty($sshresult['fail']) && !empty($sshresult['succ']) || $config['checkssh'] =='-1' || $config['backupstatus'] == '1'}ng-disabled="ture"{/if}>
                <option value="">00:00</option>
        		<option value="1">01:00</option>
        		<option value="2">02:00</option>
        		<option value="3">03:00</option>
        		<option value="4">04:00</option>
        		<option value="5">05:00</option>
        		<option value="6">06:00</option>
        		<option value="7">07:00</option>
        		<option value="8">08:00</option>
        		<option value="9">09:00</option>
        		<option value="10">10:00</option>
        		<option value="11">11:00</option>
        		<option value="12">12:00</option>
        		<option value="13">13:00</option>
        		<option value="14">14:00</option>
        		<option value="15">15:00</option>
        		<option value="16">16:00</option>
        		<option value="17">17:00</option>
        		<option value="18">18:00</option>
        		<option value="19">19:00</option>
        		<option value="20">20:00</option>
        		<option value="21">21:00</option>
        		<option value="22">22:00</option>
        		<option value="23">23:00</option>
        	</select>
        </td>
    </tr>
    <tr>
        <th>备份结果邮件通知：</th>
        <td><input type="radio" name="emailnotice" ng-model="emailnotice" {if $config['backuptime']=="1"}checked{/if} value="1" />是&nbsp;&nbsp;
            <input type="radio" {if $config['backuptime']!="1"}checked{/if}  ng-model="emailnotice" name="emailnotice" value="0" />否</td>
    </tr>
	<tr>
        <th>备份服务器IP：</th>
        <td><input type="text" name="backupserverip" ng-model="backupserverip" style="width:400px;" {if empty($sshresult['fail']) && !empty($sshresult['succ']) || $config['checkssh'] =='-1' || $config['backupstatus'] == '1'}ng-disabled="ture"{/if}  value="{$config['backupserverip']}" /></td>
    </tr>
   	<tr>
        <th>远程备份目录：</th>
        <td><input type="text" name="backuppath" ng-model="backuppath" style="width:400px;" {if empty($sshresult['fail']) && !empty($sshresult['succ']) || $config['checkssh'] =='-1' || $config['backupstatus'] == '1'}ng-disabled="ture"{/if}  value="{$config['backuppath']}" /></td>
    </tr> 
   	<tr>
        <th>SSH公钥：</th>
        <td><textarea name="backupsshkey" readonly="true" rows="10" style="width:400px;">{$idrsa}</textarea>
            <br/>
            <span class="gray">您可以通过以上公钥，配置您服务器允许ssh链接，以便传输备份文件</span>
        </td>
    </tr> 
    <tr>
        <th></th>
        <td>
            {if empty($sshresult['fail']) && !empty($sshresult['succ'])}
                <a href="javascript:;" ng-disabled="true"  class="btn btn-primary" >检测联通性</a>
            {else}
                {if $config['checkssh'] =='-1' || $config['backupstatus'] == '1'}
                <a href="javascript:;" ng-disabled="true"  class="btn btn-primary" >检测联通性</a>
                {else}
                <a href="javascript:;" ng-click="checkssh()"  ng-show="!(backupserverip==''||backuppath=='')" class="btn btn-primary" >检测联通性</a>
                <a href="javascript:;" ng-disabled="true"  ng-show="backupserverip==''||backuppath==''" class="btn btn-primary" >检测联通性</a>
                {/if}
            {/if}
            &nbsp;&nbsp;
            {if $config['backupstatus']=='0'}
                {if empty($sshresult['fail']) && !empty($sshresult['succ'])}
                    <a href="javascript:;" ng-click="startbackup()" class="btn btn-default" >开启备份</a>
                {else}
                    <a href="javascript:;" ng-disabled="true" class="btn btn-default" >开启备份</a>
                {/if}
            {else}
                <a href="javascript:;" ng-click="stopbackup()" class="btn btn-default" >关闭备份</a>
            {/if}
        </td>
    </tr> 
</table>
</div>
