<!DOCTYPE html>
<html lang="zh"  ng-controller="homeController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
    {literal}
    <style>
      table.backupsettable th{text-align:right;}
    </style>
    {/literal}
<script type="text/javascript">
  
  function reflesh()
  {
     setTimeout(function(){
          window.location.reload();
     },5000);
  }


{if $config['backupnow']=='-1' || $config['checkssh'] == '-1'}
      reflesh();
{/if}


var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http, $timeout,$location) {

    $scope.checkssh = function () {
        saveConfig();
    }

    $scope.startbackup = function () {
        $http.get('./startbackup').success(function(ret){
            window.location.reload();
        });
    }

    $scope.stopbackup = function () {
        $http.get('./stopbackup').success(function(ret){
            window.location.reload();
        });
    }

    $scope.backuptime = "{$config['backuptime']}";
    $scope.emailnotice = "{$config['emailnotice']}";
    $scope.backupserverip = "{$config['backupserverip']}";
    $scope.backuppath = "{$config['backuppath']}";

    function saveConfig () {

        var data = {
            'backuptime':$scope.backuptime,
            'emailnotice':$scope.emailnotice,
            'backupserverip':$scope.backupserverip,
            'backuppath':$scope.backuppath
        }

        var transform = function(data){
          return $.param(data);
        };
        $http.post('./writeDefaultConfig',data,{
          headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          transformRequest: transform
        }).success(function(ret){
          if(ret&&0==ret['code']) {
             //alert('成功');
              $http.get('./checkssh').success(function(ret){
                  window.location.reload();
              });
          }
        }).error(function(){

        });
    }

});

</script>

</head>
<body>
{include file="../common/nav.tpl"}
<div class="container-fluid"  ng-app="myApp" ng-controller="myCtrl">
        {include file="../common/siderbar.tpl" title="数据库备份"}
       
        <div class="toolbar ">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
                  <li role="presentation" class="{if $option=='backupinfo'}active{/if}"><a href="?option=backupinfo">备份信息</a></li>
                  <li role="presentation" class="{if $option=='backupset'}active{/if}"><a href="?option=backupset">备份设置</a></li>
                </ul>
        </div>
       
        
        <div class="table-wrap"  style="background:#fff;">
              <div class="monitordetail">
                <!-- Tab panes -->
                <div class="tab-content">
                  {if $zrmDirExits}
                    {if $option=='backupinfo'}
                    <div role="tabpanel" class="tab-pane active" id="backupinfo"><br/>{include file="../backup/info.tpl" title="备份信息"}</div>
                    {/if}
                    {if $option=='backupset'}
                    <div role="tabpanel" class="tab-pane active" id="backupset"><br/>{include file="../backup/setconfig.tpl" title="备份设置"}</div>
                    {/if}
                  {else}
                     <div role="tabpanel" class="tab-pane active" id="backupset"><br/>&nbsp;&nbsp;&nbsp;&nbsp;备份组件未就绪</div>
                  {/if}
                </div>
              </div>
        </div>
</div>
</body>
</html>
 
