<form class="form-horizontal" action="" id="savenetwork" method="post">
    <fieldset>

        <div class="form-group">
            <label class=" control-label">IP地址<span class="glyphicon glyphicon-info-sign"
                                                                       title="设置IP地址"></span></label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <input class="form-control" disabled="" value="{$network['ip']}"/>
                {else}
                    <input type="text" class="form-control" id="ip" name="ip" value="{$network['ip']}" placeholder="请输入IP地址例如（192.168.1.100）"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="serverkey" class=" control-label">子网掩码<span class="glyphicon glyphicon-info-sign"
                                                                    title="设置子网掩码"></span></label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <input class="form-control" disabled="" value="{$network['mask']}"/>
                {else}
                    <input type="text" class="form-control" id="mask" name="mask" value="{$network['mask']}" placeholder="请输入子网掩码例如（255.255.255.0）"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="serverkey" class=" control-label">网关地址<span class="glyphicon glyphicon-info-sign"
                                                                    title="设置网关地址"></span></label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <input class="form-control" disabled="" value="{$network['gateway']}">
                {else}
                    <input type="text" class="form-control" id="gateway" value="{$network['gateway']}" name="gateway" placeholder="请输入网关地址例如（192.168.1.1）"/>
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label for="serverkey" class=" control-label">访问地址<span class="glyphicon glyphicon-info-sign"
                                                                    title="如果需要公网访问，请输入公网访问地址，并保证内网也能通过该地址访问服务，否则留空"></span></label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <input class="form-control" disabled="" value="{$network['accessurl']}">
                {else}
                    <input type="text" class="form-control" id="accessurl" name="accessurl"  value="{$network['accessurl']}"
                           placeholder="如需公网访问，请输入公网访问地址，并保证内网也能通过该地址访问服务，不需要则留空"/>
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label for="serverkey" class=" control-label">端口<span class="glyphicon glyphicon-info-sign"
                                                                  title="为系统配置访问端口，默认80端口"></span></label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <input class="form-control" style="width:80px;float:left;display:inline-block;" disabled=""
                           value="{$network['port']}">
                {else}
                    <input type="text" class="form-control" style="width:80px;float:left;display:inline-block;"
                           name="port"  id="port" value="{$network['port']}" placeholder="80"/>
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label for="serverkey" class=" control-label">MAC地址<span class="glyphicon glyphicon-info-sign"
                                                                     title="设置MAC地址"></span></label>
            <div class="ipt-wrap  monitordetail" style="min-width: 600px;">
                {if empty($editnetwork)}
                    <input class="form-control" style="width:200px;display: inline-block;" disabled value="{$hardware['hard']['- MAC address'][0]}">
                {else}
                    <input class="form-control" style="width:200px;display: inline-block;"  value="{$hardware['hard']['- MAC address'][0]}">
                {/if}
                  {if $hardware['hard']['- network interface status']}
                      <span  style="display: inline-block;padding-left: 15px;" class="help-block">状态<span>{$hardware['hard']['- network interface status'][0]}</span></span>
                  {/if}

            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail" class=" control-label">&nbsp;</label>
            <div class="ipt-wrap ">
                {if empty($editnetwork)}
                    <a type="button" class="btn btn-primary" href="?editnetwork=1">修改</a>
                {else}
                    <button type="submit" class="btn btn-primary" id="submit_network">保存</button>
                    <button type="button" class="btn btn-default" href="?" id="cancle_network">取消</button>
                {/if}
            </div>
        </div>

        <div class="form-group hide" id="showmessage">
            <label for="inputEmail" class=" control-label">&nbsp;</label>
            <div class="ipt-wrap">
                <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
            </div>
        </div>


    </fieldset>
</form>