<form class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="serverkeyurl" class="control-label">系统当前版本</label>
            <div class="ipt-wrap ipt-wrap-all ">
                <span class="control-label" id="version"></span>
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail" class="control-label">可用的升级版本</label>
            <div class="ipt-wrap ipt-wrap-all">
                <span class="control-label" id="newversion"></span>
                {*<ul>
                  <li ng-repeat="item in newversionlog">item</li>
                </ul>*}
            </div>
        </div>
        <div class="form-group" >
            <label for="inputEmail" class="control-label">&nbsp;</label>
            <div class="ipt-wrap ipt-wrap-all">
                <a type="button" class="btn btn-primary"  onclick="getNewVersion()">获取新版本</a>
            </div>
        </div>
        <div style="display: none;" class="form-group " id="update_wrap" >
            <label for="inputEmail" class="control-label"></label>
            <div class="ipt-wrap">
                <p class="bg-danger" style="padding:5px;">
                    <strong>注意 </strong>
                    更新过程需要从金山云端拉取软件更新包，请确保网络可达，更新耗时为2-3小时，更新过程中云盘无法正常提供服务，请谨慎操作。
                </p>
                <button type="button" class="btn btn-primary" id="updateIpt" onclick="startUpdateSoftWare()">确定要升级
                </button>
                {*<button class="btn btn-default" id="updateIpt_cancle" ng-hide="showUpdateLog" ng-click="hasnew=false">取消</button>*}
            </div>
        </div>
        <div class="form-group" style="display: none" id="showlog" >
            <label for="serverkeyurl" class="control-label">升级日志 </label>
            <div class="">
                <textarea style="width:350px;height:400px;" id="log">systemlog</textarea>
            </div>
        </div>


    </fieldset>
</form>

<script type="text/javascript">
    $(function(){
        initUpdateData();
    });
    function initUpdateData(){
        //log
        //getUpdatelog("");
        //获取当前版本
        getVersion();
        //new
        getNewVersion();

    }

    function getVersion(){
        $("#version").text("获取版本中...");
        $.get("/platform/getVersion", function (ret) {
            if (ret.code == 0) {
                $("#version").text(ret.data);
            } else {
                $("#version").text("获取版本失败");
            }
        }, 'json');
    }
    function getNewVersion(){
        var elem=$("#newversion");
        elem.text("正在获取新版本");
        $.get("/platform/getnewversion", function (ret) {
            /*
            ret={
                code:0,
                data:1
            };
            */
            if (ret.code == 0) {
                elem.text(ret.data);
                $("#update_wrap").show();
            } else {
                elem.text("您已经是最新版本，无需更新");
            }
        }, 'json');
    }

    function getUpdatelog(sort){
        $("#showlog").show();
        var elem=$("#log");
        elem.val("升级任务初始化……");
        $.get("/platform/getsyslog?sort="+sort, function (ret) {
            if (ret.code == 0) {
                elem.val(ret.data);
            } else {
                elem.val("systemlog");
            }
        }, 'json');
    }
    function closeUpdatelog(){
        $("#showlog").hide();
        var elem=$("#log");
        elem.val("");
    }

    function startUpdateSoftWare(){
        disabledIpt("updateIpt", true);

        $.get("/platform/updatesoftware", function (ret) {
            if (ret.code == 0) {
                getUpdatelog("first");
            } else {
                disabledIpt("updateIpt", false);
                closeUpdatelog();
            }
        }, 'json');
    }


    function disabledIpt(elem, action) {
        if (true == action) {
            $("#" + elem).prop("disabled", true)
        } else {
            $("#" + elem).prop("disabled", false)
        }
    }
</script>