<form class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="serverkeyurl" class=" control-label">重启服务器：<span class="glyphicon glyphicon-info-sign"
                                                                         title="需要重新启动计算机"></span></label>
            <div class="ipt-wrap">
                <button class="btn btn-primary" id="restart" type="button"
                        {if $keys['restart']==1} disabled="disabled" {/if}
                        onclick="popReboot()" >
                    确定
                </button>
                <p class="help-block" id="message_restart"></p>
            </div>
        </div>

        <div class="form-group">
            <label for="serverkeyurl" class=" control-label">关闭服务器：<span class="glyphicon glyphicon-info-sign"
                                                                         title="需要关闭计算机"></span></label>
            <div class="ipt-wrap">
                <button class="btn btn-primary" id="poweroff" type="button" {if $keys['shutdown']==1} disabled="disabled" {/if} onclick="popPoweroff()">确定
                </button>
                <p class="help-block" id="message_poweroff"></p>
            </div>

        </div>

    </fieldset>
</form>

<div class="modal " id="modalReboot">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cancel()" aria-hidden="true">×</button>
                <h4 class="modal-title">重启计算机</h4>
            </div>
            <div class="modal-body">
                <div class="title" style="text-align: center;padding: 15px 10px;">您确定要重启计算机吗?</div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="confirm_restart()" id="restartIpt">确定</button>
                <button class="btn btn-default" onclick="cancel()">取消</button>
            </div>
        </div>
    </div>
</div>
<div class="modal " id="modalpoweroff">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cancel()" aria-hidden="true">×</button>
                <h4 class="modal-title">关闭计算机</h4>
            </div>
            <div class="modal-body">
                <div class="title" style="text-align: center;padding: 15px 10px;">您确定要关闭计算机吗?</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="confirm_poweroff()" id="poweroffIpt">确定</button>
                <button class="btn btn-default" onclick="cancel()">取消</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var sending = false;
    function popReboot() {
        openModal("modalReboot", "show");
    }
    function popPoweroff() {
        openModal("modalpoweroff", "show");
    }
    function openModal(id, action) {
        $(".modal").hide();
        if ("show" == action) {
            $("#" + id).show();
        }
    }
    function cancel() {
        $(".modal").hide();
    }
    function disabledIpt(elem, action) {
        if (true == action) {
            $("#" + elem).prop("disabled", true)
        } else {
            $("#" + elem).prop("disabled", false)
        }
    }
    function confirm_restart(){
        disabledIpt("restart", true);
        var data = {
            'action':'1'
        };
        $.post("../systeminfo/restart", data, function (ret) {
            openModal("modalReboot", "hide");
            if(ret.code==0){
                $("#message_restart").show();
                $("#message_restart").text("正在重启服务器");
            }else{
                disabledIpt("restart", false);
                $("#message_restart").text("").hide();
            }

        }, 'json');
    }
    function confirm_poweroff() {
        disabledIpt("poweroff", true);
        var data = {
            'action':'1'
        };
        $.post("../systeminfo/shutdown", data, function (ret) {
            openModal("modalpoweroff", "hide");
            if (ret.code == 0) {
                $("#message_poweroff").show().text("正在关闭服务器");
            } else {
                disabledIpt("poweroff", false);
                $("#message_poweroff").text("").hide();
            }
        }, 'json');
    }
</script>