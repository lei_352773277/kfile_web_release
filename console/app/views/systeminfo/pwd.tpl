<form class="form-horizontal" ng-controller="myCtrl" name="pwdform">
          <fieldset>
                <div class="form-group" ng-class="{ 'has-error' :  pwdform.nowpwd.$dirty  &&pwdform.nowpwd.$invalid }">
                    <label for="serverkeyurl" class="control-label">密码</label>
                    <div class="ipt-wrap ">
                        <input type="password" required  class="form-control" name="nowpwd" ng-model="nowpwd" placeholder="请输入当前密码">
                        <p ng-show="pwdform.nowpwd.$dirty  && pwdform.nowpwd.$invalid" class="help-block">请输入当前密码</p>
                    </div>
                </div>

                <div class="form-group" ng-class="{ 'has-error' :pwdform.newpwd.$dirty  && pwdform.newpwd.$invalid }">
                    <label for="serverkeyurl" class="control-label">新密码</label>
                    <div class="ipt-wrap ">
                        <input type="password" required class="form-control" {literal}ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)]){6,32}$/" {/literal}
                               minlength="6" maxlength="32" name="newpwd" placeholder="请输入6-20位的新密码" ng-model="newpwd">
                        <p ng-show="pwdform.newpwd.$dirty  &&pwdform.newpwd.$invalid" class="help-block">密码格式为（6-32位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合）</p>
                    </div>
                </div>

                <div class="form-group" ng-class="{ 'has-error' : pwdform.confirmpwd.$dirty  &&pwdform.confirmpwd.$invalid ||newpwd!=confirmpwd }">
                    <label for="serverkeyurl" class="control-label">确认新密码</label>
                    <div class="ipt-wrap ">
                        <input type="password" required class="form-control" minlength="6" maxlength="32" {literal}ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)]){6,32}$/" {/literal}
                               name="confirmpwd" placeholder="请确认新密码" ng-model="confirmpwd">
                        <p ng-show="pwdform.confirmpwd.$dirty  &&pwdform.confirmpwd.$invalid ||newpwd!=confirmpwd" class="help-block">密码格式为（6-32位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合），请保持跟两次密码输入一致</p>
                    </div>
                </div>

                <div class="form-group">
                   <label for="inputEmail" class="control-label">&nbsp;</label>
                    <div class="ipt-wrap ">
                        <button class="btn btn-primary" ng-click="savepwd()" id="pwdconfirmIpt" ng-disabled="pwdform.$invalid||(newpwd!=confirmpwd)||saveing">确定</button>
                        <p class="help-block" id="message" style="display: block;">{literal}{{message}}{/literal}</p>
                        <p class="bg-success successtips " ng-show="saveok"  id="suc"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                    </div>
                </div>

          </fieldset>
</form>
<script type="text/javascript">

    String.prototype.trim=function(){
        return this.replace(/(^\s*)|(\s*$)/g, "");
    };

    app.controller("myCtrl",function($scope,$http){
        $scope.nowpwd="";
        $scope.newpwd="";
        $scope.confirmpwd="";
        $scope.saveing=false;
        $scope.message="";
        $scope.saveok=false;

        var transform = function(data){
            return $.param(data);
        };


        $scope.savepwd=function(){
            $scope.saveing=true;
            var data={
                'nowpwd':$scope.nowpwd.trim(),
                'newpwd':$scope.newpwd,
                'confirmpwd':$scope.confirmpwd
            };
            $http.post("../account/changepwd",data,{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: transform
            }).success(function(ret){
                $scope.saveing=false;
                console.info(ret);
                if (ret.code == 0) {
                    $scope.saveok=true;
                    $scope.message="";
                } else {
                    $scope.saveok=false;
                    $scope.message=ret.msg;
                }
            }).error(function(){
                $scope.saveing=false;
                $scope.saveok=false;
                $scope.message="保存失败，请重试";
            })
        }


    });


    /*function savepwd(){
        disabledIpt("pwdconfirmIpt", true);
        var data = {
            'nowpwd': $.trim($("[name='nowpwd']").val()),
            'newpwd':$.trim($("[name='newpwd']").val()),
            'confirmpwd':$.trim($("[name='confrimpwd']").val())
        };
        $.post("../account/changepwd", data, function (ret) {
            disabledIpt("pwdconfirmIpt", false);
            //console.info(ret);
            $("#message").show();
            if (ret.code == 0) {
                $("#message").text("").hide();
                $("#suc").show();
            } else {
                $("#message").text(ret.msg);
            }
        }, 'json');
    }*/
</script>