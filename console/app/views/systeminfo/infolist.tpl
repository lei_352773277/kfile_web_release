<form class="form-horizontal">
          <fieldset>
                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">产品编号</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                        <label class="control-label">{$hardware['uuid']}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">CPU型号</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                      {foreach from=$hardware['hard']['- CPU'] item="item"}
                      <div>
                        <label class="control-label">{$item}</label>
                      </div>
                      {/foreach}
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">内存序号</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                      {foreach from=$hardware['hard']['- RAM'] item="item"}
                      <div>
                        <label class="control-label">{$item}</label>
                      </div>
                      {/foreach}
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">硬盘序号</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                    {foreach from=$hardware['hard']['- hard disk in Megacli'] item="item"}
                        <div>
                          <label class="control-label">{$item}</label>
                        </div>
                    {/foreach}
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">网卡地址</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                        {foreach from=$hardware['hard']['- MAC address'] item="item"}
                        <div>
                          <label class="control-label">{$item}</label>
                        </div>
                        {/foreach}
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">服务提供商</label>
                    <div class=" ipt-wrap ipt-wrap-all">
                        <label class="control-label">北京金山云网络技术有限公司</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="serverkeyurl" class=" control-label">授权凭证</label>
                    <div class=" ipt-wrap">
                        <textarea style="width:350px;height:100px;">{$hardware['user_code']}</textarea><br/><br/>
                        点击 <a href="http://e.ksyun.com/product/active?sn={$hardware['uuid']}&id={$hardware['user_code']}" target="_blank" style="font-weight:bold;text-decoration: underline;color: #0080ff;">这里</a> 获取企业授权码
                    </div>
                </div>


          </fieldset>
        </form>