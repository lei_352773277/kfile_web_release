<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
    <script type="text/javascript">
        var app=angular.module("myApp",[]);

    </script>
</head>
<body>
{include file="../common/nav.tpl"}


<div class="container-fluid" ui-view ng-app="myApp">
    {include file="../common/siderbar.tpl" title="系统管理"}
    <div class="toolbar">
        <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
            {if empty($config_env)}
            <li role="presentation" class="active"><a href="#network" aria-controls="network" role="tab"
                                                      data-toggle="tab">网络设置</a></li>
            {/if}

            <li role="presentation" {if !empty($config_env)}class="active"{/if}><a href="#power" aria-controls="power" role="tab" data-toggle="tab">关闭重启</a></li>
            <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">修改密码</a>
            </li>
            <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">系统信息</a></li>
            <li role="presentation"><a href="#update" aria-controls="update" role="tab" data-toggle="tab">系统升级</a></li>
        </ul>
    </div>
    <div class="table-wrap">
        <div>
            <div class="tab-content">
                {if empty($config_env)}
                <div role="tabpanel" class="tab-pane active" id="network">
                    <br/>{include file="../systeminfo/network.tpl" title="网络设置"}</div>
                {/if}
                <div role="tabpanel" class="tab-pane {if !empty($config_env)}active{/if}" id="power">
                    <br/>{include file="../systeminfo/poweroff.tpl" title="关闭重启"}</div>
                <div role="tabpanel" class="tab-pane" id="password">
                    <br/>{include file="../systeminfo/pwd.tpl" title="修改密码"}</div>
                <div role="tabpanel" class="tab-pane" id="info">
                    <br/>{include file="../systeminfo/infolist.tpl" title="系统信息"}</div>
                <div role="tabpanel" class="tab-pane" id="update">
                    <br/>{include file="../systeminfo/update.tpl" title="系统升级"}</div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
{literal}
<script type="text/javascript">
/* 判断IP地址是否合法 */
    var judgeIpIsLegal = function(ipAddr){
        var regIps = /^(((25[0-5]|2[0-4]\d|1\d{2}|[1-9]\d|[0-9])\.){3}(25[0-5]|2[0-4]\d|1\d{2}|[1-9]\d|[0-9]))$/;
        return regIps.test(ipAddr);
    };
    /* IP地址转换为二进制字符串 */
    /* 例如：172.16.4.235 --> 10101100000100000000010011101011 */
    var praseIpToBinary = function(ipAddress){
        var numArray = ipAddress.split(".");
        if(numArray.length != 4){
            showTips({
                command:false,
                payload:"IP地址输入有误"
            });
            return;
        }
        var returnIpStr = "";
        for (var i = 0; i < 4; i++) {
            var curr_num = numArray[i];
            var number_Bin = parseInt(curr_num);
            number_Bin = number_Bin.toString(2);
            var iCount = 8-number_Bin.length;
            for (var j = 0; j < iCount; j++) {
                number_Bin = "0"+number_Bin;
            }
            returnIpStr += number_Bin;
        }
        return returnIpStr;
    };

    /* 判断子网掩码是否合法 */
    /* 子网掩码必须是 1 和 0组成的连续的一段 如 11110000 */
    var judgeSubnetMask = function(ipAddress){
        var binaryIpString = praseIpToBinary(ipAddress).toString();
        var subIndex = binaryIpString.lastIndexOf("1")+1;
        var frontHalf = binaryIpString.substring(0,subIndex);
        var backHalf = binaryIpString.substring(subIndex);
        if(frontHalf.indexOf("0") != -1 || backHalf.indexOf("1") != -1){
            return false;
        }else{
            return true;
        }
    }
    /* 两个IP地址做 与 操作 返回结果 */
    /* 该功能主要用来实现 IP地址和子网掩码 相与，获取当前IP地址的IP地址段 */
    /* 以此来验证输入的网关地址是否合法 */
    var getIPsAndResult = function(ipAddr1,ipAddr2){
        var ipArray1 = ipAddr1.split(".");
        var ipArray2 = ipAddr2.split(".");
        var returnResult = "";
        if(ipArray1.length != 4 || ipArray2.length != 4 ){
            showTips({
                command:false,
                payload:"IP地址输入有误"
            });
            return;
        }
        for (var i = 0; i < 4; i++) {
            var number1 = parseInt(ipArray1[i]);
            var number2 = parseInt(ipArray2[i]);
            returnResult += number1&number2;
            if(i<3){
                returnResult += ".";
            }
        }
        return returnResult;
    }
    /* 判断网关地址是否合法 */
    var judgeGatewayResult = function(ipAddr,subnetMask,gateway){
        var andResult1 = getIPsAndResult(ipAddr,subnetMask);
        var andResult2 = getIPsAndResult(gateway,subnetMask);
        if(andResult1 == andResult2){
            return true;
        }else{
            return false;
        }
    }


    $(function () {
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })

        $("#savenetwork").submit(function(){
            var ip = $("#ip").val();
            var mask = $("#mask").val();
            var gateway = $("#gateway").val();

            if( !judgeIpIsLegal(ip)) {
                showTips({
                    command:false,
                    payload:"IP地址输入有误"
                });
               $("#ip").focus();
               return false;
            }

            if( !judgeIpIsLegal(mask) ) {
                showTips({
                    command:false,
                    payload:"子网掩码输入有误"
                });
               $("#mask").focus();
               return false;
            }

            if( !judgeSubnetMask(mask)) {
                showTips({
                    command:false,
                    payload:"子网掩码输入有误"
                });
               $("#mask").focus();
               return false;
            }

            if( !judgeGatewayResult(ip, mask, gateway)) {
                showTips({
                    command:false,
                    payload:"网关地址输入错误"
                });
                $("#gateway").focus();
               return false;
            }

            if (ip != "192.168.1.100") {
              if( judgeGatewayResult(ip,'255.255.255.0','192.168.1.1')) {
                  showTips({
                      command:false,
                      payload:"IP地址暂不支持192.168.1.1同网段"
                  });
                $("#ip").focus();
                 return false;
              }
            }

            disabledIpt("submit_network", true);
            disabledIpt("cancle_network", true);

            $.post("../systeminfo/savenetwork", $("#savenetwork").serialize(), function(data){

                disabledIpt("submit_network", false);
                disabledIpt("cancle_network", false);

                     if(data&&data.code==0) {
                         showTips({
                             command:true,
                             payload:"成功"
                         });
                         window.setTimeout(function(){
                             location.href="../systeminfo/index";
                         },1500);

                     } else {

                         showTips({
                             command:false,
                             payload:data.msg
                         });

                     }
                  },'json');
            return false;    
        })

       

    })

</script>
{/literal}