<form class="form-horizontal" action="saveconv" method="post">
  <fieldset>
        <div class="form-group">
            <label for="offline" class="control-label">可转换图片类型：<span class="glyphicon glyphicon-info-sign"  title="可转换图片类型"></span></label>
            <div class="ipt-wrap">
                  <input  class="form-control"  type="text" name="pictype" placeholder="多个类型以竖线 | 分割"  />
            </div>
        </div>

        <div class="form-group">
            <label for="offline" class="control-label">可转换文档类型：<span class="glyphicon glyphicon-info-sign"  title="可转换文档类型"></span></label>
            <div class="ipt-wrap">
                  <input  class="form-control"  type="text" name="doctype" placeholder="多个类型以竖线 | 分割"  />
            </div>
        </div>

        <div class="form-group">
            <label for="offline" class="control-label">可转换音视频：<span class="glyphicon glyphicon-info-sign"  title="可转换文档类型"></span></label>
            <div class="ipt-wrap">
                  <input  class="form-control"  type="text" name="videotype" placeholder="多个类型以竖线 | 分割"  />
            </div>
        </div>

        <div class="form-group">
            <label for="offline" class="control-label">可转换音视频大小上限：<span class="glyphicon glyphicon-info-sign"  title="可转换文档类型"></span></label>
            <div class="ipt-wrap">
                  <input  class="form-control"  type="text" name="videofilesize" placeholder="例如100MB"  />
            </div>
        </div>


        <div class="form-group hide">
            <label for="offline" class="control-label">在线转换服务<span class="glyphicon glyphicon-info-sign"  title="当维护系统的时候，您可以选择关闭"></span></label>
            <div class="ipt-wrap">
             <div class="checkbox">
                
                  <input type="radio" name="conv" id="defaultconv" checked value="0" >内置转换
               
                &nbsp;
               
                  <input type="radio" name="conv" id="ownconv" value="1"> OWA
               
              </div>
            </div>
        </div>

        <div class="form-group" style="display:none;" id="owashow">
            <label for="domainName" class="control-label">转换地址<span class="glyphicon glyphicon-info-sign"  title="关闭平台后，在首页可以看到该提示信息"></span></label>
            <div class="ipt-wrap">
                <input  class="form-control"  type="text"  />
            </div>
        </div>

        <div class="form-group">
            <label for="domainName" class="control-label"></label>
            <div class="ipt-wrap">
                <input  type="submit" class="btn btn-primary" value="保存" />
            </div>
        </div>

  </fieldset>
</form>
<script type="text/javascript">
$(function(){

     $("#defaultconv").click(function(){
        showPanel();
     })

     $("#ownconv").click(function(){
        showPanel();
     })

     function showPanel() {
        var convVal = $('input[name="conv"]:checked').val();
        if(convVal == "1") {
            $("#owashow").show();
        } else {
            $("#owashow").hide();
        }
     }
})
</script>