<form class="form-horizontal" action="saveplatform" method="post" name="saveplatform"
      ng-app="myApp" ng-controller="myCtrl" ng-submit="submit_update('saveplatform')">
    <fieldset>

        <div class="form-group hidden">
            <label for="offline" class="control-label">平台开关<span class="glyphicon glyphicon-info-sign"
                                                                 title="当维护系统的时候，您可以选择关闭"></span></label>
            <div class="ipt-wrap">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="offline" ng-disabled="status==1" value="0" value="{$keys['offline']}">打开
                    </label>
                    &nbsp;
                    <label>
                        <input type="radio" name="offline" ng-disabled="status==1" value="1" value="{$keys['offline']}">关闭
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group hidden">
            <label for="domainName" class=" control-label">关闭提示<span class="glyphicon glyphicon-info-sign"
                                                                     title="关闭平台后，在首页可以看到该提示信息"></span></label>
            <div class="ipt-wrap">
              <span>
                <textarea class="form-control" disabled="">{$keys['offline_message']}</textarea>
              </span>
              <span>
                <textarea type="text" class="form-control" value="{$keys['offline_message']}" id="domainName"
                          placeholder="系统在维护中。<br /> 请稍候访问。"></textarea>
              </span>
            </div>
        </div>


        <div class="form-group">
            <label for="sitename" class="control-label">平台名称
                <span class="glyphicon glyphicon-info-sign" title="在登陆页窗口显示改名称"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['sitename']}">
                {else}
                    <input type="text" class="form-control"  required ng-model="keys['sitename']" name="sitename"
                           placeholder="北京金山云网络科技有限公司">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="helpurl" class="control-label">帮助文档地址<span class="glyphicon glyphicon-info-sign"
                                                                   title="登录web后，点击系统右上角“帮助”可以下载该文档"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="" value="{$keys['helpurl']}">
                {else}
                    <input type="text" class="form-control" name="helpurl" required ng-model="keys['helpurl']">
                {/if}
            </div>
        </div>

        <div class="form-group hidden" ng-hide="true">
            <label for="servicetel" class=" control-label">PC端LOGO<span class="glyphicon glyphicon-info-sign"
                                                                        title="在PC客户端登录窗口显示LOGO"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['servicetel']}">
                {else}
                    <input type="text" class="form-control" id="servicetel" value="{$keys['servicetel']}">
                {/if}
            </div>
        </div>

        <div class="form-group hidden" ng-hide="true">
            <label for="logo" class=" control-label">平台LOGO<span class="glyphicon glyphicon-info-sign"
                                                                 title="web端登录窗口显示"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['logo']}">
                {else}
                    <input type="text" class="form-control" id="logo" value="{$keys['logo']}"
                           placeholder="请输入平台logo地址，例如(http://www.abc.com/11.jpg)">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows_logo" class=" control-label">PC端LOGO<span class="glyphicon glyphicon-info-sign"
                                                                          title="在PC客户端登录窗口显示LOGO"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['windows_logo']}">
                {else}
                    <input type="text" class="form-control" name="windows_logo"
                           placeholder="请输入pc端logo地址，例如(http://www.abc.com/11.jpg)" required ng-model="keys['windows_logo']">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="mobile_logo" class=" control-label">移动端LOGO<span class="glyphicon glyphicon-info-sign"
                                                                         title="在移动端登录窗口显示LOGO"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['mobile_logo']}">
                {else}
                    <input type="text" class="form-control" name="mobile_logo"
                           placeholder="请输入移动端logo地址，例如(http://www.abc.com/11.jpg)" required ng-model="keys['mobile_logo']">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="mac_logo" class=" control-label">Mac端LOGO<span class="glyphicon glyphicon-info-sign"
                                                                       title="在mac端登陆窗口显示logo"></span></label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <input type="text" class="form-control" disabled="" value="{$keys['mac_logo']}">
                {else}
                    <input type="text" class="form-control" name="mac_logo" placeholder="请输入mac端logo地址"
                           required ng-model="keys['mac_logo']">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail" class=" control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editplatform)}
                    <a class="btn btn-primary" href="?option=platform&editplatform=1">修改</a>
                {else}
                    <div>
                        <button type="submit" class="btn btn-primary" ng-disabled="saveplatform.$invalid||saveing" >{literal}{{saveTxt}}{/literal}</button>
                        <a class="btn btn-default" href="?option=platform" ng-disabled="saveing" >取消</a>
                    </div>
                {/if}
            </div>
        </div>

        {if "saveplatformok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}

    </fieldset>
</form>

<script type="text/javascript">

    app.controller("myCtrl",function($scope, $http){

        $scope.saveTxt="保存";
        $scope.saveing=false;

        function getKVInfo (v) {
            $http.get('./getKVInfo').success(function(ret){
                $scope.keys = ret.data;
                //console.info($scope.keys);
            });
        }
        getKVInfo("");

        $scope.submit_update=function(form_name){
            $scope.saveTxt="保存中";
            $scope.saveing=true;
        }

    });

</script>