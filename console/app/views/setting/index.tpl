<!DOCTYPE html>
<html lang="zh" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    <script src="{$common}/jquery.validate/jquery.validate.min.js"></script>
    <script src="{$common}/jquery.validate/messages_cn.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
    <script type="text/javascript">
        var app = angular.module("myApp", []);
    </script>

</head>
<body>
{include file="../common/nav.tpl"}

<div class="container-fluid"  >
        {include file="../common/siderbar.tpl" title="平台设置"}
        <div class="toolbar ">
            <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
                <li role="presentation" class="{if $option=='platform'}active{/if}"><a href="?option=platform" >平台设置</a></li>
                <li role="presentation" class="hide {if $option=='conv'}active{/if}"><a href="?option=conv">在线转换配置</a></li>
                <li role="presentation" class="hide {if $option=='upload'}active{/if}"><a href="?option=upload" >上传下载配置</a></li>
                <li role="presentation" class="{if $option=='email'}active{/if}"><a href="?option=email">邮件配置</a></li>
                <li role="presentation" class="{if $option=='html'}active{/if}"><a href="?option=html">修改主页</a></li>
            </ul>
        </div>
        <div class="table-wrap"  style="background:#fff;">
              <div class="">
                <div class="tab-content">
                  {if $option=='platform'}
                  <div role="tabpanel" class="tab-pane active" id="platform"><br/>{include file="./platform.tpl" title="平台设置"}</div>
                  {/if}
                  {if $option=='conv'}
                  <div role="tabpanel" class="tab-pane active" id="conv"><br/>{include file="./conv.tpl" title="在线转换配置"}</div>
                  {/if}
                  {if $option=='upload'}
                  <div role="tabpanel" class="tab-pane active" id="upload"><br/>{include file="./upload.tpl" title="上传下载配置"}</div>
                  {/if}
                  {if $option=='email'}
                  <div role="tabpanel" class="tab-pane active" id="email"><br/>{include file="./email.tpl" title="邮件配置"}</div>
                  {/if}
                  {if $option=='html'}
                  <div role="tabpanel" class="tab-pane active" id="html"><br/>{include file="./html.tpl" title="修改主页"}</div>
                  {/if}
                </div>
              </div>
        </div>
</div>
</body>
</html>

