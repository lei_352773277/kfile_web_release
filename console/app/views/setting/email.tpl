<form class="form-horizontal" action="saveemail" method="post" id="saveemailform">
    <fieldset>
        <div class="form-group hidden" ng-hide="true">
            <label for="sendemailurl" class=" control-label">发送邮件地址<span class="glyphicon glyphicon-info-sign"
                                                                         title="请输入发送邮件的地址，例如（http://192.168.1.100/email/send)"></span></label>
            <div class="ipt-wrap">
              <span ng-show="status==1">
                <input type="text" class="form-control" disabled="" value="{$keys['sendemailurl']}">
              </span>
              <span ng-show="status==2">
                <input type="text" class="form-control" id="sendemailurl" value="{$keys['sendemailurl']}">
              </span>
            </div>
        </div>

        <div class="form-group hidden" ng-hide="true">
            <label for="verifyemailurl" class=" control-label">忘记密码地址<span class="glyphicon glyphicon-info-sign"
                                                                           title="请输入忘记密码地址，例如（http://192.168.1.100/email/send)"></span></label>
            <div class="ipt-wrap">
             
              <span ng-show="status==1">
                <input type="text" class="form-control" disabled="" value="{$keys['verifyemailurl']}">
              </span>

              <span ng-show="status==2">
                <input type="text" class="form-control" id="verifyemailurl"
                       placeholder="http://host/account/verify?token=%s&email=%s" value="{$keys['verifyemailurl']}">
              </span>

            </div>
        </div>

        <div class="form-group">
            <label for="forgettitle" class=" control-label">忘记密码邮件标题<span class="glyphicon glyphicon-info-sign"
                                                                          title="忘记密码邮件标题定制"></span></label>
            <div class="ipt-wrap">
                {if empty($editemail)}
                    <input type="text" class="form-control" disabled="" value="{$keys['forgettitle']}">
                {else}
                    <input type="text" class="form-control required"  name="forgettitle" placeholder="忘记密码" value="{$keys['forgettitle']}">
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label for="mailer" class=" control-label">邮件发送模式<span class="glyphicon glyphicon-info-sign"
                                                                   title="请选择邮件发送模式"></span></label>
            <div class="ipt-wrap">
                {if empty($editemail)}
                    <select id="emailTypeSelect" style="width:200px;display:inline-block;" value="{$keys['mailer']}"
                            class="form-control" disabled>
                        <option value="smtp" label="SMTP">SMTP</option>
                    </select>
                {else}
                    <select id="emailTypeSelect" name="mailer" style="width:200px;display:inline-block;" value="{$keys['mailer']}" class="form-control required">
                        <option value="smtp" label="SMTP">SMTP</option>
                    </select>
                {/if}

            </div>

        </div>

        <div class="form-group">
            <label for="mailfrom" class=" control-label">发件地址
                <span class="glyphicon glyphicon-info-sign" title="即您的发件邮箱,例如xiaowang@163.com"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editemail)}
                    <input type="text" class="form-control" disabled="disabled" value="{$keys['mailfrom']}">
                {else}
                    <input type="text" class="form-control required" name="mailfrom" value="{$keys['mailfrom']}">
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="fromname" class=" control-label">发件人名称<span class="glyphicon glyphicon-info-sign"
                                                                    title="定制邮件中显示的名称，很多时候需要跟发件地址保持一致"></span></label>
            <div class="ipt-wrap">
                {if empty($editemail)}
                    <input type="text" class="form-control" disabled="" value="{$keys['fromname']}">
                {else}
                    <input type="text" class="form-control required" name="fromname" value="{$keys['fromname']}">
                {/if}
            </div>
        </div>

          <span class="hidden" ng-show="{$keys['mailer']}=='sendmail'">
              <div class="form-group">
                  <label for="sendmail" class=" control-label">SendMail路径</label>
                  <div class="ipt-wrap">
                      {if empty($editemail)}
                          <input type="text" class="form-control" disabled="" value="{$keys['sendmail']}">
                      {else}
                          <input type="text" class="form-control " name="sendmail" value="{$keys['sendmail']}">
                      {/if}
                  </div>
              </div>

          </span>

          <span ng-show="{$keys['mailer']}=='smtp'">
          <div class="form-group">
              <label for="smtpauth" class=" control-label">是否SMTP加密<span class="glyphicon glyphicon-info-sign"
                                                                          title="是否加密认证"></span></label>
              <div class="ipt-wrap">
                  <label>
                      <input type="radio" name="smtpauth" {if empty($editemail)} disabled="disabled" {/if}
                             value="1" {if $keys['smtpauth']==1} checked="checked" {/if} >是
                  </label>
                  &nbsp;
                  <label>
                      <input type="radio" name="smtpauth" class="required" {if empty($editemail)} disabled="disabled" {/if}
                             value="0" {if $keys['smtpauth']==0} checked="checked" {/if} >否
                  </label>
              </div>
          </div>


          <div class="form-group">
              <label for="smtpsecure" class=" control-label">SMTP加密方式<span class="glyphicon glyphicon-info-sign"
                                                                            title="ssl或者tls，请根据自己的邮件服务器配置"></span></label>
              <div class="ipt-wrap">
                  {if empty($editemail)}
                    <input type="text" class="form-control" disabled="" value="{$keys['smtpsecure']}">
                  {else}

                    <input type="text" class="form-control required" name="smtpsecure" value="{$keys['smtpsecure']}">
                  {/if}
              </div>
          </div>

          <div class="form-group">
              <label for="smtpport" class=" control-label">SMTP端口<span class="glyphicon glyphicon-info-sign"
                                                                        title="smtp服务的端口，通常情况下如果是ssl则为465，tls则为587"></span></label>
              <div class="ipt-wrap">
                  {if empty($editemail)}

                <input type="text" class="form-control" disabled="" value="{$keys['smtpport']}">
                  {else}
                <input type="text" class="form-control required" name="smtpport" value="{$keys['smtpport']}">

                  {/if}
              </div>
          </div>


          <div class="form-group">
              <label for="smtpuser" class=" control-label">SMTP用户名<span class="glyphicon glyphicon-info-sign"
                                                                         title="smtp的用户名，也许就是您的邮箱账号，例如xiaowang"></span></label>
              <div class="ipt-wrap">
                  {if empty($editemail)}

                <input type="text" class="form-control" disabled="" value="{$keys['smtpuser']}">
                  {else}

                <input type="text" class="form-control required" name="smtpuser" value="{$keys['smtpuser']}">

                  {/if}
              </div>
          </div>

          <div class="form-group">
              <label for="smtppass" class=" control-label">SMTP密码<span class="glyphicon glyphicon-info-sign"
                                                                        title="smtp的密码，也许就是您的邮箱的密码"></span></label>
              <div class="ipt-wrap">
                  {if empty($editemail)}

                <input type="password" class="form-control" disabled="" value="{$keys['smtppass']}">

                  {else}

                <input type="password" class="form-control required" name="smtppass" value="{$keys['smtppass']}">

                  {/if}
              </div>
          </div>

         <div class="form-group">
             <label for="smtphost" class=" control-label">SMTP主机<span class="glyphicon glyphicon-info-sign"
                                                                       title="发件的smtp主机地址"></span></label>
             <div class="ipt-wrap">
                 {if empty($editemail)}
                     <input type="text" class="form-control" disabled="" value="{$keys['smtphost']}">
                 {else}
                     <input type="text" class="form-control required" name="smtphost" value="{$keys['smtphost']}">
                 {/if}
             </div>
         </div>
        </span>


        <div class="form-group">
            <label for="inputEmail" class=" control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editemail)}
                    <a href="javascript:;" class="btn btn-primary btn-lg" onclick="popTestEmail();">测试配置是否正确</a>
                    <a class="btn btn-primary" href="?option=email&editemail=1">修改</a>
                {else}
                    <a href="javascript:;" class="btn btn-primary btn-lg" onclick="popTestEmail();">测试配置是否正确</a>
                    <input type="submit" class="btn btn-primary" value="保存"/>
                    <a class="btn btn-default" href="?option=email" ng-click="cancel()">取消</a>
                {/if}
            </div>
        </div>

        {if "saveemailok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}

    </fieldset>
</form>
<div class="modal " id="modalTestEmail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cancel()" aria-hidden="true">×</button>
                <h4 class="modal-title">测试邮件服务器</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label">测试地址</label>
                        <div class="ipt-wrap">
                            <input type="email" required class="form-control" id="inputQuota" placeholder="请输入接收收件的邮箱">
                            <p class="help-block" id="message"></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="testEmail()" id="testIpt">测试一次</button>
                <button class="btn btn-default" onclick="cancel()">取消</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    $(function () {
        $("#emailTypeSelect").val($("#emailTypeSelect").attr("value"));

        $("#saveemailform").validate({
           // debug:true,
            submitHandler:function(form){
                form.submit();
            }
        });
    });
    var sending = false;
    function popTestEmail() {
        openModal("modalTestEmail", "show");
    }
    function openModal(id, action) {
        if ("show" == action) {
            $("#" + id).show();
        }
    }
    function cancel() {
        $(".modal").hide();
    }
    function disabledIpt(elem, action) {
        if (true == action) {
            $("#" + elem).prop("disabled", true)
        } else {
            $("#" + elem).prop("disabled", false)
        }
    }
    function testEmail() {
        disabledIpt("testIpt", true);
        var data = {
            "user": $("#inputQuota").val()
        }
        $.post("/email/test", data, function (ret) {
            disabledIpt("testIpt", false);
            $("#message").show();
            if (ret.code == 0) {
                $("#message").text("发送成功，请到您的邮箱确认是否收到邮件");
            } else {
                $("#message").text("发送失败，请检测您的配置是否正确");
            }
        }, 'json');
    }
</script>