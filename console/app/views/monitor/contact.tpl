<div style="margin:0px 10px;">
<form action="" id="addform" method="post">
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th style="width:150px;">
                <label class="control-label" style="padding-top: 7px;">姓名</label>
            </th>
            <td>
                <div class="ipt-wrap">
                    <input type="text" name="name" required class="form-control" value="" placeholder="姓名"/>
                </div>
            </td>
        </tr>
        <tr>
            <th ><label style="padding-top: 7px;">邮箱</label></th>
            <td>
                <div class="ipt-wrap">
                    <input type="text" name="email" required class="form-control" value="" placeholder="邮箱"/>
                </div>
            </td>
        </tr>
        <tr>
            <th></th>
            <td>
                <div class="ipt-wrap" >
                    <input type="submit" class="btn btn-primary" value="添加" />
                </div>
            </td>
        </tr>
    </table>
</form>

    <strong>报警信息接收人</strong>
    <table  class="table table-striped table-bordered table-hover" >
        <tr>
            <th style="width:150px;">姓名</th>
            <th style="width:250px;">email</th>
            <th>操作</th>
        </tr>
        
        {foreach from=$contact item="item"}
        <tr>
            <td>
                {$item['name']}
            </td>
            <td>
                {$item['email']}
            </td>
             <td>
                <a href="javascript:;" class="del" style="text-decoration: underline;color: #0080ff;" email="{$item['email']}">删除</a>
            </td>
        </tr>
        {foreachelse}
        <tr>
            <td colspan="3">还没有对应信息</td>
        </tr>
        {/foreach}
    </table>
</div>
<div class="modal " id="modal_contact">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cancel()" aria-hidden="true">×</button>
                <h4 class="modal-title">删除</h4>
            </div>
            <div class="modal-body">
               <div class="modal-txt-wrap">你确定要删除该条信息吗？</div>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="contact_email_val">
                <button class="btn btn-primary" onclick="del_contact()" id="contact_ipt">确定</button>
                <button class="btn btn-default" onclick="cancel()">取消</button>
            </div>
        </div>
    </div>
</div>
{literal}
<script type="text/javascript">
$(function(){
    $('#addform').submit(function (e) {

          $.post("../monitor/addalertuser", $("#addform").serialize(), function(ret){
             if(ret && ret.code!=0) {
                 showTips({
                     command:false,
                     payload:ret.msg
                 });

             } else {
                location.href='?option=contact';
            }
          },'json');
          return false;

    })

    $(".del").click(function(){

        openModal("modal_contact", true);
        $("#contact_email_val").val($(this).attr('email'));

        /*if (confirm("您确定要删除该项吗？")) {
            var email = $(this).attr('email');
            $.post("../monitor/delalertuser", {"email":email}, function(ret){
                 if(ret && ret.code!=0) {
                     showTips({
                         command:false,
                         payload:"删除失败"
                     });
                 } else {
                    location.href='?option=contact';
                }
            },'json');
        }*/
    })

});

function del_contact(){

    var email=$("#contact_email_val").val();
    $.post("../monitor/delalertuser", {"email":email}, function(ret){
        cancel();
        if(ret && ret.code!=0) {
            showTips({
                command:false,
                payload:"删除失败"
            });
        } else {
            showTips({
                command:true,
                payload:"删除成功"
            });
            window.setTimeout(function(){
                location.href='?option=contact';
            },2000);

        }
    },'json');

}


function openModal(id, action) {
    //console.info(id, action);
    if (action) {
        $("#" + id).show();
    }
}
function cancel() {
    $(".modal").hide();
}
</script>
 {/literal}