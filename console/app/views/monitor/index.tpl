<!DOCTYPE html>
<html lang="zh"  ng-controller="homeController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
    {literal}
<script type="text/javascript">

var app = angular.module('myApp', []);

app.filter('to_trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]).filter('fmtprocess',function () {
    return function (text) {
        var map = {"php":"网站服务",
                   "rsync":"文件同步服务",
                   "redis":"缓存服务",
                   "supervisord":"API服务",
                   "mysqld":"数据库服务",
                   "keepalived":"HA高可用服务",
                   "ntpd":"时间校准服务",
                   "ecloud_image":"Media服务",
                   "ecloud_beat":"API数据整理服务",
                   "nginx":"反向代理服务",

                   "localhost.localdomain":"金山云盘一体机",

                   "rootfs":"系统区",
                   "datafs":"数据区",
                   "network":"网卡",

                   "kfile":"API服务",
                   "olc":"在线转换服务",
                   "db_main":"API数据库",
                   "db_web":"WEB数据库",
                   "db_ticket1":"数据块服务1",
                   "db_ticket2":"数据块服务2",
                   "check_pd_status":"raid物理盘",
                   "check_ld_status":"raid逻辑盘",
                   "check_media_error":"raid媒介",
                   "check_ctrl_status":"raid控制器",
                   "check_raid":"raid检测服务"
                  };
        return map[text]||text;
    };
});

app.controller('myCtrl', function($scope, $http, $timeout,$location) {
    function proxy (v) {
        $http.get('./proxy?v='+v).success(function(ret){
            var data = ret.data;
            $scope.system = data.system;
            $scope.process = data.process||[];
            $scope.net = data.net||[];
            $scope.program = data.program||[];
            $scope.filesystem = data.filesystem||[];
        });
    }
    proxy("");
    var timer = $timeout(function(){
      proxy("");
    },3000);

    $scope.isRunning = function (v) {
        //$log.debug(v);
        var map = {"Running":true,"Accessible":true};
        if(v&&v['name']) {
          return map[v['name']]||false;
        } else {
          return map[v]||v;
        }
        
      }

      $scope.isDisabled = function (v) {
        var map = {"Running":true,'Initializing':true,"Accessible":true};
        if(v&&v['name']) {
          return map[v['name']]||false;
        } else {
          return map[v]||v;
        }
      }


      $scope.action = function (name,action) {

      }

      $scope.fiterzhanwei = function (actual, expected) {
        var map = {
          'zhanwei':true,
          'syncalert':true,
          'Start program':true,
          'Stop program':true,
          'Pid file':true,
          'storage_space':true,
          'update_ssl':true,
          'synchttpskey':true,
          'syncnginxconf':true,
          'setip':true,
          'nginx':true,
          'hardware':true
        };
        if (!map[actual['name']]) {
          return true;
        }
      }
    

});

</script>
 {/literal}
</head>
<body>
{include file="../common/nav.tpl"}
<div class="container-fluid"  ng-app="myApp" ng-controller="myCtrl">
        {include file="../common/siderbar.tpl" title="监控信息"}
       
        <div class="toolbar ">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
                  <li role="presentation" class="{if $option=='system'}active{/if}"><a href="?option=system">常规信息</a></li>
                  <li role="presentation" class="{if $option=='filesystem'}active{/if}"><a href="?option=filesystem">存储信息</a></li>
                  <li role="presentation" class="{if $option=='net'}active{/if}"><a href="?option=net">网卡信息</a></li>
                  <li role="presentation" class="{if $option=='program'}active{/if}"><a href="?option=program">服务进程</a></li>
                  <li role="presentation" class="{if $option=='contact'}active{/if}"><a href="?option=contact">报警联系人</a></li>
                </ul>
        </div>
       
        
        <div class="table-wrap"  style="background:#fff;">
              <div class="monitordetail">
                <!-- Tab panes -->
                <div class="tab-content">
                  {if $option=='system'}
                  <div role="tabpanel" class="tab-pane active" id="system"><br/>{include file="../monitor/system.tpl" title="系统信息"}</div>
                  {/if}
                  {if $option=='filesystem'}
                  <div role="tabpanel" class="tab-pane active" id="filesystem"><br/>{include file="../monitor/filesystem.tpl" title="硬盘信息"}</div>
                  {/if}
                  {if $option=='net'}
                  <div role="tabpanel" class="tab-pane active" id="net"><br/>{include file="../monitor/net.tpl" title="网卡信息"}</div>
                  {/if}
                  {if $option=='program'}
                  <div role="tabpanel" class="tab-pane active" id="program"><br/>{include file="../monitor/program.tpl" title="服务进程"}</div>
                  {/if}
                  {if $option=='contact'}
                  <div role="tabpanel" class="tab-pane active" id="program"><br/>{include file="../monitor/contact.tpl" title="报警联系人"}</div>
                  {/if}
                </div>
              </div>
        </div>
</div>
</body>
</html>
 
