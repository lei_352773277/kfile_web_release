{literal}
<div style="margin:0px 10px;">
<table  class="table ng-table-responsive table-striped table-bordered table-hover" >
            <tr>
                <th>服务</th>
                <th>状态</th>
                <th>返回值</th>
                <th>最后执行时间</th>
                <th>退出值</th>
            </tr>
            <tr ng-repeat="item in program | filter:fiterzhanwei" >
                <td  style="width:20%;text-align: left">
                    <span>{{item.name|fmtprocess}}</span>
                </td>
                <td style="width:30%"  sortable="'name'" ng-click="clickRow(file)">
                    <span class="{{item.status.color}}">{{item.status.name}}</span>
                </td>
                <td  style="width:10%" sortable="'role'" ng-click="clickRow(file)">
                    {{item.output}}
                </td>
                <td style="width:15%"   sortable="'mtime'" ng-click="clickRow(file)">
                    {{item.last_started}}
                </td>
                <td  data-title="'状态'" sortable="'mtime'" ng-click="clickRow(file)">
                    {{item.exit_value}}
                </td>
            </tr>
        </table>
</div>
 {/literal}