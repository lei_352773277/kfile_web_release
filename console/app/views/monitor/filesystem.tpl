{literal}
<div style="margin:0px 10px;">
<table  class="table ng-table-responsive table-striped table-bordered table-hover" >
            <tr>
                <th>存储标识</th>
                <th>状态</th>
                <th>space_usage</th>
                <th>inodes_usage</th>
            </tr>
            <tr ng-repeat="item in filesystem | filter:fiterzhanwei" >
                <td  style="width:20%;text-align: left">
                    <a href="#/monitor/{{item.href}}">{{item.name|fmtprocess}}</a>
                </td>
                <td style="width:30%"  sortable="'name'" ng-click="clickRow(file)">
                    <span class="{{item.status.color}}">{{item.status.name}}</span>
                </td>
                <td  style="width:10%" sortable="'role'" ng-click="clickRow(file)">
                    {{item.space_usage}}
                </td>
                <td sortable="'mtime'" ng-click="clickRow(file)">
                    {{item.inodes_usage}}
                </td>
            </tr>
        </table>

        
        <table class="table table-striped table-bordered table-hover " ng-controller="raid">
             <tr>
                <td>硬盘编号</td>
                <td ng-repeat="item in itemdata.disk | filter:fiterzhanwei ">{{item['id']}}</td>
             </tr>
            <tr>
                <td>状态</td>
                <td ng-repeat="item in itemdata.disk | filter:fiterzhanwei "><span ng-class="{true:'green',false:'orange'}[item['status']=='Online, Spun Up']">{{item['status']}}</span></td>
            </tr>
        </table>

</div>
  {/literal}
  {literal}
 <script type="text/javascript">
 app.controller('raid', function($scope, $http, $timeout,$location) {
      function proxy (v) {
          $http.get('./proxy?v='+v).success(function(ret){
              var data = ret.data;
              $scope.itemdata = data;
          });
      }
      proxy("check_raid");
      var timer = $timeout(function(){
        proxy("check_raid");
      },3000);
 });
 </script>
  {/literal}