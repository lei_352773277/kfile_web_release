{literal}
<div style="margin:0px 10px;">
<table  class="table ng-table-responsive table-striped table-bordered table-hover" >
            <tr>
              <th>机器名</th>
              <th>状态</th>
              <th>负载</th>
              <th>CPU</th>
              <th>内存</th>
              <th>SWAP</th>
            </tr>
            <tr>
                <td  style="width:20%;text-align: left">
                    <a href="#/monitor/{{system.name}}">{{system.name|fmtprocess}}</a>
                </td>
                <td style="width:30%"  sortable="'name'" ng-click="clickRow(file)">
                    <span class="{{system.status.color}}">{{system.status.name}}</span>
                </td>
                <td  style="width:10%" sortable="'role'" ng-click="clickRow(file)">
                    <span ng-bind-html="system.load|to_trusted"></span>
                </td>
                <td style="width:15%"   sortable="'mtime'" ng-click="clickRow(file)">
                  <span ng-bind-html="system.cpu|to_trusted"></span>
                </td>
                <td  sortable="'mtime'" ng-click="clickRow(file)">
                  <span ng-bind-html="system.memory|to_trusted"></span>
                </td>
                <td  sortable="'mtime'" ng-click="clickRow(file)">
                    {{system.swap}}
                </td>
            </tr>
        </table>

 {/literal}