{literal}
<div style="margin:0px 10px;">
<table  class="table ng-table-responsive table-striped table-bordered table-hover" >
            <tr>
                <th>网卡</th>
                <th>状态</th>
                <th>上传速度</th>
                <th>下载速度</th>
            </tr>
            <tr ng-repeat="item in net  | filter:fiterzhanwei" >
                <td  style="width:20%;text-align: left">
                    {{item.name|fmtprocess}}
                </td>
                <td style="width:30%"  sortable="'name'" ng-click="clickRow(file)">
                     <span class="{{item.status.color}}">{{item.status.name}}</span>
                </td>
                <td  style="width:10%" sortable="'role'" ng-click="clickRow(file)">
                     <span ng-bind-html="item.upload|to_trusted"></span>
                </td>
                <td   sortable="'mtime'" ng-click="clickRow(file)">
                     <span ng-bind-html="item.download|to_trusted"></span>
                </td>
            </tr>
          </table>
</div>
 {/literal}