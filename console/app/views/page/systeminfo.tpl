<!DOCTYPE html>
<html lang="zh" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
</head>
<body >
{include file="../common/nav.tpl"}

<div class="container-fluid" ng-view ui-view>
    <div class="row">
        {include file="./siderbar.tpl" title="系统管理"}

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2"  style="background:#fff;">
          <br/>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTabs">
                  <li role="presentation" class="active"><a href="#network" aria-controls="network" role="tab" data-toggle="tab">网络设置</a></li>
                  <li role="presentation"><a href="#power" aria-controls="power" role="tab" data-toggle="tab">关闭重启</a></li>
                  <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">修改密码</a></li>
                  <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">系统信息</a></li>
                  <li role="presentation"><a href="#update" aria-controls="update" role="tab" data-toggle="tab">系统升级</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="network"><br/>{include file="../systeminfo/network.tpl" title="网络设置"}</div>
                  <div role="tabpanel" class="tab-pane" id="power"><br/>{include file="../systeminfo/poweroff.tpl" title="关闭重启"}</div>
                  <div role="tabpanel" class="tab-pane" id="password"><br/>{include file="../systeminfo/pwd.tpl" title="修改密码"}</div>
                  <div role="tabpanel" class="tab-pane" id="info"><br/>{include file="../systeminfo/infolist.tpl" title="系统信息"}</div>
                  <div role="tabpanel" class="tab-pane" id="update"><br/>{include file="../systeminfo/update.tpl" title="系统升级"}</div>
                </div>

              </div>



        </div>
</div>
         

</div>
</body>
</html>
<script type="text/javascript">
$(function(){
    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
})


</script>
