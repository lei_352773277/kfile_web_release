<div class="sidebar">

  <ul class="nav-sidebar">
    {assign var="loginedUser" value=getLoginedUser()}

    {if checkPermission($loginedUser, "dashboard_dashboard")}
    <!--
    <li {if $title == '主面板'}class="active"{/if}><a href="dashboard/dashboard"><span class="glyphicon glyphicon-list"></span> 主面板</a></li>
    -->
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '企业列表'}class="active"{/if}><a href="dashboard/domainlist"><span class="glyphicon glyphicon-list"></span> 企业列表</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '平台设置'}class="active"{/if}><a href="dashboard/listdomains"><span class="glyphicon glyphicon-wrench"></span> 平台设置</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '加密设置'}class="active"{/if}><a href="dashboard/listdomains"><span class="glyphicon glyphicon-lock"></span> 加密设置</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '客户端管理'}class="active"{/if}><a href="dashboard/listdomains"><span class="glyphicon glyphicon-phone"></span> 客户端管理</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '空间情况'}class="active"{/if}><a href="dashboard/listdomains"><span class="glyphicon glyphicon-stats"></span> 空间情况</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '监控信息'}class="active"{/if}><a href="dashboard/monitor"><span class="glyphicon glyphicon-signal"></span> 监控信息</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_listdomains")}
    <li {if $title == '系统管理'}class="active"{/if}><a href="dashboard/systeminfo"><span class="glyphicon glyphicon-cog"></span> 系统管理</a></li>
    {/if}

   
  </ul>

</div>