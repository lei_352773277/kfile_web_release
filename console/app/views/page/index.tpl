<!DOCTYPE html>
<html lang="zh"  >
<head>
	<meta charset="utf-8">
	<title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>

<div id="container">
	<div id="body">
			<div class="col-lg-4 col-lg-offset-4">
			<div class="well login-box" style="margin-top:200px; ">
			 {literal}
        	<form class="form-horizontal" name="loginform" id="loginform">
				    <fieldset>
				        <legend translate="index_1">登录</legend>
				        <div class="form-group">
				            <label for="inputEmail" class="col-lg-3 control-label" translate="index_2">用户名</label>
				            <div class="col-lg-6">
				                <input type="email" class="form-control" name="user" id="inputEmail" ng-model="user" placeholder="请输入用户名" required>
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label" translate="index_3">密码</label>
				            <div class="col-lg-6">
				                <input type="text" onfocus="this.type='password'"  class="form-control" id="inputPassword" name="password" placeholder="请输入用户名">
				            </div>
				        </div>
				       
				        <div class="form-group">
				        	<label class="col-lg-3 control-label">&nbsp;</label>
				        	<div class="col-lg-6">
				        	<button type="submit" class="btn btn-primary" >登录</button>
							<p  id="showerrormsg" class="ng-cloak text-danger text-error"></p>
									
				       		</div>
				        </div>
				        
				    </fieldset>
       		</form>
       		{/literal}
       	</div>
       </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		 $("#loginform").submit(function(){
			 //$.post("/account/login", $("#loginform").serialize(), function(data){
		 	$.post("/console/account/login", $("#loginform").serialize(), function(data){
		 		 if(data&&data.code==0) {
		 		 	 location.href='/console/setting/index';
					 //location.href='/dashboard/domainlist';
		 		 } else {
		 		 	//alert('登录失败');
		 		 	$("#showerrormsg").show().html('登录失败');
		 		 }
		 	},'json');
		 	return false;
		 })
	})
</script>
</body>
</html>