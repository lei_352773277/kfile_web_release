<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>

    <script type="text/javascript">

        var app = angular.module("myApp", []);
        app.controller("myCtrl",function($scope, $http,$q){

            $http.get('./getdomainlist').success(function(ret){
                $scope.domainlist = ret.data;
                //console.info( ret.data[0]);
            });

            $scope.user="";
            $scope.password="";
            $scope.confirmPassword="";

            $scope.saveTxt="保存";
            $scope.saveing=false;

            $scope.showMsg=false;

            $scope.submit_update=function(form_name){
                $scope.saveTxt="保存中";
                $scope.saveing=true;


                var transform = function(data){
                    return $.param(data);
                };
                var data={
                    'user':$scope.user,
                    'password': $scope.password,
                    confirmPassword :$scope.confirmPassword,
                    domainName : $scope.domainlist[0]['name'],
                    domainContact :$scope.domainlist[0]['contact'],
                    domainIdent :$scope.domainlist[0]['ident']
                };
                $http.post("../domain/reg",data,{
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: transform
                }).success(function(ret){
                    //console.info(ret);
                    $scope.saveTxt="保存";
                    $scope.saveing=false;
                    if(0 == ret.code) {
                        location.href = '../dashboard/domainlist';
                    } else {
                        $scope.showMsg=true;
                        $("#showmessage").show();
                        //var re = /\n/g;
                        //var str = data.msg.replace(re, "</br>");
                        //$("#msg").html(str);
                        $scope.msg=ret.msg;
                    }
                });

            }
        });

    </script>
</head>
<body >
{include file="../common/nav.tpl"}

<div class="container-fluid" ng-app="myApp" ng-controller="myCtrl">

         {include file="../common/siderbar.tpl" title="企业列表"}

        <div class="toolbar ">
            <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
                <li role="presentation"><a href="domainlist">企业列表</a></li>
            <li role="presentation"  class="active"><a href="createdomain" >创建企业</a></li>
            </ul>
        </div>

        <div class="table-wrap"  style="background:#fff;padding-top: 20px;">
              <div class="">
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="platform">

                    <form class="form-horizontal" name="createdomainform"  id="createdomain" method="post" ng-submit="submit_update('createdomainform')" >
                      <fieldset>
                             {if $producttype != 1}
                            <div class="form-group" ng-class="{ 'has-error' : createdomainform.domainIdent.$dirty &&createdomainform.domainIdent.$invalid }" >
                                <label  class="control-label">企业标识<span class="glyphicon glyphicon-info-sign" title="在登陆页窗口显示改名称"></span></label>
                                <div class="ipt-wrap">
                                    <input type="text" class="form-control" required  name="domainIdent"  id="domainident"  ng-model="domainlist[0]['ident']">
                                    <p ng-show="createdomainform.domainIdent.$dirty &&createdomainform.domainIdent.$invalid" class="help-block">请输入企业标识</p>
                                </div>
                            </div>
                             {/if}

                           <div class="form-group" ng-class="{ 'has-error' :createdomainform.domainName.$dirty && createdomainform.domainName.$invalid }" >
                                <label  class="control-label">企业名称<span class="glyphicon glyphicon-info-sign"  title="企业名称"></span></label>
                                <div class="ipt-wrap">
                                  <input type="text" class="form-control" required name="domainName"  id="domainName"  ng-model="domainlist[0]['name']">
                                  <p ng-show="createdomainform.domainName.$dirty &&createdomainform.domainName.$invalid" class="help-block">请输入企业名称</p>
                                </div>
                            </div>

                            <div class="form-group"  ng-class="{ 'has-error' : createdomainform.user.$dirty && createdomainform.user.$invalid }">
                                <label for="helpurl" class=" control-label">管理员登录账号<span class="glyphicon glyphicon-info-sign"  title="管理员登录账号"></span></label>
                                <div class="ipt-wrap">
                                  <input type="text" class="form-control" required  name="user"  id="user"  ng-model="user">
                                    <p ng-show="createdomainform.user.$dirty && createdomainform.user.$invalid" class="help-block">请输入管理员登录账号</p>
                                </div>
                            </div>


                            <div class="form-group" ng-class="{ 'has-error' : createdomainform.password.$dirty && createdomainform.password.$invalid }">
                                <label for="helpurl" class=" control-label">密码<span class="glyphicon glyphicon-info-sign"  title="密码"></span></label>
                                <div class="ipt-wrap">
                                  <input type="password" class="form-control" required maxlength="32"  {literal}ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)]){6,32}$/" {/literal}   name="password"  id="password"  ng-model="password">
                                    <p ng-show="createdomainform.password.$dirty && createdomainform.password.$invalid" class="help-block">密码格式为（6-32位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合）</p>
                                </div>
                            </div>


                            <div class="form-group" ng-class="{ 'has-error' : (createdomainform.confirmPassword.$dirty && createdomainform.confirmPassword.$invalid) ||password!=confirmPassword }">
                                <label for="helpurl" class=" control-label">确认密码<span class="glyphicon glyphicon-info-sign"  title="确认密码"></span></label>
                                <div class="ipt-wrap">
                                  <input type="password" class="form-control" required  name="confirmPassword" maxlength="32" {literal}ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)]){6,32}$/" {/literal} id="confirmPassword"  ng-model="confirmPassword">
                                    <p ng-show="(createdomainform.confirmPassword.$dirty && createdomainform.confirmPassword.$invalid) || password!=confirmPassword" class="help-block">密码格式为（6-32位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合），请保持跟两次密码输入一致</p>
                                </div>
                            </div>

                            <div class="form-group" ng-class="{ 'has-error' : createdomainform.domainContact.$dirty &&createdomainform.domainContact.$invalid }">
                                <label for="servicetel" class=" control-label">企业联系人<span class="glyphicon glyphicon-info-sign"  title="企业联系人"></span></label>
                                <div class="ipt-wrap">
                                  <input type="text" class="form-control"  name="domainContact" required id="domainContact" ng-model="domainlist[0]['contact']">
                                    <p ng-show="createdomainform.domainContact.$dirty &&createdomainform.domainContact.$invalid" class="help-block">请输入企业联系人</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="logo" class=" control-label"></label>
                                <div class="ipt-wrap">
                                    <button type="submit" class="btn btn-primary" ng-disabled="createdomainform.$invalid||(password!=confirmPassword)||saveing"> {literal}{{saveTxt}} {/literal}</button>
                                    <a href="domainlist" class="btn btn-default" ng-disabled="saveing">取消</a>
                                </div>
                            </div>

                            <div class="form-group"  ng-show="showMsg" style="display:none;" id="showmessage">
                                <label for="logo" class=" control-label"></label>
                                <div id="msg" class="ipt-wrap bg-danger" style="padding:5px;">
                                    {literal}{{msg}}{/literal}
                                </div>
                            </div>

                    </fieldset>
                    </form>


                  </div>
                </div>
              </div>
        </div>
</div>
</body>
</html>

