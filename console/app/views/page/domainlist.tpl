<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->

</head>
<body>
{include file="../common/nav.tpl"}
<div class="container-fluid">
    {include file="../common/siderbar.tpl" title="企业列表"}

    <div class="toolbar ">
        <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
            <li role="presentation" class="active"><a href="domainlist">企业列表</a></li>
            <li role="presentation"><a href="createdomain" >创建企业</a></li>
        </ul>
    </div>

    <div class="table-wrap"  style="background:#fff;padding-top: 20px;">
        <div style="margin:0px 10px;">
        <table class="table table-responsive table-striped table-bordered table-hover">
            <tr>
                <th style="width: 10%">ID</th>
                <th style="width: 15%">企业标识</th>
                <th style="width: 30%">企业名</th>
                <th style="width: 15%">联系人</th>
                <th >注册时间</th>
            </tr>
            {foreach from=$domainlist item="item"}
                <tr>
                    <td >
                        {$item.domain_id}
                    </td>
                    <td sortable="'size'" ng-hide="CONTEXT&&CONTEXT['producttype']==1">
                        <a href="javascript:void(0);" >{$item.ident}</a>
                    </td>
                    <td  sortable="'name'">
                        <a style="color: #0080ff;text-decoration:underline;" href="javascript:void(0);" onclick='showdomain("{$item.ident}","{$item.name|escape}","{$item.contact|escape}")' class="filelink"
                           title="点击查看企业信息">{$item.name|escape}
                        </a>
                    </td>
                    <td sortable="'role'">
                        {$item.contact}
                    </td>
                    <td  sortable="'mtime'">
                        {date("Y-m-d H:i", $item.ctime)}
                    </td>
                </tr>
                {foreachelse}
                <tr>
                    <td colspan="4">还没有对应信息</td>
                </tr>
            {/foreach}
        </table>
        </div>
    </div>
</div>

<div class="modal " id="modalDomainInfo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cancel()" aria-hidden="true">×</button>
                <h4 class="modal-title">企业信息</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label">企业名</label>
                        <div class="ipt-wrap">
                            <input type="text" class="form-control" id="name" disabled value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">管理员登录名</label>
                        <div class="ipt-wrap">
                            <input type="text" class="form-control" id="user_name" disabled value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">联系方式</label>
                        <div class="ipt-wrap">
                            <input type="text" class="form-control" id="contact" disabled value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">账号数</label>
                        <div class="ipt-wrap">
                            <input type="text" class="form-control" id="member_nums" disabled value=""/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" onclick="cancel()">关闭</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    function showdomain(domainIdent,name,contact){
        openModal("modalDomainInfo","show");
        $("#name").val(name);
        $("#contact").val(contact);

        $.get("../domain/domaininfo?domainIdent="+domainIdent, function(ret){
            if(ret&&ret.code==0) {
                    $("#user_name").val(ret['data']['user_name']);
                    $("#member_nums").val(ret['data']['member_nums']);
            } else {

            }
        },'json');
    }
    function openModal(id,action){
        if("show"==action){
            $("#"+id).show();
        }
    }
    function cancel(){
        $(".modal").hide();
    }
</script>
