<!DOCTYPE html>
<html lang="zh" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
</head>
<body>
{include file="../common/nav.tpl"}
<div class="container-fluid"  ui-view>
        {include file="../common/siderbar.tpl" title="企业列表"}
        <div class="toolbar ">
            <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
                <li role="presentation" class="active"><a href="#network" aria-controls="network" role="tab" data-toggle="tab">企业信息</a></li>
            </ul>
        </div>
        <div class="table-wrap"  style="background:#fff;padding-top: 20px;">
            <div>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="network">

                    <form class="form-horizontal" action="saveplatform" method="post">
                      <fieldset>
                        {if count($domainlist) >= 1}
                           
                            <div class="form-group">
                                <label for="sitename" class="control-label">企业标识<span class="glyphicon glyphicon-info-sign"  title="企业标识"></span></label>
                                <div class="ipt-wrap">
                                    <label class="wrap-label">{$domainlist[0]['ident']}</label>
                                </div>
                            </div>
                           

                           <div class="form-group">
                                <label for="helpurl" class="control-label">企业名称<span class="glyphicon glyphicon-info-sign"  title="企业名称"></span></label>
                                <div class="ipt-wrap">

                                    <label class="wrap-label">{$domainlist[0]['name']}</label>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="servicetel" class="control-label">企业联系方式<span class="glyphicon glyphicon-info-sign"  title="企业联系方式"></span></label>
                                <div class="ipt-wrap">

                                    <label class="wrap-label">{$domainlist[0]['contact']} </label>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="logo" class="control-label">企业创建时间<span class="glyphicon glyphicon-info-sign"  title="企业创建时间"></span></label>
                                <div class="ipt-wrap">

                                    <label class="wrap-label">{date('Y-m-d H:i',$domainlist[0]['ctime'])} </label>

                                </div>
                            </div>
                        {else}
                            <div class="form-group">
                                <label for="logo" class="control-label" style="width:300px; margin-top:20px;">还没创建企业， <a href="createdomain" class="btn btn-primary">立即创建企业</a></label>
                                <div class="ipt-wrap">
                                   
                                </div>
                            </div>
                        {/if}
                    </fieldset>
                    </form>
                  </div>
                </div>
              </div>
        </div>

</div>
</body>
</html>
<script type="text/javascript">

</script>
