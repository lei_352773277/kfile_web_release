<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->

</head>
<body >
{include file="../common/nav.tpl"}

<div class="container-fluid" ui-view>
    {include file="../common/siderbar.tpl" title="企业列表"}
    <div class="table-wrap" style="background:#fff;">
        <div role="tabpanel">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="domainprofile">
                    <h4>企业列表</h4>
                </div>
            </div>
        </div>
    </div>


</div>
</body>
</html>
