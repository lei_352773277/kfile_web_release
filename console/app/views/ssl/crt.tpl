<form class="form-horizontal">
    <div class="form-group">
        <label  class="control-label">立即生成一个证书</label>
        <div class="ipt-wrap ">
            <button type="button" class="btn btn-primary" id="createcrt_btn" onclick="creatcrt()">生成证书</button>
            <button type="button" style="display: none;" class="btn btn-primary"   id="usercrt_btn" onclick="usecrt()" >直接使用证书</button>
            <p class="help-block" id="message_createcrt"></p>
            <p class="help-block" id="message_createcrt_error"></p>
        </div>
    </div>
</form>

<script type="text/javascript">

    function creatcrt(){
        var api_common_http_host="{$keys['api_common_http_host']}" ||"";
        var ip="{$keys['ip']}" ||"";
        ip = api_common_http_host==""?ip:api_common_http_host;
        var data = {
            'shellcreatekey_ip':ip,
            'shellcreatekey':'1'
        };
        disabledIpt("createcrt_btn",true);
        $("#message_createcrt").show().text("正在生成证书，等待...");
        $.ajax(
            {
                url:"../ssl/createshellkey",
                data:data,
                type:"post",
                dataType:'html',
                success:function(ret) {
                    $("#message_createcrt").show().text("正在生成证书，等待30s");
                    //if(ret.code == 0){
                    myTimeout();
                    //}else{
                    //    disabledIpt("createcrt_btn",false);
                    //    $("#message_createcrt").text(ret.msg).hide();
                    //},
                },
                error:function(){
                    $("#message_createcrt").show().text("正在生成证书，等待30s");
                    myTimeout();
                }
            }
        )
    }
    var time=30;
    var timeoutEvent=null;
    function myTimeout(){
        time--;
        $("#message_createcrt").text("正在生成证书，等待"+time+"s");
        if(time<=0){
            clearMyTimeout(timeoutEvent);
            _open_use_crt();
            return;
        }
        timeoutEvent=setTimeout(function(){
            myTimeout();
        },1000)
    }
    function clearMyTimeout(timeoutEvent){
        clearTimeout(timeoutEvent);
        time=30;
        timeoutEvent=null;
    }
    function _open_use_crt(){
        $.get("../ssl/getCrtStatus",function(ret){
            if(ret&&0==ret['code']) {
                $("#usercrt_btn").show();
                $("#message_createcrt").html("生产证书成功，您可以直接应用该证书，或"+
                '<a style="color: #0080ff;text-decoration: underline;" href="javascript:void(0)" onclick="reset_crt()">'+"重新生成"+'</a>');
            } else {
                $("#message_createcrt").html("生产证书失败，您可以试试 "+
                '<a style="color: #0080ff;text-decoration: underline;" href="javascript:void(0)" onclick="reset_crt()">'+"重新生成"+'</a>');
            }
        },'json')

    }
    function reset_crt(){
        disabledIpt("createcrt_btn",false);
        disabledIpt("usercrt_btn",false);
        $("#usercrt_btn").hide();
        $("#message_createcrt").text("").hide();
        $("#message_createcrt_error").html("").hide();

    }

    function usecrt(){
        disabledIpt("usercrt_btn",true);
        $("#message_createcrt").text("").hide();
        $.post("../ssl/applyshellkey","",function(ret){
            /*ret={
                code:0
            }*/
            if(ret.code == 0){
                synchttps();
                $("#message_createcrt_error").show().text("应用证书成功,等待3s");
                myTimeoutSleep();
            }else{
                disabledIpt("usercrt_btn",false);
                $("#message_createcrt_error").text(ret.msg).show();
            }
        },'json')
    }
    var sleeptTime=3;
    var sleepTimeoutEvent=null;
    function myTimeoutSleep(){
        sleeptTime--;
        $("#message_createcrt_error").text("应用证书成功,等待"+sleeptTime+"s");
        if(sleeptTime<=0){
            clearMyTimeoutsleep(sleepTimeoutEvent);
            return;
        }
        sleepTimeoutEvent=setTimeout(function(){
            myTimeoutSleep();
        },1000)
    }
    function clearMyTimeoutsleep(sleepTimeoutEvent){
        clearTimeout(sleepTimeoutEvent);
        sleeptTime=3;
        sleepTimeoutEvent=null;
        $("#message_createcrt_error").text("").hide();
    }

    function synchttps () {
        $.ajax({
            url:"../ssl/synchttps",
            error:function(ret){
                checkSyncHttps();
            }
        });
    }

    function checkSyncHttps()  {
        $.ajax({
            url:'../ssl/checkHttpsIsRight',
            dataType:'json',
            success:function(ret){
                if(ret.code == 0){
                    showTips({
                        "command":true,
                        "payload":"开启https成功"
                    });
                }else{
                    showTips({
                        "command":false,
                        "payload":"开启https失败，"+ret.msg
                    });
                    setTimeout(function(){
                        setHttps();
                    },1000)
                }
            },
            error:function(){
                showTips({
                    "command":false,
                    "payload":"开启https失败，"+ret.msg
                });
                setTimeout(function(){
                    setHttps();
                },1000)
            }
        })
    }


    function setHttps(){
        $.ajax({
            type: "POST",
            url: "../ssl/setting",
            dataType:"json",
            data: {
                'https':'0'
            },
            success: function(){
                window.location.href="../ssl/index";
            },
            error:function (){
                showTips({
                    "command":true,
                    "payload":"保存成功"
                });
            }
        });
    }

</script>