<!DOCTYPE html>
<html lang="zh" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <!--[if lt IE 8]>
      <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>
    <style type="text/css">

    </style>
</head>
<body >
{include file="../common/nav.tpl"}

<div class="container-fluid" ui-view>
    {include file="../common/siderbar.tpl" title="加密设置"}
    
    <div class="toolbar ">
        <ul class="nav nav-tabs  my-tabs" role="tablist" id="myTabs">
            <li role="presentation" class="{if $option=='https'}active{/if}"><a href="?option=https">https传输加密设置</a></li>
            <li role="presentation" class="{if $option=='crt'}active{/if}"><a href="?option=crt">证书制作</a></li>
        </ul>
    </div>

    <div class="table-wrap"  >
          <div>
            <!-- Tab panes -->
            <div class="tab-content">
              {if $option=='https'}
              <div role="tabpanel" class="tab-pane active"><br/>{include file="./https.tpl" title="https传输加密设置"}</div>
              {else}
              <div role="tabpanel" class="tab-pane active"><br/>{include file="./crt.tpl" title="证书制作"}</div>
              {/if}
            </div>
          </div>
    </div>

</div>
</body>
</html>
