<form class="form-horizontal" action="savehttps" method="post" id="savehttps">
    <fieldset>
        <div class="form-group">
            <label for="https" class="control-label">启用加密传输 <span class="glyphicon glyphicon-info-sign"
                                                                  title="是否启用加密传输"></span></label>
            <div class="ipt-wrap">

                <input type="radio" name="https" {if empty($editHttps)} disabled="disabled" {/if}
                       value="1" {if $keys['https']==1} checked="checked" {/if} />启用
                &nbsp;
                <input type="radio" name="https" {if empty($editHttps)} disabled="disabled" {/if}
                       value="0" {if $keys['https']==0} checked="checked" {/if} />关闭
                <span style="margin-left:50px;">还没有证书？<a href="?option=crt"
                                                         style="text-decoration:underline;color:#0080ff;">点击这里</a>立即生成证书</span>

            </div>
        </div>

        <div class="form-group">
            <label for="https" class="control-label">加密传输端口 <span class="glyphicon glyphicon-info-sign"
                                                                  title="配置https加密传输端口,默认端口443"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <input type="text" class="form-control" style="width:80px;" disabled="disabled"
                           value="{$keys['api_common_https_port']}">
                {else}
                    <input type="text" class="form-control" name="api_common_https_port"
                           style="width:80px;display: inline-block;" value="{$keys['api_common_https_port']}"
                           placeholder="443"/>
                    &nbsp;
                    <button id="testsslportIpt" type="button" class="btn btn-primary" onclick="testsslport()">测试一下
                    </button>
                    <p class="help-block" id="testsslportMsg"></p>
                {/if}

            </div>
        </div>

        <div class="form-group hidden" ng-show="false">
            <label for="serverkeyurl" class="control-label">服务端证书crt内容 <span class="glyphicon glyphicon-info-sign"
                                                                             title="'ssl_11'|translate"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <span ng-show="status==1">
                <input type="text" class="form-control" disabled="" value="{$keys['servercrturl']}"/>
              </span>
                {else}
                    <span ng-show="status==2">
                <input type="text" class="form-control" name="servercrturl" value="{$keys['servercrturl']}"
                       placeholder="'ssl_12'|translate"/>
              </span>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="serverkey" class="control-label">服务端证书crt内容<span class="glyphicon glyphicon-info-sign"
                                                                         title="配置服务端crt证书的内容"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <textarea class="form-control" style="height:150px;" disabled="">{$keys['servercrt']}</textarea>
                {else}
                    <textarea class="form-control" style="height:150px;" name="servercrt"
                              placeholder="请把服务端证书内容粘贴到这里">{$keys['servercrt']}</textarea>
                {/if}
            </div>
        </div>

        <div class="form-group hidden" ng-show="false">
            <label for="serverkeyurl" class="control-label">客户端证书crt内容 <span class="glyphicon glyphicon-info-sign"
                                                                             title="'ssl_17'|translate"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <span ng-show="status==1">
                <input type="text" class="form-control" disabled="" value="{$keys['serverkeyurl']}"/>
              </span>
                {else}
                    <span ng-show="status==2">
                <input type="text" class="form-control" name="serverkeyurl" value="{$keys['serverkeyurl']}"
                       placeholder="请把服务端证书key内容粘贴到这里"/>
              </span>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="serverkey" class="control-label">服务端证书key内容 <span class="glyphicon glyphicon-info-sign"
                                                                          title="配置服务端证书key的内容"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <textarea class="form-control" style="height:150px;" disabled="">{$keys['serverkey']}</textarea>
                {else}
                    <textarea class="form-control" style="height:150px;" name="serverkey"
                              placeholder="请把服务端证书key内容粘贴到这里">{$keys['serverkey']}</textarea>
                {/if}
            </div>
        </div>


        <div class="form-group hidden" ng-show="false">
            <label for="clientkeyurl" class="control-label">'ssl_22'|translate <span
                        class="glyphicon glyphicon-info-sign" title="配置客户端证书crt的访问地址"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <input type="text" class="form-control" disabled="" value="{$keys['clientkeyurl']}"/>
                {else}
                    <input type="text" class="form-control" name="clientkeyurl" value="{$keys['clientkeyurl']}"
                           placeholder="客户端证书地址url"/>
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label for="clientkey" class="control-label">客户端证书crt内容<span class="glyphicon glyphicon-info-sign"
                                                                         title="配置客户端证书crt的访问内容"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <span ng-show="status==1">
                <textarea class="form-control" style="height:150px;" disabled="">{$keys['clientkey']}</textarea>
              </span>
                {else}
                    <span ng-show="status==2">
                   <textarea class="form-control" name="clientkey" style="height:150px;"
                             placeholder="请把客户端证书内容粘贴到这里">{$keys['clientkey']}</textarea>
              </span>
                {/if}
            </div>
        </div>

        <div class="form-group hidden" ng-show="false">
            <label for="clientkeyurl" class="control-label">'ssl_28'|translate <span
                        class="glyphicon glyphicon-info-sign" title="'ssl_29'|translate"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <span ng-show="status==1">
                <input type="text" class="form-control" disabled="" value="{$keys['webcrturl']}"/>
              </span>
                {else}
                    <span ng-show="status==2">
                   <input type="text" class="form-control" name="webcrturl" value="{$keys['webcrturl']}"
                          placeholder="'ssl_30'|translate"/>
              </span>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="clientkey" class="control-label">网站根证书安装说明 <span class="glyphicon glyphicon-info-sign"
                                                                         title="配置网站根证书安装说明地址，如果没有请留空"></span></label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <input type="text" class="form-control" disabled="" value="{$keys['web_crt_help']}"/>
                {else}
                    <input type="text" class="form-control" name="web_crt_help" value="{$keys['web_crt_help']}"
                           placeholder="网站根证书安装说明地址"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail" class="control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editHttps)}
                    <a class="btn btn-primary" href="?editHttps=1">修改</a>
                {else}
                    <button type="submit" id="saveHttps" class="btn btn-primary">保存</button>
                    <a class="btn btn-default" href="?">取消</a>
                {/if}
            </div>
        </div>
        {if "savehttps" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}

    </fieldset>
</form>
<script type="text/javascript">

    function testsslport() {
        change_button_status("testsslportIpt",true,"测试中");
        var port = $("[name='api_common_https_port']").val();
        $.get('../ssl/checkport?port=' + port, function (ret) {
            change_button_status("testsslportIpt",false,"测试一下");
            $("#testsslportMsg").show();
            if (ret.code == 0) {
                $("#testsslportMsg").text("端口" + port + "可用");
            } else {
                $("#testsslportMsg").text(ret.msg);
            }
        }, "json")
    }

    $(function () {
        $("#savehttps").submit(function () {

            change_button_status("saveHttps",true,"保存中");

            if(!checkIptValue()){
                change_button_status("saveHttps",false,"保存");
                return false;
            }
            //check port
            var port = $("[name='api_common_https_port']").val();
            $.get('../ssl/checkport?port=' + port,function(ret){
                if (ret && ret.code == 0) {
                    //submit
                    $.ajax({
                        url:"../ssl/savehttps", 
                        data:$("#savehttps").serialize(), 
                        type:'post',
                        dataType:'json',
                        success:function (ret) {
                            change_button_status("saveHttps",false,"保存");
                            if (ret && ret.code != 0) {
                                showTips({
                                    command: false,
                                    payload: "启用https，需要先设置证书信息"

                                });
                            } else {
                                location.href = '?result=savehttps';
                            }
                        },
                        error:function(){
                            location.href = '?result=savehttps';
                        }
                    });

                }else{
                    $("#testsslportMsg").show();
                    $("#testsslportMsg").text(ret.msg);
                    change_button_status("saveHttps",false,"保存");
                }

            }, "json");
            return false;

        })
    });

    function checkIptValue() {
        if($("[name='api_common_https_port']").val()==''){
            showTips({
                command: false,
                payload: "请设置加密传输端口."

            });
            return false;
        }

        if($("[name='servercrt']").val()==''){
            showTips({
                command: false,
                payload: "请设置服务端证书crt内容."

            });
            return false;
        }
        if($("[name='serverkey']").val()==''){
            showTips({
                command: false,
                payload: "请设置服务端证书key内容."

            });
            return false;
        }
        if($("[name='clientkey']").val()==''){
            showTips({
                command: false,
                payload: "请设置客户端证书crt内容."

            });
            return false;
        }
        return true;
    }

</script>