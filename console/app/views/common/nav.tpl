<nav class="nav-kingfile clearfix">

  <div class="header">
    <a href="#"></a>
  </div>

  <div id="navbar" class="nav-content clearfix">
    <ul class="nav-kingfile-left pull-left clearfix">
      <li class="active">
        <a href="#">控制台</a>
      </li>
    </ul>
    <ul class="nav-kingfile-right pull-right clearfix">
      {assign var="loginedInfo" value=getLoginedUser()}
      <li>
        <a href="javascript:void(0);">
          {$loginedInfo['name']}&nbsp;&nbsp;&nbsp;&nbsp;
        </a>
        <span class="vertical-line-nav"></span>
      </li>
      <li>
        <a href="../account/logout">退出</a>
      </li>
    </ul>
</nav>



<div class="messagebar alert alert-dismissible " role="alert">
  <button type="button" class="close" onclick="close_alert()"><span aria-hidden="true" style="font-size: 16px;">×</span><span class="sr-only">Close</span></button>
  <div class="tipsText">Best check yo self, you're not looking too good.</div>
</div>

<script type="text/javascript">
  function disabledIpt(elem, action) {
    if (true == action) {
      $("#" + elem).prop("disabled", true)
    } else {
      $("#" + elem).prop("disabled", false)
    }
  }

  function change_button_status(elem, action,txt) {
    if (true == action) {
      $("#" + elem).prop("disabled", true);

    } else {
      $("#" + elem).prop("disabled", false)
    }
    $("#" + elem).val(txt);
    $("#" + elem).text(txt);
  }

  function showTips(msg) {
    var command = msg.command;
    var tipsText = msg.payload;
    $(".alert").show();
    $(".alert .tipsText").html(tipsText);
    if (command) {
      $(".alert").removeClass("alert-danger").addClass("alert-success");
      setTimeout(function () {
        if (command) {
          $(".alert").hide();
        }
      }, 2000);
    } else {
      $(".alert").removeClass("alert-success").addClass("alert-danger");

    }
  }
  function close_alert(){
    $(".alert").hide();
  }


</script>