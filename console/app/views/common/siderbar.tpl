<div class="sidebar">

  <ul class="nav-sidebar">
    {assign var="loginedUser" value=getLoginedUser()}

    {if checkPermission($loginedUser, "dashboard_dashboard")}
    <!--
    <li {if $title == '主面板'}class="active"{/if}><a href="dashboard/dashboard"><span class="glyphicon glyphicon-list"></span> 主面板</a></li>
    -->
    {/if}


    {if checkPermission($loginedUser, "setting_index")}
    <li {if $title == '平台设置'}class="active"{/if}><a href="../setting/index"><span class="glyphicon glyphicon-wrench"></span> 平台设置</a></li>
    {/if}

    {if checkPermission($loginedUser, "dashboard_domainlist")}
    <li {if $title == '企业列表'}class="active"{/if}><a href="../dashboard/domainlist"><span class="glyphicon glyphicon-list"></span> {if 1== constant("PRODUCTTYPE")}企业信息{else}企业列表{/if}</a></li>
    {/if}


    {if checkPermission($loginedUser, "clients_index")}
    <li {if $title == '客户端管理'}class="active"{/if}><a href="../clients/index"><span class="glyphicon glyphicon-phone"></span> 客户端管理</a></li>
    {/if}

    {if checkPermission($loginedUser, "sysnotice_index")}
    <li {if $title == '公告管理'}class="active"{/if}><a href="../sysnotice/index"><span class="glyphicon glyphicon-list"></span> 公告管理</a></li>
    {/if}


    {if checkPermission($loginedUser, "ssl_index")}
    <li {if $title == '加密设置'}class="active"{/if}><a href="../ssl/index"><span class="glyphicon glyphicon-lock"></span> 加密设置</a></li>
    {/if}

    {if checkPermission($loginedUser, "space_index")}
    <li {if $title == '空间情况'}class="active"{/if}><a href="../space/index"><span class="glyphicon glyphicon-stats"></span> 空间情况</a></li>
    {/if}

    {if checkPermission($loginedUser, "monitor_index")}
    <li {if $title == '监控信息'}class="active"{/if}><a href="../monitor/index"><span class="glyphicon glyphicon-signal"></span> 监控信息</a></li>
    {/if}

    {if checkPermission($loginedUser, "backup_index")}
    <li {if $title == '数据库备份'}class="active"{/if}><a href="../backup/index"><span class="glyphicon glyphicon-backup"></span> 数据库备份</a></li>
    {/if}

    {if checkPermission($loginedUser, "systeminfo_index")}
    <li {if $title == '系统管理'}class="active"{/if}><a href="../systeminfo/index"><span class="glyphicon glyphicon-cog"></span> 系统管理</a></li>
    {/if}


  </ul>

</div>