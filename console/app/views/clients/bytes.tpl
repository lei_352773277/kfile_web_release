<form class="form-horizontal" action="savebytes" name="savebytesform" ng-submit="submit_update('savewhitelistform')" method="post" class="clients">

        <div class="form-group">
            <label for="windows" class="control-label">
                上传最大速度：
                <span class="glyphicon glyphicon-info-sign" title="限制客户端的最大上传速度"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input class="form-control" style="width:200px; display:inline-block;" value="{$keys['upbytes']}" disabled="disabled" > bytes
                {else}
                    <input class="form-control" style="width:200px; display:inline-block;"  value="{$keys['upbytes']}"  name="upbytes" placeholder="不填则为不限制" > bytes
                {/if}
            </div>

        </div>
        <div class="form-group">

            <label for="windowssync" class="control-label">
                下载最大速度：<span class="glyphicon glyphicon-info-sign" title="限制客户端的最大下载速度"></span>
            </label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input class="form-control" style="width:200px; display:inline-block;" value="{$keys['downbytes']}"  disabled="disabled" > bytes
                {else}
                    <input class="form-control" style="width:200px; display:inline-block;" value="{$keys['downbytes']}"  name="downbytes"  placeholder="不填则为不限制" > bytes
                {/if}
            </div>

        </div>
        
        <div class="form-group">
            <label  class="control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                <a class="btn btn-primary" href="?option=bytes&editdownload=1">修改</a>
                {else}
                <button type="submit" class="btn btn-primary" ng-disabled="saveing" >{literal}{{saveTxt}}{/literal}</button>
                <a type="button"  class="btn btn-default" ng-disabled="saveing" href="?option=bytes" >取消</a>
                {/if}
            </div>
        </div>
        {if "saveok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}
</form>