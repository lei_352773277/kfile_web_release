<form class="form-horizontal" name="saveupdateform" action="saveupdate" method="post"
      class="clients" ng-submit="submit_update('saveupdateform')"
      xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

    <div class="form-group">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">windows升级</div>
    </div>
    <div class="form-group">

        <label class="control-label">升级地址<span class="glyphicon glyphicon-info-sign"
                                               title="升级地址"></span></label>
        <div class="ipt-wrap">
            {if empty($editupdate)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['windows_updateurl']}"/>
            {else}
                <input type="text" class="form-control" required name="windows_updateurl" ng-model="keys['windows_updateurl']"/>
            {/if}
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign" title=""></span></label>

        <div class="ipt-wrap">

            <select name="windows_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['windows_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['windows_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['windows_update_level']=="3"}selected{/if}>所有用户</option>
            </select>

        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="升级到版本"></span></label>
        <div class="ipt-wrap">
            {if empty($editupdate)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['windows_update_version']}"/>
            {else}
                <input type="text" class="form-control"  name="windows_update_version" required ng-model="keys['windows_update_version']"/>
            {/if}
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包<span class="glyphicon glyphicon-info-sign" title="升级包"></span></label>

        <div class="ipt-wrap">
            {if empty($editupdate)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['windows_update_path']}"/>
            {else}
                <input type="text" class="form-control" name="windows_update_path" required ng-model="keys['windows_update_path']"/>
            {/if}
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级信息<span class="glyphicon glyphicon-info-sign" title="升级信息"></span></label>

        <div class="ipt-wrap">
            {if empty($editupdate)}
                <textarea type="text" class="form-textarea" rows="3" disabled="disabled">{$keys['windows_update_msg']}</textarea>
            {else}
                <textarea type="text" class="form-textarea" rows="3" name="windows_update_msg" required ng-model="keys['windows_update_msg']"></textarea>
            {/if}
        </div>
    </div>
    <thead>
    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; ">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">windows同步盘升级</div>
    </div>
    </thead>
    <div class="form-group">

        <label class="control-label">升级地址<span class="glyphicon glyphicon-info-sign" title="升级地址"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control"  name="windowssync_updateurl"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['windowssync_updateurl']}"
                    {else}required ng-model="keys['windowssync_updateurl']" {/if}  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign"
                                               title="windows同步盘升级级别"></span></label>

        <div class="ipt-wrap">

            <select name="windowssync_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['windowssync_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['windowssync_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['windowssync_update_level']=="3"}selected{/if}>所有用户</option>
            </select>

        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="windows同步盘升级到版本"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control" name="windowssync_update_version"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['windowssync_update_version']}"
                    {else}required ng-model="keys['windowssync_update_version']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包<span class="glyphicon glyphicon-info-sign"
                                              title="升级包"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control" name="windowssync_update_path"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['windowssync_update_path']}"
                    {else}required ng-model="keys['windowssync_update_path']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级信息<span class="glyphicon glyphicon-info-sign"
                                               title="升级信息"></span></label>


        <div class="ipt-wrap">
            <textarea type="text" class="form-textarea" name="windowssync_update_msg" required rows="3"
                    {if empty($editupdate)} disabled="disabled"{/if}
                   >{$keys['windowssync_update_msg']}</textarea>
        </div>
    </div>

    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; ">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">android升级</div>
    </div>

    <div class="form-group">
        <label class="control-label">升级地址<span class="glyphicon glyphicon-info-sign" title="升级地址"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control" name="android_updateurl"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['android_updateurl']}"
                    {else}required ng-model="keys['android_updateurl']" {/if}
                  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign"
                                               title="升级级别"></span></label>
        <div class="ipt-wrap">

            <select name="android_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['android_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['android_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['android_update_level']=="3"}selected{/if}>所有用户</option>
            </select>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="升级到版本"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="android_update_version"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['android_update_version']}"
                    {else}required ng-model="keys['android_update_version']" {/if}
                  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包<span class="glyphicon glyphicon-info-sign"
                                              title="升级包"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="android_update_path"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['android_update_path']}"
                    {else}required ng-model="keys['android_update_path']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级信息<span class="glyphicon glyphicon-info-sign"
                                               title="升级信息"></span></label>
        <div class="ipt-wrap">
            <textarea type="text" class="form-textarea" name="android_update_msg" required rows="3"
                    {if empty($editupdate)} disabled="disabled" {/if}
                   >{$keys['android_update_msg']}</textarea>
        </div>
    </div>
    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; ">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">iphone升级</div>
    </div>
    <div class="form-group">
        <label class="control-label">升级地址<span class="glyphicon glyphicon-info-sign" title="升级地址"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="iphone_updateurl"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['iphone_updateurl']}"
                    {else}required ng-model="keys['iphone_updateurl']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign"
                                               title="升级级别"></span></label>
        <div class="ipt-wrap">

            <select name="iphone_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['iphone_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['iphone_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['iphone_update_level']=="3"}selected{/if}>所有用户</option>
            </select>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="升级到版本"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="iphone_update_version"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['iphone_update_version']}"
                    {else}required ng-model="keys['iphone_update_version']" {/if}
                  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包 <span class="glyphicon glyphicon-info-sign"
                                               title="升级包"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="iphone_update_path"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['iphone_update_path']}"
                    {else}required ng-model="keys['iphone_update_path']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级信息 <span class="glyphicon glyphicon-info-sign"
                                                title="升级信息"></span></label>
        <div class="ipt-wrap">
            <textarea type="text" class="form-textarea" name="iphone_update_msg" required rows="3"
                    {if empty($editupdate)} disabled="disabled"{/if}
                   >{$keys['iphone_update_msg']}</textarea>
        </div>
    </div>
    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; ">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">mac升级</div>
    </div>
    <div class="form-group">

        <label class="control-label">升级地址 <span class="glyphicon glyphicon-info-sign"
                                                title="升级地址"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="mac_updateurl"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['mac_updateurl']}"
                    {else}required ng-model="keys['mac_updateurl']" {/if}
                  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign"
                                               title="升级级别"></span></label>
        <div class="ipt-wrap">


            <select name="mac_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['mac_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['mac_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['mac_update_level']=="3"}selected{/if}>所有用户</option>
            </select>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="升级到版本"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="mac_update_version"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['mac_update_version']}"
                    {else}required ng-model="keys['mac_update_version']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包<span class="glyphicon glyphicon-info-sign"
                                              title="升级包"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control" name="mac_update_path"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['mac_update_path']}"
                   {else}required ng-model="keys['mac_update_path']" {/if}
                   />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级信息<span class="glyphicon glyphicon-info-sign"
                                               title="升级信息"></span></label>

        <div class="ipt-wrap">
            <textarea type="text" class="form-textarea" name="mac_update_msg" required rows="3"
                    {if empty($editupdate)} disabled="disabled" {/if}
                   >{$keys['mac_update_msg']}</textarea>
        </div>
    </div>

    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; ">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">office升级</div>
    </div>

    <div class="form-group">
        <label class="control-label"> 升级地址<span class="glyphicon glyphicon-info-sign"
                                                title="升级地址"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="office_updateurl"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['office_updateurl']}"
                    {else}required ng-model="keys['office_updateurl']" {/if}
                  />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级级别<span class="glyphicon glyphicon-info-sign"
                                               title="升级级别"></span></label>
        <div class="ipt-wrap">


            <select name="office_update_level" class="form-control" {if empty($editupdate)} disabled="disabled" {/if} >
                <option value="1" {if $keys['office_update_level']=="1"}selected{/if}>内部测试</option>
                <option value="2" {if $keys['office_update_level']=="2"}selected{/if}>外部测试</option>
                <option value="3" {if $keys['office_update_level']=="3"}selected{/if}>所有用户</option>
            </select>

        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级到版本<span class="glyphicon glyphicon-info-sign"
                                                title="升级到版本"></span></label>

        <div class="ipt-wrap">
            <input type="text" class="form-control" name="office_update_version"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['office_update_version']}"
                    {else}required ng-model="keys['office_update_version']" {/if}/>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label">升级包<span class="glyphicon glyphicon-info-sign" title="升级包"></span></label>
        <div class="ipt-wrap">
            <input type="text" class="form-control" name="office_update_path"
                    {if empty($editupdate)} disabled="disabled" value="{$keys['office_update_path']}"
                    {else}required ng-model="keys['office_update_path']" {/if}
                    />
        </div>
    </div>
    <div class="form-group">

        <label class="control-label"> 升级信息<span class="glyphicon glyphicon-info-sign"
                                                title="升级信息"></span></label>
        <div class="ipt-wrap">
            <textarea type="text" rows="3" class="form-textarea" name="office_update_msg" required  rows="3"
                    {if empty($editupdate)} disabled="disabled" {/if}
                  >{$keys['office_update_msg']}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label">&nbsp;</label>
        <div class="ipt-wrap">

            {if empty($editupdate)}
                <a class="btn btn-primary" href="?option=update&editupdate=1">修改</a>
            {else}
                <button type="submit" class="btn btn-primary" ng-disabled="saveupdateform.$invalid||saveing" >{literal}{{saveTxt}}{/literal}</button>
                <a type="button" class="btn btn-default" ng-disabled="saveing" href="?option=update">取消</a>
            {/if}
        </div>
    </div>
    {if "saveupdateok" == $result}
        <div class="form-group">
            <label for="inputEmail" class=" control-label">&nbsp;</label>
            <div class="ipt-wrap">
                <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
            </div>
        </div>
    {/if}
</form>