<form class="form-horizontal" action="savewhitelist" name="savewhitelistform" ng-submit="submit_update('savewhitelistform')" method="post" class="clients">

        <div class="form-group">
            <label for="windows" class="control-label">
                内部测试名单：
                <span class="glyphicon glyphicon-info-sign" title="开发组升级名单"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <textarea class="form-control" disabled="disabled" >{$keys['test_users']}</textarea>
                {else}
                    <textarea class="form-control" required name="test_users" placeholder="请输入内部测试名单" ng-model="keys['test_users']"></textarea>
                    <span style="color:#ccc;">多个用户用","逗号分割</span>
                {/if}
            </div>

        </div>
        <div class="form-group">

            <label for="windowssync" class="control-label">
                外部测试名单：<span class="glyphicon glyphicon-info-sign" title="测试组升级名单"></span>
            </label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <textarea class="form-control" disabled="disabled" >{$keys['kingsoft_users']}</textarea>
                {else}
                    <textarea class="form-control" name="kingsoft_users" required  placeholder="请输入测试组升级名单" ng-model="keys['kingsoft_users']"></textarea>
                    <span style="color:#ccc;">多个用户用","逗号分割</span>
                {/if}
            </div>

        </div>
        
        <div class="form-group">
            <label  class="control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                <a class="btn btn-primary" href="?option=whitelist&editdownload=1">修改</a>
                {else}
                <button type="submit" class="btn btn-primary" ng-disabled="savewhitelistform.$invalid||saveing" >{literal}{{saveTxt}}{/literal}</button>
                <a type="button"  class="btn btn-default" ng-disabled="saveing" href="?option=whitelist" >取消</a>
                {/if}
            </div>
        </div>
        {if "saveok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}
</form>