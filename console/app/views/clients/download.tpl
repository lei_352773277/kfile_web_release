<form class="form-horizontal" name="saveDownloadForm" action="savedownload" method="post" class="clients" ng-submit="submit_update('saveDownloadForm')"
      xmlns="http://www.w3.org/1999/html">

    <div class="form-group">
        <label  class="control-label">
            二维码
            <span class="glyphicon glyphicon-info-sign" title="请对: http://{$keys['api_common_http_host']}:{$keys['api_common_http_port']}/welcome/download 这个地址生成二维码，然后获取二维码图片地址"></span>
        </label>
        <div class="ipt-wrap">
            {if empty($editdownload)}
                <input type="text" class="form-control" disabled value="{$keys['client_windows_sync']}" />
            {else}
                <input type="text" class="form-control"  ng-model="keys['client_windows_sync']" name="codepic"
                   placeholder="请对: http://{$keys['api_common_http_host']}:{$keys['api_common_http_port']}/welcome/download 这个地址生成二维码，然后获取二维码图片地址"/>
            {/if}
        </div>

    </div>
    <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>Windows客户端</h4></label></div>
    <div class="form-group">
            <label for="windows" class="control-label">
                安装包地址
                <span class="glyphicon glyphicon-info-sign" title="Windows客户端地址"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$keys['client_windows']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="keys['client_windows']" name="windows"  placeholder="请输入安装包地址"/>
                {/if}
            </div>

        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_windows_size']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_windows_size']" name="client_windows_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_windows_version']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_windows_version']" name="client_windows_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容
                <span class="glyphicon glyphicon-info-sign" title="更新内容"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_windows_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_windows_msg']" name="client_windows_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>

        <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>Windows同步盘</h4></label></div>
        <div class="form-group">

            <label for="windowssync" class="control-label">
                安装包地址<span class="glyphicon glyphicon-info-sign" title="Windows同步盘客户端地址"></span>
            </label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$keys['client_win_sync']}"/>
                {else}
                    <input type="text" class="form-control"   name="win_sync" ng-model="keys['client_win_sync']" placeholder="请输入安装包地址"/>
                {/if}
            </div>

        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_win_sync_size']}" />
                {else}
                    <input type="text" class="form-control" ng-model="downinfo['client_win_sync_size']" name="client_win_sync_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_win_sync_version']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_win_sync_version']" name="client_win_sync_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容：
                <span class="glyphicon glyphicon-info-sign" title="更新内容"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_win_sync_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_win_sync_msg']" name="client_win_sync_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>

        <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>Mac客户端</h4></label></div>

        <div class="form-group">

            <label for="mac" class="control-label">安装包地址<span class="glyphicon glyphicon-info-sign" title="Mac客户端地址"></span></label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['client_mac']}"/>
                {else}
                <input type="text" class="form-control"  name="mac" ng-model="keys['client_mac']" placeholder="请输入安装包地址"/>
                {/if}
            </div>

        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_mac_size']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_mac_size']" name="client_mac_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_mac_version']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_mac_version']" name="client_mac_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容
                <span class="glyphicon glyphicon-info-sign" title="更新内容"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_mac_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_mac_msg']" name="client_mac_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>


        <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>iPhone客户端</h4></label></div>

        <div class="form-group">

            <label for="iphone" class="control-label">安装包地址<span class="glyphicon glyphicon-info-sign" title="iPhone客户端地址"></span></label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['client_iphone']}"/>
                {else}
                    <input type="text" class="form-control" name="iphone" ng-model="keys['client_iphone']"/>
                {/if}
            </div>

        </div>
        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_iphone_size']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_iphone_size']" name="client_iphone_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_iphone_version']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_iphone_version']" name="client_iphone_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容
                <span class="glyphicon glyphicon-info-sign" title="更新内容"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_iphone_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_iphone_msg']" name="client_iphone_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>

        <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>Android客户端</h4></label></div>  
        <div class="form-group">

            <label for="android" class="control-label">安装包地址<span class="glyphicon glyphicon-info-sign" title="android客户端地址"></span></label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['client_android']}"/>
                {else}
                    <input type="text" class="form-control"   name="android" ng-model="keys['client_android']"/>
                {/if}
            </div>

        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_android_size']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_android_size']" name="client_android_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_android_version']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_android_version']" name="client_android_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容：
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_android_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_android_msg']" name="client_android_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>

        <div class="form-group" style="border-top:1px solid #F5F5F5;padding-top:15px; "><label for="windows" class="control-label"><h4>Office插件</h4></label></div>  

        <div class="form-group">

            <label for="offline" class="control-label">安装包地址<span class="glyphicon glyphicon-info-sign" title="office插件客户端地址"></span></label>

            <div class="ipt-wrap">
                {if empty($editdownload)}
                <input type="text" class="form-control" disabled="disabled" value="{$keys['client_office']}"/>
                {else}
                    <input type="text" class="form-control"  name="office" ng-model="keys['client_office']"/>
                {/if}
            </div>

        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                安装包大小
                <span class="glyphicon glyphicon-info-sign" title="安装包大小"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_office_size']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_office_size']" name="client_office_size"  placeholder="请输入安装包大小"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                版本号
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_office_version']}" />
                {else}
                    <input type="text" class="form-control" ng-model="downinfo['client_office_version']" name="client_office_version"  placeholder="请输入版本号"/>
                {/if}
            </div>
        </div>

        <div class="form-group">
            <label for="windows" class="control-label">
                更新内容：
                <span class="glyphicon glyphicon-info-sign" title="版本号"></span>
            </label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                    <input type="text" class="form-control" disabled value="{$downinfo['client_office_msg']}" />
                {else}
                    <input type="text" class="form-control"  ng-model="downinfo['client_office_msg']" name="client_office_msg"  placeholder="请输入更新内容"/>
                {/if}
            </div>
        </div>


        <div class="form-group">
            <label  class="control-label">&nbsp;</label>
            <div class="ipt-wrap">
                {if empty($editdownload)}
                <a type="button" class="btn btn-primary" href="?option=download&editdownload=1">修改</a>
                {else}
                <button type="submit"  class="btn btn-primary" ng-disabled="saveing" >{literal}{{saveTxt}}{/literal}</button>
                <a type="button"  class="btn btn-default" ng-disabled="saveing" href="?option=download" >取消</a>
                {/if}
            </div>
        </div>
        {if "savedownloadok" == $result}
            <div class="form-group">
                <label for="inputEmail" class=" control-label">&nbsp;</label>
                <div class="ipt-wrap">
                    <p class="bg-success successtips"><span class="glyphicon glyphicon-ok"></span> 保存操作成功</p>
                </div>
            </div>
        {/if}
</form>