<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/console.css">
    <link rel="stylesheet" type="text/css" href="{$common}/../console/icons.css">
    <script src="{$common}/jquery/2.1.4/jquery.min.js"></script>
    <script src="{$common}/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script src="{$common}/angular.js/1.4.6/angular.min.js"></script>
    {*<script src="{$common}/jquery.validate/jquery.validate.min.js"></script>*}
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <title>{$title}</title>

<script type="text/javascript">
    var app = angular.module("myApp", []);
    app.controller("myCtrl",function($scope, $http){

        function getKVInfo (v) {
            $http.get('./getKVInfo').success(function(ret){
                $scope.keys = ret.data;
                $scope.downinfo = eval('('+ret['data']['client_download_info']+')');
                //console.info($scope.keys);
            });
        }
        getKVInfo("");

        $scope.saveTxt="保存";
        $scope.saveing=false;

        $scope.submit_update=function(form_name){
            $scope.saveTxt="保存中";
            $scope.saveing=true;
        }

    });

</script>
{literal}
    <style>
      .ipt-wrap {width:70%;}
    </style>
{/literal}
</head>
<body>
{include file="../common/nav.tpl"}

<div class="container-fluid" ng-app="myApp" ng-controller="myCtrl">

    {include file="../common/siderbar.tpl" title="客户端管理"}
    <div class="toolbar ">
        <ul class="nav nav-tabs my-tabs" role="tablist" id="myTabs">
            <li role="presentation" class="{if $option=='download'}active{/if}">
                <a href="?option=download" >下载地址配置</a>
            </li>
            <li role="presentation" class="{if $option=='update'}active{/if}">
                <a href="?option=update" >升级服务配置</a>
            </li>
            <li role="presentation" class="{if $option=='whitelist'}active{/if}">
                <a href="?option=whitelist" >升级名单配置</a>
            </li>
            <li role="presentation" class="{if $option=='bytes'}active{/if}">
                <a href="?option=bytes" >客户端限速</a>
            </li>
        </ul>
    </div>
    <div class="table-wrap">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane {if $option=='download'}active{/if}" id="download">
                    <br/>{include file="./download.tpl" title="下载地址配置"}</div>
                <div  class="tab-pane {if $option=='update'}active{/if}" id="update">
                    <br/>{include file="./update.tpl" title="升级服务配置"}</div>
                {if $option=='whitelist'}
                <div  class="tab-pane active" id="config">
                    <br/>{include file="./whitelist.tpl" title="升级级别配置"}</div>
                {/if}

                {if $option=='bytes'}
                <div  class="tab-pane active" id="config">
                    <br/>{include file="./bytes.tpl" title="升级级别配置"}</div>
                {/if}
            </div>
        </div>

    </div>
</div>
</body>
</html>
