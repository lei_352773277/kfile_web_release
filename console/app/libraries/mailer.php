<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 
class Mailer {
 
    var $mail;
 
    public function __construct()
    {
        require_once('PHPMailer/PHPMailerAutoload.php');
 
        // the true param means it will throw exceptions on errors, which we need to catch
        $this->mail = new PHPMailer;
 
        $this->mail->isSMTP(); // telling the class to use SMTP
 
        $this->mail->CharSet = "utf-8";                  // 一定要設定 CharSet 才能正確處理中文
        $this->mail->SMTPDebug  = 0;                     // enables SMTP debug information
        $this->mail->SMTPAuth   = true;                  // enable SMTP authentication
        $this->mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $this->mail->Host       = "smtp.163.com";      // sets GMAIL as the SMTP server
        $this->mail->Port       = 465;                   // set the SMTP port for the GMAIL server
        $this->mail->Username   = "nil_work@163.com";// GMAIL username
        $this->mail->Password   = "";       // GMAIL password
        //$this->mail->AddReplyTo('nil_work@163.com', '小黄');
        //$this->mail->SetFrom('nil_work@163.com', '测试账号');
    }
	
	public function opts($smtpAuth,$smtpSecure,$host,$port,$mailfrom,$username,$password,$nickname) {
		$this->mail->SMTPDebug  = 0;                     // enables SMTP debug information
        $this->mail->SMTPAuth   = $smtpAuth;                  // enable SMTP authentication
        $this->mail->SMTPSecure = $smtpSecure;                 // sets the prefix to the servier
        $this->mail->Host       = $host;      // sets GMAIL as the SMTP server
        $this->mail->Port       = $port;                   // set the SMTP port for the GMAIL server
        $this->mail->Username   = $username;// GMAIL username
        $this->mail->Password   = $password;       // GMAIL password
        $this->mail->SetFrom($mailfrom, $nickname);
        $this->mail->IsHTML(true);
	}	
	
	
 
    public function sendmail($to, $subject, $body){
        try{
            $this->mail->AddAddress($to);
 
            $this->mail->Subject = $subject;
            $this->mail->Body    = $body;
 
            $result = $this->mail->Send();
            if ($result){
                return array('code'=>0,'data'=>"send ok");
            } else {
                $info = $this->mail->ErrorInfo;
                return array('code'=>1,'data'=>$info);
            }
        } catch (phpmailerException $e) {
            return array('code'=>1,'msg'=>$e->errorMessage()); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            return array('code'=>1,'msg'=>$e->getMessage()); //Boring error messages from anything else!
        }
    }
}
 
/* End of file mailer.php */
 