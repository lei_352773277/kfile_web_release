<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Account_model extends MY_Model
{

    private $_users_table = "users";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function info($token)
    {
        $data = $this->config->config['console'];
        if($token != md5($data['superadmin'])) {
            return array('code'=>122,'msg'=>'错误的用户名或者密码');
        } else {
            return array('code'=>0,'data'=>array());
        }
        $post_data = json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function account_validate($token, $vtype, $value) {
        $post_data = json_encode(array('token'=>$token,'vtype'=>$vtype,'value'=>$value));
        $res = $this->request_netdisk_server("/account/validate", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function login($username, $password) {
        $user = $this->loginTokens($username, $password);
        if (empty($user)) {
            $user = array('code'=>LOGIN_FAILE);
        } else {
            $user = array('code'=>OK, 'data'=>$user);
        }
        return $user;
    }

    //登录
    function loginTokens($username, $password)
    {
        $user = $this->getUserByEmail($username);
        if (empty($user)) {
            return false;
        }
        if (md5(md5($password).$user['salt']) === $user['pwd']) {
           return $user;
        } else {
            return false;
        }
    }

    
    function getUserByEmail($email) {
        return $this->db->get_where($this->_users_table, array('email'=>$email))->row_array();
    }


    function getUserById($userId) {
        return $this->db->get_where($this->_users_table, array('user_id'=>$userId))->row_array();
    }


    function changepwd($userId, $curPwd, $newPwd) {
        $user = $this->getUserById($userId);
        if (md5(md5($curPwd).$user['salt']) === $user['pwd']) {
            $this->load->helper('string');
            $salt = random_string('alnum',16);
            $newPwdVal = md5(md5($newPwd).$salt);
            $newUser = array("pwd"=>$newPwdVal,'salt'=>$salt);
            $ret=$this->edituser($userId, $newUser);
            return array('code'=>0,'data'=>$ret);
        } else {
            return array('code'=>9999,'msg'=>"当前密码不正确");
        }
    }

    function edituser($user_id, $user) {
        $this->db->where('user_id', $user_id);
        return $this->db->update("users",$user);
    }



}