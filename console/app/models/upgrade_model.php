<?php
/**
 * 升级相关
 */
class Upgrade_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function v3_2_4()
    {
        $sql = "
            CREATE TABLE IF NOT EXISTS `users` (
          `user_id` int(10) NOT NULL AUTO_INCREMENT,
          `user_type` tinyint(1) NOT NULL,
          `enable` tinyint(1) NOT NULL DEFAULT '1',
          `name` varchar(128) NOT NULL,
          `email` varchar(128) NOT NULL,
          `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `description` varchar(1024) DEFAULT NULL,
          `salt` varchar(45) DEFAULT NULL,
          `pwd` varchar(45) DEFAULT NULL,
          PRIMARY KEY (`user_id`),
          UNIQUE KEY `email_UNIQUE` (`email`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;";
        $this->db->query($sql);

        $sql = "
            INSERT INTO `users` (`user_id`, `user_type`, `enable`, `name`, `email`, `created_time`, `description`, `salt`, `pwd`) VALUES
            (1, 1, 1, '管理员', 'wangpan@kingsoft.com', '2016-11-01 19:54:46', NULL, 'y1QHKt0WzqvWPQM9', '1a2cbc681a24ddedfa9947455287948f');";
        $this->db->query($sql);

        $sql = "
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`,`value`) VALUES ('client_win_sync','http://e.ksyun.com/product/down?c=windows&v=1.5');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('client_download_info');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('client_mac');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('client_office');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('client_windowssync');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windowssync_updateurl');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windowssync_update_level');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windowssync_update_version');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windowssync_update_path');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windowssync_update_msg');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('mac_updateurl');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('mac_update_level');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('mac_update_version');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('mac_update_path');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('mac_update_msg');

        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('office_updateurl');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('office_update_level');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('office_update_version');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('office_update_path');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('office_update_msg');

        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('windows_update_msg');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('android_update_msg');
        INSERT INTO `platform`.`ec_platform_setting` (`keyname`) VALUES ('iphone_update_msg');";

        $this->db->query($sql);
    }


}