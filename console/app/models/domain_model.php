<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Domain_model extends MY_Model
{
    var $loginKey = "";
    function __construct()
    {
        $this->loginKey = $_SERVER['KFILE_LOCAL_LOGIN_KEY'];
        parent::__construct();
    }

    function info($token)
    {
        $post_data = json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function signup($domain_data)
    {
        $post_data = json_encode($domain_data);
        $res = $this->request_netdisk_server("/account/signup", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function get_domainlist()
    {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/org/inner/domain/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function domain_info($token, $id)
     {        
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token,'domainId'=>$id));
        $res = $this->request_netdisk_server("/org/inner/domain/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function domain_info_by_domainIdent($token, $domainIdent)
     {        
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token,'domainIdent'=>$domainIdent));
        $res = $this->request_netdisk_server("/org/inner/domain/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function get_member_count($token, $domainId) {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token,'domainId'=>$domainId));
        $res = $this->request_netdisk_server("/org/inner/domain/members/count", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function domain_config_detail($token, $domainId) {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token,'domainId'=>$domainId));
        $res = $this->request_netdisk_server("/org/inner/domain/config/detail", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function get_super($domain_id) {
        $db2 = $this->load->database('organization', TRUE);
        $str_sql = "select name,user_id from EC_XStaff where super_type=1 and domain_id=".$domain_id;
        $row = $db2->query($str_sql)->row_array();
        return $row;
     }

     function reset_super_pwd($user_id) {
        $db2 = $this->load->database('account', TRUE);
        $db2->where("user_id",$user_id);
        $db2->set('password', '2ca3966846783f96bf5a5c3142132ea6');
        $db2->update("EC_Account");
     }

     function login_v2($login_data)
     {
        $post_data = json_encode($login_data);
        $res = $this->request_netdisk_server_2("/account/login", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

    function info_v2($token)
    {
        $post_data = json_encode(array("token" => $token));
        $res = $this->request_netdisk_server_v2("/account/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

     //创建默认的目录和文件
     function create_default_folder_and_file($username, $pwd, $ident)
     {
        $config = $this->config->item('netdisk.api');
        $this->load->helper('string');
        define("KINGFILE_USER_AGENT", $config['user_agent'] . ';' . random_string('alnum', 16));

        $login_data = ["loginType" => 0, "loginTag" => $username, "userPwd" => $pwd, 'domainIdent' => $ident];
        $info = $this->login_v2($login_data);
        if (!empty($info) && 0 == $info['code']) {
            $token = $info['data']['token'];
            $res_info = $this->info_v2($token);
            if (!empty($res_info)) {
                $name = '金山企业云盘帮助文档';
                $desc = '金山企业云盘帮助文档';
                $quota = 1125899906842624;//默认大小
                $perm = [['user_xid'=>$res_info['data']['first_dept'],'role'=>4369]];
                $folder_ret = $this->dir_share_root_create($token,$name,$desc,$quota,$perm);
                if (!empty($folder_ret) && 0==$folder_ret['code']) {
                    $xid = $folder_ret['data']['xid'];
                    $url = '/agent/file/upload?xid='.$xid.'&token='.$token.'&agent='.KINGFILE_USER_AGENT;
                    $post_data = array('file'=>"@/web/console/conf/doc/同步盘帮助文档.pptx");
                    $res = $this->request_netdisk_server_v2($url, $post_data);

                    $url = '/agent/file/upload?xid='.$xid.'&token='.$token.'&agent='.KINGFILE_USER_AGENT;
                    $post_data = array('file'=>"@/web/console/conf/doc/金山企业云盘新手引导-web端.ppt");
                    $res = $this->request_netdisk_server_v2($url, $post_data);

                    $url = '/agent/file/upload?xid='.$xid.'&token='.$token.'&agent='.KINGFILE_USER_AGENT;
                    $post_data = array('file'=>"@/web/console/conf/doc/金山企业云盘新手引导-PC增强版.ppt");
                    $res = $this->request_netdisk_server_v2($url, $post_data);

                }
            }
        }
     }

    function  dir_share_root_create($token,$name,$desc,$quota,$perm)
    {
        $post_data = array("token"=>$token,'name'=>$name,'desc'=>$desc,'perm'=>$perm);
        if($quota>0) {
            $post_data['quota'] = $quota;
        } else {
            $post_data['quota'] = PBQUOTA;
        }
        $post_data = $this->my_json_encode($post_data);
        $res = $this->request_netdisk_server_2("/xfile/dir/share/root/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_root_create, ';
            $error_msg .= 'msg=response error value, params='.$post_data.', res='.$res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


}