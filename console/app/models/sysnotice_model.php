<?php
class Sysnotice_model extends MY_Model
{

    var $loginKey = "";

    function __construct()
    {   
        $this->loginKey = $_SERVER['KFILE_LOCAL_LOGIN_KEY'];
        parent::__construct();
    }

    //发布公告
    function create($title, $content)
    {
        $post_data = json_encode(array('token'=>$this->loginKey, 'title'=>$title, 'content'=>$content));
        $res = $this->request_netdisk_server("/announce/inner/create", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    //公告列表
    function all($pageMax=1000, $hint)
    {
        $post_data = json_encode(array('token'=>$this->loginKey, 'pageMax'=>$pageMax, 'hint'=>$hint));
        $res = $this->request_netdisk_server("/announce/inner/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function del($id)
    {
        $post_data = json_encode(array('token'=>$this->loginKey, 'announceId'=>$id));
        $res = $this->request_netdisk_server("/announce/inner/withdraw", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function get($id)
    {
        $post_data = json_encode(array('token'=>$this->loginKey, 'announceId'=>$id));
        $res = $this->request_netdisk_server("/announce/inner/get", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }



}