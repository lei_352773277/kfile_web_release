<?php
/**
 * 系统设置相关
 * User: qyuan
 * Date: 16-8-29
 * Time: 上午10:04
 */
class Systeminfo_model extends MY_Model
{
    private $networkConfig = "../conf/network";

    //读取配置文件
    function getNetWorkConfig()
    {
        $str = file_get_contents($this->networkConfig);
        $result = array(
            'ip'=>'',
            'mask'=>'',
            'gateway'=>'',
            'accessurl'=>'',
            'port'=>'',
            'httpsport'=>''
        );
        if(!empty($str))  {
            $list = explode("|", $str);
            $result['ip'] = $list[0];
            $result['mask'] = $list[1];
            $result['gateway'] = $list[2];
            $result['accessurl'] = $list[3];
            $result['port'] = $list[4];
            $result['httpsport'] = $list[5];
        }
        return $result;
    }

    //写配置文件
    function writeNetWorkConfig($arr)
    {
        $result = $this->getNetWorkConfig();
        if (is_array($arr)) {
            foreach($arr as $k=>$v) {
                $result[$k] = $v;
            }
        }
        $str = $result['ip']."|".$result['mask']."|".$result['gateway']."|";
        $str .= $result['accessurl']."|".$result['port']."|".$result['httpsport'];
        $oldStr = file_get_contents($this->networkConfig);
        if($oldStr!==$str) {
            file_put_contents($this->networkConfig, $str);
        }
    }

    /**
     *获取系统信息
     */
     function getSystemInfo()
     {
        //$info = $this->getKVInfo();
        $info = array();
        $uuid = file_get_contents(DOMAINIDENT);
        $haredware = file_get_contents(HARDWARE);
        $hardware_arr = explode("\n", $haredware);
        $tmp = array();
        $hard_arr = array();
        foreach ($hardware_arr as $key=>$value) {
            $hard_key = trim($value);
            if(strpos($hard_key, "- ")===0) {
            //if( in_array($hard_key, array("- CPU","- RAM","- MAC address","- hard disk in Megacli")) ) {
                $tmp = $hard_key;
            } else {
                if(!empty($hard_key)&&!empty($tmp)) {
                    $hard_arr[$tmp][] = $hard_key;
                }
            }
        }
        $source_arr = array(
                $uuid,
                implode("|", $hard_arr['- CPU']),
                implode("|", $hard_arr['- RAM']),
                implode("|", $hard_arr['- MAC address']),
                implode("|", $hard_arr['- hard disk in Megacli'])
            );
        $usercode = bin2hex(rc4("kingfileisking",base64_encode(implode("&", $source_arr))));

        $info['user_code'] = $usercode;
        $info['hard'] = $hard_arr;
        $info['uuid'] = $uuid;
        return  $info;
    }

}