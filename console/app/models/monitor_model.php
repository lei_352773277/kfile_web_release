<?php
/**
 * 监控相关
 * User: qyuan
 * Date: 16-8-29
 * Time: 上午10:04
 */
class Monitor_model extends MY_Model
{
    var $_alertlist_table = "alertlist";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	public function proxy($v)
	{
		if (empty($v)) {
            $v = "";
        } else {
            $v = '/'.$v;
        }
        if ('/check_raid' ==  $v) {
            $info = file_get_contents(RAIDSTATUS);
            $diskArr = explode("\n", $info);
            $disk = array();
            $process = array();
            $find = false;
            foreach ($diskArr as $k=>$v) {
                $flag = false;
                if (trim($v) == '=') {
                    $find = true;
                    $flag = true;
                }
                if($find) {
                    if (!$flag) {
                        $vv_arr = explode(" - ", $v);
                        if (count($vv_arr) == 2) {
                            $process['disk'.$vv_arr[0]] = $vv_arr[1];
                        }
                    }
                } else {
                    $v_arr = explode(" - ", $v);
                    if(is_array($v_arr)) {
                        $disk[] = array('id'=>'disk'.$v_arr[0],'status'=>$v_arr[1]);
                    }
                }
            }

            foreach($disk as $key=>$value) {
                if(isset($process[$value['id']])) {
                    $value['status'] = $value['status'].", ".$process[$value['id']];
                    $disk[$key] = $value;
                }
            }
            $data['disk'] = $disk;
        } else {
        	$res = $this->curl(MONITORURL.$v);
        	$data = json_decode($res,true);
        }
        return array('code'=>API_RET_SUCC,'data'=>$data);
	}

    function proxyaction($url,$data)
    {
        $res = $this->curl(MONITORURL.$url,$data);
        $data = json_decode($res,true);
        echo json_encode(array('code'=>0,'data'=>$data));
    }

    function getalertlist()
    {
        $table   = $this->_alertlist_table;
        $str_sql  = "select  email,name  from {$table}";
        return $this->db->query($str_sql)->result_array();
    }

    
    function addAlertUser($email,$name)
    {
        $table   = $this->_alertlist_table;
        $str_sql  = "Insert Into {$table} (email,name)values('".$email."','".$name."')";
        return $this->db->query($str_sql);  
    }

    function delAlertUser($email)
    {
        $table   = $this->_alertlist_table;
        $str_sql  = "Delete from  {$table}  where email = '".$email."' limit 1";
        return $this->db->query($str_sql);  
    }


}