<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Platform_model extends MY_Model
{
    var $infos = "";
    var $_platform_table = "ec_platform_setting";
    var $_alertlist_table = "alertlist";
    var $loginKey = "";

    function __construct()
    {
        $this->loginKey = $_SERVER['KFILE_LOCAL_LOGIN_KEY'];
        parent::__construct();
        $this->load->database();
    }

    function info()
    {
        $table   = $this->_platform_table;
        $str_sql  = "select * from {$table}";
        return $this->db->query($str_sql)->result_array();
    }

    function setInfo($data) {
        foreach($data as $key=>$value) {
            $this->db->update($this->_platform_table,array('value'=>$value), array('keyname'=>$key));
        }
        $chk = $this->db->affected_rows();
        return $chk;
    }

    function addKey($keyname, $value="")
    {
        $table   = $this->_platform_table;
        $sql = "insert into {$table} (keyname,value) values ('{$keyname}','{$value}')";
        return $this->db->query($sql);
    }

    function get_value($keyname) 
    {
        $table   = $this->_platform_table;
        $str_sql  = "select  value  from {$table} where keyname = '{$keyname}' limit 1";
        return $this->db->query($str_sql)->result_array();
    }


    function getStorgeSpace()
    {
        $stroge_server = $this->getKeyVal('stroge_server');
        file_get_contents($stroge_server."/_cluster_status");
        $responseInfo = $http_response_header;
        $map = array();
        foreach($responseInfo as $k=>$v) {
            $vlist = explode(": ", $v);
            if(is_array($vlist) && count($vlist)==2) {
                $map[$vlist[0]] = $vlist[1];
            }
        }
        if(!empty($map)) {
            return array('code'=>0,'data'=>array('total'=>$map['X-Cluster-Total-Size']/1024/1024/1024,'used'=>$map['X-Cluster-Avail-Size']/1024/1024/1024));
        } else {
            return array('code'=>0,'data'=>array('total'=>0,'used'=>0));
        }
    }

    //验证升级的玩意
    private function isTest($username)
    {
        $data = $this->infos;
        $strUsers =  $data['test_users'];
        $testUsers = explode(",", $strUsers);
        if(is_array($testUsers)) {
            if(in_array($username, $testUsers)) {
                return True;
            } else {
               return False;
            } 
        } else {
            return False;
        }
    }

    private function isKingsoft($username)
    {
        $data = $this->infos;
        $strUsers = $data['kingsoft_users'];
        $kingsoftUsers = explode(",", $strUsers);
        if(is_array($kingsoftUsers)) {
            if(in_array($username, $kingsoftUsers)) {
                return True;
            } else {
               return False;
            }
        } else {
            return False;
        }
    }

    public function isUpdate($username, $clientVersion,$section="win")
    {
        $serverVersion  = "1.0.0.64";
        $level = '1';
        $path  = 'http://';
        $infos = $this->info();
        $data = array();
        foreach($infos as $k=>$v) {
            $data[$v['keyname']] = $v['value'];
        }
        $this->infos = $data;
        switch($section) {
            case "win":
                $serverVersion = $data['windows_update_version'];
                $level = $data['windows_update_level'];
                $path = $data['windows_update_path'];
                break;
            case 'office':
                $serverVersion = $data['office_update_version'];
                $level = $data['office_update_level'];
                $path = $data['office_update_path'];
                break;
            case "android":
                $serverVersion = $data['android_update_version'];
                $level = $data['android_update_level'];
                $path = $data['android_update_path'];
                break;
            case "iphone":
                $serverVersion = $data['iphone_update_version'];
                $level = $data['iphone_update_level'];
                $path = $data['iphone_update_path'];
                break;
            case "winsync":
                $serverVersion = $data['windowssync_update_version'];
                $level = $data['windowssync_update_level'];
                $path = $data['windowssync_update_path'];
                break;
            case "mac":
                $serverVersion = $data['mac_update_version'];
                $level = $data['mac_update_level'];
                $path = $data['mac_update_path'];
                break;
        }

        if($this->comp($clientVersion, $serverVersion) == -1) {
            if($level == '1' and $this->isTest($username)){
                return array('path'=>$path,'targetversion'=>$serverVersion);
            }
            if($level == '2' and $this->isKingsoft($username)) {
                return array('path'=>$path,'targetversion'=>$serverVersion);
            }
            if($level == '3') {
                return array('path'=>$path,'targetversion'=>$serverVersion);
            }
        }
        return NULL;
    }

    private function comp($client_ver, $server_ver)
    {

        #1:client > server    -1:client < server    0:client = server
        if(empty($client_ver)) {
            return 0;
        }
        $str_client = str_replace(".","",$client_ver);
        if(!is_numeric($str_client)) {
            return 0;
        }

        $clis = explode(".", $client_ver);
        $slis = explode(".", $server_ver);
        foreach($clis as $key=>$value) {
            if(intval($value) > intval($slis[$key])) {
                return 1;
            } else if (intval($value) < intval($slis[$key])) {
                return -1;
            }
        }
        return 0;
    }

    //设置全局的参数
    function setGlobalSetting()
    {
        $dists = $this->getKVInfo();
        $forgettitle = $dists['forgettitle'];
        $ip = $dists['ip'];
        $wanip = $dists['api_common_http_host'];
        $port = $dists['api_common_http_port'];
        $access_ip = trim($wanip)==""?$ip:$wanip;

        if (2 == PRODUCTMODEL) {
            $access_ip = "pan.ksyun.com";
        }

        $setting = array(
            'email_send_url'=>"http://console.com/email/send",
            //'forget_password_email_url'=>"http://".$access_ip.":".$port."/account/verify",
            'forget_password_email_subject'=>$forgettitle,
            'email_base_url'=>"http://".$access_ip.":".$port,
            'dual_factor_device_bind_email_subject'=>"金山企业云盘设备绑定",
            'dual_factor_ban_email_subject'=>"关闭双重认证");
        $res_data = $this->config_global_setting($setting);
        return $res_data;
    }

    /**
     * 设置API全局的setting
     */
     function config_global_setting($settings) {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token,'settings'=>$settings));
        $res = $this->request_netdisk_server("/config/inner/global/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     /**
     *设置企业的setting
     */
     function config_domain_setting($domainId, $setting) {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token, 'domainId'=>$domainId, 'settings'=>$setting));
        $res = $this->request_netdisk_server("/org/inner/domain/config/settings", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

     function config_license_setting($license, $domainId) {
        $token = $this->loginKey;
        $post_data = json_encode(array("token" => $token, 'domainId'=>$domainId, 'license'=>$license));
        $res = $this->request_netdisk_server("/org/inner/domain/config/license/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
     }

    //获取所以的keyvalue
    function getKVInfo()
    {
        $ret_data = $this->info();
        $data = array();
        foreach($ret_data as $k=>$v) {
            $data[$v['keyname']] = $v['value'];
        }
        return $data;
    }

    //获取一个值
    function getKeyVal($keyname)
    {
        $table   = $this->_platform_table;
        $str_sql  = "select  value  from {$table} where keyname = '{$keyname}' limit 1";
        $result = $this->db->query($str_sql)->row_array();
        return $result['value'];
    }

    //发送邮件
    function sendemail($to, $subject, $body)
    {
        /*
        $url = EMAILSERVER.'/sendpmail'; // 请求的url
        $postArray = array (
                'sender' => '金山云盘服务',
                'receiver' => $to,
                'subject' => $subject,
                'body' => $body,
                'format' => 'html' 
        );
        $res = $this->curl ( $url, json_encode ( $postArray ));
        //$res = $this->curl ( $url, json_encode ( $postArray ));
        */
        $postArray = array (
                'recipients' => $to,
                'subject' => $subject,
                'body' => $body,
                'format' => 'html'
        );
        $header=array(
            'Content-Type'=>"Application/json",
            'X-Entry'=>'com.ksyun.pan',
            'X-Entry-Secret'=>'72dd8119ab4a12c62bda99285ac8e705',
            'X-Version'=>1,
            'X-Timestamp'=>time()*1000,
            'X-Client-Ip'=>$this->input->ip_address()
        );
        $url = 'http://emailsrv.com/emails';
        $res = $this->curl ( $url, json_encode ( $postArray ),$header);
        return $res;
    }



}