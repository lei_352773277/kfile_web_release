<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array(
    'domain/reg' => array(
        array(
                'field' => 'user',
                'label' => '管理员登录账号',
                'rules' => 'required'
            ),
        array(
                'field' => 'password',
                'label' => '密码',
                'rules' => 'required'
            ),
        array(
                'field' => 'confirmPassword',
                'label' => '确认密码',
                'rules' => 'required'
            ),
        array(
                'field' => 'domainName',
                'label' => '企业名称',
                'rules' => 'required'
            ),
        array(
                'field' => 'domainContact',
                'label' => '企业联系人',
                'rules' => 'required'
            )
    )                        
);
