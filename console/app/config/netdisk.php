<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: qyuan
 * Date: 15-3-19
 * Time: 上午11:34
 */
define('LICENSE',false);
define('SERVER', false);
define('SYSTEM',true);

if(isset($_SERVER['KFILE_TARGET_ENV'])) {
    if($_SERVER['KFILE_TARGET_ENV'] == "allInOne") {
        define("STORGETYPE",1);//1 for all in one , 2 kdfs
        define("PRODUCTMODEL",3);//
        define('MONITOR',true);
        define('SPACE', true);
    } elseif ($_SERVER['KFILE_TARGET_ENV'] == "public") {
        define("STORGETYPE",1);//1 for all in one , 2 kdfs
        define("PRODUCTMODEL",2);
        define('MONITOR',false);
        define('SPACE', false);
    } elseif ($_SERVER['KFILE_TARGET_ENV'] == "private") {
        define("STORGETYPE",2);//1 for all in one , 2 kdfs
        define("PRODUCTMODEL",3);
        define('MONITOR',true);
        define('SPACE', true);
    } else {
        define("STORGETYPE",2);//1 for all in one , 2 kdfs
        define("PRODUCTMODEL",3);
        define('MONITOR',true);
        define('SPACE', true);
    }
} else {
    define("STORGETYPE",2);//1 for all in one , 2 kdfs
    define("PRODUCTMODEL",3);//1是私有云，2公有云，3是混合云
    define('MONITOR',true);
    define('SPACE', true);
}

if (isset($_SERVER['KFILE_SINGLE_DOMAIN_MODE'])) {
    if ($_SERVER['KFILE_SINGLE_DOMAIN_MODE'] == 1) {//单企业
        define("PRODUCTTYPE",1);
    } else { //多企业
        define("PRODUCTTYPE",2);
    }
} else {
    define("PRODUCTTYPE",2);//1 单企业  2多企业
}


$_SERVER['KFILE_LOCAL_LOGIN_KEY'] = 'Sy_GkrFdfb-X6t3D-rFiUqngsE8Bl_U0';
define('API_SUPER_TOKEN','wangpan@kingsoft.com');//api super token
define('MONITORURL','http://192.168.8.103:2812');
define("SERVERURLCONFIG",false);
define('DEFAULT_DOMAIN_NAME','qq.com');
define("NETWORKCONFIG","../conf/network");
define("RESTARTCONF","../conf/restart");
define("SHUTDOWNCONF","../conf/poweroff");
define("PWDCONF","../conf/pwdconf");
define("DOMAINIDENT","../conf/domainident");
define("RAIDSTATUS","../conf/raidstatus");
define("RESETDATA","../conf/init_db");
define("HARDWARE","/share/kfile_hardware.info");
define("UPGRADE","../conf/upgrade");
define("NETDISKCONFIG","/web/netdisk/app/config/netdisk.php");


$config['netdisk.api'] = array(
    'server' => 'http://192.168.8.103/api/v1',
    'default_version' => 2,
    //'user_agent' => 'ekp_web'
	'user_agent' => 'kingsoft-ecloud-web'
);

$config['console'] = array(
	'superadmin'=>'wangpan@kingsoft.com',
	'superpwd' =>'wangpan@kingsoft.com'
);


//开发环境
$config['netdisk.resources'] = array(
    'css' => '/dist/console',
    'img' => '/dist/img',
    'js' => 'http://localhost:3000',
    'common' =>'/dist/common',
    'debug' => false,
    'title' => '金山企业云盘',
    'cookie_domain'=>'.ksc.com'
);

//匿名可以访问的页面
$config['anonymous_access_pages'] = array(
    "account_login",
    "email_send",
    "ping_index",
    "platform_shellcontactlist",
    "platform_shellgetkey",
    "platform_getclientkey",
    "platform_config",
    "platform_winplus",
    "platform_android",
    "platform_iphone",
    "platform_savenetwork",
    "platform_updatespace",
    "platform_updatekey",
    "platform_clearinfo",
    "welcome_index",
    "backup_notify"
);


/*
//生产环境
$config['netdisk.resources'] = array(
    'css' => '/console/dist/2',
    'img' => 'static/',
    'js' => '/console/dist/2',
    'common' =>'/console/dist/common',
    'debug' => false,
    'title' => '金山云盘',
    'cookie_domain'=>'123.59.14.9'
);
*/
