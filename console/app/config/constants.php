<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');




define('API_RET_SUCC',0);

/*权限*/
define('PERM_CODE_VIEW',0x00000001);
define('PERM_CODE_READ',0x00000010);
define('PERM_CODE_EDIT',0x00000100);
define('PERM_CODE_DELETE',0x00001000);
define('PERM_CODE_GRANT',0x00010000);

define('ROLE_NOTHING',0x00000000); //0
define('ROLE_VIEWER',PERM_CODE_VIEW|PERM_CODE_READ); //17
define('ROLE_EDITOR',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT);//273
define('ROLE_MANIPULATOR',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT|PERM_CODE_DELETE);//4369
define('ROLE_ADMIN',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT|PERM_CODE_DELETE|PERM_CODE_GRANT); //69905
define('ROLE_GOD',0x11111111);//286331153


/*
 * 定义CURL的常量
 */
define("CURL_POST", 'post');
define("CURL_PUT", 'put');
define("CURL_DELETE", 'delete');


/*front response code*/
define('API_RES_OK',10000);
define('NOT_LOGIN','noLogin');
define('API_RES_EMPTY','API_RES_EMPTY');
define('LOGIC_ERROR','LOGIC_ERROR');
define('REQUEST_PARAMS_ERROR','REQUEST_PARAMS_ERROR');
define('SIGN_ERROR','SIGN_ERROR');
define("LOGIN_FAILE",10001);




/*oauth2 client*/
define("CLIENT_ID","client_id");
define("CLIENT_SECRET","secret_1");


define("OK", 0);


/* End of file constants.php */
/* Location: ./application/config/constants.php */