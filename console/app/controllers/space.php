<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 空间管理
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Space extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }


    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $option = $this->input->get('option');
        if (empty($option)) {
            $option = 'space';
        }
        $data['option'] = $option;
        $this->parser->parse("space/index.tpl",$data);
    }

    public function getspace()
    {
    	if(STORGETYPE=="1") {
           $ret_data = $this->platform_model->get_value('spacedata');
           $space_str = $ret_data[0]['value'];
           $space_list = explode("|", $space_str);
           $data = array('code'=>API_RET_SUCC,'data'=>array('total'=>$space_list[0]/1024/1024/1024,'used'=>$space_list[1]/1024/1024/1024));
        } else if (STORGETYPE == "2") {
        	$data = $this->platform_model->get_value('stroge_server');
        	$storge_server =  $data[0]['value'];
            $data = $this->platform_model->getStorgeSpace($storge_server);
        }
        self::res($data);
    }

    public function saveconfig()
    {
        
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */