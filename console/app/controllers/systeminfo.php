<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 系统设置
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Systeminfo extends MY_Controller {
    private $configFile = "../conf/network";

    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('systeminfo_model');
    }


    public function index()
    {

        $data = $this->config->config['netdisk.resources'];
        $dicts = $this->platform_model->getKVInfo();

        $editnetwork = $this->input->get('editnetwork');
        $data['editnetwork'] = $editnetwork;
        //$info = $this->platform_model->getSystemInfo();
        //$data['system'] = $info;
        $data['config_env'] = isset($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])?$_SERVER['KFILE_CONFIG_IP_FROM_ENV']:0;
        $data['keys'] = $dicts;
        $result = $this->input->get('result');
        $data['result'] = $result;
        $data['network'] = $this->systeminfo_model->getNetWorkConfig();
        $data['hardware'] = $this->systeminfo_model->getSystemInfo();
        $this->parser->parse("systeminfo/index.tpl",$data);
    }

    //保存网络
    public function savenetwork()
    {
        $result = array();
        $ip = $this->input->post('ip');
        if(!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
           self::res(array('code'=>9999,'msg'=>'错误的ip地址'));  
        }
        $result['ip'] = $ip;

        $mask = $this->input->post('mask');
        if(!filter_var($mask, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
           self::res(array('code'=>9999,'msg'=>'错误的掩码地址'));  
        }
        $result['mask'] = $mask;

        $gateway = $this->input->post('gateway');
        if(!filter_var($gateway, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
           self::res(array('code'=>9999,'msg'=>'错误的网关地址'));  
        }
        $result['gateway'] = $gateway;

        $accessurl = $this->input->post('accessurl');

        if(!filter_var($accessurl, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            if(!filter_var("http://".$accessurl,FILTER_VALIDATE_URL)) {
                self::res(array('code'=>9999,'msg'=>'错误的访问地址'));  
            }
        }
        $result['accessurl'] = $accessurl;
        
        $port = $this->input->post('port');
        $port = intval($port);
        if ($port<10 || $port > 65535) {
            self::res(array('code'=>9999,'msg'=>'错误的端口'));
        }
        if ($port != $_SERVER['KFILE_HTTP_PORT'] && $port != "80") {
            if (!checkPortIsVaild("127.0.0.1", $port)) {
               self::res(array('code'=>9999,'msg'=>'端口被占用'));   
            }
        }
        $result['port'] = $port;

        $this->systeminfo_model->writeNetWorkConfig($result);
        $this->platform_model->setInfo(
            array('ip'=>$ip,
            'mask'=>$mask,
            'gateway'=>$gateway,
            'api_common_http_host'=>$accessurl,
            'api_common_http_port'=>$port));
        $this->platform_model->setGlobalSetting();
        self::res(array('code'=>0,'data'=>''));
    }

    /*重启*/
    function restart()
    {
        $action = $this->input->post('action');
        if ("1" == $action) {
            if(file_exists(RESTARTCONF)) {
                $shutdown_str = file_get_contents(RESTARTCONF);
                if("restart" == $shutdown_str) {
                    self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'重启中'));
                } else {
                    file_put_contents(RESTARTCONF, "restart");
                    self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'重启中'));
                }
            } else {
                file_put_contents(RESTARTCONF, "restart");
                self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'重启中'));  
            }
        } else {
            self::res(array('code'=>9999,'msg'=>'错误的参数'));  
        }
    }

    /*关机*/
    function shutdown()
    {
        $action = $this->input->post('action');
        if ("1" == $action) {
            if(file_exists(SHUTDOWNCONF)) {
                $shutdown_str = file_get_contents(SHUTDOWNCONF);
                if("poweroff" == $shutdown_str) {
                    self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'关闭中'));
                } else {
                    file_put_contents(SHUTDOWNCONF, "poweroff");
                    self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'关闭中'));
                }
            } else {
                file_put_contents(SHUTDOWNCONF, "poweroff");
                self::res(array('code'=>API_RET_SUCC,'data'=>'','msg'=>'关闭中'));  
            }
        } else {
            self::res(array('code'=>9999,'msg'=>'错误的参数'));  
        }
    }

    /*修改密码*/
    function savepwd() {
        $nowpwd = $this->input->post('nowpwd');
        $newpwd = $this->input->post('newpwd');
        $confirmpwd = $this->input->post('confirmpwd');
        if(empty($nowpwd)){
            self::res(array('code'=>9998,'msg'=>'当前密码不能为空'));
        }
        if(empty($newpwd) ||empty($confirmpwd)){
            self::res(array('code'=>9998,'msg'=>'新密码和确认密码不能为空'));
        }
        if($newpwd!==$confirmpwd) {
            self::res(array('code'=>99999,'msg'=>'两次输入的密码不一致'));
        }
        if (file_exists(PWDCONF)) {
            $password = file_get_contents(PWDCONF);
            if(trim($password) === md5($nowpwd)) {
                file_put_contents(PWDCONF, md5($newpwd));
                self::res(array('code'=>0,'data'=>''));
            } else {
               self::res(array('code'=>99999,'msg'=>'当前密码不正确'));
            }
        } else {
            $data = $this->config->config['console'];
            if($data['superpwd'] == $nowpwd) {
                file_put_contents(PWDCONF, md5($newpwd));
                self::res(array('code'=>0,'data'=>''));
            } else {
                self::res(array('code'=>99999,'msg'=>'当前密码不正确'));
            }
        }
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */