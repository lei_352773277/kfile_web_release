<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Email extends MY_Controller {
    public function __construct(){
        parent::__construct();
    }
    public function test(){
        $email = $this->input->post('user');
        $this->load->helper('email');
        if(valid_email($email)) {
            $mail_body = "这是一封测试邮件服务器是否正常的邮件";
            $this->load->library('mailer');

            $this->load->model('platform_model');
            $paltform = $this->platform_model->getKVInfo();
            $smtpAuth = $paltform['smtpauth'];
            $smtpSecure =  strtolower($paltform['smtpsecure']);
            $host = $paltform['smtphost'];
            $port = $paltform['smtpport'];
            $username = $paltform['smtpuser'];
            $smtppass = $paltform['smtppass'];
            $fromname = $paltform['fromname'];
            $mailfrom = $paltform['mailfrom'];

            $this->mailer->opts($smtpAuth,$smtpSecure,$host,$port,$mailfrom,$username,$smtppass,$fromname);



            $res = $this->mailer->sendmail(
                $email,
                '测试',
                '这是测试信 '.date('Y-m-d H:i:s'),
                $mail_body
            );
            self::res($res);
        } else {
            self::res(array('code'=>1,'msg'=>'错误的email地址'));
        }
    }

    public function send() {
        $to   = $this->input->post('to');
        $subject  = $this->input->post('subject');
        $body     = $this->input->post('body');
        $body = urldecode($body);
        if ( PRODUCTMODEL == 2) {
            $this->load->model('platform_model');
            $res = $this->platform_model->sendemail($to, $subject, $body);
            header("mail:".$res);
            return $res;
        } else {
            $this->load->library('mailer');
            $this->load->model('platform_model');
            $paltform = $this->platform_model->getKVInfo();
            $smtpAuth = $paltform['smtpauth'];
            $smtpSecure = strtolower($paltform['smtpsecure']);
            $host = $paltform['smtphost'];
            $port = $paltform['smtpport'];
            $username = $paltform['smtpuser'];
            $password = $paltform['smtppass'];
            $fromname = $paltform['fromname'];
            $mailfrom = $paltform['mailfrom'];

            $this->mailer->opts($smtpAuth,$smtpSecure,$host,$port,$mailfrom,$username,$password,$fromname);
            return $this->mailer->sendmail(
                $to,
                $subject,
                $body
            );
        }
    }

}
/* End of file epaper.php */