<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 客户端管理
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Setting extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }

    public function index()
    {
    	$option = $this->input->get('option');
    	$option = empty($option)?"platform":$option;

    	$editplatform = $this->input->get('editplatform');
    	$result = $this->input->get('result');
        $data = $this->config->config['netdisk.resources'];
        $data['editplatform'] = $editplatform;
        $editemail = $this->input->get('editemail');
        $data['editemail'] = $editemail;
        $dicts = $this->platform_model->getKVInfo();
        $data['keys'] = $dicts;
        $data['result'] = $result;
        $data['option'] = $option;
        $data['indextpl'] = $this->loadIndexTpl();

        $this->parser->parse("setting/index.tpl",$data);
    }

    function getKVInfo()
    {
        $dicts = $this->platform_model->getKVInfo();
        echo json_encode(array('code'=>0,'data'=>$dicts));
    }

    //保存平台信息
    public function saveplatform()
    {
    	$sitename = $this->input->post('sitename');
    	$helpurl = $this->input->post('helpurl');
    	$windows_logo = $this->input->post('windows_logo');
    	$mobile_logo = $this->input->post('mobile_logo');
    	$mac_logo = $this->input->post('mac_logo');
    	$data = array(
    		'sitename'=>$sitename,
    		'helpurl'=>$helpurl,
    		'windows_logo'=>$windows_logo,
    		'mobile_logo'=>$mobile_logo,
    		'mac_logo'=>$mac_logo
    	);
    	$this->platform_model->setInfo($data);
		header("location:index?option=platform&result=saveplatformok");
    }

    //保存email信息
    public function saveemail()
    {
    	$forgettitle = $this->input->post('forgettitle');
    	$mailer = $this->input->post('mailer');
    	$mailfrom = $this->input->post('mailfrom');
    	$fromname = $this->input->post('fromname');
    	$sendmail = $this->input->post('sendmail');
    	$smtpauth = $this->input->post('smtpauth');
    	$smtpsecure = $this->input->post('smtpsecure');
    	$smtpport = $this->input->post('smtpport');
    	$smtpuser = $this->input->post('smtpuser');
    	$smtppass = $this->input->post('smtppass');
    	$smtphost = $this->input->post('smtphost');
    	$data = array(
    		'forgettitle'=>$forgettitle,
    		'mailfrom'=>$mailfrom,
    		'fromname'=>$fromname,
    		'sendmail'=>$sendmail,
    		'smtpauth'=>$smtpauth,
    		'smtpsecure'=>$smtpsecure,
    		'smtpport'=>$smtpport,
    		'smtpuser'=>$smtpuser,
    		'smtppass'=>$smtppass,
    		'smtphost'=>$smtphost
    	);
    	$this->platform_model->setInfo($data);
        $this->platform_model->setGlobalSetting();
    	header("location:index?option=email&result=saveemailok");
    }

    //保存在线转换
    public function saveconv()
    {
        $pictype = $this->input->post('pictype');
        $doctype = $this->input->post('doctype');
        $videotype = $this->input->post('videotype');
        $videofilesize = $this->input->post('videofilesize');

        $dataArr = array(
            'convparams'=>array(
                'pictype'=>$pictype,
                'doctype'=>$doctype,
                'videotype'=>$videotype,
                'videofilesize'=>$videofilesize
            )
        );
        $data = json_encode($dataArr);
        $this->platform_model->setInfo($data);
        header("location:index?option=conv&result=saveconvok");
    }


    //获取模板
    private function getTplPath()
    {
        if (isset($_SERVER['KFILE_TARGET_ENV']) && "public" == $_SERVER['KFILE_TARGET_ENV']) {
            return "../../netdisk/app/views/page/index_2.tpl";
        } else {
            return "../../netdisk/app/views/page/index_3.tpl";
        }
    }

    //重置代码
    public function recover()
    {
        $tpl = $this->getTplPath();
        if (file_exists($tpl.".bak")) {
            $default_html = file_get_contents($tpl.".bak");
            file_put_contents($tpl, $default_html);
        }
        header("location:index?option=html&result=reok");
    }

    //保存代码
    public function savecode()
    {
        $htmlcode = $this->input->post('htmlcode');
        if (!empty($htmlcode)) {
            $tpl = $this->getTplPath();
            if(file_exists($tpl.".bak")) {
                file_put_contents($tpl, $htmlcode);
            } else {
                $defalut = file_get_contents($tpl);
                file_put_contents($tpl.".bak", $defalut);
                file_put_contents($tpl, $htmlcode);
            }
        }
        header("location:index?option=html&result=reok");
    }

    //获取模板
    function loadIndexTpl()
    {
        $tpl = $this->getTplPath();
        $res = file_get_contents($tpl);
        return $res;
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */