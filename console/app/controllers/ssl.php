<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 客户端管理
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Ssl extends MY_Controller {
    private $pemfile = "/share/CA/cacert.pem";
    private $keyfile = "/share/CA/server.key";
    private $crtfile = "/share/CA/server.crt";


    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model("systeminfo_model");
    }


    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $editHttps = $this->input->get('editHttps');
        $result = $this->input->get('result');
        $dicts = $this->platform_model->getKVInfo();


        $option = $this->input->get('option');
        $option = empty($option)?"https":$option;
        $data['option'] = $option;

        $data['editHttps']=$editHttps;
        $data['result'] = $result;
        $data['keys'] = $dicts;
        $this->parser->parse("ssl/index.tpl",$data);

    }

    //保存平台信息
    public function savehttps()
    {
        $https = $this->input->post('https');
        $serverkey = $this->input->post('serverkey');
        $clientkey = $this->input->post('clientkey');
        $servercrt = $this->input->post('servercrt');
        $api_common_https_port = $this->input->post('api_common_https_port');
        $web_crt_help=$this->input->post('web_crt_help');
        if ("1" == $https) {
            if(empty($serverkey)||empty($servercrt)) {
                self::res(array('code'=>99998,'msg'=>'证书信息为空，请先生成证书'));
            }
        }

        $data = array(
            'https'=>$https,
            'serverkey'=>$serverkey,
            'clientkey'=>$clientkey,
            'servercrt'=>$servercrt,
            'api_common_https_port'=>$api_common_https_port,
            'web_crt_help'=>$web_crt_help
        );
        $this->platform_model->setInfo($data);
        $this->synchttps();
    }

     /*应用证书*/
    function applyshellkey() {
        $pemfile = $this->pemfile;
        $keyfile = $this->keyfile;
        $crtfile = $this->crtfile;
        if(!file_exists($pemfile) || !file_exists($keyfile) || !file_exists($crtfile)) {
            self::res(array('code'=>99999,'msg'=>'生成证书失败，请先生成证书信息'));
        }
        $pem = file_get_contents($pemfile);
        $key = file_get_contents($keyfile);
        $crt = file_get_contents($crtfile);
        if(empty($pem)||empty($key)||empty($crt)) {
            self::res(array('code'=>99998,'msg'=>'生成证书失败，请先生成证书信息'));
        } else {
            //$dicts = $this->platform_model->getKVInfo();
            //$ip = $dicts['ip'];
            //$wanip = $dicts['api_common_http_host'];
            //$port = $dicts['api_common_http_port'];
            //$access_ip = trim($wanip)==""?$ip:$wanip;

            $data = array(
                'https'=>'1',
                'serverkey'=>$key,
                'clientkey'=>$pem,
                'servercrt'=>$crt,
                'servercrturl'=>'http://console.com/platform/shellgetkey?k=servercrt',
                'serverkeyurl'=>'http://console.com/platform/shellgetkey?k=serverkey',
                'clientkeyurl'=>'http://console.com/platform/getclientkey'

                //'servercrturl'=>'http://'.$access_ip.':'.$port.'/platform/shellgetkey?k=servercrt',
                //'serverkeyurl'=>'http://'.$access_ip.':'.$port.'/platform/shellgetkey?k=serverkey',
                //'clientkeyurl'=>'http://'.$access_ip.':'.$port.'/platform/getclientkey'
            );
            $ret = $this->platform_model->setInfo($data);
            self::res(array('code'=>API_RET_SUCC,'data'=>$ret));
        }
    }

    /*创建证书*/
    function createshellkey() {
        $ip = $this->platform_model->getKeyVal('ip');
        $wanip = $this->platform_model->getKeyVal('api_common_http_host');
        $access_ip = trim($wanip)==""?$ip:$wanip;

        //忽略掉从数据库读取配置信息
        if (isset($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
            if (1==intval($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
                $access_ip = $_SERVER['KFILE_WEB_INTERNET_DOMAIN'];
            }
        }

        exec("/usr/local/bin/kfile_createssl.sh  ".$access_ip, $res);
        self::res(array('code'=>API_RET_SUCC,'data'=>$res));
    }

    //获取证书状态
    function getCrtStatus() {
        $pemfile = $this->pemfile;
        $keyfile = $this->keyfile;
        $crtfile = $this->crtfile;
        if (!file_exists($pemfile) || !file_exists($keyfile) || !file_exists($crtfile)) {
            self::res(array('code'=>99999,'msg'=>'生成证书失败，请先生成证书信息'));
        }
        $pem = file_get_contents($pemfile);
        $key = file_get_contents($keyfile);
        $crt = file_get_contents($crtfile);
        if (empty($pem)||empty($key)||empty($crt)) {
            self::res(array('code'=>99998,'msg'=>'生成证书失败，请先生成证书信息'));
        }
        self::res(array('code'=>0,'data'=>''));
    }

    /*开启https*/
    function synchttps() {

        $dicts = $this->platform_model->getKVInfo();
        $serverkey = $dicts['serverkey'];
        $servercrt = $dicts['servercrt'];
        //$clientkey = $dicts['clientkey'];
        $https = $dicts['https'];
        $sslport = $dicts['api_common_https_port'];

        //忽略掉从数据库读取配置信息
        if (isset($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
            if (1==intval($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
                if(isset($_SERVER['KFILE_API_COMMON_INTERNET_HTTPS_PORT'])) {
                    $sslport = $_SERVER['KFILE_API_COMMON_INTERNET_HTTPS_PORT'];
                }
            }
        }

        if ($https == "1") {
            if(empty($serverkey)||empty($servercrt)) {
                self::res(array('code'=>99998,'msg'=>'证书信息为空，请先生成证书'));
            }

            $data = array("httpsport"=>$sslport);
            $this->systeminfo_model->writeNetWorkConfig($data);
        }

        exec("/usr/local/bin/kfile_synchttpskey.sh", $res);
    }

    /*检测端口是否可用*/
    function checkport()
    {
        $port = $this->input->get('port');
        if ($port != $_SERVER['KFILE_HTTP_PORT']) {//不是web端口
            if ($port == $_SERVER['KFILE_HTTPS_PORT']) { //用的现在的https端口
                self::res(array('code'=>API_RET_SUCC,'data'=>''));
            } else {
                if (checkPortIsVaild("127.0.0.1", $port)) {
                    self::res(array('code'=>API_RET_SUCC,'data'=>''));
                } else {
                    self::res(array('code'=>9999,'msg'=>'端口被占用')); 
                }
            }
        } else {
            self::res(array('code'=>9999,'msg'=>'端口被占用')); 
        }
    }

    //检测https是否成功
    function checkHttpsIsRight()
    {
        $httpsPort = $this->platform_model->getKeyVal('api_common_https_port');
        $https = $this->platform_model->getKeyVal('https');
        if ($httpsPort == $_SERVER['KFILE_HTTPS_PORT'] && "1"== $https) { 
            self::res(array('code'=>API_RET_SUCC,'data'=>''));
        } else {
            self::res(array('code'=>9999,'msg'=>'https为启用')); 
        }
    }


}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */