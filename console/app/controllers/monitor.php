<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monitor extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('monitor_model');
    }

    public function index()
    {
        $option = $this->input->get('option');
        $option = empty($option)?"system":$option;
        $editnetwork = $this->input->get('editnetwork');
        $data = $this->config->config['netdisk.resources'];
        $monitor = $this->monitor_model->proxy('');
        $data['monitor'] = $monitor;
        $data['option'] = $option;
        if("contact" == $option) {
            $data['contact'] = $this->contactlist();
        }
        $this->parser->parse("monitor/index.tpl",$data);
    }


    /*代理获取监控信息*/
    function proxy()
    {
        $v = $this->input->get('v');
        $data = $this->monitor_model->proxy($v);
        echo json_encode($data);
    }

    /*获取报警的联系人*/
    private function contactlist() {
        $res = $this->monitor_model->getalertlist();
        return $res;
    }

    /*添加接收报警的联系人*/
    function addalertuser() {
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $this->load->helper('email');
        if(!valid_email($email)) {
            self::res(array('code'=>9999,'msg'=>'错误的email地址'));
        }
        
        if(empty($name)) {
            self::res(array('code'=>9999,'msg'=>'姓名不能为空'));
        }

        $this->monitor_model->addAlertUser($email,$name);
        self::res(array('code'=>API_RET_SUCC,'data'=>''));
    }

    /*删除接收报警的联系人*/
    function delalertuser() {
        $email = $this->input->post('email');
        $this->monitor_model->delAlertUser($email);
        self::res(array('code'=>API_RET_SUCC,'data'=>''));
    }


}