<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backup extends MY_Controller {
    private $zrmDir = "../conf/mysql_zrm_config";
    private $configFile = "../conf/mysql_zrm_config/mysql_zrm_config";
    private $sshPubKeyFile = "../conf/mysql_zrm_config/id_rsa.pub";
    private $sshReslutFile = "../conf/mysql_zrm_config/check_ssh_result";
    private $backupReport = "../conf/mysql_zrm_config/backup_result.html";

    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('monitor_model');
    }

    //获取key
    private function getIdRsaPub()
    {
        return file_get_contents($this->sshPubKeyFile);
    }

    //写配置文件
    private function writeBackupConfig($arr)
    {
        $data = $arr['backuptime']."|".$arr['emailnotice']."|".$arr['backupserverip']."|";
        $data .= $arr['backuppath']."|".$arr['backupstatus']."|".$arr['checkssh']."|".$arr['backupnow'];
        file_put_contents($this->configFile, $data);
    }

    //解析备份报告
    private function parseBackUpInfo()
    {
    	$str = file_get_contents($this->backupReport);
        $itemList = array();
        if(!empty($str)) {
        	$strList = explode('<tr class="r_normal">', $str);
        	array_shift($strList);
        	foreach($strList as $k=>$v) {
        		$rowList = explode("</td>", $v);
        		$item = array();
        		foreach ($rowList as $kk=>$vv) {
        			$c_backup_set = '<td align="right" class="c_backup_set">';
        			$c_backup_directory = '<td align="left" class="c_backup_directory">';
        			$c_backup_level = '<td align="center" class="c_backup_level">';
        			$c_backup_size = '<td align="right" class="c_backup_size">';
        			$c_backup_time = '<td align="left" class="c_backup_time">';
        			$c_backup_status = '<td align="left" class="c_backup_status">';
        			$c_comment = '<td align="left" class="c_comment">';
        			if (strpos($vv, $c_backup_set)!==false) {
        				$item['set'] = trim(str_replace($c_backup_set, '', $vv));
        			} else if (strpos($vv, $c_backup_directory)!==false) {
        				$directory = trim(str_replace($c_backup_directory, '', $vv));
        				$splitlist =  explode("/", $directory);
        				$item['directory'] = array_pop($splitlist);
        			} else if (strpos($vv, $c_backup_level)!==false) {
        				$item['level'] = trim(str_replace($c_backup_level, '', $vv));
        			} else if (strpos($vv, $c_backup_size)!==false) {
        				$item['size'] = trim(str_replace($c_backup_size, '', $vv));
        			} else if (strpos($vv, $c_backup_time)!==false) {
        				$item['time'] = trim(str_replace($c_backup_time, '', $vv));
        			} else if (strpos($vv, $c_backup_status)!==false) {
        				$item['status'] = trim(str_replace($c_backup_status, '', $vv));
        			} else if (strpos($vv, $c_comment)!==false) {
        				$item['comment'] = trim(str_replace($c_comment, '', $vv));
        			}
        		}
        		array_push($itemList, $item);
        	}
        }
    	return $itemList;
    }

    //获取ssh结果
    private function getsshresult()
    {
        //ip:result,
        $res = file_get_contents($this->sshReslutFile);
        $result = array('succ'=>array(),'fail'=>array());
        if(!empty($res)) {
            $ipList = explode(",", $res);
            foreach ($ipList as $k=>$v) {
                $ipVal = explode(":", $v);
                if(!empty($ipVal)&&count($ipVal)==2) {
                    if("1" == $ipVal[1]) {
                        array_push($result['succ'],$ipVal[0]);
                    } else {
                        array_push($result['fail'],$ipVal[0]);
                    }
                }
            }
            return $result;
        } else {
            return $result;
        }
    }

    //读备份配置文件
    private function readBackupConfig() {
        $res = file_get_contents($this->configFile);
        $resList = explode("|", $res);
        $arr = array(
            'backuptime'=>empty($resList[0])?"":$resList[0],
            'emailnotice'=>empty($resList[1])?"0":$resList[1],
            'backupserverip'=>empty($resList[2])?"":$resList[2],
            'backuppath'=>empty($resList[3])?"":$resList[3],
            'backupstatus'=>empty($resList[4])?"0":$resList[4],
            'checkssh'=>empty($resList[5])?"-2":$resList[5],
            'backupnow'=>empty($resList[6])?"-2":$resList[6]
        );
        return $arr;
    }

    //备份首页
    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $option = $this->input->get('option');
        $option = empty($option)?"backupinfo":$option;
        $data['option'] = $option;
        if (!is_dir($this->zrmDir)) {
            $data['zrmDirExits'] = false;
        } else {
            $data['zrmDirExits'] = true;
            $logList = $this->parseBackUpInfo();
            $data['loglist'] = $logList;
            $data['idrsa'] = $this->getIdRsaPub();
            $data['config'] = $this->readBackupConfig();
            $data['sshresult'] = $this->getsshresult();
        }

        $this->parser->parse("backup/index.tpl",$data);
    }

    //备份完成后通知
    public function noitfy()
    {
        $contactList = $this->monitor_model->getalertlist();
        if (!empty($contactList)) {
            $logList = $this->parseBackUpInfo();
            $backup = $logList[0];
            if ($backup) {
                foreach ($contactList as $item) {
                    $data = "to=".$item['email'].'&subject=金山企业云盘备份通知&body='.$backup['directory']."status:".$backup['status'];
                    $this->monitor_model->curl("http://console.com/email/send",$data);
                }
            }
        }
    }

    //立即检测一次
    public function checkssh()
    {
        $arr = $this->readBackupConfig();
        $arr['checkssh'] = '-1';
        $this->writeBackupConfig($arr);
    }

    //立即备份一次
    public function invokebackup()
    {
        $arr = $this->readBackupConfig();
        $arr['backupnow'] = '-1';
        $this->writeBackupConfig($arr);
        header("location:./index");
    }

    //开启备份
    public function startbackup()
    {
        $arr = $this->readBackupConfig();
        $arr['backupstatus'] = '1';
        file_put_contents($this->sshReslutFile,"");
        $this->writeBackupConfig($arr);
        self::res(array('code'=>0,'data'=>''));
    }

    //关闭备份
    public function stopbackup()
    {
        $arr = $this->readBackupConfig();
        $arr['backupstatus'] = '0';
        $this->writeBackupConfig($arr);
        self::res(array('code'=>0,'data'=>''));
    }

    //写备份配置文件
    public function writeDefaultConfig()
    {
        $backuptime = $this->input->post('backuptime');
        $emailnotice = $this->input->post('emailnotice');
        $backupserverip = $this->input->post('backupserverip');
        $backuppath = $this->input->post('backuppath');
        $backupstatus = "0";
        $checkssh = "-2";
        $backupnow = "-2";
        $arr = array(
            'backuptime'=>$backuptime,
            'emailnotice'=>$emailnotice,
            'backupserverip'=>$backupserverip,
            'backuppath'=>$backuppath,
            'backupstatus'=>$backupstatus,
            'checkssh'=>$checkssh,
            'backupnow'=>$backupnow
        );
        $this->writeBackupConfig($arr);
        self::res(array('code'=>0,'data'=>''));
    }
    
}