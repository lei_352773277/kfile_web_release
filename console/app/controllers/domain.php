<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Domain extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('domain_model');
    }

    /**
    * 列出所有的企业列表
    */
    function listall() {
        $res_data = $this->domain_model->get_domainlist(API_SUPER_TOKEN);
        self::res($res_data);
     }

    /**
    * 获取企业信息
    */
    function domaininfo()
    {
        $domain_id = $this->input->get('id');
        if (!empty($domain_id)) {
            $domain_id =  intval($domain_id);
            $ret_data = $this->domain_model->domain_info(API_SUPER_TOKEN, $domain_id);
        } 
        $domainIdent = $this->input->get('domainIdent');
        if (!empty($domainIdent)) {
            $ret_data = $this->domain_model->domain_info_by_domainIdent(API_SUPER_TOKEN, $domainIdent);
        }
        //$detail = $this->domain_model->domain_config_detail(API_SUPER_TOKEN, $ret_data['data']['domain_id']);
        //$ret_data['data']['detail'] = $detail['data'];
        $nums = $this->domain_model->get_member_count(API_SUPER_TOKEN, $ret_data['data']['domain_id']);

        $admininfo = $this->domain_model->get_super($ret_data['data']['domain_id']);
        $ret_data['data']['user_name'] = $admininfo['name'];
        $ret_data['data']['member_nums'] = $nums['data']['count'];
        self::res($ret_data);
    }

    //创建新企业
    function reg()
    {
        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $user = $this->input->post('user');
        //$email = $this->input->post('email');
        $password = $this->input->post('password');
        $confirmPassword = $this->input->post('confirmPassword');
        $domainName = $this->input->post('domainName');
        $domainContact = $this->input->post('domainContact');
        $domainIdent = $this->input->post('domainIdent');



        if ($password !== $confirmPassword) {
            return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'两次密码输入不一致'));
        }
        if (strlen($password) <6 || strlen($password) > 32) {
            return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'密码格式和个数不正确'));
        }

        if (PRODUCTTYPE==1) {
            if (file_exists(DOMAINIDENT)) {
                $domainIdent = file_get_contents(DOMAINIDENT);
                $domainIdent = trim($domainIdent);
            }
        }

        $post_data = array(
            "userName"=>$user,
            "userPwd"=>$password,
            "confirmPwd"=>$confirmPassword,
            "domainName"=>$domainName,
            'domainIdent'=>$domainIdent,
            "domainContact"=>$domainContact
        );
        $res_data = $this->domain_model->signup($post_data);

        if(0 == $res_data['code']) {
            $this->init_default_folder($user, $password, $domainIdent);
        }

        self::res($res_data);
    }

    function init_default_folder($user, $pwd, $ident)
    {
        $host = "console.com"; 
        $path = "/domain/create_default_folder_and_file";
        $fp = fsockopen($host, 80, $errno, $errstr, 60);
        if (!$fp) {
            //echo "$errstr ($errno)<br />\n";
        } else {
            $poststring = '';
            $formdata = array ("user" => $user, 'pwd'=>$pwd, 'ident'=>$ident); 
            foreach($formdata AS $key => $val){ 
                $poststring .= urlencode($key) . "=" . urlencode($val) . "&"; 
            } 

            $poststring = substr($poststring, 0, -1); 
            $out =  "POST $path HTTP/1.1\r\n"; 
            $out .=  "Host: $host\r\n"; 
            $out .=  "Content-type: application/x-www-form-urlencoded\r\n"; 
            $out .=  "Content-length: ".strlen($poststring)."\r\n"; 
            $out .=  "Connection: close\r\n\r\n"; 
            $out .=  $poststring . "\r\n\r\n"; 
            fwrite($fp, $out);
            usleep(1000);
            fclose($fp);
        }
    }

    function create_default_folder_and_file() {
        $user = $this->input->post('user');
        $pwd = $this->input->post('pwd');
        $ident = $this->input->post('ident');
        $this->domain_model->create_default_folder_and_file($user, $pwd, $ident);
    }

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */