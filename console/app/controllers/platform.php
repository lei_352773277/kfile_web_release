<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Platform extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('domain_model');
        $this->load->model('platform_model');
        $this->load->model('monitor_model');

    }

    function action()
    {
        $action = $this->input->post('action');
        $name = $this->input->post('name');
        if(empty($action)) {
            self::res(array('code'=>9999,'data'=>'','msg'=>'no action'));
        }
        $this->platform_model->proxyaction('/'.$name,"action=".$action);  
    }

    /*供监控使用*/
    function shellcontactlist() {
        $res = $this->monitor_model->getalertlist();
        $list = array();
        foreach ($res as $k=>$v) {
            echo 'set alert '.$v['email']." but not on { instance } \r\n";
        }
    }

    /*供监控取单个配置*/
    function shellgetkey() {
        $k = $this->input->get('k');
        if (strpos($_SERVER['HTTP_USER_AGENT'], "curl")!==false) {
            $value = $this->platform_model->getKeyVal($k);
            echo $value;
        } else {
            if (in_array($k, array("clientkeyurl","clientkey"))) {
                $value = $this->platform_model->getKeyVal($k);
                echo $value;
            }
        }
    }

    //获取指定的键值
    function getk()
    {
        $k = $this->input->get('k');
        if (strpos($_SERVER['HTTP_USER_AGENT'], "curl")!==false) {
            $value = $this->platform_model->getKeyVal($k);
            echo $value;
        } else {
            if (in_array($k, array("clientkeyurl","clientkey"))) {
                $value = $this->platform_model->getKeyVal($k);
                echo $value;
            }
        }
    }


    /*获取客户端https的key*/
    function getclientkey() {
        $value = $this->platform_model->getKeyVal('clientkey');
        echo $value;
    }


    /*取配置文*/
    function config()
    {
        $ret_data =  $this->platform_model->getKVInfo();
        $data = array();
        foreach($ret_data as $k=>$v) {
            if(in_array($k,array('offline',
                'offline_message',
                'sitename',
                'windows_logo',
                'mobile_logo',
                'web_server',
                'api_common_http_host',
                'api_common_http_port',
                'api_common_https_port',
                'api_server',
                'api_server_upload',
                'media_server',
                'windows_updateurl',
                'android_updateurl',
                'iphone_updateurl',
                'windowssync_updateurl',
                'mac_updateurl',
                'office_updateurl',
                'https',
                'ip',
                'clientkeyurl',
                'adconfig',
                'upbytes',
                'downbytes',
                //'serverkeyurl',
                //'sendemailurl',
                //'verifyemailurl',
                'forgettitle'))) {
                $data[$k] = $v;
            }
        }

        //忽略掉从数据库读取配置信息
        if (isset($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
            if (1==intval($_SERVER['KFILE_CONFIG_IP_FROM_ENV'])) {
                $data['ip'] = $_SERVER['KFILE_WEB_INTERNET_DOMAIN'];
                $data['api_common_http_host'] = $_SERVER['KFILE_WEB_INTERNET_DOMAIN'];
                $data['api_common_http_port'] = $_SERVER['KFILE_API_COMMON_INTERNET_HTTP_PORT'];
                $data['api_common_https_port'] = $_SERVER['KFILE_API_COMMON_INTERNET_HTTPS_PORT'];
            }
        }

        $ip = trim($data['api_common_http_host'])==""?$data['ip']:trim($data['api_common_http_host']);
        $port = $data['api_common_http_port'];
        $sslport = $data['api_common_https_port'];

        $data['clientkeyurl'] = "http://".$ip.":".$port."/platform/getclientkey";
        $data['sendemailurl'] = "http://".$ip.":".$port."/email/send";
        $data['verifyemailurl'] = "http://".$ip.":".$port."/account/verify";

        if (!SERVERURLCONFIG) {
            if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
                $ip = $_SERVER['HTTP_X_ORIGIN_HOST'];
                $data['api_common_http_host'] = $ip;
            }
            if ("1" == $data['https']) {
                $data['api_server'] = '//'.$ip.":".$sslport."/api";
                $data['web_server'] = '//'.$ip.":".$sslport;
                $data['media_server'] = '//'.$ip.":".$sslport."/olc";
            } else {
                $data['api_server'] = '//'.$ip.":".$port."/api";
                $data['web_server'] = '//'.$ip.":".$port;
                $data['media_server'] = '//'.$ip.":".$port."/olc";
            }
        }
        $data['product_type'] = PRODUCTTYPE;
        if (PRODUCTTYPE==1) {
            if (file_exists(DOMAINIDENT)) {
                $domainIdent = file_get_contents(DOMAINIDENT);
                $data['default_domain_ident'] = trim($domainIdent);
            } else {
                $data['default_domain_ident'] = DEFAULT_DOMAIN_NAME; 
            }
        }

        if(isset($data['adconfig'])) {
            $data['adconfig'] = json_decode($data['adconfig'],true);
        }

        echo json_encode(array('code'=>API_RET_SUCC,'data'=>$data));
    }

    function getserverinfo()
    {
        return "CPU_INFO";
    }

    /**
    *获取所有的设置
    */
    function getsetting()
    {
        $ret_data = $this->platform_model->info();
        $info = $this->getserverinfo();
        array_push($ret_data,array('keyname'=>'serverinfo','value'=>$info));
        self::res(array('code'=>0,'data'=>$ret_data));
    }

    /**
    * 更新设置
    */
    function setting()
    {
        $data = $this->input->post('data');
        $ret_data =$this->platform_model->setInfo($data);
        self::res(array('code'=>$ret_data>0?0:$ret_data));
    }

    function getspace()
    {
        if(STORGETYPE=="1") {
           $ret_data = $this->platform_model->get_value('spacedata');
           $space_str = $ret_data[0]['value'];
           $space_list = explode("|", $space_str);
           $data = array('code'=>API_RET_SUCC,'data'=>array('total'=>$space_list[0]/1024/1024/1024,'used'=>$space_list[1]/1024/1024/1024));
        } else if (STORGETYPE == "2") {
            $data = $this->platform_model->getStorgeSpace();
        }
        self::res($data);
    }


    /*windowssync*/
    function winsync()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('domainId');
        
        $result = $this->platform_model->isUpdate($username, $clientVersion, 'winsync');
        if(!empty($result)) {
            $msgArr = $this->platform_model->get_value('windowssync_update_msg');
            $msg =  $msgArr[0]['value'];
            if(empty($msg)) {
                $msg = "请更新";
            }
            echo json_encode(array('url'=>$result['path'],
                'upversion'=>$result['targetversion'],
                'upgradeinfo'=>base64_encode($msg),
                'result'=>'ok'));
        } else {
            echo json_encode(array('result'=>'noUpdate'));
        }
    }

    /*windows更新*/
    function winplus()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('domainId');
		
		if(1933 == $domainId)
		{
			echo json_encode(array('result'=>'noUpdate'));
			exit;
		}
        
        $result = $this->platform_model->isUpdate($username, $clientVersion);
        if("2.0.0.3" == $clientVersion) {
            if(!empty($result)) {
                echo "Code=Ok&Path=".$result['path'];
            } else {
                echo "Code=NOUPDATE";
            }
        } else {
            if(!empty($result)) {
                $msgArr = $this->platform_model->get_value('windows_update_msg');
                $msg =  $msgArr[0]['value'];
                if(empty($msg)) {
                    $msg = "请更新";
                }
                echo json_encode(array('url'=>$result['path'],
                    'upversion'=>$result['targetversion'],
                    'upgradeinfo'=>base64_encode($msg),
                    'result'=>'ok'));
            } else {
                echo json_encode(array('result'=>'noUpdate'));
            }
        }
    }

    /*mac*/
    function mac()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('domainId');
        if(1933 == $domainId)
        {
            echo json_encode(array('result'=>'noUpdate'));
            exit;
        }
        $result = $this->platform_model->isUpdate($username, $clientVersion, 'mac');
        if(!empty($result)) {
            $msgArr = $this->platform_model->get_value('mac_update_msg');
            $msg =  $msgArr[0]['value'];
            if(empty($msg)) {
                $msg = "请更新";
            }
            echo json_encode(array('url'=>$result['path'],
                'upversion'=>$result['targetversion'],
                'upgradeinfo'=>base64_encode($msg),
                'result'=>'ok'));
        } else {
            echo json_encode(array('result'=>'noUpdate'));
        }
    }

    function office()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('domainId');
        
        $result = $this->platform_model->isUpdate($username, $clientVersion,'office');
        if(!empty($result)) {
            $msgArr = $this->platform_model->get_value('office_update_msg');
            $msg =  $msgArr[0]['value'];
            if(empty($msg)) {
                $msg = "请更新";
            }
            echo json_encode(array('url'=>$result['path'],
                'upversion'=>$result['targetversion'],
                'upgradeinfo'=>base64_encode($msg),
                'result'=>'ok'));
        } else {
            echo json_encode(array('result'=>'noUpdate'));
        }
    }

    /*android更新*/
    function android()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('DomainId');
        
        $result = $this->platform_model->isUpdate($username, $clientVersion,'android');
        if(!empty($result)) {
            $msgArr = $this->platform_model->get_value('android_update_msg');
            $msg =  $msgArr[0]['value'];
            if(empty($msg)) {
                $msg = "请更新";
            }
            echo json_encode(array('url'=>$result['path'],
                'upversion'=>$result['targetversion'],
                'upgradeinfo'=>base64_encode($msg),
                'result'=>'ok'));
        } else {
            echo json_encode(array('result'=>'noUpdate'));
        }
    }

    /*iphone更新*/
    function iphone()
    {
        $clientVersion  = $this->input->get('Version');
        $username = $this->input->get('UserName');
        $domainId = $this->input->get('DomainId');
        
        $result = $this->platform_model->isUpdate($username, $clientVersion,'iphone');
        if(!empty($result)) {
            $msgArr = $this->platform_model->get_value('iphone_update_msg');
            $msg =  $msgArr[0]['value'];
            if(empty($msg)) {
                $msg = "请更新";
            }
            echo json_encode(array(
                'data'=>array(
                'url'=>$result['path'],
                'upversion'=>$result['targetversion'],
                'upgradeinfo'=>base64_encode($msg)
                ),
                'code'=>0));
        } else {
            echo json_encode(array('code'=>0,'data'=>array('url'=>"")));
        }
    }

    /*设置API的配置*/
    function  config_global_setting()
    {
        $res_data =  $this->_config_global_setting();
        self::res($res_data);
    }

    /*设置全局变量*/
    private function _config_global_setting()
    {
        $forgettitle = $this->platform_model->get_value('forgettitle');
        $forgettitle_value =  $forgettitle[0]['value'];

        $ip_arr = $this->platform_model->get_value('ip');
        $ip = $ip_arr[0]['value'];
        $wanip_arr = $this->platform_model->get_value('api_common_http_host');
        $wanip = $wanip_arr[0]['value'];
        $access_ip = trim($wanip)==""?$ip:$wanip;
        $port = $this->platform_model->get_value('api_common_http_port');
        $port = $port[0]['value'];

        $setting = array(
            'email_send_url'=>"http://".$access_ip.":".$port."/email/send",
            //'forget_password_email_url'=>"http://".$access_ip.":".$port."/account/verify",
            'forget_password_email_subject'=>$forgettitle_value,
            'email_base_url'=>"http://".$access_ip.":".$port,
            'dual_factor_device_bind_email_subject'=>"金山企业云盘设备绑定",
            'dual_factor_ban_email_subject'=>"关闭双重认证"
            );
        $res_data = $this->platform_model->config_global_setting(API_SUPER_TOKEN, $setting);
        return $res_data;
    }

    /*更新存储空间*/
    function updatespace()
    {
        $spaceData = $this->input->post('spacedata');
        if(!empty($spaceData)) {
            $space_list = explode("|", $spaceData);
            if(count($space_list) == 2) {
                $this->platform_model->setInfo(array('spacedata'=>implode("|", $space_list))); 
                self::res(array('code'=>API_RET_SUCC,'data'=>''));
            } 
        } else {
           self::res(array('code'=>9999,'data'=>'','msg'=>'没有需要更新的值')); 
        }
    }

    /*更新值*/
    function updatekey()
    {   
        $key = $this->input->post('keyname');
        $value = $this->input->post('value');
        $data = array();
        $data[$key] = $value;
        $ret = $this->platform_model->setInfo($data);
        self::res($ret);
    }


    /*重置密码*/
    function clearinfo() {
        if (file_exists(PWDCONF)) {
            unlink(PWDCONF);
            die('ok');
        }
    }

    /*重置超管的密码*/
    function resetinfo() {
        $id=$this->input->get('id');
        $domainident = file_get_contents(DOMAINIDENT);
        if (trim($domainident) == $id) {
            $ret_data = $this->domain_model->domain_info_by_domainIdent(API_SUPER_TOKEN, $id);
            $domain_id = $ret_data['data']['domain_id'];
            $super = $this->domain_model->get_super($domain_id);
            $this->domain_model->reset_super_pwd($super['user_id']);
            die('ok');
        } else {
            die('err');
        }
    }

    /*重置数据*/
    function resetdata() {
        $id=$this->input->get('id');
        $domainident = file_get_contents(DOMAINIDENT);
        if (trim($domainident) == $id) {
            if(file_exists(RESETDATA)) {
                file_put_contents(RESETDATA, "init_db");
            }
            die('ok');
        } else {
            die('err');
        }  
    }

    private function _getnewversion() {
        $domainIdent = file_get_contents(DOMAINIDENT);
        $api = $_SERVER['API_VERSION'];
        $web = $_SERVER['WEB_VERSION'];
        $versionList = array($api,$web);
        $res = $this->domain_model->curl("http://e.ksyun.com/product/getnewversion?sn=".trim($domainIdent)."&api=".$api."&web=".$web);
        return $res;
    }

    /*获取新版本*/
    function getnewversion() {
        $res = $this->_getnewversion();
        if (!empty($res)) {
            $arr = json_decode($res,true);
            self::res($arr);
        } else {
            self::res(array('code'=>1,'msg'=>'您已经是最新版本，无需更新'));
        }
    }   

    /*当前版本*/
    function getVersion() {
        $api = $_SERVER['API_VERSION'];
        $web = $_SERVER['WEB_VERSION'];
        $versionList = array($api,$web);
        self::res(array('code'=>API_RET_SUCC,'data'=>implode(",", $versionList)));
    }

    /*更新软件*/
    function updatesoftware() {
        $json = $this->_getnewversion();
        $arr = json_decode($json,true);
        if (API_RET_SUCC == $arr['code']) {
            $api = $arr['data']['api'];
            $web = $arr['data']['web'];
            $sign = md5($api.$web."kingfileisking");
            if($sign == $sign) {
               $kfile_env = $_SERVER['KFILE_ENV'];
               $kfile_target_env = $_SERVER['KFILE_TARGET_ENV'];
               $oldapi = $_SERVER['API_VERSION'];
               $oldweb = $_SERVER['WEB_VERSION'];
               $existsStr = file_get_contents(UPGRADE);
               $updateStr = $kfile_env."|".$kfile_target_env."|".$api."|".$web."|".$oldapi."|".$oldweb;
               if ($updateStr != trim($existsStr)) {
                    file_put_contents(UPGRADE,$updateStr);
               }
               self::res(array('code'=>API_RET_SUCC,'data'=>''));
            } else {
                self::res(array('code'=>9999,'msg'=>'升级失败，错误的签名'));
            }
        } else {
            self::res(array('code'=>9999,'msg'=>'无须升级'));
        }
    }

    /*获取升级log*/
    function getsyslog() {
        $sort = $this->input->get('sort');
        if($sort != 'first') {
            //sleep(5);
        }
        exec("tail -1000 /logs/upgrade.log",$output);
        self::res(array('code'=>API_RET_SUCC,'data'=>$output));
    }

    /*获取系统log*/
    function getsyseventlog() {
        $sort = $this->input->get('sort');
        if($sort != 'first') {
            //sleep(5);
        }
        exec("tail -1000 /logs/upgrade.log",$output);
        self::res(array('code'=>API_RET_SUCC,'data'=>$output));
    }

}

/* End of file platform.php */
/* Location: ./application/controllers/platform.php */
