<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('account_model');
    }

    /**
    * 获取登陆的用户信息
    */
    function info() {
    	$ret_data = $this->account_model->info($this->token);
    	self::res($ret_data);
    }

    /**
    * 登陆
    */
    function login() {
    	$userName = $this->input->post('user');
        $userPwd = $this->input->post('password');
        $res_data = $this->account_model->login($userName, $userPwd);
        $data = $this->config->config['netdisk.resources'];
        if(OK == $res_data['code']) {
            $allowLoginIpArray = array(
                "114.255.44.132",
                "219.141.176.228",
                "219.141.176.227",
                "219.141.176.229",
                "219.141.176.235",
                "219.141.176.232",
                "61.143.47.183",
                "221.122.32.238",
                "101.241.76.131",
                "211.100.31.84"
            );

            $ip = $this->input->ip_address();
            if (!in_array($ip, $allowLoginIpArray)) {
                //self::res(array('code'=>12,'msg'=>"access no"));
            }
            
            $this->load->library('session');
            $this->session->set_userdata(array('userid'=>$res_data['data']['user_id']));

            $config = $this->config->item('netdisk.api');
            $this->load->helper('string');
            define("KINGFILE_USER_AGENT", $config['user_agent'] . ';' . random_string('alnum', 16));
            $this->write_token($res_data);
        }
        self::res($res_data);
    }
    /*
        * 写cookie
     * */
    private function write_token($res_data)
    {
        /*print_r($res_data);
        exit;*/

        if (API_RET_SUCC == $res_data['code']) {

            $device_id = array(
                'name' => 'device_id',
                'value' => KINGFILE_USER_AGENT,
                'expire' => '86500',
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($device_id);
        }
    }
    /**
    * 退出登陆
    */
    function logout() {
    	$this->load->helper('cookie');
        $data = $this->config->config['netdisk.resources'];
        //$res_data = $this->account_model->logout($this->token);
        //delete_cookie('netdisk_ssid',$data['cookie_domain']);
        delete_cookie('netdisk_ssid');
        delete_cookie('netdisk_device_id');
        header("location:/console");
    }

    /**
    * 修改密码
    */
    function changepwd()
    {
        
        $nowpwd = $this->input->post('nowpwd');
        $newpwd = $this->input->post('newpwd');
        $confirmpwd = $this->input->post('confirmpwd');
        if(empty($nowpwd)){
            self::res(array('code'=>9998,'msg'=>'当前密码不能为空'));
        }
        if(empty($newpwd) ||empty($confirmpwd)){
            self::res(array('code'=>9998,'msg'=>'新密码和确认密码不能为空'));
        }
        if($newpwd!==$confirmpwd) {
            self::res(array('code'=>99999,'msg'=>'两次输入的密码不一致'));
        }
        $account = $this->account_info;

        $ret_data = $this->account_model->changepwd($account['user_id'], $nowpwd, $newpwd);

        if ($ret_data['code']==API_RET_SUCC) {
            self::res(array("code"=>0,'data'=>''));
        } else {
            self::res(array("code"=>9999,'msg'=>$ret_data['msg']));
        }
    }


    /**
    * 验证账号的有效性
    */
    function vaildate()
    {
        $vtype = $this->input->post('vtype');
        $value = $this->input->post('value');
        if($vtype==2 && $value == "") {
            self::res(array('code'=>0,'data'=>array()));
        }
        $ret_data = $this->account_model->account_validate($this->token, $vtype, $value);
        self::res($ret_data);
    }
    

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */