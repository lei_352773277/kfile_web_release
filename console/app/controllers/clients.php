<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 客户端管理
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Clients extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }


    public function index()
    {
        $editdownload = $this->input->get('editdownload');
        $editupdate = $this->input->get('editupdate');
        $option = $this->input->get('option');
        $option = empty($option)?"download":$option;
        $result = $this->input->get('result');
        $data = $this->config->config['netdisk.resources'];
        $dicts = $this->platform_model->getKVInfo();
        $data['keys'] = $dicts;
        if (isset($data['keys']['client_download_info'])) {
            $client_download_info = $data['keys']['client_download_info'];
        } else {
            @$this->platform_model->addKey("client_download_info", "");
            $client_download_info = '';
        }
        if ($client_download_info) {
            $infoArr = json_decode($client_download_info, true);
            $data['downinfo'] = $infoArr;
        }
        $data['editdownload'] = $editdownload;
        $data['editupdate'] = $editupdate;
        $data['option'] = $option;
        $data['result'] = $result;
        $this->parser->parse("clients/index.tpl",$data);
    }

    //获取客户端信息
    function getKVInfo()
    {
        $dicts = $this->platform_model->getKVInfo();
        foreach($dicts as $k=>$v) {
            //只显示需要的值
            if(strpos($k, "_")===False) {
                unset($dicts[$k]);
            }
        }
        echo json_encode(array('code'=>0,'data'=>$dicts));
    }

    public function savedownload()
    {
        $codepic=$this->input->post('codepic');//二维码
        $windows = $this->input->post('windows');

        $windowsSync = $this->input->post('win_sync');
        $mac = $this->input->post('mac');
        $android = $this->input->post('android');
        $iphone = $this->input->post('iphone');
        $ipad = $this->input->post('ipad');
        $office = $this->input->post('office');

        $downloadDict = array(
            'client_windows_size'=>$this->input->post('client_windows_size'),
            'client_windows_version'=>$this->input->post('client_windows_version'),
            'client_windows_msg'=>$this->input->post('client_windows_msg'),
            'client_win_sync_size'=>$this->input->post('client_win_sync_size'),
            'client_win_sync_version'=>$this->input->post('client_win_sync_version'),
            'client_win_sync_msg'=>$this->input->post('client_win_sync_msg'),
            'client_mac_size'=>$this->input->post('client_mac_size'),
            'client_mac_version'=>$this->input->post('client_mac_version'),
            'client_mac_msg'=>$this->input->post('client_mac_msg'),
            'client_android_size'=>$this->input->post('client_android_size'),
            'client_android_version'=>$this->input->post('client_android_version'),
            'client_android_msg'=>$this->input->post('client_android_msg'),
            'client_iphone_size'=>$this->input->post('client_iphone_size'),
            'client_iphone_version'=>$this->input->post('client_iphone_version'),
            'client_iphone_msg'=>$this->input->post('client_iphone_msg'),
            'client_office_size'=>$this->input->post('client_office_size'),
            'client_office_version'=>$this->input->post('client_office_version'),
            'client_office_msg'=>$this->input->post('client_office_msg')
        );

        $this->platform_model->setInfo(
            array(
                'client_windows_sync'=>$codepic,
                'client_windows'=>$windows,
                'client_win_sync'=>$windowsSync,
                'client_mac'=>$mac,
                'client_android'=>$android,
                'client_iphone'=>$iphone,
                'client_ipad'=>$ipad,
                'client_office'=>$office
            )
        );

        $this->platform_model->setInfo(
            array(
                'client_download_info'=>json_encode($downloadDict)
            )
        );
        
        header("location:index?option=download&result=savedownloadok");
    }

    //保存升级白名单
    public function savewhitelist()
    {
        $test_users = $this->input->post('test_users');
        $kingsoft_users = $this->input->post('kingsoft_users');
        $this->platform_model->setInfo(
            array(
                'test_users'=>$test_users,
                'kingsoft_users'=>$kingsoft_users)
        );
        header("location:index?option=whitelist&result=saveok");
    }


    //保存升级白名单
    public function savebytes()
    {
        $upbytes = $this->input->post('upbytes');
        $downbytes = $this->input->post('downbytes');

        $ret = $this->platform_model->get_value('upbytes');
        if(empty($ret)) {
            $this->platform_model->addKey('upbytes');
        }

        $ret = $this->platform_model->get_value('downbytes');
        if(empty($ret)) {
            $this->platform_model->addKey('downbytes');
        }

        $this->platform_model->setInfo(
            array(
                'upbytes'=>$upbytes,
                'downbytes'=>$downbytes)
        );
        header("location:index?option=bytes&result=saveok");
    }


    public function saveupdate()
    {
        $android_update_level = $this->input->post('android_update_level');
        $android_update_path = $this->input->post('android_update_path');
        $android_update_version = $this->input->post('android_update_version');
        $android_updateurl = $this->input->post('android_updateurl');
        $android_update_msg = $this->input->post('android_update_msg');

        $iphone_update_level = $this->input->post('iphone_update_level');
        $iphone_update_path = $this->input->post('iphone_update_path');
        $iphone_update_version = $this->input->post('iphone_update_version');
        $iphone_updateurl = $this->input->post('iphone_updateurl');
        $iphone_update_msg = $this->input->post('iphone_update_msg');

        $windows_update_level = $this->input->post('windows_update_level');
        $windows_update_path = $this->input->post('windows_update_path');
        $windows_update_version = $this->input->post('windows_update_version');
        $windows_updateurl = $this->input->post('windows_updateurl');
        $windows_update_msg = $this->input->post('windows_update_msg');

        $office_update_level = $this->input->post('office_update_level');
        $office_update_path = $this->input->post('office_update_path');
        $office_update_version = $this->input->post('office_update_version');
        $office_updateurl = $this->input->post('office_updateurl');
        $office_update_msg = $this->input->post('office_update_msg');

        $mac_update_level = $this->input->post('mac_update_level');
        $mac_update_path = $this->input->post('mac_update_path');
        $mac_update_version = $this->input->post('mac_update_version');
        $mac_updateurl = $this->input->post('mac_updateurl');
        $mac_update_msg = $this->input->post('mac_update_msg');

        $windowssync_update_level = $this->input->post('windowssync_update_level');
        $windowssync_update_path = $this->input->post('windowssync_update_path');
        $windowssync_update_version = $this->input->post('windowssync_update_version');
        $windowssync_updateurl = $this->input->post('windowssync_updateurl');
        $windowssync_update_msg = $this->input->post('windowssync_update_msg');


        $this->platform_model->setInfo(array(
            'android_update_level'=>$android_update_level,
            'android_update_path'=>$android_update_path,
            'android_update_version'=>$android_update_version,
            'android_updateurl'=>$android_updateurl,
            'android_update_msg'=>$android_update_msg,
            
            'iphone_update_level'=>$iphone_update_level,
            'iphone_update_path'=>$iphone_update_path,
            'iphone_update_version'=>$iphone_update_version,
            'iphone_updateurl'=>$iphone_updateurl,
            'iphone_update_msg'=>$iphone_update_msg,

            'windows_update_level'=>$windows_update_level,
            'windows_update_path'=>$windows_update_path,
            'windows_update_version'=>$windows_update_version,
            'windows_updateurl'=>$windows_updateurl,
            'windows_update_msg'=>$windows_update_msg,

            'mac_update_level'=>$mac_update_level,
            'mac_update_path'=>$mac_update_path,
            'mac_update_version'=>$mac_update_version,
            'mac_updateurl'=>$mac_updateurl,
            'mac_update_msg'=>$mac_update_msg,

            'windowssync_update_level'=>$windowssync_update_level,
            'windowssync_update_path'=>$windowssync_update_path,
            'windowssync_update_version'=>$windowssync_update_version,
            'windowssync_updateurl'=>$windowssync_updateurl,
            'windowssync_update_msg'=>$windowssync_update_msg,

            'office_update_level'=>$office_update_level,
            'office_update_path'=>$office_update_path,
            'office_update_version'=>$office_update_version,
            'office_updateurl'=>$office_updateurl,
            'office_update_msg'=>$office_update_msg

            ));
            
        header("location:index?option=update&result=saveupdateok");
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */