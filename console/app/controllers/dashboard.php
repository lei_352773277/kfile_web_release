<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 控制面板
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Dashboard extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('domain_model');
    }

    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $this->parser->parse("page/dashboard.tpl",$data);
    }

    //企业列表
    public function domainlist()
    {
        $data = $this->config->config['netdisk.resources'];
        $res_data = $this->domain_model->get_domainlist();
        $data['domainlist'] = $res_data['data'];
        //$data['domainlist']=array();
        if (1 == PRODUCTTYPE) {
            $this->parser->parse("page/domaininfo.tpl",$data);
        } else {
            $this->parser->parse("page/domainlist.tpl",$data);
        }
    }

    public function getdomainlist(){
        $res_data = $this->domain_model->get_domainlist();
        echo json_encode(array('code'=>0,'data'=>$res_data));
        /*echo json_encode(array(
            'code'=>0,
            'data'=>array('0'=>array(
                'ident'=>'ident',
                'contact'=>'contact',
                'name'=>'name'
            ))
        ));*/
    }

    //创建企业
    public function createdomain()
    {
        $data = $this->config->config['netdisk.resources'];
        $data['producttype'] = PRODUCTTYPE;
        $this->parser->parse("page/createdomain.tpl",$data);
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */