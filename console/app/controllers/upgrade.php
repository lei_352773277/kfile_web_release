<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upgrade extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('upgrade_model');
    }

	public function v3_2_4()
	{
		$this->upgrade_model->v3_2_4();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */