<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sysnotice extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('sysnotice_model');
    }

    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $page = $this->input->get('page');
        $hint = $this->input->get('hint');
        $page = intval($page);
        $page = 200;
        $hint = intval($hint);

        $notice = $this->sysnotice_model->all($page, $hint);
        $data['notice'] = $notice;
        $option = $this->input->get('option');
        if(empty($option)) {
            $option = 'all';
        }
        $data['option'] = $option;
        $data['edit'] = $this->input->get('edit');
        $data['result'] = $this->input->get('result');
        $this->parser->parse("sysnotice/index.tpl",$data);
    }

    public function create()
    {
        $title = $this->input->post('title', TRUE);
        $content = $this->input->post('content');
        if (empty($title) || empty($content)) {
            echo "<script>alert('请输入标题跟公告内容');history.go(-1);</script>";
            //header("location:index?option=create&result=saveerror");
            exit;
        }

        $tpl = file_get_contents(APPPATH."views/sysnotice/noticetmpl.tpl");
        $fmtcontent = str_replace("CONTENT",$content, $tpl);

        //$fmtcontent = base64_encode($fmtcontent);
        //$fmtcontent = htmlentities($fmtcontent);

        $res = $this->sysnotice_model->create($title, $fmtcontent);
        header("location:index?option=all");
    }

    public function del()
    {
        $id = $this->input->get('id');
        $sign = $this->input->get('sign');
        if (md5($id) != $sign) {
            echo "<script>alert('删除公告失败');history.go(-1);</script>";
            //header("location:index?option=create&result=saveerror");
            exit;
        }
        $res = $this->sysnotice_model->del($id);
        header("location:index?option=all");
    }


    public  function  show()
    {
        $id = $this->input->get('id');
        $sign = $this->input->get('sign');
        if (md5($id) != $sign) {
            echo "预览失败";
            exit;
        }
        $res = $this->sysnotice_model->get($id);
        $res = $res['data']['content'];
        //$res = html_entity_decode($res['data']['content']);
        echo $res;
        //exit;

        //$tpl = file_get_contents(APPPATH."views/sysnotice/noticetmpl.tpl");

        //$res = str_replace("CONTENT",  $res['data']['content'], $tpl);
        //echo $res;
    }





    
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */