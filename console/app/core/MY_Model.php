<?php
/**
 * Created by PhpStorm.
 * User: qyuan
 * Date: 15-3-19
 * Time: 下午2:33
 */
class MY_Model extends CI_Model
{
    var $db2 = "";
    var $platform_db = "";

    function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->config->load('netdisk');

    }
    function my_json_encode($data) {
        return json_encode($data);
    }

    function request_netdisk_server($url, $data = "", $version = '')
    {
        $config = $this->config->item('netdisk.api');
        $this->curl->create($config['server'] . $url);
        $device_id = $this->input->cookie('netdisk_device_id', TRUE);
        if ($data !== "") {
            $this->curl->post($data);
        }
        $version = $version === "" ? $config['default_version'] : $version;
        $this->curl->http_header("v", $version);
        $this->curl->http_header('User-Agent', $device_id);
        $this->curl->http_header('source-ip', $this->input->ip_address());
        $this->curl->http_header('Content-Type','application/json');
        $res = $this->curl->execute();
        if (!$data) {
            log_message('error', 'url=' . $config['server'] . $url . "; post=" . $data . "; res=" . $res);
        }
        log_message('debug', 'url=' . $config['server'] . $url . "; post=" . $data . ";header="."v=".$version.",device_id=".$device_id.",add=".$this->input->ip_address()."; res=" . $res);
        return $res;
    }

    function curl($url,$data="",$header=array(),$cookie=array()) {
        $this->curl->create($url);
        if(""!==$data) {
            $this->curl->post($data);
        }
        if(!empty($header)) {
            foreach ($header as $k => $v) {
                $this->curl->http_header($k,$v);
            }
        }
        if (!empty($cookie)) {
            $this->curl->set_cookies($cookie);
        }
        $res = $this->curl->execute();
        log_message('debug', 'url=' . $url . "; post=" . $data . ";  res=" . $res);
        return $res;
    }

    function curlInfo($url,$data="") {
        $this->curl->create($url);
        if(""!==$data) {
            $this->curl->post($data);
        }
        $res = $this->curl->execute();
        $info = $this->curl->info;
        log_message('debug', 'url=' . $url . "; info=" . $$info);
        return $info;
    }

    function request_netdisk_server_v2($url, $data = "", $version = '')
    {
        $config = $this->config->item('netdisk.api');
        $this->curl->create($config['server'] . $url);
        if ($data !== "" ) {
            if (is_array($data)) {
                $this->curl->create('http://kingfile.api.com' . $url);
                $this->curl->postfile($data);
            } else {
                $this->curl->post($data);
                $this->curl->http_header('Content-Type','application/json');
            }
        } else {
            $this->curl->http_header('Content-Type','application/json');
        }
        $version = $version === "" ? $config['default_version'] : $version;
        $this->curl->http_header("v", $version);
        $this->curl->http_header('User-Agent', KINGFILE_USER_AGENT);
        $this->curl->http_header('source-ip', $this->input->ip_address());
        $res = $this->curl->execute();
        $this->curl->debug();
        if (!$data) {
            if (is_array($data)) {
                log_message('error', 'url=' . $url . "; res=" . $res);
            } else {
                log_message('error', 'url=' . $url . "; post=" . $data . "; res=" . $res);
            }   
        }
        log_message('debug', 'url=' .  $url . "; header="."v=".$version.",device_id=".KINGFILE_USER_AGENT.",add=".$this->input->ip_address()."; res=" . $res);
        return $res;
    }


}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
