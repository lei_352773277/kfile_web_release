<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User: qyuan
 * Date: 15-3-19
 * Time: 下午2:33
 */
class MY_Controller extends CI_Controller
{
    public $account_info = "";
    protected $token = "";

    function __construct()
    {
        parent::__construct();
        $this->form_validation->set_error_delimiters('', '');
        $this->load->helper('string');
        $this->request_id = random_string('alnum', 16);

        $token = $this->input->cookie('netdisk_ssid', TRUE);
        $this->token = $token;

        $className = $this->router->class;
        $methodName = $this->router->method;
        $sign = $className."_".$methodName;
        if (!in_array($sign, $this->config->config['anonymous_access_pages'])) {

            $this->load->library('session');
            $userId = $this->session->userdata('userid');
            if(!empty($userId)) {
                $this->load->model('account_model');
                $res_data = $this->account_model->getUserById($userId);
                $uri = uri_string();
                if(!empty($res_data)) {
                    $this->account_info = $res_data;
                    if(!checkPermission($res_data, $sign)) {
                        die("ban access");
                    }
                } else {
                     header("location:/");
                     exit;
                }

            } else {
                header("location:/");
                exit; 
            }

            /*
            if(empty($this->token)) {
                if ($this->input->is_ajax_request()) {
                    self::res(array('code'=>2001,'msg'=>'Login is required'));
                } else {
                    header("location:/");
                    exit;
                }
            }
            */
        }

        //$res_data = $this->user_model->info($token);

        /*
        $uri = uri_string();
        if(API_RET_SUCC == $res_data['code']) {
            $this->account_info = $res_data;
            $this->token = $token;

            $is_super = $res_data['data']['is_superuser'];
            if (1 != $is_super) {
                if (in_array($uri, array("files", "audit", "department"))) {
                    header("location:/home");
                    exit;
                }
            } else {
                if (in_array($uri, array("home"))) {
                    header("location:/files");
                    exit;
                }
            }
        }
        */
    }

    /**
     * 输出给前端的格式
     * @param $result
     * @param string $data
     */
    protected function response($result, $data = "") {
        die(json_encode(array("ret"=>$result,'data'=>$data)));
    }

    protected function res($res_data) {
        if(empty($res_data)) {
            die(json_encode(array('code'=>99999,'msg'=>'api response empty')));
        } else {
            die(json_encode($res_data));
        }
        /*
        if(API_RET_SUCC === $res_data['code']) {
            if(isset($res_data['data'])) {
                self::response(API_RES_OK, $res_data['data']);
            } else {
                self::response(API_RES_OK);
            }
        } else {
            self::response($res_data['ret'],$res_data['data']);
        }
        */
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */