-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: platform
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alertlist`
--

DROP TABLE IF EXISTS `alertlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertlist` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertlist`
--

LOCK TABLES `alertlist` WRITE;
/*!40000 ALTER TABLE `alertlist` DISABLE KEYS */;
INSERT INTO `alertlist` VALUES (1,'黄前源','huangqianyuan@kingsoft.com');
/*!40000 ALTER TABLE `alertlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ec_platform_setting`
--

DROP TABLE IF EXISTS `ec_platform_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ec_platform_setting` (
  `keyname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ec_platform_setting`
--

LOCK TABLES `ec_platform_setting` WRITE;
/*!40000 ALTER TABLE `ec_platform_setting` DISABLE KEYS */;
INSERT INTO `ec_platform_setting` VALUES ('android_update_level',''),('android_update_path',''),('android_update_version',''),('android_updateurl','http://123.59.14.9/platform/android'),('api_server','//123.59.14.139/api'),('api_server_upload',''),('client_android','http://ecloud.kssws.ks-cdn.com/client/android/LiveSpace_v389.apk'),('client_from_ksyun','0'),('client_ipad','333'),('client_iphone','http://ecloud.kssws.ks-cdn.com/client/iphone/kingfile.ipa'),('client_mac','http://www.baidu.com/1111.exe'),('client_windows','http://s.ksyun.com.kssws.ks-cdn.com/client/1.0.0.100/EkuaipanPlusSetup_1.0.0.100.exe'),('client_windows_sync','ddf32'),('clientkey','-----BEGIN CERTIFICATE-----\nMIIDqzCCApOgAwIBAgIJAPGxDYdbH8QjMA0GCSqGSIb3DQEBBQUAMGwxCzAJBgNV\nBAYTAnpoMRAwDgYDVQQIDAdiZWlqaW5nMRAwDgYDVQQHDAdiZWlqaW5nMREwDwYD\nVQQKDAhraW5nc29mdDEOMAwGA1UECwwFa2ZpbGUxFjAUBgNVBAMMDTEyMy41OS4x\nNC4xMzkwHhcNMTUwOTI4MTIyODA4WhcNMTUxMDI4MTIyODA4WjBsMQswCQYDVQQG\nEwJ6aDEQMA4GA1UECAwHYmVpamluZzEQMA4GA1UEBwwHYmVpamluZzERMA8GA1UE\nCgwIa2luZ3NvZnQxDjAMBgNVBAsMBWtmaWxlMRYwFAYDVQQDDA0xMjMuNTkuMTQu\nMTM5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0SPcu3FXi1lDuc0q\nioufsgYwWdv2jWoEpgr/U33POditQ+LEe65P9K1TS64HpRZAVh1U15eik8zjSyL2\nDM/HP/87JOGhBqJ7pTfm9Vk81VgQ12T2oYY28K+ab5SUQG043fLtJ5OLH/jDzstL\nI4mSnopE1K/4y2MUv1uLpM/Gx05p5m5YhkgKa6C6nJ+vSQVt9f/zxbBa+ax2RDiW\nd8NDQOexvdTyVMSkRsezZpIud44+Z2083Xv6EALmAop0ebUQdOvuaUBbuKLMeop7\nAtsnJan3hovHH68l6wIjtqTNMzVdUWhPmQEWdjFMsjo38U08aP2Vpdc7+6trTsEr\nByCLEwIDAQABo1AwTjAdBgNVHQ4EFgQUUFgKUJkOdhA3WtkXSbNTgzuoMmMwHwYD\nVR0jBBgwFoAUUFgKUJkOdhA3WtkXSbNTgzuoMmMwDAYDVR0TBAUwAwEB/zANBgkq\nhkiG9w0BAQUFAAOCAQEAHyWiHma+elLV6B8yrMP69HIYpXyVDMz/3CKkrVnP/RlS\nHCwoYEJ3VIgiqpw/yDIOT6rfxaxkfmypVn4W343m+ra6luqJkEZ0Cabk8qIWH6CZ\nIusdSF/wJECXKENFzNp0rhgNO7ru9dPm3WWgUgl9xDcFZSUpzkVkYJLAIYUipl3M\nsmJujVAcSxzJ4J1CxX1MSy5jBRwuepjmET5soUytJyVLAXy75XhePWyeCS1RErDg\nCvU6IhMCxo0E4W6hHQQVwnYpnxc5wC7zcR830rBn4XUIBKgJHxinJYK/v9j/utBX\nCbpXZ0oQSXhqg1M0pN4TX4VUzR+mrSko6Fu+naITjA==\n-----END CERTIFICATE-----'),('clientkeyurl','http://123.59.14.139/platform/getclientkey'),('display_offline_message','1'),('forgettitle',''),('fromname','金山企业云服务'),('helpurl','http://ecloud.kssws.ks-cdn.com/help/1/%E9%87%91%E5%B1%B1%E4%BA%91%E7%9B%98%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C.pdf'),('https','1'),('iphone_update_level',''),('iphone_update_path',''),('iphone_update_version',''),('kingsoft_users',''),('logo','http://123.59.14.139/imgs/logo/6613dcdb3a4b442a7fe10bafae0b3eea.png'),('mailer','smtp'),('mailfrom','nil_work@163.com'),('media_server','//123.59.14.139/olc'),('mobile_logo','http://123.59.14.139/imgs/logo/6613dcdb3a4b442a7fe10bafae0b3eea.png'),('offline','0'),('offline_image',''),('offline_message','网站在维护中。<br /> 请稍候访问。'),('sendemailurl',''),('sendmail','smtp88'),('servercrt','jj'),('servercrturl','pppp'),('serverkey','yi'),('servicetel','33123'),('sitename','金山云盘'),('smtpauth','1'),('smtphost','smtp.163.com'),('smtppass','124522769'),('smtpport','465'),('smtpsecure','ssl'),('smtpuser','nil_work@163.com'),('stroge_server','http://192.168.18.10:8088/v1'),('test_users',''),('verifyemailurl',''),('web_server','//123.59.14.139'),('windows_logo','http://123.59.14.139/imgs/logo/6613dcdb3a4b442a7fe10bafae0b3eea.png'),('windows_update_level',''),('windows_update_path',''),('windows_update_version',''),('windows_updateurl','http://123.59.14.9/platform/winplus');
/*!40000 ALTER TABLE `ec_platform_setting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-11 16:16:39
