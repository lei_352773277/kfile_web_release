#!/bin/bash
curl -o /tmp/list.txt   http://e.ksc.com:8080/platform/shellcontactlist
new=`cat /tmp/list.txt`
cur=`cat /usr/local/monit/monit.d/alertlist`
if [ "$new" != "$cur" ]; then
   cat /tmp/list.txt > /usr/local/monit/monit.d/alertlist
   /usr/local/monit/bin/monit reload
   echo "yes"
else
   echo "no"
fi