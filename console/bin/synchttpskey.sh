#!/bin/bash
SERVERNAME="http://console.com"
https=`curl $SERVERNAME/platform/shellgetkey?k=https`
NGINXCRT="/usr/local/openresty/nginx/ssl/nginx.crt"
NGINXKEY="/usr/local/openresty/nginx/ssl/nginx.key"
NGINXSSLCONF="/usr/local/openresty/nginx/conf/vhosts/api_server_https.conf"
NOWTIME=$(date "+%Y%m%d%H%M%S")

function restart_nginx(){
   # /usr/local/openresty/nginx/sbin/nginx -s reload     # 重启nginx
   echo "重启";
}

if [ "$https" != "0" ]; then
   #开启https
    servercrturl=`curl $SERVERNAME/platform/shellgetkey?k=servercrturl`
    servercrt=`curl -o $NGINXCRT".tmp" $servercrturl`
        
    if [ ! -f "$NGINXCRT" ]; then
       touch $NGINXCRT
    fi
    
    serverkeyurl=`curl $SERVERNAME/platform/shellgetkey?k=serverkeyurl`
    serverkey=`curl -o $NGINXKEY".tmp" $serverkeyurl`
       
    if [ ! -f "$NGINXKEY" ]; then
       touch $NGINXKEY
    fi
     
    KEYTMP=`cat $NGINXKEY".tmp"`
    KEY=`cat $NGINXKEY`
    if [ "$KEYTMP" != "$KEY" ]; then
       #echo $NGINXKEY".tmp"
       mv $NGINXKEY $NGINXKEY""$NOWTIME
       mv  $NGXINKEY".tmp" $NGINXKEY
    fi
    
    CRTTMP=`cat $NGINXCRT".tmp"`
    CRT=`cat $NGINXCRT`
    if [ "$CRTTMP" != "$CRT" ]; then
       #echo NGINXCRT".tmp"
       mv $NGINXCRT  $NGINXCRT""$NOWTIME
       mv $NGINXCRT".tmp" $NGINXCRT
    fi

    if [ ! -f "$NGINXSSLCONF" ]; then
       mv $NGINXSSLCONF".bak" $NGINXSSLCONF
    fi 

   restart_nginx;
   echo "no"
else 
   #关闭https
   if [ -f "$NGINXSSLCONF" ]; then
      mv $NGINXSSLCONF $NGINXSSLCONF".bak"
   fi
   restart_nginx;
   echo "yes"
fi