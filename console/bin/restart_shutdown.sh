#!/bin/bash
SERVERNAME="http://e.ksc.com:8080"
restartvalue=`curl $SERVERNAME/platform/shellgetkey?k=restart`
if [ "1" == "$restartvalue" ]; then 
  update=`curl -d 'action=0' $SERVERNAME/platform/restart`
  reboot
else 
  shutdownvalue=`curl $SERVERNAME/platform/shellgetkey?k=shutdown`
  if [ "1" == "$shutdownvalue" ]; then
     update=`curl -d 'action=0' $SERVERNAME/platform/shutdown`
     #poweroff
  fi
fi
