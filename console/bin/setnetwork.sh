# set network
DEVICE="eth0"
SERVERNAME="http://e.ksc.com:8080"
IPADDR=`curl $SERVERNAME/platform/shellgetkey?k=ip`
NETMASK=`curl $SERVERNAME/platform/shellgetkey?k=mask`
GATEWAY=`curl $SERVERNAME/platform/shellgetkey?k=gateway`

mask=`ifconfig |grep inet| sed -n '1p'|awk '{print $4}'|awk -F ':' '{print $2}'`
ip=`ifconfig |grep inet| sed -n '1p'|awk '{print $2}'|awk -F ':' '{print $2}'`
gateway=$(netstat -r | grep 'default' | awk '{ print $2}')

if [ "$mask" != "$NETMASK" ] || [ "$ip" != "$IPADDR" ] || [ "$gateway" != "$GATEWAY" ]; then
#GATEWAY=192.168.138.1
#IPADDR=192.168.138.116
#NETMASK=255.255.255.0

  set -e

  fname=ifcfg-${DEVICE}
  cd /etc/sysconfig/network-scripts
  for s in UUID HWADDR; do
      grep $s ifcfg-eth0 >>${fname}.t
  done

  cat <<EOF >>${fname}.t
BOOTPROTO=static
ONBOOT=yes
NM_CONTROLLED=no
TYPE="Ethernet"
DEVICE=${DEVICE}
GATEWAY=${GATEWAY}
IPADDR=${IPADDR}
NETMASK=${NETMASK}
EOF

  mv -S .bak ${fname}.t ${fname}
                                                                                                                                                                           
  /etc/init.d/network restart
else
 echo "ok"
fi




















