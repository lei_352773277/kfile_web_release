-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: platform
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alertlist`
--

DROP TABLE IF EXISTS `alertlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertlist` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertlist`
--

LOCK TABLES `alertlist` WRITE;
/*!40000 ALTER TABLE `alertlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `alertlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domainlogo`
--

DROP TABLE IF EXISTS `domainlogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domainlogo` (
  `domainId` int(11) NOT NULL,
  `logo` blob,
  `logotype` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`domainId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domainlogo`
--

LOCK TABLES `domainlogo` WRITE;
/*!40000 ALTER TABLE `domainlogo` DISABLE KEYS */;
/*!40000 ALTER TABLE `domainlogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ec_platform_setting`
--

DROP TABLE IF EXISTS `ec_platform_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ec_platform_setting` (
  `keyname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ec_platform_setting`
--

LOCK TABLES `ec_platform_setting` WRITE;
/*!40000 ALTER TABLE `ec_platform_setting` DISABLE KEYS */;
INSERT INTO `ec_platform_setting` VALUES ('android_update_level','12'),('android_update_msg','15'),('android_update_path','14'),('android_update_version','13'),('android_updateurl','11'),('api_block_http_host',''),('api_block_http_port','80'),('api_block_https_host',''),('api_block_https_port','443'),('api_common_http_host','192.168.1.100'),('api_common_http_port','80'),('api_common_https_host',''),('api_common_https_port','443'),('api_server','//42.62.61.233/api'),('api_server_upload',''),('client_android','5'),('client_from_ksyun','0'),('client_ipad','0'),('client_iphone','4'),('client_mac','3'),('client_office','6'),('client_windows','1'),('client_windows_sync',''),('client_win_sync','2'),('clientkey',''),('clientkeyurl','http://192.168.1.100/platform/getclientkey'),('display_offline_message','1'),('forgettitle','01231244444444444'),('fromname','0212312454'),('gateway','192.168.1.1'),('helpurl','0'),('https','0'),('ip','192.168.1.100'),('iphone_update_level','17'),('iphone_update_msg','20'),('iphone_update_path','19'),('iphone_update_version','18'),('iphone_updateurl','16'),('kingsoft_users',''),('logo','http://s.ksyun.com.kssws.ks-cdn.com/help/ksyun/QQ%E6%88%AA%E5%9B%BE20151109143514.png'),('mac_update_level','22'),('mac_update_msg','25'),('mac_update_path','24'),('mac_update_version','23'),('mac_updateurl','21'),('mailer','smtp'),('mailfrom','01231'),('mask','255.255.255.0'),('media_server','//192.168.1.100/olc'),('mobile_logo','0'),('office_update_level','27'),('office_update_msg','30'),('office_update_path','29'),('office_update_version','28'),('office_updateurl','26'),('offline','0'),('offline_image',''),('offline_message','网站在维护中。<br /> 请稍候访问。'),('restart',''),('sendemailurl','http://192.168.1.100/email/send'),('sendmail','0'),('server_name','server_name localhost 127.0.0.1 192.168.1.100;'),('servercrt','1'),('servercrturl','http://192.168.1.100/platform/shellgetkey?k=servercrt'),('serverkey',''),('serverkeyurl','http://192.168.1.100/platform/shellgetkey?k=serverkey'),('servicetel',''),('shellcreatekey','1'),('shellcreatekey_ip','123.59.14.9'),('shutdown',''),('sitename','0'),('smtpauth','0'),('smtphost','0'),('smtppass','0'),('smtpport','465'),('smtpsecure','ssl'),('smtpuser','haohongyan222@163.com'),('spacedata','0|0'),('stroge_server','http://192.168.18.10:8088/v1'),('test_users',''),('trialconfig','{\"space\":1,\"member\":23,\"days\":33}'),('verifyemailurl','http://192.168.1.100/account/verify'),('web_server','//192.168.1.100'),('windows_logo','0'),('windows_update_level','2'),('windows_update_msg','5'),('windows_update_path','4'),('windows_update_version','3'),('windows_updateurl','1'),('windowssync_update_level','7'),('windowssync_update_msg','10'),('windowssync_update_path','9'),('windowssync_update_version','8'),('windowssync_updateurl','6');
/*!40000 ALTER TABLE `ec_platform_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(1) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(1024) DEFAULT NULL,
  `salt` varchar(45) DEFAULT NULL,
  `pwd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,1,'管理员','wangpan@kingsoft.com','2016-08-15 16:00:00',NULL,'y1QHKt0WzqvWPQM9','1a2cbc681a24ddedfa9947455287948f');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-20  9:36:33
