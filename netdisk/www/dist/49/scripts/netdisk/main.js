(function() {
    var vendorPath = 'http://localhost:3000/scripts/vendor';
    var common = 'http://localhost:3000/common/';
    require.config({
        //baseUrl: 'http://localhost:3000/scripts/',
        skipDataMain: true,
        paths: {
            'angular': vendorPath + '/angular/angular',
            //'angular-aria': vendorPath + '/angular-aria/angular-aria',
           // 'angular-material': vendorPath + '/angular-material/angular-material',
            'angular-route': vendorPath + '/angular-route/angular-route',
            'angular-resource': vendorPath + '/angular-resource/angular-resource',
            //'angular-animate': vendorPath + '/angular-animate/angular-animate',
            //'angular-cookies': vendorPath + '/angular-cookies/angular-cookies',
            'angular-sanitize': vendorPath + '/angular-sanitize/angular-sanitize.min',
            //'angular-messages': vendorPath + '/angular-messages/angular-messages.min',
            'ui.router': vendorPath + '/angular-ui-router/release/angular-ui-router.min',
            'bindonce': vendorPath + '/angular-bindonce/bindonce',
            'jQuery': vendorPath + '/jquery/jquery.min',
            'highcharts': vendorPath + '/highcharts/highcharts',
            'ng-clip': vendorPath + '/ng-clip/src/ngClip',
            'app': 'netdisk/app',
            'bootstrap': 'netdisk/bootstrap',
            'spinjs': vendorPath + '/spinjs/spin',
            'netdisk-tpl': 'netdisk/netdisk-tpl',
            'angular-locale-zh-cn': vendorPath + '/ng-locale/angular-locale_zh-cn',
            'angular-i18n':vendorPath + '/angular-i18n/angular-locale_zh-cn',
            'angular-translate': vendorPath + '/angular-translate/angular-translate',
            'angular-translate-loader': vendorPath + '/angular-translate-loader-static-files-master/angular-translate-loader-static-files',
            "angularjs-slider":vendorPath + "/angularjs-slider/dist/rzslider.min",
	        'ng-table':vendorPath + '/ng-table/dist/ng-table',
            //'bootstrap-material-design':vendorPath+'/bootstrap-material-design/dist/js/material',
            'angular-tree-control':vendorPath+'/angular-tree-control/angular-tree-control',
            'angular-ui-bootstrap':vendorPath+'/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min',
            'ng-flow':vendorPath+'/ng-flow/dist/ng-flow-standalone',
            'ng-file-upload-shim':vendorPath+'/ng-file-upload/ng-file-upload-all.min',
            'ImageView':vendorPath+'/imageViewer/ImageView',
            'browserPrefix':vendorPath+'/imageViewer/browserPrefix',
            'imageViewer':vendorPath+'/imageViewer/imageViewer',
            'ngInfiniteScroll':vendorPath+'/ngInfiniteScroll/build/ng-infinite-scroll.min',
            'contextMenu':vendorPath+'/angular-bootstrap-contextmenu/contextMenu',
            'flexslider':vendorPath+'/FlexSlider-master/jquery.flexslider-min',
            'jqxdomain':vendorPath+'/jQuery-ajaxTransport-XDomainRequest-master/jquery.xdomainrequest.min',
            'crossxhr':vendorPath+'/cors-proxy-browser-master/crossxhr',
            'angular-md5':vendorPath+'/angular-md5-master/angular-md5.min',

            //"jquery":vendorPath+'/simditor/scripts/jquery.min',
            //"simpleModule":vendorPath+'/simditor/scripts/module.min',
            //"hotkeys":vendorPath+'/simditor/scripts/hotkeys.min',
            //"uploader":vendorPath+'/simditor/scripts/uploader.min',
            //"simditor":vendorPath+'/simditor/scripts/simditor.min',

            'respond':vendorPath+'/respond/dest/respond.min'
            //'angular-strap':vendorPath+'/angular-strap/dist/angular-strap',
            //'angular-strap.tpl':vendorPath+'/angular-strap/dist/angular-strap.tpl'
        },
        shim: {
            'jQuery': {
                exports: 'jQuery'
            },
            'angular': {
                deps: ['jQuery'],
                exports: 'angular'

            },
            'angular-ui-bootstrap':{
                deps: ['angular','jQuery']
            },
            'ui.router': {
                deps: ['angular']
            },
            /*
            'angular-animate': {
                deps: ['angular']
            },

            'angular-aria': {
                deps: ['angular']
            },
            */
            /*
            'angular-material': {
                deps: ['angular', 'angular-aria']
            },
            */
            'angular-resource': {
                deps: ['angular']
            },
            'angular-cookies': {
                deps: ['angular']
            },
            'angular-sanitize': {
                deps: ['angular']
            },
            'bindonce': {
                deps: ['angular']
            },
            'ng-clip': {
                deps: ['angular']
            },
            'highcharts': {
                deps: ['jQuery']
                //exports:'highcharts'
            },
            'spinjs': {},
            'angular-locale-zh-cn': {
                deps: ['angular']
            },
            'angular-i18n':{
                deps:['angular']
            },
            'netdisk-tpl': {
                deps: ['angular']
            },
            'angular-translate': {
                deps: ['angular']
            },
            'angular-translate-loader': {
                deps: ['angular','angular-translate']
            },
	        'angularjs-slider': {
                deps: ['angular']
            },
            'ng-table':{
                deps:['angular']
            },
            //'bootstrap-material-design':{
            //    deps:['jQuery']
            //},
            'angular-tree-control':{
                deps:['angular']
            },
            'ng-flow':{
                deps:['angular']
            },
            'ng-file-upload-shim':{
                deps:['angular']
            },
            'imageViewer':{
                deps:['angular']
            },
            'respond':{
                deps:['angular']
            },
            'contextMenu':{
                deps:['angular']
            },
            /*
            'angular-messages':{
                deps:['angular']
            },
            */
            'flexslider':{
                deps: ['jQuery']
            },
            'jqxdomain':{
                deps: ['jQuery']
            },
            'crossxhr':{
                deps: ['jQuery']
            },
            'angular-md5':{
                deps:['angular']
            },
            'ngInfiniteScroll':{
                deps:['angular']
            }
        }
    });
}());