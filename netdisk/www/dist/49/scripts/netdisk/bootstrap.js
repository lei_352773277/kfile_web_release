/**
 * bootstrap file for application.
 */
define([
  'angular',
  'app',
  'netdisk/initConfig',
  'netdisk/controllers/_base',
  'netdisk/states/_base',
  'netdisk/services/_base',
  'netdisk/directives/_base',
  //'netdisk/factory/_base',
  'netdisk/filters/_base'
], function(angular){
  'use strict';

	angular.element(document).ready(function(){
		angular.bootstrap(document, ['netdiskConsole']);
	});
});
