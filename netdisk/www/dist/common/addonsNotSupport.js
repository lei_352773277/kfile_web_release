window._browserIsNotSupported = true;
if(window.attachEvent){
  window.attachEvent('onload', function(){
    var lowestSupportedIEVersion = 9;
    if(window.LOWEST_IE_VERSION != undefined){
      lowestSupportedIEVersion = window.LOWEST_IE_VERSION;
    }
    var el = document.createElement('div'),
        elStyle = el.style,
        docBody = document.getElementsByTagName('body')[0],
        linkStyle = 'color:#06F;text-decoration: underline;';
        el.innerHTML =	'尊敬的用户您好：<br />'+
        '使用'+PRODUCT+'，您的浏览器版本太低，需要安装Internet Explorer '+lowestSupportedIEVersion+'（或以上），'+
        '<a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" style="color:#000;text-decoration: underline;" target="_blank">下载安装IE' + lowestSupportedIEVersion + '</a>。';
    // elStyle.width = '100%';
    elStyle.width = 'auto';
    elStyle.color = '#000';
    elStyle.fontSize = '14px';
    elStyle.lineHeight = '180%';
    elStyle.margin = '10px';
    elStyle.backgroundColor = '#fffbd4';
    elStyle.border = '1px solid #CCC';
    elStyle.padding = '10px';
    // elStyle.background = '#F00 url(styles/images/not-support-ie67.png) 48px 48px no-repeat';
    // elStyle.padding = '40px 40px 48px 160px';
    docBody.innerHTML = '';
    docBody.className  = '';
    docBody.appendChild(el);
    // docBody.insertBefore(el,docBody.firstChild);
  });
}
window.onerror = function(sMessage,sUrl,sLine){
    return false;
}