<footer class="footer">
    <div class="copy">
        Copyright &copy; 2015-2017 京ICP备16027148号-1
        <br>
        《中华人民共和国增值电信业务经营许可证》
    </div>
</footer>
<footer class="mobile-footer hidden">
    <span>更多内容请访问电脑版</span>
</footer>