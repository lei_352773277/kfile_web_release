<nav class="nav-kingfile nav-kingfile-link clearfix ng-cloak">
	<div class="header">
		<a class="logobrand" ng-if="showLogoType=='img'" href="/">
			<img ng-if="logotext!=''" ng-cloak class="ng-cloak" {literal}ng-src="{{logotext}}"{/literal}/>
			<img ng-if="logotext==''" class="ng-cloak" src="{$css}/styles/img/logo/logo-office.png"/>
		</a>
	</div>
	<div id="navbar" class="nav-content clearfix">
        {literal}<header-right-link></header-right-link>{/literal}
	</div>
</nav>