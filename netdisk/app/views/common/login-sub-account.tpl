<form class="form-login" name="loginform" ng-submit="login($event)">
    <div class="form-group-login"
         ng-class="{ 'has-error' : loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !='' }">
        <label for="domainIdent" class="">
            <i class="login-icon icon-domain"></i>
            <span>企业域名：</span>
        </label>
        <div class="input-group-login ">
            <input type="text" tabindex="1" class="login-ipt" id="domainIdent" autocomplete="off"
                    name="domainIdent" ng-model="domainIdent"
                   placeholder="请输入注册企业时设立的企业域名"
                   placeholderkey="请输入注册企业时设立的企业域名" required>
        </div>
        <p ng-cloak class="ng-cloak help-block" ng-if="false"
           ng-show="loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !=''">
            请输入企业域</p>
    </div>

    <div class="form-group-login"
         ng-class="{ 'has-error' : loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !='' }">
        <label for="user" class="">
            <i class="login-icon icon-user"></i>
            <span>用户账号：</span>
        </label>
        <div class="input-group-login">
            {*<span class="login-icon icon-user"></span>
            <span class="icon-sep">&nbsp;</span>*}
            <input type="text" tabindex="2" ng-trim="true" class="login-ipt" id="user"
                   autocomplete="off" aria-autocomplete="none"  name="user"
                   ng-model="user" placeholder="请输入您的用户名/手机号"
                   placeholderkey="请输入您的用户名/手机号" required>
        </div>

        {*<p ng-cloak class="ng-cloak help-block" ng-show="loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !=''">请输入您的账号</p>*}
    </div>
    <div class="form-group-login"
         ng-class="{ 'has-error' : loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''}">
        <label for="password" >
            <i class="login-icon icon-pwd"></i>
            <span>用户密码：</span>
        </label>
        <div class="input-group-login">
            {* <span class="login-icon icon-pwd"></span>
             <span class="icon-sep">&nbsp;</span>*}
            <input type="text" tabindex="3" onfocus="this.type='password'" class="login-ipt"
                   id="password" autocomplete="off"
                   name="password" ng-model="password"
                   placeholder="请输入您的密码"
                   placeholderkey="请输入您的密码" required>
        </div>
        {*<p ng-cloak class="ng-cloak help-block" ng-show="loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''">请输入正确格式的密码</p>*}

    </div>

    <div class="login-error-forget clearfix">
        {literal}
            <p ng-cloak class="ng-cloak text-error" ng-if="loginErrorMsg!=''" >
                {{loginErrorMsg}}
            </p>
        {/literal}
        <a  class="login-a forget-text" href="/account/forget" >忘记密码</a>
    </div>
    <div class="login-btns">
        {literal}
            <button tabindex="4" type="submit" ng-disabled="loging" class="btn-login">{{loginbtn}}
            </button>
        {/literal}
    </div>
    <div class="login-register clearfix">
        <span class="text">
            无账号?
        </span>
        <a  class="login-a register" href="/register" target="_blank" >立即注册</a>
    </div>
</form>