
<div class="nav-header-mobile-custom hidden">
	<div class="nav-header-mobile-container clearfix">
		<div class="logo-wrap pull-left clearfix">
			<a class="logo pull-left" href="/" title="金山企业云盘 金山网盘">
			</a>
			<div class="pull-left">
				<div style="font-size: 16px;line-height: 16px;padding-top: 6px;font-weight: bold;">金山企业云盘</div>
				<div style="font-size: 12px;line-height: 12px;">pan.ksyun.com</div>
			</div>
		</div>
		<div class="menu-list  pull-right">
			{if $device_download_addr!='other'}
				<a class="btn-download pull-right" href="{{$device_download_addr}}">下载客户端</a>
			{else}
				<a class="btn-download pull-right" href="#" ng-click="openM()">下载客户端</a>
            {/if}
		</div>
	</div>
</div>