<div class="nav-header-custom">
	<div class="nav-header-container header-container-customize clearfix">
		<div class="nav-header-logo">
			<a class="customize-logo" href="#" title="金山企业云盘 金山网盘">
                {if $logo!=''}
				<img  src="{{$logo}}" alt="金山企业云盘 金山网盘">
				{else}
				<img  src="{{$css}}/styles/img/logo/logo.png" alt="金山企业云盘 金山网盘">
				{/if}

			</a>
		</div>

		<div class="nav-header-right clearfix" >
            {if $userInfo && $userInfo['super_type']>=0}
				<div class="header-login-info clearfix" style="">
					{if $userInfo['super_type']==0 ||$userInfo['super_type']==2}
						<a href="/dashboard" >
                            {{$userInfo['user_name']|truncate:12:'...'}}
							<span class="glyphicon glyphicon-menu-down"></span>
						</a>
					{else}
						<a href="/account/signup">
                            {{$userInfo['user_name']|truncate:12:'...'}}
							<span class="glyphicon glyphicon-menu-down"></span>
						</a>
					{/if}
					<div class="my-pull-down">

						<a href="/account/logout">退出登录</a>
					</div>
				</div>
                {if $userInfo['super_type']==0 ||$userInfo['super_type']==2}
					<a class=" btn-header btn-header-register" href="/dashboard" >打开网盘</a>
                {else}
					<a class=" btn-header btn-header-register" href="/account/signup" >打开网盘</a>
                {/if}


            {else}
            {/if}
		</div>
	</div>
</div>

<div class="nav-header-mobile-custom hidden">
	<div class="nav-header-mobile-container clearfix">
		<div class="logo-wrap pull-left clearfix">
			<a class="logo pull-left" href="/" title="金山企业云盘 金山网盘">
			</a>
			<div class="pull-left">
				<div style="font-size: 16px;line-height: 16px;padding-top: 6px;font-weight: bold;">金山企业云盘</div>
				<div style="font-size: 12px;line-height: 12px;">pan.ksyun.com</div>
			</div>
		</div>
		<div class="menu-list  pull-right">
			<a class="btn-download pull-right" href="{{$device_download_addr}}">下载客户端</a>
		</div>
	</div>
</div>