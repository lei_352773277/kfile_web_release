<header class="container clearfix">
    <div class="nav-header-logo">
        <a class="logo" href="/" title="飞鸟云 "></a>
    </div>
    <div class="nav-header-right">
        <a class=" btn-header btn-header-register" href="/register" >免费试用</a>
    </div>
    <div class="nav-header-menu" id="nav-main-menu">
        <ul class="">
            <li {$indexclass}>
                <a href="/"  title="首页">首页</a>
            </li>
            <li {$downloadclientclass}>
                <a href="/downloadclient"  title="下载客户端">下载客户端</a>
            </li>
            <li {$shopclass}>
                <a href="/shop"  title="价格">价格</a>
            </li>
            <li {$loginclass}>
                <a href="/login" title="登录">登录</a>
            </li>
        </ul>
    </div>
</header>

