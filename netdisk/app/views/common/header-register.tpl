
<header class="container clearfix">
	<div class="nav-header-logo">
		<a class="logo" href="/" title="飞鸟云 "></a>
		<span class="register-txt">注册账号</span>
	</div>

	<div class="nav-header-right">
		<span class="has-account">我已有账号，现在</span>
		<a class="btn-header btn-header-login" href="/login" >登录</a>
	</div>
</header>