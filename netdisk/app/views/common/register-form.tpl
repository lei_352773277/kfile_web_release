<form  name="registerform" ng-submit="register($event)">
    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.domainName.$invalid && !registerform.domainName.$pristine  && registerform.domainName !='' }">
        <label for="domainName">
            <span>企业名称：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="1" class="login-ipt" id="domainName" autocomplete="off"
                   name="domainName" ng-model="domainName" ng-pattern="/^\S+/"
                   placeholder="您的企业名称" placeholderkey="您的企业名称" required>
        </div>
        <p ng-cloak class="ng-cloak help-block"
           ng-show="registerform.domainName.$invalid && !registerform.domainName.$pristine  && registerform.domainName !=''">
            请输入您的企业名称</p>
    </div>
    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.domainIdent.$invalid && !registerform.domainIdent.$pristine  && registerform.domainIdent !='' }">
        <label for="domainIdent">
            <span>企业域名：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="2" class="login-ipt" id="domainIdent" autocomplete="off"
                    name="domainIdent" ng-model="domainIdent" ng-pattern="/^([A-Za-z0-9-_.])+$/"
                   ng-keyup="mykeyup()"
                   placeholder="企业域名，如：qq.com，支持字母/数字和.-_ 30个字符"
                   placeholderkey="企业域名，如：qq.com，支持字母/数字和.-_ 30个字符" required>
        </div>
        <p ng-cloak class="ng-cloak help-block"
           ng-show="(registerform.domainIdent.$invalid && !registerform.domainIdent.$pristine  && registerform.domainIdent !='')||ident_error">
            {literal}{{ident_error_msg}}{/literal}</p>
    </div>

    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.user.$invalid && !registerform.user.$pristine  && registerform.user !='' }">
        <label for="user" >
            <span>用户名：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="3" ng-trim="true" class="login-ipt" id="user"
                   autocomplete="off" aria-autocomplete="none"  name="user" ng-pattern="/^([A-Za-z0-9-_.])+$/"
                   ng-model="user" placeholder="登录使用，如admin，支持字母数字和._- 30个字符"
                   placeholderkey="登录使用，如admin，支持字母数字和._- 30个字符" required>
        </div>
        <p ng-cloak class="ng-cloak help-block" ng-show="registerform.user.$invalid && !registerform.user.$pristine  && registerform.user !=''">请输入您的用户名</p>
    </div>
    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.phone.$invalid && !registerform.phone.$pristine  && registerform.phone !='' }">
        <label for="phone" >
            <span>手机号：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="4" ng-trim="true" class="login-ipt" id="phone"
                   autocomplete="off" aria-autocomplete="none"  name="phone"
                   ng-pattern="/^((13\d|14[57]|15[^4,\D]|17[13678]|18\d)\d{literal}{8}{/literal}|170[^346,\D]\d{literal}{7}{/literal})$/"
                   ng-model="phone" placeholder="请输入您的手机号"
                   placeholderkey="请输入您的手机号" required>
        </div>
        <p ng-cloak class="ng-cloak help-block" ng-show="registerform.phone.$invalid && !registerform.phone.$pristine  && registerform.phone !=''">手机号格式不正确</p>
    </div>
    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.password.$invalid && !registerform.password.$pristine && registerform.password !=''}">
        <label for="password">
            <span>密码：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="5" onfocus="this.type='password'" class="login-ipt"
                   id="password" autocomplete="off"
                   name="password" ng-model="password"
                   placeholder="设置密码，≥6位，字母/数字和特殊符号"
                   placeholderkey="设置密码，≥6位，字母/数字和特殊符号" required>
        </div>
        <p ng-cloak class="ng-cloak help-block" ng-show="registerform.password.$invalid && !registerform.password.$pristine && registerform.password !=''">请输入正确格式的密码</p>

    </div>
    <div class="form-group-register"
         ng-class="{ 'has-error' : registerform.verificationCode.$invalid && !registerform.verificationCode.$pristine && registerform.verificationCode !=''}">
        <label for="verificationCode">
            <span>验 证 码：</span>
        </label>
        <div class="input-group-register">
            <input type="text" tabindex="6"  class="login-ipt min-ipt"
                   id="verificationCode" autocomplete="off"
                   name="verificationCode" ng-model="verificationCode"
                   placeholder="手机验证码"
                   placeholderkey="手机验证码" required>
            <button class="verifiCode-btn" ng-disabled="timeouting" ng-click="verifiCode()" ng-bind="code_txt" >
            </button>
        </div>
        <p ng-cloak class="ng-cloak help-block" ng-show="registerform.verificationCode.$invalid && !registerform.verificationCode.$pristine && registerform.verificationCode !=''">
            手机验证码校验结果错误，请确认。
        </p>

    </div>
    <div class="login-btns">
        <p class="error" ng-show="errorMsg!=''">{literal}{{errorMsg}}{/literal}</p>
        {literal}
            <button tabindex="7" type="submit" ng-disabled="(registerform.$invalid && !registerform.$pristine)||registering" class="btn-login">{{submit}}
            </button>
        {/literal}
    </div>
    <div class="agreement clearfix">
        <span class="text">
            点击”立即注册“表示同意
        </span>
        <a  class="agreement-a" href="/register" target="_blank" >《飞鸟企业云用户协议》</a>
    </div>
</form>