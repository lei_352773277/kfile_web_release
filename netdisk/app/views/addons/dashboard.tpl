<!DOCTYPE html>
<html lang="zh" ng-controller="homeController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{$paltform['sitename']}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/addons/addons.css">
    {literal}
        <style type="text/css">
            .ng-cloak, .x-ng-cloak, .ng-hide {
                display: none !important;
            }
        </style>
    {/literal}
    <!--[if lt IE 9]>
    <script src="{$common}/addonsNotSupport.js"></script>
    <![endif]-->
    {if !$debug}
        <script data-main="{$js}/scripts/addons/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/addons/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/addons/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../addons/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body infinite-scroll="myPagingFunction()" id="body" list="{$list}" class="{if empty($list)}listall{/if}"
      source="{$source}" infinite-scroll-disabled="scroll_switch">
<ks-navbar-header></ks-navbar-header>
<div class="container-fluid addonsview" ng-view ui-view>

</div>
{literal}
    <alert ng-repeat="alert in alerts" type="{{alert.type}}" class="messagebar" ng-cloak
           close="closeAlert($index)"><span ng-bind-html="alert.tipsText|to_trusted"></span></alert>
{/literal}
{literal}
    <file-path></file-path>
    <script>
        //允许被选择的控件列表
        var allowedControls = {
            'TEXTAREA': 1,
            'INPUT': 1
        };
        document.oncontextmenu = function () {
            return true;
        };
        document.onkeydown = function () {
            keyCodeOfF5 = 116 == window.event.keyCode;//F5键
            keyCodeOfF11 = 122 == window.event.keyCode;//F11键
            keyCodeOfF = 70 == window.event.keyCode;//组合键CTR + F
            keyCodeOfN = 78 == window.event.keyCode;//组合键CTR + N
            keyCodeOfO = 79 == window.event.keyCode;//组合键CTR + O
            keyCodeOfP = 80 == window.event.keyCode;//组合键CTR + P
            keyCodeOfLeft = 37 == window.event.keyCode;//组合键ALT + <-
            keyCodeOfRight = 39 == window.event.keyCode;//组合键ALT + ->
            keyCodeOfEnter = 13 == window.event.keyCode;//回车键
            keyCodeOfBackSpace = 8 == window.event.keyCode;//回退删除键
            if (keyCodeOfF5 ||
                    keyCodeOfF11 ||
                    window.event.ctrlKey && (keyCodeOfN || keyCodeOfO || keyCodeOfP || keyCodeOfF) ||
                    window.event.altKey && (keyCodeOfLeft || keyCodeOfRight)) {
                window.event.keyCode = 0;
                window.event.returnValue = false;
            }
            else if (keyCodeOfBackSpace) {
                window.event.returnValue = 1 == allowedControls[event.srcElement.tagName];
            }
            else if (keyCodeOfEnter) {//回车事件处理
                //FireEnterEvent(window.event.srcElement);
            }
        }
    </script>
{/literal}
</body>

</html>
