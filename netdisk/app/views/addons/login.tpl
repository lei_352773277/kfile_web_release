<!DOCTYPE html>
<html lang="zh"  ng-controller="loginController">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    {*<title>{$paltform['sitename']}</title>*}
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="{$css}/styles/addons/login.css">
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
     </script>
    <script src="{$common}/addonsNotSupport.js"></script>
  	<![endif]-->

    {if !$debug}
    <script data-main="{$js}/scripts/addons/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/addons/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/addons/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
	<script>
    	require(['../addons/bootstrap'], function(bootstrap) {});
    </script>


</head>
<body  ng-cloak class="ng-cloak"  id="body" list="{$list}" class="{if empty($list)}listall{/if}" source="{$source}" >

<div id="container">
	<div>
		<div class="login-box">
            {if $paltform['productmodel'] !=2}
			<a href="javascript:;" ng-click="setServer()" title="服务器设置"><span class="glyphicon glyphicon-cog pull-right" style="padding-top:5px;right:30px;z-index:999;"> </span></a>
            {/if}
			<div class="logo">
				<span class="logo-img" style="background:url({$paltform['windows_logo']}) no-repeat 0%;"></span>
			</div>
        	<form class="form-horizontal" d="{$paltform['producttype']}" name="loginform" ng-submit="login($event)">
				    <fieldset>
				         {if $paltform['producttype'] != 1}
				        <div class="form-group" ng-class="{ 'has-error' : loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !='' }">
				            <div class="">
								<input type="text" class="form-control" id="domainIdent" autocomplete="off" aria-describedby="inputGroupSuccess1Status" name="domainIdent" ng-model="domainIdent" placeholder="企业标识(例如：kingsoft.com)" required>
								<p ng-cloak class="ng-cloak help-block"  ng-show="loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !=''">请输入企业标识</p>
						    </div>
				        </div>
				        {/if}
						<div class="form-group" ng-class="{ 'has-error' : loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !='' }">
				            <div class="">
								<input type="text" class="form-control" id="inputGroupSuccess1" autocomplete="off" aria-describedby="inputGroupSuccess1Status" name="user" ng-model="user"   placeholder="用户名/邮箱/手机" required>
				                <p ng-cloak class="ng-cloak help-block"  ng-show="loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !=''">请输入您的账号</p>
				            </div>
				        </div>
				        <div class="form-group" ng-class="{ 'has-error' : loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''}">
				            <div class="">
								<input type="text" onfocus="this.type='password'" class="form-control" id="inputGroupSuccess1" autocomplete="off" aria-describedby="inputGroupSuccess1Status" name="password" ng-model="password" placeholder="请输入密码" required>
				                <p ng-cloak class="ng-cloak help-block"   ng-show="loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''">请输入正确格式的密码</p>
				            </div>
				        </div>
						
				        <div class="form-group">
				        	{literal}
				        		<button type="submit" ng-disabled="loging" class="btn btn-primary btn-block">{{loginbtn}}</button>
				        	 {/literal}
				        </div>
				       {literal}
				        <div class="form-group ng-cloak" ng-cloak>
				        	<div class="">
				        		<p ng-cloak class="ng-cloak text-danger"  ng-show="loginErrorCode!=''" ng-bind="loginErrorCode|formatErrorText" class="help-block"></p>
				       		</div>
				        </div>
				        {/literal}

				    </fieldset>
       		</form>
       	</div>
       </div>
    </div>
</div>
{literal}
    <script>
    //允许被选择的控件列表
    var allowedControls = {
        'TEXTAREA': 1,
        'INPUT': 1
    };
    document.oncontextmenu =function(){
        return true;
    }
    document.onkeydown = function(){
        keyCodeOfF5 = 116 == window.event.keyCode;//F5键
        keyCodeOfF11 = 122 == window.event.keyCode;//F11键
        keyCodeOfF = 70 == window.event.keyCode;//组合键CTR + F
        keyCodeOfN = 78 == window.event.keyCode;//组合键CTR + N
        keyCodeOfO = 79 == window.event.keyCode;//组合键CTR + O
        keyCodeOfP = 80 == window.event.keyCode;//组合键CTR + P
        keyCodeOfLeft = 37 == window.event.keyCode;//组合键ALT + <-
        keyCodeOfRight = 39 == window.event.keyCode;//组合键ALT + ->
        keyCodeOfEnter = 13 == window.event.keyCode;//回车键
        keyCodeOfBackSpace = 8 == window.event.keyCode;//回退删除键
        if (keyCodeOfF5 ||
        keyCodeOfF11 ||
        window.event.ctrlKey && (keyCodeOfN || keyCodeOfO || keyCodeOfP || keyCodeOfF) ||
        window.event.altKey && (keyCodeOfLeft || keyCodeOfRight)) {
            window.event.keyCode = 0;
            window.event.returnValue = false;
        }
        else 
            if (keyCodeOfBackSpace) {
                window.event.returnValue = 1 == allowedControls[event.srcElement.tagName];
            }
            else 
                if (keyCodeOfEnter) {//回车事件处理               
                    //FireEnterEvent(window.event.srcElement);
                }
    }
</script>
{/literal}
</body>
</html>