<!DOCTYPE html>
<html lang="zh" ng-controller="sController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>{$linkInfo['name']}-金山企业云盘</title>
    <meta name="keywords" content="{$linkInfo['name']},金山企业网盘，网盘，同步盘、一体机，私有云，公有云，协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP、NAS、SaaS" />
    <meta name="description" content="{$linkInfo['name']},金山企业云盘-企业云存储平台、协作办公工具、文件共享助手" />

    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/netdisk.console.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>



</head>
<body id="body" class="root-link" data-link="{$link}" data-size="{$linkInfo['xsize']}">

{include file="../common/header-link.tpl"}

<span style="position:absolute;top:0;left:0"><object type="application/x-shockwave-flash" data="/dist/common/crossxhr.swf" width="1" height="1" id="FlashHttpRequest_gateway" style="visibility: visible;"><param name="wmode" value="transparent"><param name="allowscriptaccess" value="always"></object></span>
{literal}
    <div class="loading">
        <loading></loading>
        <span class="loading-txt" ng-bind="loading.tipsText"></span>
    </div>
{/literal}
{literal}
    <alert ng-repeat="alert in alerts" type="{{alert.type}}" class="messagebar" ng-cloak
           close="closeAlert($index)"><span ng-bind-html="alert.tipsText|to_trusted"></span></alert>
{/literal}


<div class="container-fluid link-content">


    <div class="col-lg-8 col-lg-offset-2">
        <div class="pull-right">
            <a class="my-a my-a-large" href="javascript:void(0)" ng-click="report('{$linkInfo["xid"]}','{$token}')" translate="report">举报</a>
        </div>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-default swrap">
            {if $status==0}
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="pull-right ctime">

                    <span translate="mod_link_share_date">发布时间</span>：{$linkInfo['ctime']|date_format:'%Y-%m-%d %H:%M:%S'}</span>
                        <span class="panel-txt" title="{$linkInfo['name']}">{$linkInfo['name']}</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="iconwrap">
                        <span class="glyphicon glyphicon-file"></span>
                        <p class="downbtn">
                            <a onclick="_hmt.push(['_trackEvent', 'link', 'download', '浏览器下载-link']);" href="{$server['server']}{$server['api']}/agent/file/download?xid={$linkInfo['xid']}&token={$token}&fileVer={$linkInfo['file_version']}&agent={$agent}"
                               class="btn btn-primary"><span
                                        class="glyphicon glyphicon-download-alt"></span> {literal} {{'comm_down'|translate}}（{{filesize|readablizeBytesFile}}）   {/literal}
                            </a>
                        </p>
                    </div>
                </div>
            {elseif $status==1}
                <div class="panel-body">
                    <div class="iconwrap">

                        <form class="form-inline" name="linkshareform">
                            <div class="form-group">
                                <label for="password" translate="comm_pwd_input">请输入密码：</label>
                                <input type="password" class="form-control" id="password" ng-model="password"
                                       {literal}placeholder="{{'comm_pwd'|translate}}" {/literal}>
                            </div>
                            <button type="button" class="btn btn-primary" ng-disabled="linkshareform.$invalid"
                                    ng-click="confirmpwd()" translate="comm_submit"></button>
                            {literal}
                                <p ng-cloak class="ng-cloak text-danger" style="margin-top:10px;"
                                   ng-show="errorcode!=''" class="">{{errMsg}}</p>
                            {/literal}

                        </form>
                    </div>
                </div>
            {elseif $status==2}
                <div class="panel-body">
                    <div class="iconwrap">

                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;"
                                   translate="mod_link_share_empty">抱歉，外链已经被删除或者账户已过期！</p>
                            </div>

                        </form>
                    </div>
                </div>
            {elseif $status==3}

                <div class="panel-heading">
                    <h3 class="panel-title"><span class="pull-right ctime">
                    <span translate="mod_link_share_date">发布时间</span>：{$linkInfo['ctime']|date_format:'%Y-%m-%d %H:%M:%S'}</span>
                        <span class="panel-txt" title="{$linkInfo['name']}">{$linkInfo['name']}</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="iconwrap">
                        <span class="glyphicon glyphicon-file" ></span>
                        <p class="downbtn">
                            <a href="javascript:void(0)" onclick="_hmt.push(['_trackEvent', 'link', 'download', '极速下载-link']);" ng-click="download('{$linkInfo["xid"]}','{$token}')" class="btn m-r-10" {literal}ng-class="{true:'btn-default',false:'btn-primary'}[operating]" ng-disabled="operating" {/literal} >
                                {literal} {{'download_btn_2'|translate}}
                                    <span ng-show="xtype!=1">（{{filesize|readablizeBytesFile}}）</span>
                                {/literal}
                            </a>

                            <a  ng-show="xsize<=download_bypc_immediately && xtype==0" onclick="_hmt.push(['_trackEvent', 'link', 'download', '浏览器下载-link']);"
                                    href="{$server['server']}{$server['api']}/agent/file/download?xid={$linkInfo['xid']}&token={$token}&fileVer={$linkInfo['file_version']}&agent={$agent}"
                               class="btn btn-default">{literal} {{'download_btn_3'|translate}}（{{filesize|readablizeBytesFile}}）{/literal}</a>

                        </p>
                    </div>
                </div>

            {elseif $status==4}
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="pull-right ctime">
                    <span translate="mod_link_share_date">发布时间</span>：{$linkInfo['ctime']|date_format:'%Y-%m-%d %H:%M:%S'}</span>
                        <span class="panel-txt" title="{$linkInfo['name']}">{$linkInfo['name']}</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="iconwrap" ng-show="xsize<=download_bypc_immediately && xtype==0">
                        <span class="glyphicon glyphicon-file"></span>
                        <p class="downbtn">
                            <a  onclick="_hmt.push(['_trackEvent', 'link', 'download', '浏览器下载-link']);"  href="{$server['server']}{$server['api']}/agent/file/download?xid={$linkInfo['xid']}&token={$token}&fileVer={$linkInfo['file_version']}&agent={$agent}"
                               class="btn btn-primary"><span
                                        class="glyphicon glyphicon-download-alt"></span> {literal} {{'comm_down'|translate}}（{{filesize|readablizeBytesFile}}）   {/literal}
                            </a>
                        </p>
                    </div>
                    <div class="iconwrap" ng-show="xsize>download_bypc_immediately && xtype==0">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="max_download">
                                    文件过大，请通过PC客户端下载！</p>
                            </div>
                        </form>
                    </div>
                    <div class="iconwrap" ng-show="xtype==1">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="folder_download_not_support">
                                    该系统不支持文件夹下载，请通过Windows系统客户端下载</p>
                            </div>
                        </form>
                    </div>
                </div>
            {elseif $status==5}
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="pull-right ctime">
                    <span translate="mod_link_share_date">发布时间</span>：{$linkInfo['ctime']|date_format:'%Y-%m-%d %H:%M:%S'}</span>
                        <span class="panel-txt" title="{$linkInfo['name']}">{$linkInfo['name']}</span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="iconwrap">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="max_download">
                                    文件过大，请通过PC客户端下载！</p>
                            </div>
                        </form>
                    </div>
                </div>

            {elseif $status==6}
                <div class="panel-body">
                    <div class="iconwrap">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="mod_link_share_perm">抱歉，外链权限不足！</p>
                            </div>
                        </form>
                    </div>
                </div>

            {elseif $status==7}
                <div class="panel-body">
                    <div class="iconwrap">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="mod_link_has_report">外链已经被举报</p>
                            </div>
                        </form>
                    </div>
                </div>
            {elseif $status==8}
                <div class="panel-body">
                    <div class="iconwrap">
                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;" translate="file_link_probation_period">试用期用户，暂时不提供外链功能！</p>
                            </div>
                        </form>
                    </div>
                </div>
            {else}
                <div class="panel-body">
                    <div class="iconwrap">

                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;"
                                   translate="mod_link_share_empty">抱歉，外链已经被删除或者账户已过期！</p>
                            </div>

                        </form>
                    </div>
                </div>
            {/if}
            {literal}
                <div ng-class="{'has-error' : msg!='' }">
                    <p style=" text-align: center;margin-bottom: 5px;" ng-show="msg!=''" class="help-block" ng-bind-html="msg"></p>
                </div>
            {/literal}
        </div>
        {if $status==0}
        {elseif $status==3}
            <div class="link-tip">
                {literal}{{'download_txt_6'|translate}}{/literal}

                <a class="my-a"  onclick="_hmt.push(['_trackEvent', 'software', 'download', 'Windows增强版-from-link']);"  href="{$platform['client_windows']}" title="下载金山企业云盘" translate="download_txt_7"></a>

                {literal}{{'download_txt_4'|translate}}{/literal}
                <br/>
                {literal}{{'download_txt_12'|translate}}{/literal}
                <a class="my-a p-l-3" href="javascript:void(0)" ng-click="start_pc_again()" title="启动web下载服务" translate="download_txt_13"></a>

            </div>
        {elseif $status==4}
        <div class="link-tip">
            {literal}
            {{'download_txt_1'|translate}}{{'download_txt_9'|translate}}
            {/literal}
            <a class="my-a" onclick="_hmt.push(['_trackEvent', 'software', 'download', 'Mac客户端-from-link']);"  href="{$platform['client_mac']}"  title="下载金山企业云盘" translate="download_txt_7"></a>

        </div>
        {elseif $status==100}
            <div class="link-tip">
                {literal}
                    {{'download_txt_1'|translate}}{{'download_txt_9'|translate}}
                {/literal}
                <a class="my-a" onclick="_hmt.push(['_trackEvent', 'software', 'download', 'Windows增强版','{$OS},{$browser}']);" href="{$platform['client_windows']}" title="下载金山企业云盘" translate="download_txt_7"></a>

            </div>
        {/if}

        {*<div  style="text-align: center;">
            下载金山企业云盘客户端，享受极速的文件下载体验。
            <a href="https://kss.ksyun.com/ecloud/public/client/2.1.0.68/KsyunpanSetup_2.1.0.68.exe" title="下载金山企业云盘"
               style="color: blue;">点击下载&gt;&gt;</a>
        </div>*}
    </div>
</div>
<input type="hidden" id="xsize"  value="{$linkInfo['xsize']}">
<input type="hidden" id="xtype"  value="{$linkInfo['xtype']}">
</body>
</html>
