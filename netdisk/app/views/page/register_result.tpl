<!DOCTYPE html>
<html lang="zh" ng-controller="registresultController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>金山企业云盘-专注企业服务的网盘云存储和协同办公平台</title>
    <meta name="keywords" content="金山企业网盘，网盘，同步盘、一体机，私有云，公有云，勒索病毒,协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP，企业云存储，云存储，网络硬盘，网络U盘" />
    <meta name="description" content="金山企业云盘是金山企业云的升级版，为企业提供最专业的文件存储与协同办公服务，具备文件集中管理、在线协作办公、安全管控、移动办公等优势功能，助力企业挣脱办公困扰，提升业绩，欢迎咨询：400-028-9900" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/regist.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/registresult.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body class="initkingfile-page">
{include file="../common/header-register.tpl"}

<div id="container" class="container-wrap-login">
    <div class="container">
        <div class="login-wrap">
            <div class="login">
                <div class="tab-content">
                    <div class="tab-header">
                        <span class="header-point" style="margin-right: 8px;"></span>
                        <span class="act-title" translate="register_1">注册即可免费使用</span>
                        <span class="act-img"></span>
                        <span class="act-title" translate="register_2">欢迎体验高效办公</span>
                        <span class="header-point"></span>
                    </div>
                    <p class="kingfile" translate="register_3">金山企业云盘注册</p>
                    <div class="regist-progress">
                        <div class="step active">1</div><hr /><div class="step active">2</div><hr /><div class="step active">3</div>
                    </div>
                    <div class="regist-progress-title"><p class="step-active" translate="register_4">注册账号</p><p class="step-active" style="text-align: center;" translate="register_5">配置信息</p><p class="step-active" style="text-align: right;" translate="register_6">注册成功</p></div>
                    <div class="reg-result-img"></div>
                    <p class="reg-result-t1" translate="register_11">恭喜，您已注册成功了！</p>
                    <p class="reg-result-t2" translate="register_12">新用户立享15天免费使用，快来体验吧！</p>
                    <p class="reg-result-t3"><span ng-bind="endtime"></span><span translate="register_13">s后自动进入金山企业云盘</span></p>
                    <button class="to-pan" ng-click="toPan()" translate="register_14">立即进入金山企业云盘</button>
                </div>

            </div>

        </div>
    </div>
</div>
{include file="../common/footer-register.tpl"}
</body>
</html>