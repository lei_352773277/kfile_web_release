<!DOCTYPE html>
<html lang="zh" ng-controller="homeController" id="root">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/netdisk.console.css">
    {literal}
        <style type="text/css">
            .ng-cloak, .x-ng-cloak, .ng-hide {
                display: none !important;
            }


        </style>
    {/literal}
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    <script>

    </script>
    <script>
        FileAPI = {
            //only one of jsPath or jsUrl.
            jsPath: '{$common}/',
            jsUrl: '{$common}/FileAPI.min.js',

            //only one of staticPath or flashUrl.
            staticPath: '{$common}/',
            flashUrl: '{$common}/FileAPI.flash.swf'
        }

    </script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>

    <title>{$paltform['sitename']}</title>
</head>
<body class="body">
{literal}
    <alert ng-repeat="alert in alerts" type="{{alert.type}}" class="messagebar" ng-cloak
           close="closeAlert($index)"><span ng-bind-html="alert.tipsText|to_trusted"></span></alert>
{/literal}
{literal}
    <div class="loading">
        <loading></loading>
        <span class="loading-txt" ng-bind="loading.tipsText"></span>
    </div>
{/literal}
{*<nav class="nav-kingfile clearfix ng-cloak">
        <div id="navbar" class="nav-content clearfix">
            <header-left></header-left>
            <header-right></header-right>
        </div>
</nav>*}
<div class="fly-main"  ng-view ui-view>
</div>
{literal}
<div  ngf-drop="openUpload($event)"
      ngf-select
      ng-model="filesupload"
      ngf-drag-over-class="'dragover'" ngf-multiple="true" ngf-allow-dir="true"
      class="drop-box" id="drop-box">
    <div class="drop-box-text" translate="drag_txt">拖动文件上传到当前目录下(只支持在谷歌浏览器下拖拽文件夹)</div>
</div>
{/literal}
</body>
</html>
