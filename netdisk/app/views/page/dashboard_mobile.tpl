<!DOCTYPE html>
<html lang="zh" ng-controller="homeMobileController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
    <meta name="description" content="金山企业云盘是金山云旗下的企业网盘产品，为中小企业提供最专业的文件存储与协同办公服务，具备文档集中管理、安全管控、全平台覆盖、远程办公等优势功能，全面助力企业信息化发展，提高企业员工工作效率。" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/netdisk.mobile.console.css">
    {literal}
        <style type="text/css">
            .ng-cloak, .x-ng-cloak, .ng-hide {
                display: none !important;
            }

        </style>
    {/literal}
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>

    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    <script>

    </script>
    <script>
        FileAPI = {
            //only one of jsPath or jsUrl.
            jsPath: '{$common}/',
            jsUrl: '{$common}/FileAPI.min.js',

            //only one of staticPath or flashUrl.
            staticPath: '{$common}/',
            flashUrl: '{$common}/FileAPI.flash.swf'
        }

    </script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
    <title>{$paltform['sitename']}</title>
</head>
<body class="innner-body body-sidebar-full" infinite-scroll="myPagingFunction()" infinite-scroll-disabled="scroll_switch"  >

<nav class="nav-top-mobile clearfix ng-cloak">
    <div class="sidebar-wrap pull-left">
        <sidebar-mobile-full-btn></sidebar-mobile-full-btn>
    </div>
    <div class="logo pull-left">
        <img ng-if="logotext!=''" ng-cloak class="ng-cloak" {literal}ng-src="{{logotext}}"{/literal}/>
        <img ng-if="logotext==''" class="ng-cloak" src="{$css}/styles/img/logo/logo-office.png"/>
    </div>
    <div class="down pull-right">
        {if $device_download_addr!='other'}
            <a class="btn-download pull-right" href="{{$device_download_addr}}">下载客户端</a>
        {else}
            <a class="btn-download pull-right" href="#" ng-click="openM()">下载客户端</a>
        {/if}
    </div>
    {literal}
        <alert ng-repeat="alert in alerts" type="{{alert.type}}" class="messagebar" ng-cloak
               close="closeAlert($index)"><span ng-bind-html="alert.tipsText|to_trusted"></span></alert>
    {/literal}
</nav>
{literal}
    <div class="loading">
      <loading></loading>
    </div>
{/literal}

{literal}
<div class="nav-mobile-left">
    <div class="info">
        <em class="icon"></em>
        <div class="name">{{userinfo.user_name}}</div>
    </div>
    <div class="sidebar-mobile">
        <ul class="nav-sidebar">
            <li ng-class="{true:'active'}[activeElem=='domainMobile']" ng-hide="userinfo.super_type==2">
                <a href="#/domainMobile">
                    <span class="glyphicon glyphicon-list"></span>
                    <span translate="nav_sidebar_domain">公司文件</span>
                </a>
            </li>
            <li ng-class="{true:'active'}[activeElem=='personMobile']" ng-if="userinfo.is_superuser==0 && userinfo.super_type==0">
                <a href="#/personMobile">
                    <span class="glyphicon glyphicon-briefcase"></span>
                    <span translate="nav_sidebar_person">个人文件</span>
                </a>
            </li>
            <li ng-class="{true:'active'}[activeElem=='chargeMobile']" ng-if="CURRUSER['productmodel']==2 && userinfo.is_superuser==1">
                <a href="#/chargeMobile">
                    <span class="glyphicon glyphicon-buy"></span>
                    <span translate="top_shop">购买</span>
                </a>
            </li>
        </ul>
    </div>
</div>
{/literal}
<div class="container-fluid inner-container"  ng-view ui-view>

</div>

{*<a href="/dashboard/pc">切换到PC版</a>*}
</html>
