<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$paltform['sitename']}-意见反馈</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >

	{if $fromMobile==1}
		<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/feedbackFromMobile.css">
		{else}
		<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/feedback.css">
	{/if}
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
     </script>
    <script src="{$common}/NotSupport.js"></script>
  	<![endif]-->
	{literal}
	<style>

	</style>
	{/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body  >
{include file="../common/header.tpl"}
<div class="feedback-wrap">
	<div class="container feedback">
		<div class="feedback-content">
			<div class="title" >
				<span class="text" >意见反馈</span>
			</div>
			<form action="/feedback/save" name="feedback" method="post">
				<h5>您的意见或建议是：</h5>
				<textarea name="content" placeholder="请在此输入您的建议和反馈，感谢！"></textarea>
				<div class="form-item">请留下您的联系方式，便于我们跟踪和改进，感谢！</div>
				<div class="form-item">
					<span>您的QQ：</span><input class="form-control" type="text" name="qq" ng-model="qq">
				</div>
				<div class="form-btn">
					<input type="submit" class="btn btn-lg btn-lg-custom" value="提交">
					<input type="hidden" class="btn btn-lg btn-lg-custom" name="from" value="{$from}">
				</div>
			</form>
		</div>
	</div>
</div>

{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>