<!DOCTYPE html>
<html lang="zh" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
	<meta name="description" content="金山企业云盘，100000+用户的选择，覆盖教育行业、科技行业、文娱广告、建筑设计、餐饮行业、医疗行业、律师行业、金融行业、汽车行业及政府事业等，以稳定优质的服务引领网盘市场" />
	<title>{$paltform['sitename']}-{if $industry=='edu'}教育行业案例
		{elseif $industry=='science'}科技行业案例
        {elseif $industry=='recreational'}文娱行业案例
        {elseif $industry=='build'}建筑设计案例
        {elseif $industry=='food'}餐饮旅游案例
        {elseif $industry=='health'}医疗行业案例
        {elseif $industry=='lawyer'}律师行业案例
        {elseif $industry=='bank'}金融行业案例
        {elseif $industry=='car'}汽车行业案例
        {elseif $industry=='gov'}政府事业案例{/if}</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/usercase.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
    {literal}
		<style>

		</style>
    {/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
        require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>

<body id="body" >
{include file="../common/header.tpl"}
<div class="banner banner-{$industry}">
	<div class="w1100 banner-content clearfix">
        {if $industry=='edu'}
			<div class="txt">
				<h1>教育行业</h1>
				<h2>走进学生心灵深处才有真正的教育，<br>
					金山企业云盘助您贴近学生</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
		{/if}
        {if $industry=='science'}
			<div class="txt txt-white-font">
				<h1>科技行业</h1>
				<h2>缩短沟通，你我用心演绎，体验科技魅力。<br>
					金山企业云盘魅力等你来发现。</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='recreational'}
			<div class="txt">
				<h1>文娱行业</h1>
				<h2>及时相应客户需求，金山企业云盘，<br>
					快速高效，拉近你和我！</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='build'}
			<div class="txt">
				<h1>建筑设计</h1>
				<h2>多画、多分析、多创意，<br>
					金山企业云盘为您保留每一个版本，见证您的成长。</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='food'}
			<div class="txt txt-white-font">
				<h1>餐饮行业</h1>
				<h2>金山企业云盘存储客户资料、派发活动宣传材料，<br>
					更多功能助您提供更多美食，客户满意是我们不懈追求的目标。</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='health'}
			<div class="txt">
				<h1>医疗行业</h1>
				<h2>搭建医疗云平台，<br>
					金山企业云盘，为医疗进步贡献力量！</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='lawyer'}
			<div class="txt txt-white-font">
				<h1>律师行业</h1>
				<h2>金山企业云盘，存储案例、及时响应客户，<br>
					助力所有辩护都通向正义！</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='bank'}
			<div class="txt">
				<h1>金融行业</h1>
				<h2>分类保存客户资料，<br>
					管理多类型项目，减少管理成本！</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='car'}
			<div class="txt txt-white-font">
				<h1>汽车行业</h1>
				<h2>有效管理经销商、<br>
					供应商及客户资料，提升汽车销售业绩</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
        {if $industry=='gov'}
			<div class="txt txt-white-font">
				<h1>政府事业</h1>
				<h2>我们始终坚持对产品和服务的极致追求，<br>
					选择金山企业云盘就是选择放心</h2>
				<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner">立即体验</a>
			</div>
        {/if}
	</div>
</div>
<div class="intro-tip w1100 clearfix">
    {if $industry=='edu'}
	<span class="tip-1">收批作业</span>
	<span class="tip-2">资料共享</span>
	<span class="tip-3">多校区协作</span>
	<span class="tip-4">集成教学系统</span>
    {/if}
    {if $industry=='science'}
		<span class="tip-1">资料共享</span>
		<span class="tip-2">移动办公</span>
		<span class="tip-3">集中管理</span>
		<span class="tip-4">安全保证</span>
    {/if}
    {if $industry=='recreational'}
		<span class="tip-1">快速响应</span>
		<span class="tip-2">跨国传输</span>
		<span class="tip-3">文件加密</span>
		<span class="tip-4">文件协作</span>
    {/if}
    {if $industry=='build'}
		<span class="tip-1">传输神速</span>
		<span class="tip-2">时时同步</span>
		<span class="tip-3">权限限制</span>
		<span class="tip-4">版本记录</span>
    {/if}
    {if $industry=='food'}
		<span class="tip-1">文件管理</span>
		<span class="tip-2">及时响应</span>
		<span class="tip-3">空间限制</span>
		<span class="tip-4">系统搭建</span>
    {/if}
    {if $industry=='health'}
		<span class="tip-1">安全保障</span>
		<span class="tip-2">多终端访问</span>
		<span class="tip-3">资料共享</span>
		<span class="tip-4">集成现有系统</span>
    {/if}
    {if $industry=='lawyer'}
		<span class="tip-1">安全保障</span>
		<span class="tip-2">及时响应</span>
		<span class="tip-3">在线编辑</span>
		<span class="tip-4">多终端访问</span>
    {/if}
    {if $industry=='bank'}
		<span class="tip-1">资料分发</span>
		<span class="tip-2">权限控制</span>
		<span class="tip-3">跨国传输</span>
		<span class="tip-4">存储备份</span>
    {/if}
    {if $industry=='car'}
		<span class="tip-1">快速响应</span>
		<span class="tip-2">权限控制</span>
		<span class="tip-3">跨国传输</span>
		<span class="tip-4">多终端访问</span>
    {/if}
    {if $industry=='gov'}
		<span class="tip-1">安全保障</span>
		<span class="tip-2">资料共享</span>
		<span class="tip-3">协同办公</span>
		<span class="tip-4">集成现有系统</span>
    {/if}
</div>
<div class="usercase-intro">
    {if $industry=='edu'}
	<div class="usercase-intro-item  w1100">
		<ul class="usercase-intro-item-{$industry}  clearfix">
			<li>
				<em >
					<span class="icon icon-1"></span>
				</em>
				<h3>教学互动，帮助学生提高学习成绩</h3>
				<p>
					学生随时查看老师课件，课堂上遗忘的要点随时补充回来；
					老师通过云端收取、批注学生作业，督促学生成绩进步；
					课件更新、作业批注后，学生可立刻收到消息通知
				</p>
			</li>
			<li class="pull-right">
				<em >
					<span class="icon icon-2"></span>
				</em>
				<h3>教师资料共享，帮助教师提高教学水平</h3>
				<p>
					教学资料汇总，帮助积累学校教学资源；
					科研资料共享，利于教师探讨教学经验；
					一个账号可在任何地方登录，无需随身携带教学资料
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-3"></span>
				</em>
				<h3>多校区协作，提升学校口碑</h3>
				<p>
					丰富的权限设置，保护科研资料；
					几十G的课件共享，一个链接即可多次分发，提高分发效率；
					中英文支持，与国际校区科研接轨，提升学校科研水平
				</p>
			</li>
			<li class="pull-right">
				<em >
					<span class="icon icon-4"></span>
				</em>
				<h3>集成教学系统，不影响教学习惯</h3>
				<p>
					学校组织结构集成，自动创建组织人员信息；
					自动同步备份教案、科研资料，自动共享课程；
					定制学校名称、logo，变成自己的教学云系统，提升学校形象
				</p>
			</li>
		</ul>
	</div>
    {/if}
    {if $industry=='science'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>内部资料共享，提升工作效率</h3>
					<p>
						产品需求共享给设计和研发工程师，需求有变更相关人员能立刻收到消息通知；
						研发出新的软件包共享给测试工程师，测试工程师直接下载测试；
						产品与销售共享，及时获取用户需求，提升产品体验
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>互联网文化理念，工作不限地点</h3>
					<p>
						无需随身携带存储设备，在家里使用账号登录即可工作，工作时时同步；
						技术资料共享，员工们可以使用移动端利用空闲时间随时充电；
						客户销售展示，一个链接即可到达
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>文档集中管理，积累企业文化知识</h3>
					<p>
						需求文档、设计文档、测试文档等文档众多，集中存放云端，提升IT资源利用率；
						历史版本追踪，大于100个历史保存，修改痕迹一览无余；
						文件备注，重要信息，修改信息一目了然，让文档会说话
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>文件安全保证，严防数据泄密</h3>
					<p>
						文档数据分块加密保存，AES256加密，理论破解需要一亿年；
						传输过程网银级别加密，保证数据传输安全；
						权限控制及操作审计，避免内部恶意操作
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='recreational'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>快速响应客户要求，提升业务效率</h3>
					<p>
						广告设计图、宣传策划方案，一个链接秒达客户那里，重视每一个客户；分割管理每一个客户的文件，客户管理不再出错；方案一有改动，立刻收到消息，快速响应不耽误；移动端支持，随时随地支持客户，响应客户要求。
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>中英文支持，跨国传输</h3>
					<p>
						支持中文和英文，轻松应对多国客户；全球拥有多个数据中心，跨国高速传输，快速稳定；
						任意分发、共享文件，全球客户不受影响。
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>多重文件加密，安全可靠</h3>
					<p>
						存储采用AES256加密技术，理论上破解需要1亿年；数据传输采用ssl网银级别加密，设计图、方案传输安全有保障；完善的数据备份和容灾机制，确保文件万无一失；提供数据自动备份、回收站误删恢复，详细日志查询、设置外链有效期及密码保护敏感文件，切实保障企业数据安全 。
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='build'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>设计源文件传输，更快的网盘<br>没有之一</h3>
					<p>
						传输设计图、勘察报表、测量报告等资料，高传输速度促成多家合作
						超多设计图样式也能同时快速传输，断点续传，保证传输质量，不做无用功
						秒传功能，大大提升图纸传输速度，促成企业业绩提升45%
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>本地云端时时同步<br>设计传输两不误</h3>
					<p>
						设计软件一般安装在本地，本地编辑后自动同步上传到云端
						更新后的设计图、勘察报表等自动同步到本地，查看编辑更省心
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>权限设置，不同项目不同的<br>设计人员，互不影响</h3>
					<p>
						权限控制，层层递减，支持多个项目并行，无权限人员看不到相关文件
						丰富权限设置，查看者、编辑者、操作者和管理者，高效限制
						按设计项目分类，按群组、人员设置权限，分配灵活
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>设计文稿版本记录<br>保证工作成果</h3>
					<p>
						多人交叉编辑设计文稿，记录每个文稿版本，理清混乱的文稿版本
						大于100份历史文稿痕迹记录，防止工作成果丢失
						对每版文稿进行备注，修改信息清楚明了，提高工作效率
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='food'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>文件管理</h3>
					<p>
						全国餐饮分店的账单、材料、数据集中保存，随时掌控全部信息；
						分类管理重要大客户，客户资料不在混淆；
						权限控制，合适的餐饮店人员看到合适的文件；
						厨师技术交流，分享当前最流行菜系，做法，提示厨师技术水平；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>及时响应客户</h3>
					<p>
						分类整理客户材料，更加了解每一个客户；
						准确记录每个客户姓名、爱好及电话，做到宾至如归；
						客户每一笔订单都记录在案，及时响应客户要求，小小餐饮也有大生意；
						分发电子菜单给客户，提前点餐不排队；
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>空间限制</h3>
					<p>
						限制员工个人空间大小，避免空间浪费；
						限制共享空间大小，避免员工私自挪用；
						限制员工根目录创建，避免文件混乱；
						批量更改个人空间、公司空间，灵活配置，体恤员工；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>系统搭建</h3>
					<p>
						ldap域接入，一键导入用户，省去构建组织架构步骤；
						域用户白名单，支持域中部分用户访问；
						定制餐饮系统logo、名称，变声自己的云系统，提升形象；
						接入原有餐饮系统，节省IT资源，节省成本；
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='health'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>安全保障，保证患者信息</h3>
					<p>
						分块存储，存储加密，确保医患机密数据安全；
						网银级别传输加密，数据传输过程安全保证，防止攻击；
						多层级权限管理，避免患者资料数据泄露；
						操作日志审计，避免患者资料被恶意更改；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>多终端访问</h3>
					<p>
						无时间地点限制，随时浏览患者信息；
						全平台支持，随时上传存储患者信息、医疗资料、患者视频；
						在外急救，使用移动端调取患者资料，了解患者疾病历史；
						疑难杂症研讨会上，使用移动端随时记录讨论心得；
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>资料共享</h3>
					<p>
						医疗案例共享，积累案例，提升医生水平；
						行业资料共享，紧跟最新医疗技术，救治更多病人；
						合作伙伴分享资料，采购最新医疗设备；
						患者资料共享，清楚了解患者的发病历史，治疗更精确；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>集成现有系统</h3>
					<p>
						自动同步医院现有组织结构和人员信息，快速搭建云平台；
						定制医院级面、logo、名称，提升医院形象；
						集成现有存储资源，完善系统信息；
						区分不同科室对文件的访问权限，搭建医疗平台；
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='lawyer'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>安全保障，保证原告信息</h3>
					<p>
						分块存储，存储加密，确保原告机密数据安全；
						网银级别传输加密，数据传输过程安全保证，防止攻击；
						多层级权限管理，避免原告资料数据泄露；
						操作日志审计，避免原告资料被恶意更改；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>及时响应</h3>
					<p>
						法律文书、起诉书等，一个链接秒达原告那里，重视每一次辩护；
						分割管理每一次案件信息，案件管理不再出错；
						文件证据一有更新，立刻收到消息，快速响应不耽误；
						移动端支持，随时调查取证、摘录案卷材料。
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>在线编辑</h3>
					<p>
						收发、整理和编辑文件档案资料；
						文件锁定，正在更改的法律文书不受影响；
						在线编辑，一边编辑一边保存，随时与当事人互动；
						几个律师一起编辑起诉书，研讨案件情况；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>多终端访问</h3>
					<p>
						拍照上传，随时上传调查出来的证据；
						全终端支持，出庭时做开庭记录,展示文件证据；
						在外调查时，随时上传谈话记录,当事人声明书；
						负责多个案件时，随时了解案件进展，摘录案卷材料；
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='bank'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>资料分发，帮助客户改善业绩</h3>
					<p>
						与团队共同参与项目工作，项目资料及时分发；
						分发给客户相关的财务报表，一个链接及时到达，提高业绩；
						典型项目共享，积累项目经验
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>权限控制，关键文件给关键的人看</h3>
					<p>
						不同的项目仅能让项目相关人员查看，管控项目；
						不同资历的团队管理不同的项目，合适的人看合适的项目；
						7种权限层层管理，权限不断收敛，便于管控
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>跨国传输，国外客户也能提供最佳服务</h3>
					<p>
						支持中文和英文，轻松应对国际税务；
						全球拥有多个数据中心，跨国高速传输，快速稳定；
						任意分发、共享文件，客户的跨境交易不受影响
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>存储备份，避免客户资料损坏丢失</h3>
					<p>
						咨询报告、税务表、财务交易表及审计等相关的材料一键备份到云端；
						金山企业云盘同步盘，时时自动同步，windows操作习惯，更了解你；
						office插件应用，本地文件一键保存到云端，方便快捷；
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='car'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>快速响应，提升业务效率</h3>
					<p>
						最新汽车介绍，一个链接秒达客户那里，重视每一个客户；
						统一管理全国销售报表，不在收取大量分散报表；
						报表一有改动，立刻收到消息，快速响应不耽误；
						移动端支持，随时随地支持客户，响应客户要求；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>权限控制，关键文件给关键的人看</h3>
					<p>
						不同的4S店销售报表仅能让相关人员查看，管控相关人员；
						按照客户分类管理汽车信息，合适的人看合适的汽车；
						7种权限层层管理，权限不断收敛，便于管控；
						将工作文档 、培训资料、巡店图片分权限管理，安全有保障；
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>跨国传输，国外客户也能提供最佳服务</h3>
					<p>
						支持中文和英文，轻松应对进口汽车资料共享；
						全球拥有多个数据中心，跨国传输速度达到M级/s，快速稳定；
						任意分发、共享文件，全球客户不受影响
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>多终端访问，随时随地服务客户</h3>
					<p>
						移动端拍照上传客户资料，意向表等信息，便于管理；
						金山企业云盘同步盘，时时自动同步，windows操作习惯，更了解你；
						office插件应用，本地文件一键保存到云端，方便快捷；
						Mac和iPad支持，随时向客户进行销售展示
					</p>
				</li>
			</ul>
		</div>
    {/if}
    {if $industry=='gov'}
		<div class="usercase-intro-item  w1100">
			<ul class="usercase-intro-item-{$industry}  clearfix">
				<li>
					<em >
						<span class="icon icon-1"></span>
					</em>
					<h3>安全保障，保证数据信息</h3>
					<p>
						分块存储，存储加密，确保报刊机密数据安全；
						网银级别传输加密，数据传输过程安全保证，防止攻击；
						多层级权限管理，避免未发布报刊数据泄露；
						操作日志审计，避免已审计报刊被恶意更改；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-2"></span>
					</em>
					<h3>资料共享</h3>
					<p>
						已发布报刊共享，积累报刊，见证报刊历史；
						行业资料共享，紧跟时代趋势，关注民生大事；
						合作伙伴材料共享，广告资料全都合理管理；
						印刷厂资料共享，一个链接发送，轻松印刷；
					</p>
				</li>
				<li>
					<em >
						<span class="icon icon-3"></span>
					</em>
					<h3>协同办公</h3>
					<p>
						多人编辑一份报刊，在线编辑，直接上传
						权限控制，不同人看到的内容不同，控制传播范围；
						文件锁定，当报刊进行编辑时，锁定仅能自己编辑，防止混乱；
						报刊版本记录，超100份历史版本，随时追溯；
					</p>
				</li>
				<li >
					<em >
						<span class="icon icon-4"></span>
					</em>
					<h3>集成现有系统</h3>
					<p>
						自动同步党校报刊部现有组织结构和人员信息；
						定制报刊部专属页面，提升形象
						集成现有存储资源，完善系统信息；
					</p>
				</li>
			</ul>
		</div>
    {/if}
</div>

<div class="usercase-cases-list w1100 clearfix">
    {if $industry=='edu'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em><span class="tip-3"></span></em>
			<em class="last"><span class="tip-4"></span></em>
		</div>
		<div class="usercase-cases-list-{$industry} m40 clearfix">
			<em><span class="tip-5"></span></em>
			<em ><span class="tip-6"></span></em>
		</div>
    {/if}
    {if $industry=='science'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em><span class="tip-3"></span></em>
			<em class="last"><span class="tip-4"></span></em>
		</div>
		<div class="usercase-cases-list-{$industry} m40 clearfix">
			<em><span class="tip-5"></span></em>
			<em ><span class="tip-6"></span></em>
		</div>
    {/if}
    {if $industry=='recreational'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em><span class="tip-3"></span></em>
			<em class="last"><span class="tip-4"></span></em>
		</div>
    {/if}
    {if $industry=='build'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em ><span class="tip-4"></span></em>
			<em class="last"><span class="tip-3"></span></em>
		</div>
    {/if}
    {if $industry=='food'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em  my-tooltip-usercase  my-tooltip-usercase-type="food1">
				<span class="tip-3"></span>
				<my-tooltip-usercase-template-food1></my-tooltip-usercase-template-food1>
			</em>

		</div>
    {/if}
    {if $industry=='health'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em  my-tooltip-usercase  my-tooltip-usercase-type="health1">
				<span class="tip-4"></span>
				<my-tooltip-usercase-template-health1></my-tooltip-usercase-template-health1>
			</em>

			<em><span class="tip-3"></span></em>
			<em><span class="tip-1"></span></em>
			<em  class="last"><span class="tip-2"></span></em>

		</div>
    {/if}
    {if $industry=='lawyer'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em><span class="tip-3"></span></em>
			<em class="last"><span class="tip-4"></span></em>
		</div>
    {/if}
    {if $industry=='bank'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em><span class="tip-3"></span></em>
			<em class="last"><span class="tip-4"></span></em>
		</div>
		<div class="usercase-cases-list-{$industry} m40 clearfix">
			<em><span class="tip-5"></span></em>
		</div>
    {/if}
    {if $industry=='car'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
			<em><span class="tip-2"></span></em>
			<em  my-tooltip-usercase  my-tooltip-usercase-type="car1">
				<span class="tip-3"></span>
				<my-tooltip-usercase-template-car1></my-tooltip-usercase-template-car1>
			</em>
		</div>
    {/if}
    {if $industry=='gov'}
		<div class="usercase-cases-list-{$industry} clearfix">
			<em><span class="tip-1"></span></em>
		</div>
    {/if}
</div>




{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>