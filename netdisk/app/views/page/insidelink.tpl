<!DOCTYPE html>
<html lang="zh" ng-controller="sController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>金山企业云盘</title>
    <meta name="keywords" content="金山企业网盘，网盘，同步盘、一体机，私有云，公有云，协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP、NAS、SaaS" />
    <meta name="description" content="金山企业云盘外链、外链内容（发布时间、名称等）、下载、金山企业云盘-企业云存储平台、协作办公工具、文件共享助手" />

    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/netdisk.console.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
    <title>{$title}</title>
</head>
<body id="body">
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">金山企业云盘</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div>

    </div>

</nav>

<div class="container-fluid">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-default swrap">
            {if $status==1}
                <div class="panel-body">
                    <div class="iconwrap">

                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;"
                                   translate="mod_insiledlink_empty">抱歉，您访问的内链不存在或者已经被删除了！</p>
                            </div>

                        </form>
                    </div>
                </div>
            {elseif $status==2}
                <div class="panel-body">
                    <div class="iconwrap">

                        <form class="form-inline">
                            <div class="form-group">
                                <div><span class="glyphicon glyphicon-exclamation-sign"></span></div>
                                <p style="color:#aaa;font-size:25px;padding-top: 20px;"
                                   translate="mod_insiledlink_valid">抱歉，您访问的内链权限不足！</p>
                            </div>

                        </form>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>

</body>
</html>
