<!DOCTYPE html>
<html lang="zh" ng-controller="loginController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {*<title>{$paltform['sitename']}</title>*}
    <title>网盘登录</title>
    <meta name="keywords" content="飞鸟云盘、企业网盘、容灾备份、SaaS、权限管控、日志追溯、在线编辑"/>
    <meta name="description" content="企业成员登录、管理员登录  企业标示，用户名，密码、忘记密码、注册"/>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/login.css">
    <script>
        /*var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();*/
    </script>
    <!--[if lt IE 9]>
    <script type="text/javascript">
        var PRODUCT = '网盘登录';
    </script>
    <script src="{$common}/NotSupport.js"></script>
    <![endif]-->
    {literal}
        <style>

        </style>
    {/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body ng-cloak class="ng-cloak login-page">
<div class="header header-other">
    {include file="../common/header.tpl"}
</div>

{*<div id="container" class="container-wrap-login-mobile hidden">
    <div class="login">
        <div ng-show="selected==1" class="login-sub-account">
            <div class="title">企业成员登录</div>
            {include file="../common/login-sub-account.tpl" title="子帐号登录" }
        </div>
        <div ng-show="selected==2" class="login-admin-account">
            <div class="title">管理员登录</div>
            <iframe style="width:100%;padding-left:2.5%; padding-right:2.5%; " id="login-iframe" name="login-iframe"
                    frameborder="0" height="200px"
                    scrolling="no" src="https://passport.ksyun.com/iframe-pan-login.html">
            </iframe>
        </div>
        <div class="tab">
			<span ng-show="selected==1" ng-click="tabLogin(2);">
				切换到管理员登录
				<span class="icon"></span>
			</span>
            <span ng-show="selected==2" ng-click="tabLogin(1);">切换到企业成员登录<span class="icon"></span></span>
        </div>
    </div>
</div>*}

<div id="container" class="login-box">
    <div class="container">
        <div class="login-wrap">
            <div  class="login-form-parent">
                {include file="../common/login-sub-account.tpl" }
            </div>
        </div>
    </div>
    <div class="tip">
        <p>为了数据安全，如您输入5次错误密码，账号将会被锁定，请您知悉</p>
    </div>
</div>

{include file="../common/footer.tpl"}

</body>
</html>