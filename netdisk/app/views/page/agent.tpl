<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
	<meta name="description" content="金山企业云盘是金山云旗下的企业网盘产品，为中小企业提供最专业的文件存储与协同办公服务，具备文档集中管理、安全管控、全平台覆盖、远程办公等优势功能，全面助力企业信息化发展，提高企业员工工作效率。" />
	<title>{$paltform['sitename']}-代理合作</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/agent.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
     </script>
    <script src="{$common}/NotSupport.js"></script>
  	<![endif]-->
	{literal}
	<style>

	</style>
	{/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body  >
{include file="../common/header.tpl"}
<div class="banner">
	<div class="w1100">
		<div class="txt">
			<h1>我们期待和你携手共赢</h1>
			<h2>加入金山云，与我们一起共创云时代</h2>
		</div>
	</div>
</div>

<div class="agent-intro">
	<div class="agent-intro-item agent-intro-item-1 w1100">
		<h1>长远的支持</h1>
		{*<p>
			长远的合作承诺。<br>
			云存储业务已经成为金山公司的核心发展业务，为此金山云将对授权合作伙伴提供从市场、销售、培训等更全面的支持。<br>
			金山公司将用自身的资源协助授权合作伙伴不断壮大，不断提升其在业内的地位。<br>
			金山云也会帮助这些中小型授权合作伙伴发现更多的市场机会。<br>
			一对一的渠道经理实地工作配合扶持，在技术、培训、销售等方面为其提供支持。<br>
		</p>*}
		<ul class="agent-intro-list clearfix">
			<li>
				长远的合作承诺。
			</li>
			<li>
				云存储业务已经成为金山公司的核心发展业务，为此金山云将对授权合作伙伴提供从市场、销售、培训等更全面的支持。
			</li>
			<li>
				金山公司将用自身的资源协助授权合作伙伴不断壮大，不断提升其在业内的地位。
			</li>
			<li>
				金山云也会帮助这些中小型授权合作伙伴发现更多的市场机会。
			</li>
			<li>
				一对一的渠道经理实地工作配合扶持，在技术、培训、销售等方面为其提供支持。
			</li>
		</ul>
	</div>
	<div class="agent-intro-item agent-intro-item-2 w1100 clearfix">
			<h1>完善的职责</h1>
			{*<p>
				金山云将授权合作伙伴的职责定位在为最终用户提供管理便捷，安全无忧的金山企业云服务的销售推广。<br>
				授权合作伙伴为最终用户提供金山企业云推广和提高客户满意度。
			</p>*}
			<ul class="agent-intro-list clearfix">
				<li>
					金山云将授权合作伙伴的职责定位在为最终用户提供管理便捷，安全无忧的金山企业云服务的销售推广。
				</li>
				<li>
					授权合作伙伴为最终用户提供金山企业云推广和提高客户满意度。
				</li>
			</ul>
	</div>
	<div class="agent-intro-item agent-intro-item-3 w1100">
		<h1>合作双赢</h1>
		{*<p>
			通过加盟金山云授权合作伙伴，将为授权合作伙伴带来更多的商业机会和发展空间，帮助授权合作伙伴赢得客户的信赖和得到理想的商业回报。<br>
			作为金山云授权合作伙伴，您将拥有金山云长远的承诺和来自金山办公产品、技术与培训、市场推广等全方位的支持。<br>
			金山公司作为国内领先的应用软件产品和服务供应商，以其领先的产品，完善的服务，成为各行各业人所公认的最佳合作伙伴。相信我们的合作，将有助于双方的共同发展！
		</p>*}
        <ul class="agent-intro-list clearfix">
            <li>
				通过加盟金山云授权合作伙伴，将为授权合作伙伴带来更多的商业机会和发展空间，帮助授权合作伙伴赢得客户的信赖和得到理想的商业回报。
            </li>
            <li>
				作为金山云授权合作伙伴，您将拥有金山云长远的承诺和来自金山办公产品、技术与培训、市场推广等全方位的支持。
            </li>
			<li>
				金山公司作为国内领先的应用软件产品和服务供应商，以其领先的产品，完善的服务，成为各行各业人所公认的最佳合作伙伴。相信我们的合作，将有助于双方的共同发展！
			</li>
        </ul>
	</div>
</div>
<div class="agent-intro-contact w1100">
	<div class="btn-intro-lg">招商热线：010-62927777-5008</div>
    <h3>如果您有关于代理方面的任何疑问，请您致电010-62927777-5008或发邮件kscxs@kingsoft.com进行询问。</h3>
</div>

<div class="agent-intro-usercase w1100">
	<div class="agent-intro-usercase-title">
		<h1>他们都已加入金山云</h1>
	</div>
	<ul class="clearfix">
		<li>
			<em class="icon icon-1"></em>
			<h2>珠海市金方达</h2>
			<h3>珠海市金方达科技有限公司</h3>
		</li>
		<li>
			<em class="icon icon-2"></em>
			<h2>江苏宏信</h2>
			<h3>江苏宏信电子科技有限公司</h3>
		</li>
		<li class="last">
			<em class="icon icon-6"></em>
			<h2>志远天辰科技</h2>
			<h3>北京志远天辰科技有限公司</h3>
		</li>
	</ul>

</div>



{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>