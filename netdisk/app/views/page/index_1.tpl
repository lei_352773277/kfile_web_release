<!DOCTYPE html>
<html lang="zh"  ng-controller="loginController">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$paltform['sitename']}</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/index1.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	{literal}
		<style>

		</style>
	{/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

	{/if}
	<script>
		require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>
<body  ng-cloak class="ng-cloak" >
<div class="header">
	<div class="logo" style="font-size: 28px;width: 960px;margin: 0 auto 0px;color: #1899eb;padding-top: 10px;">
		{$paltform['sitename']}
	</div>
	<div class="language index-language" >
		{literal}
			<div class="btn-group" data-ng-class="{open:openDropDown}">

				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-ng-click="openDropDown=!openDropDown">
					<span class="glyphicon glyphicon-align-justify"></span>
					{{'lang'|translate}}
				</button>
				<ul class="dropdown-menu">
					<li>
						<a href="javascript:void(0);" ng-click="openDropDown=!openDropDown;changeLanguage('cn');">
							<i class="cn"></i>{{'cn'|translate}}
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" ng-click="openDropDown=!openDropDown;changeLanguage('en');">
							<i class="en"></i>{{'en'|translate}}
						</a>
					</li>
				</ul>
			</div>
		{/literal}
	</div>
</div>
<div id="container" class="index">
	<span class="cloud-wave-1">漂浮的云</span>
	<span class="cloud-wave-2">漂浮的云</span>
	<span class="cloud-wave-3">漂浮的云</span>

	<div class="container   index-main ">
		<div class="row ">
			<div class="col-md-12">
				<span class="cloudTop ">云</span>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="hidden-xs col-sm-7 col-md-8">
						<div class="index-main-bg"></div>
					</div>
					<div class="col-sm-5 col-md-4 clearfix ">
						<div class="login col-xs-12 " >
							<span class="center-block" style="font-size: 28px;text-align: center;margin-bottom: 40px;margin-top: 40px;color: #56c1ec;font-weight: 500;" translate="index_login">
									登&nbsp;$nbsp;录
							</span>
							<form class="form-horizontal" name="loginform" ng-submit="login($event)">

								{if $platform['producttype'] != 1}
									<div class="form-group form-group-login" ng-class="{ 'has-error' : loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !='' }">
										<label for="domainIdent" class="sr-only"></label>
										<div class="input-group-login center-block">
											<span class="login-icon icon-domain"></span>
											<span class="icon-sep">&nbsp;</span>
											<input type="text" tabindex="1" class="form-control login-ipt" id="domainIdent" autocomplete="off" aria-describedby="inputGroupSuccess1Status" name="domainIdent" ng-model="domainIdent" {literal}placeholder="{{'index_login_place_filed'|translate}}"{/literal} required>
										</div>
										{*<p ng-cloak class="ng-cloak help-block" style="position:absolute;right:-90px;top:4px;" ng-show="loginform.domainIdent.$invalid && !loginform.domainIdent.$pristine  && loginform.domainIdent !=''">请输入企业域</p>*}
									</div>
								{/if}
								<div class="form-group form-group-login" ng-class="{ 'has-error' : loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !='' }">
									<label for="inputEmail" class="sr-only"></label>
									<div class="input-group-login center-block">
										<span class="login-icon icon-user"></span>
										<span class="icon-sep">&nbsp;</span>
										<input type="text" tabindex="2" class="form-control login-ipt" id="user" autocomplete="off" aria-autocomplete="none" aria-describedby="inputGroupSuccess1Status" name="user" ng-model="user" {literal}placeholder="{{'index_login_place_name'|translate}}"{/literal} placeholderKey="index_login_place_name" required>
									</div>

									{*<p ng-cloak class="ng-cloak help-block" style="position:absolute;right:-90px;top:4px;" ng-show="loginform.user.$invalid && !loginform.user.$pristine  && loginform.user !=''">请输入您的账号</p>*}
								</div>
								<div class="form-group form-group-login" ng-class="{ 'has-error' : loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''}">
									<label for="inputPassword" class="sr-only"></label>
									<div class="input-group-login center-block">
										<span class="login-icon icon-pwd"></span>
										<span class="icon-sep">&nbsp;</span>
										<input type="text" tabindex="3"   onfocus="this.type='password';" class="form-control login-ipt" id="password" autocomplete="off" aria-describedby="inputGroupSuccess1Status" name="password" ng-model="password" {literal}placeholder="{{'index_login_place_pwd'|translate}}"{/literal} placeholderKey="index_login_place_pwd" required>
									</div>
									{*<p ng-cloak class="ng-cloak help-block"  style="position:absolute;right:-130px;top:4px;" ng-show="loginform.password.$invalid && !loginform.password.$pristine && loginform.password !=''">请输入正确格式的密码</p>*}

								</div>

								<div class="form-group  form-group-login">
									<label  class="sr-only">&nbsp;</label>
									<div class="input-group-login center-block">

										{literal}
											<button tabindex="4"  type="submit" ng-disabled="loging" class="btn btn-primary btn-login">{{loginbtn}}</button>
										{/literal}
										{literal}
											<p ng-cloak class="ng-cloak text-danger text-error" ng-if="loginErrorMsg!=''" class="help-block">{{loginErrorMsg}}</p>
										{/literal}
										{if $paltform['sendemailurl'] != ''}
											<div class="login-forget" style="margin-top: 20px;">
												<hr class="hr"/>
												<span class="login-forget-text">
													<a href="{$base_url}account/forget" translate="index_login_forgetPwd">忘记密码</a>
												</span>
												<hr class="hr"/>
											</div>
										{/if}

									</div>
								</div>
								{if $paltform['https']}
									<div class="form-group form-group-login">
										<label  class="sr-only ">&nbsp;</label>
										<div ng-show='ishttp' class="login-help input-group-login center-block">

											<span translate="index_login_https_info_1">提示：开启加密传输功能，您需要先安装根证书，</span>
											<a href="{$paltform['web_crt']}" style="color:#aaa;font-weight:bold;" translate="index_login_https_down">下载证书</a>，
											<a href="{$paltform['web_crt_help']}" style="color:#aaa;font-weight:bold;" translate="index_login_https_help">查看帮助文档</a>
										</div>
									</div>
								{/if}

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container download-content">
	<div class="row">
		{*<div class="col-sm-2  download-item">
			<span class="sep sep-left"></span>
			<a class="download-item-a " href="{$paltform['client_windows']}" {literal}title="{{'index_login_down_win'|translate}}" {/literal}>
				<i class="download-icon download-icon-windows center-block"></i>
				<span class="download-text">Windows</span>
			</a>
			<span class="sep"></span>
		</div>*}
		<div class="col-sm-2  download-item">
			<span class="sep sep-left"></span>
			<a class="download-item-a " href="{$paltform['client_win_sync']}" {literal}title="{{'index_login_down_win_sync'|translate}}" {/literal}>
				<i class="download-icon download-icon-windows-sync center-block"></i>
				<span class="download-text">Windows同步盘</span>
			</a>
			<span class="sep"></span>
		</div>
		<div class="col-sm-2  download-item ">

			<a class="download-item-a " href="{$paltform['client_android']}" {literal}title="{{'index_login_down_android'|translate}}" {/literal}>
				<i class="download-icon download-icon-android center-block"></i>
				<span class="download-text">Android</span>
			</a>
			<span class="sep"></span>
		</div>
		<div class="col-sm-2  download-item">
			<a class="download-item-a " href="{$paltform['client_iphone']}" {literal}title="{{'index_login_down_iPhone'|translate}}" {/literal}>
				{*<img src="/dist/common/i/iphone.png" style="width:35px;height:50px" />*}
				<i class="download-icon download-icon-iphone center-block"></i>
				<span class="download-text">iPhone</span>
			</a>
			<span class="sep"></span>
		</div>
		<div class="col-sm-2  download-item">
			<a class="download-item-a " href="{$paltform['client_iphone']}" {literal}title="{{'index_login_down_iPad'|translate}}" {/literal}>
				{*<img src="/dist/common/i/iphone.png" style="width:35px;height:50px" />*}
				<i class="download-icon download-icon-ipad center-block"></i>
				<span class="download-text">iPad</span>
			</a>
			<span class="sep"></span>
		</div>

		<div class="col-sm-4 download-item">
			<a class="download-item-a download-item-a-erweima" href="#">
				<img src="{$paltform['client_windows_sync']}" />
				<span class="download-text" translate="index_login_down_Scan">扫描下载客户端</span>
			</a>
			<span class="sep"></span>
		</div>

	</div>
</div>

</body>
</html>