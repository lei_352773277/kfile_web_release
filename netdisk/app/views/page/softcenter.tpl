<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$paltform['sitename']}-应用中心</title>
	<link rel="stylesheet" type="text/css" href="{$css}/styles/softcenter/softcenter.css"/>
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <script src="{$common}/jquery.min.js" type="text/javascript"></script>
</head>
<body>
<div class="main">
	<!--<iframe name="test" src=""></iframe>-->
	<div class="title">应用中心</div>
	<table class="table table-hover">
		<tbody>
		<tr>
			<td class="icon icon-office">
				<span style="background: url('http://kss.ksyun.com/s.ksyun.com/Pictures/sync1.png') no-repeat 5px center;"></span>
			</td>
			<td class="text">
				<i>Windows同步盘</i>
				<span>同步盘诞生了！本地与云端自动同步，离线办公等等统统可以搞定，快来试试吧。</span></td>
			<td>16.6MB</td>
			<td><a href="{$paltform['client_win_sync']}" target="_blank">立即下载</a></td>
		</tr>
		<tr>
			<td class="icon icon-office">
				<span></span>
			</td>
			<td class="text">
				<i>office插件</i>
				<span>office和WPS中预览编辑云端文件，同时兼具Outlook插件，真正的办公神器在这里。</span></td>
			<td>16.2MB</td>
			<td><a href="{$paltform['client_office']}" target="_blank">立即下载</a></td>
		</tr>
		<tr>
			<td class="icon icon-apple"><span></span></td>
			<td class="text">
				<i>iOS端</i>
				<span>金山企业云盘全新的iOS端上线了，随时随地查看文件，走上人生高峰。</span></td>
			<td>15.3MB</td>
			<td><a href="javascript:void(0)" onclick='openScan("apple")'>手机扫描</a></td>
		</tr>
		<tr>
			<td class="icon icon-android"><span></span></td>
			<td class="text">
				<i>Android端</i>
				<span>金山企业云盘Android端来了，上传、下载、外链等等，工作生活两不误。</span></td>
			<td>1.8MB</td>
			<td><a href="javascript:void(0)" onclick='openScan("android")'>手机扫描</a></td>
		</tr>
		<tr>
			<td class="icon icon-ipad"><span style="background: url('http://kss.ksyun.com/s.ksyun.com/Pictures/ipad.png') no-repeat 7px center;"></span></td>
			<td class="text">
				<i>iPad端</i>
				<span>金山企业云盘iPad端新鲜出炉了，给客户演示再也不用拿死沉死沉的电脑了。</span></td>
			<td>15.3MB</td>
			<td><a href="javascript:void(0)" onclick='openScan("apple")'>手机扫描</a></td>
		</tr>
		</tbody>
	</table>
</div>
<div class="backdrop hide"></div>
<div class="modal hide">
	<div class="modal-content">
		<div class="scan">
			<img src="{$paltform['client_windows_sync']}" />
		</div>
		<div>手机扫描二维码，可迅速下载</div>
		<div><a class="close" href="javascript:void(0)" onclick="closeModal()">关闭</a></div>
	</div>
</div>
<script type="text/javascript">

	var tip1 = false;
	$(function () {
		$(".text span").hover(function (event) {
			//event.stopPropagation();
			//event.preventDefault();
			/*if (tip1) {
			 return;
			 }*/
			var pos = $(this).position();
			var top = pos.top;
			var left = pos.left;
			var width = $(this).outerWidth();

			var tip = "<div class='tip'>" + $(this).text() + "</div>";

			$("body").append(tip);
			$(".tip").css({
				width: width,
				position: "absolute",
				top: top + 10,
				left: left + 10

			});
			//tip1 = true;

		}, function () {
			$(".tip").remove();
			//tip1 = false;
		});

	});
	/*模态框*/
	function openScan(type) {
		modal("show", type);
	}
	function closeModal() {
		modal("hide");
	}
	function modal(isShow, type) {
		if (isShow == "show"){
			$(".backdrop").show();
			$(".modal").show();
			var bodyWidth = $("body").outerWidth();
			var modalWidth = $(".modal").outerWidth();
			var left = (bodyWidth - modalWidth) / 2;
			$(".modal").css({
				left: left
			})
		}
		else{
			$(".backdrop").hide();
			$(".modal").hide();
		}
	}

	/*function modal(isShow, type) {
	 if (isShow == "show") {
	 var backdrop = '<div class="backdrop"></div>';

	 var modal = '<div class="modal">'
	 + '<div class="modal-content">'
	 + '<div class="scan-' + type + '"></div>'
	 + '<div>手机扫描二维码，可迅速下载到手机里</div>'
	 + '<div><a class="close" href="javascript:void(0)" onclick="closeModal()">关闭</a></div>'
	 + '</div>'
	 + '</div>';
	 $("body").append(backdrop);
	 $("body").append(modal);
	 var bodyWidth = $("body").outerWidth();
	 var modalWidth = $(".modal").outerWidth();
	 var left = (bodyWidth - modalWidth) / 2;
	 $(".modal").css({
	 left: left
	 })

	 }
	 if (isShow == "hide") {
	 $(".backdrop").remove();
	 $(".modal").remove();
	 }

	 }*/


	//
	jQuery.fn.extend({
		modal: function (options) {
			return this(function () {
				var backdrop = '<div class="backdrop"></div>';
				var show = options.show;
			})
		}
	})
</script>
</body>
</html>