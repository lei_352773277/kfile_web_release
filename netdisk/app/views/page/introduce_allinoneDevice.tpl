<!DOCTYPE html>
<html lang="zh" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="服务器、金山企业云盘、企业网盘、文件管理、云存储、NAS" />
	<meta name="description" content="金山企业云盘一体机，金山企业云盘软件+高性能服务器，一体化服务，助你既享受到私有云般的私密安全，又享受到公有云般便利，只需一根网线，简单配置即可体验云端服务，快来咨询吧：400-028-9900 " />
	<title>金山企业云盘一体机</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/introduce.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '金山企业云盘一体机';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	{literal}
		<style>

		</style>
	{/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

	{/if}
	<script>
		require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>

<body id="body">
{include file="../common/header.tpl"}

<div class="banner banner-allinone">
	<div class="w1100">
		<div class="txt">
			<h1>金山企业云盘一体机，时刻在此，等你！</h1>
			<h2>购买即享三重豪礼：免费送货豪礼；免费赠送人员数量；免费专业技术追踪</h2>
			<h3>立即联系购买：010-62927777-5008</h3>
		</div>
	</div>
</div>

<div class="allinone-intro">
	<div class="allinone-intro-item w1100">
		<h1>金山企业云盘一体机</h1>
		<ul class="allinone-intro-list clearfix">
			<li>
				<em class="allinone-intro-icon allinone-intro-icon-1"></em>
				<h3>颠覆私有云搭建模式，大智慧才能更简洁</h3>
				<p>
					金山企业云盘软件系统与优质服务器结合，共同为您<br>
					搭建专属私有云平台，省去服务器采购、搭建等流程<br>
					一根网线即插即用，私有云搭建，就是如此简单！
				</p>
			</li>
			<li>
				<em class="allinone-intro-icon allinone-intro-icon-2"></em>
				<h3>超静音、无繁琐、办公，更懂你</h3>
				<p>
					无需购买搭建服务器，无需专门运维人员，<br>
					更无需专门机房，节省成本<br>
					低分贝，超静音，可以放在办公室和家里，<br>
					我始终坚持，默默为您服务！
				</p>
			</li>
		</ul>
	</div>
	<div class="allinone-intro-contact w1100 m50">
		<div class="btn-intro-lg">立即联系购买：010-62927777-5008</div>
	</div>

	<div class="allinone-intro-item w1100 m50">
		<dl class="clearfix">
			<dt class="pull-left">
				<img src="{$css}/styles/img/product/allinone-img-1.jpg" alt="塔式一体机">
			</dt>
			<dd class="pull-right">
				<h1>塔式一体机配置详情</h1>
				<div class="m50">
					<img src="{$css}/styles/img/product/allinone-img-3.jpg" alt="塔式一体机配置详情">
				</div>
			</dd>
		</dl>
	</div>
	<div class="allinone-intro-item w1100 m50">
		<dl class="clearfix">
			<dt class="pull-right">
				<img src="{$css}/styles/img/product/allinone-img-2.jpg" alt="机架式一体机">
			</dt>
			<dd class="pull-left">
				<h1>机架式一体机配置详情</h1>
				<div class="m50">
					<img src="{$css}/styles/img/product/allinone-img-4.jpg" alt="机架式一体机配置详情">
				</div>
			</dd>
		</dl>
	</div>
</div>
<div class="intro-tip w1100 clearfix">
	<span class="private-intro-tip-1">知名厂商，品质保证</span>
	<span class="private-intro-tip-2">企业私有，私密性强</span>
	<span class="private-intro-tip-3">超级静音，办公首选</span>
	<span class="private-intro-tip-4">超低成本，超高服务</span>
</div>

{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>