<!DOCTYPE html>
<html lang="zh" ng-controller="regController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
    <meta name="description" content="金山企业云盘是金山云旗下的企业网盘产品，为中小企业提供最专业的文件存储与协同办公服务，具备文档集中管理、安全管控、全平台覆盖、远程办公等优势功能，全面助力企业信息化发展，提高企业员工工作效率。" />
    <title>金山企业云盘-购买结果</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/charge.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body>
{include file="../common/header.tpl"}
<div class="common-wrap">
    <div class="container">
        <div class="center-block content">
            <div class="form">
                {if $orderResult==0}
                    <div class="title">
                        <span class="text">订单支付成功</span>
                    </div>
                    <div class="msg clearfix">
                        <span class="pull-left alipay_suc "></span>
                        <div class="pull-left info">
                            订单编号：{$orderInfo['order_id']}<br>
                            购买后企业状态：总空间：{$orderInfo['space']}G &nbsp;&nbsp;总人数:{$orderInfo['member']+$orderInfo['free_member']}人<br>
                            到期时间：{$orderInfo['expiredate']}<br>
                            <a href="/dashboard#/charge" type="button" class="btn btn-default btn-custom-back">返回金山企业云盘</a>
                            <br>
                            <a href="https://docs.ksyun.com/read/latest/108/_book/buyingFAQ.html"  class="dami-ad" style="margin-top: 10px;" >
                                如需开发票请点击查看流程
                            </a>
                        </div>

                    </div>
                {else}
                    <div class="title">
                        <span class="text">订单支付失败</span>
                    </div>
                    <div class="msg clearfix">
                        <span class="pull-left alipay_error "></span>
                        <div class="pull-left info">
                            订单编号：{$orderInfo['id']}<br>
                            错误描述：{$errmsg}<br>
                            <a href="/dashboard#/charge" type="button" class="btn btn-default btn-custom-back">重新购买</a>
                        </div>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>

{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>