<!DOCTYPE html>
<html lang="zh"  ng-controller="loginController">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	{*<title>{$paltform['sitename']}</title>*}
	<title>网盘登录</title>
	<meta name="keywords"    content="金山企业云盘、企业网盘、容灾备份、SaaS、权限管控、日志追溯、在线编辑" />
	<meta name="description" content="企业成员登录、管理员登录  企业标示，用户名，密码、忘记密码、注册" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/login.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '网盘登录';
     </script>
    <script src="{$common}/NotSupport.js"></script>
  	<![endif]-->
	{literal}
	<style>

	</style>
	{/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body  ng-cloak class="ng-cloak login-page login-page-customize {$classtype}" >
{include file="../common/header-customize.tpl"}

<div id="container" class="container-wrap-login-mobile hidden">
	<div class="login">
		<div ng-show="selected==1" class="login-sub-account" >
			<div class="title">企业成员登录</div>
            {include file="../common/login-sub-account.tpl" title="子帐号登录" }
		</div>
		<div ng-show="selected==2" class="login-admin-account" >
			<div class="title">管理员登录</div>
			<iframe style="width:100%;padding-left:2.5%; padding-right:2.5%; " id="login-iframe" name="login-iframe" frameborder="0"  height="200px"
					scrolling="no" src="https://passport.ksyun.com/iframe-pan-login.html">
			</iframe>
		</div>
		<div class="tab">
			<span ng-show="selected==1" ng-click="tabLogin(2);">
				切换到管理员登录
				<span class="icon"></span>
			</span>
			<span ng-show="selected==2" ng-click="tabLogin(1);">切换到企业成员登录<span class="icon"></span></span>
		</div>
	</div>
</div>

<div id="container" class=" container-wrap-login" >
	<div class="container-bg">
        {if $bg!=''}
			<img  src="{$css}/styles/img/custom/custom-bg-{$paltform['from']}.jpg" alt="金山企业云盘 金山网盘">
        {else}
			<img  src="{$css}/styles/img/custom/custom-bg.png" alt="金山企业云盘 金山网盘">
        {/if}
	</div>
	<div class="container clearfix">
		<div class="info">
			<div class="ident"><img src="{$css}/styles/img/custom/custom-ident-{$paltform['from']}.png" alt=""></div>
			<div class="txt">{$paltform['info']['txt']}</div>
			<div class="qrcode">

				<img src="{$css}/styles/img/custom/custom-qrcode-{$paltform['from']}.jpg" alt="">
				<br>
				<a  href="http://www.wzeye.cn">微信公众号</a>
			</div>
		</div>
		<div class="login-wrap " >
				<div class="login" >
					{if $userInfo && $userInfo['super_type']>=0}
						<div class="haslogin" style="padding-bottom: 20px;">
							<div class="welcometext">欢迎使用金山企业云盘</div>
							<div class="face">
							</div>

							<div class="userinfo">
								{$userInfo['user_name']}
							</div>
							<div class="form-group  form-group-login">
								<label  class="sr-only">&nbsp;</label>
								<div class="input-group-login center-block">
									{if $userInfo['super_type']==0 ||$userInfo['super_type']==2}
										{literal}
											<a href="/dashboard"  class="btn btn-primary btn-login" translate="index_text_3">进入金山企业云盘</a>
										{/literal}
									{else}
										{literal}
											<a href="/account/signup"  class="btn btn-primary btn-login" translate="index_text_3">进入金山企业云盘</a>
										{/literal}
									{/if}
								</div>
							</div>
						</div>
					{else}
						<div class="login-logo-warp" >
							{literal}
							<ul class="nav nav-tabs my-tabs my-tabs-login" role="tablist" id="myTabs">
								<li role="presentation"  ng-class="{true:'active',false:''}[selected==1]" >
									<a href="javascript:;" ng-click="tabLogin(1);" aria-controls="logo" role="tab" data-toggle="tab"
									   translate="index_child_login">子帐号登录
									</a>
								</li>
								<li role="presentation" ng-class="{true:'active',false:''}[selected==2]" >
									<a href="javascript:;" ng-click="tabLogin(2);" aria-controls="logo" role="tab" data-toggle="tab"
									   translate="index_admin_login">管理员登录
									</a>
								</li>
							</ul>
							{/literal}
						</div>

						<div class="tab-content">
							<div ng-show="selected==1" class="login-sub-account" >
								{include file="../common/login-sub-account.tpl" title="子帐号登录" }
							</div>

							<div ng-show="selected==2" class="login-admin-account" style="width: 260px;margin: 0 auto;">
								<iframe style="width:100%;" id="login-iframe" name="login-iframe" frameborder="0"  height="200px"   scrolling="no" src="https://passport.ksyun.com/iframe-pan-login.html"></iframe>
								<div class="login-forget clearfix" style="margin-top: 0;">
									<span class="login-forget-text pull-left">
											<a href="https://passport.ksyun.com/retrieve.html" translate="index_login_forgetPwd" target="_blank">忘记密码</a>
									</span>
									<span class="login-bottom pull-right">没有金山云账号? <a id="register" href="/register" target="_blank">免费注册</a></span>
								</div>

							</div>
						</div>
					{/if}

				</div>
			</div>

	</div>
</div>

<div class="download-client-customize clearfix">
	<div class="download-item pull-left">
		<a class="download-item-a " href="{$paltform['client_windows']}" title="下载Windows增强版">
			<i class="download-icon download-icon-windows center-block"></i>
			<div class="download-text clearfix">
				<i class="line pull-left"></i>
				下载Windows增强版
				<i class="line pull-right"></i>
			</div>
		</a>
	</div>
	<div class="download-item pull-left">
		<a class="download-item-a " href="{$paltform['client_mac']}" title="下载Mac版">
			<i class="download-icon download-icon-mac center-block"></i>
			<div class="download-text clearfix">
				<i class="line pull-left"></i>
				下载Mac版
				<i class="line pull-right"></i>
			</div>
		</a>
	</div>
    {if isset($paltform['client_android']) && $paltform['client_android']!='' }
	<div class="download-item pull-left">
		<a class="download-item-a " href="{$paltform['client_android']}" title="下载Android客户端">
			{*<i class="download-icon download-icon-android center-block"></i>*}
			<img src="{$css}/styles/img/custom/qr-code-android-{$paltform['from']}.png" alt="扫描下载Android">
			<div class="download-text clearfix">
				<i class="line pull-left"></i>
				扫描下载Android
				<i class="line pull-right"></i>
			</div>
		</a>
	</div>
    {/if}
    {if isset($paltform['client_ios']) && $paltform['client_ios']!='' }
	<div class="download-item pull-left">
		<a class="download-item-a " href="{$paltform['client_ios']}" title="下载iPhone/iPad客户端">
			{*<i class="download-icon download-icon-iphone center-block"></i>*}
			<img src="{$css}/styles/img/custom/qr-code-ios-{$paltform['from']}.png" alt="扫描下载iPhone/iPad">
			<div class="download-text clearfix">
				<i class="line pull-left"></i>
				扫描下载iPhone
				<i class="line pull-right"></i>
			</div>
		</a>
	</div>
    {/if}
    {if isset($paltform['client_more']) && $paltform['client_more']!='' }
	<div class="download-item download-item-more pull-right">
		<a class="download-item-a " href="{$paltform['client_more']}" title="下载更多客户端">
					更多客户端>>
		</a>
	</div>
    {/if}
</div>
</body>
</html>