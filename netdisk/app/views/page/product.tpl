<!DOCTYPE html>
<html lang="zh"  ng-controller="productController">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
	<meta name="description" content="金山企业云盘是金山云旗下的企业网盘产品，为中小企业提供最专业的文件存储与协同办公服务，具备文档集中管理、安全管控、全平台覆盖、远程办公等优势功能，全面助力企业信息化发展，提高企业员工工作效率。" />
	<title>{$paltform['sitename']}</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	{*<link rel="stylesheet" type="text/style" href="{$style}/styles/netdisk/product.style">*}
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	{literal}
		<style>

		</style>
	{/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

	{/if}
	<script>
		require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>
{literal}
<body id="body" ng-class="{1:'ani1',2:'ani2',3:'ani3',4:'ani4',5:'ani5'}[ani]">
{/literal}
{include file="../common/header.tpl"}


<div infinite-scroll="myPagingFunction()" infinite-scroll-distance="3">
</div>

<div class="container-fluid product-bg-1" >
	<div class="container  product-content product-content-1">
		<div class="protxt1">
			<div class="font-big text-center" >用户至上 用心服务</div>
			<div class="font-small text-center">为您提供安全、可靠、稳定的企业云服务平台</div>
		</div>
	</div>
</div>

<div class="container-fluid product-bg-2" >
	<div class="container  product-content product-content-2">
		<div class="proani2">
			<div>
				<div class="pro-ani-1 ani zoomin ani2-zoomin1" ></div>
				<div class="pro-ani-2 ani zoomin ani2-zoomin2" ></div>
				<div class="pro-ani-3 ani zoomin ani2-zoomin3" ></div>
			</div>

		</div>
		<div class="protxt2">
			<em ><span>金山云</span>——国内知名</em>
			<h1>云服务提供商</h1>
			<div>100P+数据存储量</div>
			<p >
				金山企业云盘--依托金山25年安全<br>
				经验及金山云技术积累，安全稳定<br/>
				运营9年,100000+企业用户
			</p>
		</div>


	</div>
</div>
<div class="container-fluid product-bg-3" >
	<div class="container  product-content product-content-3">
		<div class="protxt3">
			<h1>安全制胜&nbsp;&nbsp;永不丢失</h1>
			<em></em>
			<p>
				https协议、分块存储，文件还原等多重维度，保证安全<br>
				网银级别安全机制，数据安保策略历经考验，固若金汤
			</p>
			<div></div>
		</div>
		<div class="proani3">
			<h1></h1>
			<em ></em>
			<div >
				<div class="pro-ani-1 ani zoomin ani3-zoomin3"  ></div>
			</div>
		</div>

	</div>
</div>
<div class="container-fluid product-bg-4" >
	<div class="container  product-content product-content-4">
		<div class="proani4">
			<div >
				<div class="pro-ani-center"></div>
				<div class="pro-ani-1 animated flipInY" id="ani-custom">
					<em class="animated bounceInDown" >客户</em>
				</div>
				<div class="pro-ani-2 animated flipInY" id="ani-out">
					<em class="animated bounceInDown" >异地</em>

				</div>
				<div class="pro-ani-3 animated flipInY" id="ani-home">
					<em class="animated bounceInDown" >家</em>

				</div>
				<div class="pro-ani-4 animated flipInY" id="ani-team">
					<em class="animated bounceInDown" >团队</em>
				</div>
			</div>
		</div>
		<div class="protxt4">
			<h1>文件协同&nbsp;&nbsp;高效办公</h1>
			<em></em>
			<p>
				同事之间一起编辑文档，给客户做演示，文件存储，<br>
				团队间协作等等，一个Kingfile，全部搞定。
			</p>
		</div>

	</div>
</div>
<div class="container-fluid product-bg-5" >
	<div class="container  product-content product-content-5">
		<div class="protxt5">
			<i></i>
			<h1>多端支持&nbsp;&nbsp;移动办公</h1>
			<em></em>
			<p>
				电脑、手机、平板多终端支持，业务因移动而不再<br>
				丢失，企业文件随身携带，一端更改，多端同步
			</p>
		</div>
		<div class="proani5">
			<div>
				<div class="pro-ani-1 ani rotatein"></div>
			</div>
		</div>
	</div>
</div>



{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>