<!DOCTYPE html>
<html lang="zh"  ng-controller="regController">
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <!--<link rel="stylesheet" type="text/style" href="{$css}/scripts/vendor/bootstrap-material-design/dist/style/material.min.style" >-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
    require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body>

<div id="container">
	<div id="body">
			<div class="col-lg-4 col-lg-offset-4">
			<div class="well login-box" style="margin-top:200px; ">
        	<form class="form-horizontal" ng-submit="reg()">
				    <fieldset>
				        <legend>注册</legend>
				        <div class="form-group">
				            <label for="domainIdent" class="col-lg-3 control-label">域</label>
				            <div class="col-lg-6">
				                <input type="text" class="form-control" id="domainIdent" ng-model="domainIdent" placeholder="请输入您的企业域">
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputEmail" class="col-lg-3 control-label">邮箱</label>
				            <div class="col-lg-6">
				                <input type="email" class="form-control" id="inputEmail" ng-model="user" placeholder="请输入您的登录邮箱">
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label">密码</label>
				            <div class="col-lg-6">
				                <input type="password" class="form-control" id="inputPassword" ng-model="password" placeholder="请输入密码">
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label">确认密码</label>
				            <div class="col-lg-6">
				                <input type="password" class="form-control" id="inputPassword" ng-model="confirmPassword" placeholder="请输入确认密码">
				            </div>
				        </div>

				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label">手机号</label>
				            <div class="col-lg-6">
				                <input type="text" class="form-control" id="inputPassword" ng-model="phone" placeholder="请输入手机号">
				            </div>
				        </div>

				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label">企业名</label>
				            <div class="col-lg-6">
				                <input type="text" class="form-control" id="inputPassword" ng-model="domainName" placeholder="请输入企业名">
				            </div>
				        </div>


				        <div class="form-group">
				            <label for="inputPassword" class="col-lg-3 control-label">联系人</label>
				            <div class="col-lg-6">
				                <input type="text" class="form-control" id="inputPassword" ng-model="domainContact" placeholder="请输入联系人">
				            </div>
				        </div>

				        <div class="form-group">
				        	<label class="col-lg-3 control-label">&nbsp;</label>
				        	<div class="col-lg-6">
				        	<button type="submit" class="btn btn-primary">注册</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;已有账号，<a href="/">登录</a>
				       		</div>
				        </div>
				    </fieldset>
       		</form>
       	</div>
       </div>
    </div>
</div>

</body>
</html>