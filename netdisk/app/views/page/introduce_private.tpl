<!DOCTYPE html>
<html lang="zh" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="高可用、系统集成、高私密性、文件安全、内网部署" />
	<meta name="description" content="金山企业云盘私有云，经过公网验证，技术可靠，内网加密部署，数据安全可靠，更有1对1技术支持，解决您后顾之忧！欢迎致电咨询：400-028-9900" />
	<title>金山企业云盘私有云</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/introduce.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '金山企业云盘私有云';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	{literal}
		<style>

		</style>
	{/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

	{/if}
	<script>
		require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>

<body id="body" >
{include file="../common/header.tpl"}
<div class="banner banner-private">
	<div class="w1100">
		<div class="txt">
			<h1>金山企业云盘私有云</h1>
			<h2>简单部署    量身定制   安全可靠  高可用</h2>
			<h3>传输速度质的飞越，部署简单，满足专业化的需求</h3>
			<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner" title="免费试用">免费试用</a>
		</div>
	</div>
</div>

<div class="private-intro">
	<div class="private-intro-item w1100">
		<h1>超强的文件管理功能</h1>
		<ul class="private-intro-list clearfix">
			<li>
				<em class="private-intro-icon private-intro-icon-1"></em>
				<h3>传输速度质的飞越</h3>
				<p>
					60G+大文件轻松传输，更高技术允许
					更大能耐；神秘功能百倍效率提升传
					输速度，更快更安全；内部网络，秒
					传功能，没有快，只有更快；全球多
					数据中心，大文件跨国也能极速传输；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-2"></em>
				<h3>细粒度管理，科学合理</h3>
				<p>
					不仅限于公有云所有功能，私有更懂你；
					AD域人员集成，快速添加成员，即时享用；
					子管理员分配，只能管，不能用，管、用分离；
					互联网管理哲学，减轻管理负担；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-3"></em>
				<h3>后台管控，时时监控</h3>
				<p>
					独立运维管控页面，集中管理企业IT资源；
					平台设置服务，接入自身邮件系统；
					企业运维定制，配置企业访问IP、端口；
					完善的监控体系，时时监控，一有问题，马上警报；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-4"></em>
				<h3>高可用方案，更安全</h3>
				<p>
					多种高可用方案，轻松应对高并发；
					一处损坏，自动切换服务器，保证服务不间断；
					根据使用场景和工作流程，按需选择高可用方案；
					弹性扩容，空间不再受到局限；
				</p>
			</li>
		</ul>
		<ul class="private-intro-list clearfix" style="margin-top: 50px;">
			<li>
				<em class="private-intro-icon private-intro-icon-5"></em>
				<h3>内部部署，高私密性</h3>
				<p>
					部署简单，按需选择部署方案；
					兼容市面上所有存储系统，避免重复投资；
					数据存储在自有服务器中，全面掌控企业数据；
					内网部署，任何外部人员都无法看到；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-6"></em>
				<h3>系统集成，个性化定制</h3>
				<p>
					提供完整成熟的API接口，降低集成失败率；
					无缝融入企业现有资源，高效利用现有IT资源；
					提供系统集成技术支持服务，轻松集成；
					支持公司形象、企业平台及功能定制；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-7"></em>
				<h3>专业技术1对1支持</h3>
				<p>
					专职销售经理服务，量身定制部署方案；
					提供产品实施，应用服务及员工培训；
					专业技术全程跟踪，1对1服务；
					全天在线，7*24小时，时刻为您服务；
				</p>
			</li>
			<li>
				<em class="private-intro-icon private-intro-icon-8"></em>
				<h3>跨地域分享，超高灵活度</h3>
				<p>
					内外网同时部署，内外均可访问；
					内网端口设置，提升IT资源利用率；
					外网访问网址设定，一套系统两种访问方式；
					英文版全平台支持，跨国交流也方便；
				</p>
			</li>
		</ul>
	</div>
</div>
<div class="intro-tip w1100 clearfix">
	<span class="private-intro-tip-1">公网验证，技术可靠</span>
	<span class="private-intro-tip-2">内网加密，安全牢固</span>
	<span class="private-intro-tip-3">内网部署，数据可靠</span>
	<span class="private-intro-tip-4">专业技术，全天服务</span>
</div>





{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>