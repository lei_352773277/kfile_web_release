<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="金山企业云盘，金山企业网盘，企业网盘，企业云盘，网盘，一体机，企业云存储，office插件，私有云，公有云，协同办公，文件共享，文件管理，移动办公，数据存储" />
    <meta name="description" content="金山企业云盘是金山云旗下的企业网盘产品，为企业提供最专业的文件存储与协同办公服务，具备文档集中管理、安全管控、全平台覆盖、远程办公等优势功能，全面助力企业信息化发展，提高企业员工工作效率。" />
    <title>网盘购买</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/shop.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!--[if lt IE 9]>
    <script type="text/javascript">
        var PRODUCT = '网盘购买';
    </script>
    <script src="{$common}/NotSupport.js"></script>
    <![endif]-->
    {literal}
        <style>

        </style>
    {/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body>
{include file="../common/header.tpl"}

<div class="shop">
    <div class="shop-package w1100">
        <h1>价格参考</h1>
        <h2>您可以根据需求灵活调整套餐</h2>
        <div class="buy">
            9.9元限时抢购30G5人套餐，
            {if $userInfo && $userInfo['super_type']>=0}
                {if $userInfo['super_type']==0 ||$userInfo['super_type']==2}
                    <a class=" my-a" href="/account/logout" translate="shop_buy_btn_2">点击购买（需要超管用户）</a>
                {else}
                    <a class="my-a" href="/account/signup/charge" translate="shop_buy_btn_1" >点击购买</a>
                {/if}
            {else}
                <a class="my-a"  href="/login/?from=charge" translate="shop_buy_btn_1">点击购买</a>
            {/if}
        </div>
        <ul class="clearfix">
            <li class="a">
                <h1>白银版</h1>
                <em></em>
                <h2><span>3000</span>元 </h2>
                <h3>3600元</h3>
                <p>
                    50G  10人  1年
                </p>
                {if $userInfo && $userInfo['super_type']==1}
                    <a class="shop-btn " href="/account/signup/charge">立即购买</a>
                {else}
                    <a class="shop-btn " href="/login">立即购买</a>
                {/if}
            </li>
            <li class="b">
                <h1>黄金版</h1>
                <em></em>
                <h2><span>7500</span>元</h2>
                <h3>8700元</h3>
                <p>
                    100G  50人  1年
                </p>
                {if $userInfo && $userInfo['super_type']==1}
                    <a class="shop-btn" href="/account/signup/charge">立即购买</a>
                {else}
                    <a class="shop-btn" href="/login">立即购买</a>
                {/if}
            </li>
            <li class="c">
                <h1>钻石版</h1>
                <em></em>
                <a class="service"
                   href="http://chat32.live800.com/live800/chatClient/chatbox.jsp?companyID=462131&amp;configID=56872&amp;jid=1949975560&amp;lan=zh&amp;enterurl=http%3A%2F%2Fwww.ksyun.com%2F&amp;pagetitle=%E9%87%91%E5%B1%B1%E4%BA%91-%E5%85%A8%E7%90%83%E9%AB%98%E5%93%81%E8%B4%A8%E4%BA%91%E6%9C%8D%E5%8A%A1%E4%B8%93%E5%AE%B6&amp;firstEnterUrl=http%3A%2F%2Fwww.ksyun.com%2F"
                   target="_blank">
                    <span class="service-icon"></span>
                    <p>联系客服</p>
                </a>
                <a class="shop-btn" href="javascript:void(0)">400-028-9900</a>
            </li>
        </ul>
    </div>
</div>



{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>