<!DOCTYPE html>
<html lang="zh" ng-controller="forgetpwdController">
<head>
    <meta charset="utf-8">
    <title>{$paltform['sitename']}-忘记密码</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/forget.css">
    {literal}
        <style>

        </style>
    {/literal}
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body ng-cloak class="ng-cloak">
{include file="../common/header.tpl"}
<div class="forget-wrap">
    <div class="container">
        <div class="center-block forget-content">
            <div class="form">
                <div class="title">
                    <span class="text" translate="index_login_forgetPwd">忘记密码</span>
                </div>
                <div ng-show="status==1">
                    <form class="form-horizontal " name="forgetform" ng-submit="login()">
                        <h5>企业标识/邮箱帐号</h5>
                        {if $platform['producttype'] != 1}
                            <div class="form-group"
                                 ng-class="{ 'has-error' : forgetform.domainIdent.$invalid && !forgetform.domainIdent.$pristine  && forgetform.domainIdent !='' }">

                                <div class="col-sm-12">
                                    <input type="text" class="form-control" tabindex="1" name="domainIdent"
                                           ng-model="domainIdent"
                                           {literal}placeholder="{{'nav_user_ident_forget'|translate}}"{/literal}
                                           placeholderkey="nav_user_ident_forget" required>
                                    <p class="help-block"
                                       ng-show="forgetform.domainIdent.$invalid && !forgetform.domainIdent.$pristine"
                                       translate="forget_place_identification">请输入您的企业标识</p>

                                </div>
                            </div>
                        {/if}
                        <div class="form-group"
                             ng-class="{ 'has-error' : forgetform.user.$invalid && !forgetform.user.$pristine }">
                            <div class="col-sm-12">
                                <input type="email" class="form-control" tabindex="2" name="user"
                                       ng-model="user"
                                       {literal}placeholder="{{'nav_user_email_forget'|translate}}"{/literal}
                                       placeholderkey="nav_user_email_forget" required>

                                <p ng-cloak ng-show="forgetform.user.$invalid && !forgetform.user.$pristine"
                                   class="help-block" translate="forget_error_email">请输入您的登录邮箱</p>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="clearfix">
                                    {literal}
                                        <button class="btn btn-custom btn-custom-half " type="submit"
                                                tabindex="3"
                                                ng-disabled="forgetform.$invalid||loging"
                                                class="btn btn-primary">{{loginbtn}}
                                        </button>
                                    {/literal}
                                    <a class="btn btn-custom btn-custom-back" href="/login" translate="forget_back_login">返回登录</a>
                                </div>

                            </div>
                        </div>
                        {literal}
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <p ng-cloak class="ng-cloak text-danger" ng-show="loginErrorCode!=''"
                                       class="help-block">{{loginError}}</p>
                                </div>
                            </div>
                        {/literal}
                    </form>
                </div>
                <div ng-show="status==2" ng-cloak class="ng-cloak">
                    <div class="forget-result-wrap">
                            <div class="forget-result-icon"></div>
                            {literal}
                            <p>
                                {{user}}
                                <span translate="forget_resetPwd_info_1">
                                    您的重置密码邮件
                                </span>
                                <br>
                                <span translate="forget_resetPwd_info_2">
                                    已经发往您的邮箱,您需要进入邮箱重置密码
                                </span>
                                <br>
                            </p>
                            {/literal}
                            <a class="btn btn-custom btn-custom-back" href="/" translate="forget_back_login">返回登录</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file="../common/footer.tpl"}
    {include file="../common/toolbar.tpl"}
</body>
</html>