<!DOCTYPE html>
<html lang="zh"  ng-controller="indexController">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>金山企业云盘-专注企业服务的网盘云存储和协同办公平台</title>
	<meta name="keywords" content="金山企业网盘，网盘，同步盘、一体机，私有云，公有云，勒索病毒,协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP，企业云存储，云存储，网络硬盘，网络U盘" />
	<meta name="description" content="金山企业云盘是金山企业云的升级版，为企业提供最专业的文件存储与协同办公服务，具备文件集中管理、在线协作办公、安全管控、移动办公等优势功能，助力企业挣脱办公困扰，提升业绩，欢迎咨询：400-028-9900" />

	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/index2.css">
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/flexslider.css">
	<script>
		/*var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();*/
	</script>
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '金山企业云盘-企业云存储';
     </script>
    <script type="text/javascript" src="{$common}/NotSupport.js"></script>
  	<![endif]-->
	{literal}
	<style>

	</style>
	{/literal}
    <script type="text/javascript" src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script type="text/javascript" src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body  ng-cloak class="ng-cloak index" >
{include file="../common/header.tpl"}
{include file="../common/header-index-mobile.tpl"}
<input type="hidden" value="{$css}" id="css_addr">
<div class="banner banner-slider">
	<div class="flexslider">
		<ul class="slides">
			<li class="slide-item-2">
				<div class="w1100">
					<div class="txt">
						<h1>&nbsp;</h1>
						<h4>&nbsp;</h4>
						<a style="margin-top: 20px;" href="/account/signup" class="btn-banner" title="立即抢购" >立即抢购</a>
					</div>
				</div>
			</li>
			{*<li class="slide-item-4">
				<div class="w1100">
					<div class="txt">
						<h1>&nbsp;</h1>
						<h2>&nbsp;</h2>
						<h3>&nbsp;</h3>
						<a href="http://activity.ksyun.com/1710/index.html?ch=00022.00011&hmsr=ksyun%E5%AE%98%E7%BD%91banner%E9%A1%B5&hmpl=1710&hmcu=&hmkw=&hmci="
						   class="btn-banner btn-banner-ad" style="margin-top: 133px; margin-left: 168px;" title="查看详情">查看详情</a>
					</div>
				</div>
			</li>*}
			<li class="slide-item-1">
                <div class="w1100">
                    <div class="txt">
                        <h2 style="margin-bottom: 20px;font-size: 34px;">对抗“勒索病毒”--金山企业云盘保您文件万无一失</h2>
                        <h1 style="font-size: 20px;">网银级别存储加密 https传输加密 数据三备份 文件历史版本</h1>
                        <h3>您要的安全都在这里，快来体验文件被保护的感觉吧</h3>
						<a href="/account/signup"
						   class="btn-banner" title="立即抢购" >立即抢购</a>
                    </div>
                </div>
			</li>
			<li class="slide-item-3">
				<div class="w1100">
					<div class="txt">
						<h1>金山企业云盘，不止每个企业都需要</h1>
						<h2>文件轻松传，秒杀传统传输工具</h2>
						<h3>文件共享快，协同办公更方便，使用金山企业网盘，轻松解决各种数据难题，工作效率提升45%</h3>
						<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup"
						   class="btn-banner" title="免费试用">免费试用</a>
					</div>
				</div>
			</li>

		</ul>
	</div>
</div>
<div class="banner-mobile hidden">
	<div class="item-1">
	</div>
</div>
<div class="btn-mobile-list hidden">
	<a class="btn-header btn-header-register" href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" target="_blank" >注册</a>
	<a class="btn-header btn-header-login "  href="/login" >登  录</a>
</div>
<div class="container-mobile hidden">
	<ul class="item-list">
		<li>
			<h1>文档管理 协同办公</h1>
			<p>
				文件共享  文件外链  文件管理  历史版本   在线预览编辑
			</p>
			<em class="item-img-1"></em>
		</li>
		<li>
			<h1>安全制胜 永不丢失</h1>
			<p>
				网银级别加密   传输加密  权限控制  容灾备份 日志追溯
			</p>
			<em class="item-img-2"></em>
		</li>
		<li>
			<h1>移动办公  随时创造价值</h1>
			<p>
				office插件   全平台支持  轻松分发、权限灵活、极速传输
			</p>
			<em class="item-img-3"></em>
		</li>
	</ul>
</div>
<div id="container" class="index-container">
	<div class="container w1100">
		<div class="index-intro">
			<div class="index-intro-item">
				<div class="index-intro-item-1">
					<div class="title">安全制胜  永不丢失</div>
					<ul class="clearfix">
						<li>
							<em class="item-img-1"></em>
							<h1>全程网银级别加密</h1>
							<p>
								支持HTTPS协议，网银级别SSL加密，<br>
								有效防止数据传输过程中被黑客攻击盗取，<br>
								保证通信安全。
							</p>
						</li>
						<li>
							<em class="item-img-2"></em>
							<h1>领先的数据安全存储方案</h1>
							<p>数据分块，数据块采用AES256加密算法，<br>即使拿到数据存储盘也无法获取数据，<br>技术上保证数据安全。</p>
						</li>
						<li>
							<em class="item-img-3"></em>
							<h1>容灾备份</h1>
							<p>系统对每块数据实行3副本备份，<br>确保数据安全，固若金汤。</p>
						</li>
					</ul>
					<ul class="clearfix m50">
						<li>
							<em class="item-img-4"></em>
							<h1>多级权限管控</h1>
							<p>7种权限控制，限制文件上传、<br>下载、查看、编辑、删除等操作，<br>关键文件谁能看，你说了算。</p>
						</li>
						<li>
							<em class="item-img-5"></em>
							<h1>详尽日志追溯</h1>
							<p>企业用户对文件的所有操作<br>都有迹可循，有效杜绝恶意删除、修改公司数据。</p>
						</li>
						<li>
							<em class="item-img-6"></em>
							<h1>客户端加锁</h1>
							<p>支持客户端加锁，移动办公<br>过程中，也能时刻防止数据被他人篡改。</p>
						</li>
					</ul>
				</div>
				<div class="index-intro-item-2">
					<div class="title">文档管理   协同办公</div>
					<ul class="clearfix">
						<li>
							<em class="item-img-1"><span></span></em>
							<h1>文件集中管理</h1>
							<p>基于企业组织架构的文件管理，不限<br>文件格式，集中管理，文件的上传、下载、查看、<br>编辑、共享一站搞定。</p>
						</li>
						<li>
							<em class="item-img-2"><span></span></em>
							<h1>文件分发</h1>
							<p>内部分发通过权限共享，即时分发文件，<br>一处更新，全平台同时更新，再也不用担心文件<br>是不是最新版本了。</p>
						</li>
						<li>
							<em class="item-img-3"><span></span></em>
							<h1>文件外链</h1>
							<p>一次上传文件，通过文件外链的形式<br>跨地域，多次分发给合作伙伴，提高业务水平，<br>提升企业业绩。</p>
						</li>

					</ul>
					<ul class="clearfix m50">
						<li>
							<em class="item-img-4"><span></span></em>
							<h1>多级权限管控</h1>
							<p>7种权限管控，不同的员工享有不同的<br>文件操作权限，贴合企业文件管理。</p>
						</li>
						<li>
							<em class="item-img-5"><span></span></em>
							<h1>消息动态</h1>
							<p>已订阅的文件被修改就能收到消息，<br>随时随地掌控文件变化，参与协作办公。</p>
						</li>
						<li>
							<em class="item-img-6"><span></span></em>
							<h1>历史版本</h1>
							<p>文件修改历史版本全部保存，防止文件<br>误修改，更有历史版本备注，<br>修改了哪里，一目了然。</p>
						</li>
					</ul>
				</div>
				<div class="index-intro-item-3">
					<div class="title">移动办公   随时创造价值</div>
					<ul class="clearfix">
						<li>
							<em class="item-img-1"></em>
							<h1>office插件</h1>
							<p>完美融合MS office 和 WPS，轻松实现<br>本地文档一键保存到云端；同时，完美兼容Outlook<br>邮件系统，邮件附件转存云端，云端文件生成<br>邮件附件，轻松搞定。</p>
						</li>
						<li>
							<em class="item-img-2"></em>
							<h1>全平台支持</h1>
							<p>支持web端、window端同步盘与<br>非同步盘、Mac端、Android端、iPhone端、iPad端等，<br>随时随地移动办公。</p>
						</li>
						<li>
							<em class="item-img-3"></em>
							<h1>超强办公助手</h1>
							<p>摆脱时间、空间的束缚，随时掌控<br>文件变化，不在单位也能胸有成竹，<br>提升企业价值创造力。</p>
						</li>
					</ul>
				</div>
				<div class="index-intro-item-4">
					<div class="title">多种解决方案  按需选择</div>
					<ul class="clearfix">
						<li>
							<em class="item-img-1"></em>
							<h1>公有云</h1>
							<p>
								金山云计算技术和高性能服务器集群，<br>SAAS模式，按需使用，<br>省时间、省成本、高安全、高效率。
							</p>
						</li>
						<li>
							<em class="item-img-2"></em>
							<h1>私有云</h1>
							<p>强大的软件兼容性，适用于多种存储系统，<br>专业技术人员跟踪，企业内部部署，<br>一次部署，终身享用。</p>
						</li>
						<li>
							<em class="item-img-3"></em>
							<h1>私有云一体机</h1>
							<p>金山企业云盘软件系统与高性能服务器<br>完美结合，为您提供软硬结合的私有云服务，<br>无需搭建维护，一根网线简单配置即可享受<br>私有云带来的便捷。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="index-intro-usercase m50 m-b-20">
			<div class="usercase-title">
				<h1>他们都选择了使用金山企业云盘</h1>
				<p>25年安全经验  9年稳定运营  100000+客户的选择</p>
				<a class="btn-usercase-free" href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup">免费试用</a>
			</div>
            {include file="../common/usercase-list.tpl"}
		</div>
	</div>
</div>

{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>