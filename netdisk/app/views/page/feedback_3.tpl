<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{$paltform['sitename']}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/feedback.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <!--[if lt IE 9]>
     <script type="text/javascript">
		var PRODUCT = '{$paltform['sitename']}';
     </script>
    <script src="{$common}/NotSupport.js"></script>
  	<![endif]-->
	{literal}
	<style>

	</style>
	{/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body style="padding-top:0px;">
<div class="feedback-wrap">
	<div class="container feedback">
		<div class="feedback-content">
			<div class="title" >
				<span class="text" >意见反馈</span>
			</div>
			<div style="margin:60px 90px;">
				<div style="margin-bottom:10px;">
					<strong>客服热线：</strong>400-028-990
				</div>
				<div style="margin-bottom:10px;">
					<strong>客服邮箱：</strong>kscxs@kingsoft.com
				</div>
				<div style="margin-bottom:10px;">
					<strong>金山企业云盘技术支持QQ群：</strong>3420025632
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>