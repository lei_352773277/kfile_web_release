<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<title>{$paltform['sitename']}</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	{literal}
	<style>
		.login-box{font-family:'微软雅黑';margin-top:50px;}
	</style>
	{/literal}
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
</head>
<body>

<div id="container">
	<div id="body">
		<div class="col-lg-4 col-lg-offset-4 login-box">
			<h1 class="">{if $paltform['logo']!=""}<img src="{$paltform['logo']}" />{/if}{$paltform['sitename']}</h1>
			<div class="well">
				{if $paltform['offline_image']!=""}
					<img src="{$paltform['offline_image']}" />
				{/if}
				{if $paltform['display_offline_message']=='1'}
					{$paltform['offline_message']}
				{/if}
       		</div>
       </div>
    </div>
</div>

</body>
</html>