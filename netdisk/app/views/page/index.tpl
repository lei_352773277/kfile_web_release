<!DOCTYPE html>
<html lang="zh" ng-controller="indexController">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>飞鸟云</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/index1.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '网盘登录';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
	{/if}
	<script>
		require(['../netdisk/bootstrap'], function (bootstrap) {});
	</script>
</head>
<body ng-cloak class="ng-cloak">


<div class="header ">
	{include file="../common/header.tpl"}
</div>
<div class="banner" id="banner">
	<div class="container">
		<h1 class="txt1">企业数据文件保险柜</h1>
		<div class="txt2"></div>
		<a class="btn-banner btn-register" href="/register">免费试用</a>
	</div>
</div>

<div class="woker">
	<div class="container">
		<div class="item item1">
			<i></i>
			<div>
				<span>无需运维</span>
				<span>注册账号就能使用</span>
			</div>
		</div>
		<div class="item item2">
			<i></i>
			<div>
				<span>全程加密</span>
				<span>存储/传输加密保障安全</span>
			</div>

		</div>
		<div class="item item3">
			<i></i>
			<div>
				<span>专业服务</span>
				<span>技术可靠 全天服务</span>
			</div>
		</div>
	</div>

</div>

<div class="intro">
	<div class="container">
		<h2>精益求精，真正实用的企业网盘</h2>
		<h3>企业数据存储/分发/管理，多层技术保障，安全易用效率高</h3>
		<ul class="list list1 clearfix">
			<li class="item item1">
				<i></i>
				<h4>文档数据管控</h4>
				<p>收集分散文件，工作内容权责清晰，常<br>用格式在线编辑，历史版本有序保留，便捷检索</p>
			</li>
			<li class="item item2">
				<i></i>
				<h4>智能增量同步</h4>
				<p>智能同步算法，本地云端保持一致，所<br>有文件夹可直接增量同步，操作简单，节省时间</p>
			</li>
			<li class="item item3">
				<i></i>
				<h4>极速传输</h4>
				<p>局域网加速，妙传，断点续传，不限大<br>小，稳定传输，您体验上传下载如飞的感觉</p>
			</li>
		</ul>
		<ul class="list list2 clearfix">
			<li class="item item4">
				<i></i>
				<h4>企业数据的安全卫士</h4>
				<p>ES256加密算法，存储加密，网银级别的传输加密，文件分块存储，容灾备份，安全无忧</p>
			</li>
			<li class="item item5">
				<i></i>
				<h4>高效共享协作</h4>
				<p>权限灵活配置，协作方便，对内内链，对外外链，轻松分发；文件变化即有动态可查，方便高效</p>
			</li>
			<li class="item item6">
				<i></i>
				<h4>高性价比</h4>
				<p>一站式运维和管理服务，专业技术，全天服务；按需购买，注册一个账号即可享受优质服务</p>
			</li>
		</ul>
	</div>
</div>

<div class="use">
	<div class="container">
		<ul class="list clearfix">
			<li class="item item1">
				<i>1</i>
				<div>
					<h3>注册账号</h3>
					<h4>免费使用，注册账号</h4>
				</div>
				<div class="dir"></div>
			</li>
			<li class="item item2">
				<i>2</i>
				<div>
					<h3>创建团队</h3>
					<h4>创建部门，导入团队</h4>
				</div>
				<div class="dir"></div>
			</li>
			<li class="item item3">
				<i>3</i>
				<div>
					<h3>开始使用</h3>
					<h4>创建文件夹，配置权限</h4>
				</div>
			</li>
		</ul>
	</div>
</div>

{include file="../common/register-footer.tpl"}

{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>