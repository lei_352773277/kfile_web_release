<!DOCTYPE html>
<html lang="zh" ng-controller="entconfigController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>金山企业云盘-专注企业服务的网盘云存储和协同办公平台</title>
    <meta name="keywords" content="金山企业网盘，网盘，同步盘、一体机，私有云，公有云，勒索病毒,协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP，企业云存储，云存储，网络硬盘，网络U盘" />
    <meta name="description" content="金山企业云盘是金山企业云的升级版，为企业提供最专业的文件存储与协同办公服务，具备文件集中管理、在线协作办公、安全管控、移动办公等优势功能，助力企业挣脱办公困扰，提升业绩，欢迎咨询：400-028-9900" />
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/regist.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/entconfig.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body class="initkingfile-page">
{include file="../common/header-register.tpl"}

<div class="initkingfile-mobile-wrap hidden">
    <div class="title" translate="register_7">开通金山企业云盘服务</div>
    <div class="user">
        <span>帐号：</span>{$ksyun_user['email']}
    </div>
    <form class="form-horizontal " name="initkingfile">
        <div class="form-group" ng-class="{ 'has-error' : initkingfile.domainIdent.$invalid && !initkingfile.domainIdent.$pristine }">
                <input type="text" class="form-control" ng-pattern="/^([A-Za-z0-9-_.])+$/"
                       id="inputGroupSuccess2"
                       name="domainIdent" ng-model="domainIdent" required
                       ng-keyup="mykeyup()"
                       {literal}placeholder="{{'initkingfile_4'|translate}}"{/literal}">
                <p  ng-cloak ng-show="initkingfile.domainIdent.$invalid && !initkingfile.domainIdent.$pristine"
                    class="help-block" translate="initkingfile_5"></p>
        </div>
        <div class="form-group"
             ng-class="{ 'has-error' : initkingfile.domainName.$invalid && !initkingfile.domainName.$pristine }">
                <input type="text" class="form-control"
                       ng-pattern="/^\S+/" id="domainName"
                       name="domainName"
                       ng-model="domainName"
                       value="{$ksyun_user['company_name']}" required
                       {literal}placeholder="{{'initkingfile_9'|translate}}"{/literal}>
                <p ng-cloak ng-show="initkingfile.domainName.$invalid && !initkingfile.domainName.$pristine"
                   class="help-block" translate="initkingfile_7"></p>
        </div>
        <div class="form-group">
                {literal}
                    <button class="btn btn-lg btn-lg-custom" ng-click="submit()"
                            ng-disabled="initkingfile.$invalid||loging" translate="initkingfile_8">开通服务
                    </button>
                {/literal}
        </div>
        {literal}
            <div class="form-group">
                    <p ng-cloak class="ng-cloak text-danger" ng-show="loginErrorCode!=''"
                       class="help-block" ng-bind="errorMsg"></p>
            </div>
        {/literal}
    </form>
    <div class="tip">
        <em>说明</em>
        <p>1、企业标示设置后无法更改，请慎重</p>
        <p>2、新用户默认免费试用15天，配备5人、30G空间</p>
        <p>3、有任何疑问请拨打客服电话：400-028-9900</p>
        <p>4、官方网站：https://pan.ksyun.com</p>
    </div>
</div>

<div id="container" class="container-wrap-login">
    <div class="container">
        <div class="login-wrap">
            <div class="login">
                <div class="tab-content">
                    <div class="tab-header">
                        <span class="header-point" style="margin-right: 8px;"></span>
                        <span class="act-title" translate="register_1">注册即可免费使用</span>
                        <span class="act-img"></span>
                        <span class="act-title" translate="register_2">欢迎体验高效办公</span>
                        <span class="header-point"></span>
                    </div>
                    <p class="kingfile" translate="register_3">金山企业云盘注册</p>
                    <div class="regist-progress">
                        <div class="step active">1</div><hr /><div class="step active">2</div><hr /><div class="step">3</div>
                    </div>
                    <div class="regist-progress-title"><p class="step-active" translate="register_4">注册账号</p><p style="text-align: center;" class="step-active" translate="register_5">配置信息</p><p style="text-align: right;" translate="register_6">注册成功</p></div>
                    <!--<div class="login-admin-account" style="width: 440px;margin: 0 auto;">
                        <iframe style="width:100%;" id="login-iframe" name="login-iframe" frameborder="0" height="460px" scrolling="no" src="https://passport.ksyun.com/iframe-common-register.html"></iframe>
                    </div>-->
                    <form class="form-horizontal form-entconfig" name="initkingfile">

                        <!--<h5 translate="initkingfile_2">您正在开通金山企业云服务，请配置企业标识和企业名</h5>-->
                        <p class="entconfig-notice"><span class="warn-img"></span><span translate="register_8">企业标识用于企业成员登录，切不可更改，请牢记</span></p>
                        <div class="user">
                            <span translate="register_9">帐号：</span>{$ksyun_user['username']}
                        </div>
                        {literal}
                        <p class="entconfig-notice" style="text-indent: 18px;" translate="register_10">您配置完企业信息就可以使用金山企业云盘了，加油！</p>
                        <div class="form-group"
                             ng-class="{ 'has-error' : initkingfile.domainIdent.$invalid && !initkingfile.domainIdent.$pristine }">
                            <label class="sr-only">&nbsp;</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" ng-pattern="/^([A-Za-z0-9-_.])+$/"
                                       id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status"
                                       name="domainIdent" ng-model="domainIdent" required
                                       ng-keyup="mykeyup()"
                                       placeholder="{{'initkingfile_4'|translate}}" placeholderkey="initkingfile_4">
                                <p ng-cloak ng-show="initkingfile.domainIdent.$invalid && !initkingfile.domainIdent.$pristine"
                                   class="help-block" translate="initkingfile_5"></p>

                            </div>
                        </div>
                        <div class="form-group"
                             ng-class="{ 'has-error' : initkingfile.domainName.$invalid && !initkingfile.domainName.$pristine }">
                            <label for="inputEmail" class="sr-only">&nbsp;</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"
                                       ng-pattern="/^\S+/" id="domainName"
                                       aria-describedby="inputGroupSuccess1Status" name="domainName"
                                       ng-model="domainName"
                                       value="" required
                                       placeholder="{{'initkingfile_9'|translate}}" placeholderkey="initkingfile_9">
                                <p ng-cloak ng-show="initkingfile.domainName.$invalid && !initkingfile.domainName.$pristine"
                                   class="help-block" translate="initkingfile_7"></p>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-lg btn-lg-custom entconfig-btn" ng-class="{true:'',false:'entconfig-btn-active'}[initkingfile.$invalid||loging]" ng-click="submit()"
                                        ng-disabled="initkingfile.$invalid||loging" translate="initkingfile_8">开通服务
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <p ng-cloak class="ng-cloak text-danger" ng-show="loginErrorCode!=''"
                                   class="help-block" ng-bind="errorMsg"></p>
                            </div>
                        </div>
                        {/literal}
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

{include file="../common/footer-register.tpl"}

</body>
</html>