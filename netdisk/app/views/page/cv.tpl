<!DOCTYPE html>
<html lang="zh" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
</head>

<body id="body" >
<script type="text/javascript">
	function trackPageview(pageURL) {
		try{
			_hmt.push(['_trackPageview', pageURL]);
		}catch(e) {

		}
	}

	function trackEvent(category, action, opt_label, opt_value) {
		try{
			_hmt.push(['_trackEvent', category, action, opt_label, opt_value]);
		}catch(e) {

		}
	}

</script>
</body>
</html>