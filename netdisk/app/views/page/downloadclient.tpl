<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>同步盘下载</title>
    <meta name="keywords" content="飞鸟云盘、企业网盘、极速传输、传输加密、https加密、上传、下载、大文件传输"/>
    <meta name="description" content="飞鸟云盘支持PC同步盘，专一服务，您的文件大管家"/>

    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/download.css">
    <script>
    </script>
    <!--[if lt IE 9]>
    <script type="text/javascript">
        var PRODUCT = '同步盘下载';
    </script>
    <script src="{$common}/NotSupport.js"></script>
    <![endif]-->
    {literal}
        <style>

        </style>
    {/literal}
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body>

<div class="header header-other">
    {include file="../common/header.tpl"}
</div>
<div class="download-wrap">
    <div class="download-content">
        <ul class="clearfix">
            <li class="item item-win-sync">
                <div class="icon-wrap">
                    <i class="icon icon-win-sync"></i>
                    <h3>Windows同步盘</h3>
                </div>
                <p class="intro">
                    版本:&nbsp;{$client_info['win_sync_version']}<br>
                    大小:&nbsp;{$client_info['win_sync_size']}<br>
                    适合:&nbsp;Windonw XP /Win7 /WIn8 /Win10
                </p>
                <p class="update">
                    {foreach from=$client_info['win_sync_update'] item=i}
                        {$i}
                        <br>
                    {/foreach}
                </p>
                <a class="download-btn" href="{$paltform['client_win_sync']}">立即下载</a>
            </li>
        </ul>

    </div>
    <div class="more">
        <p>更多客户端支持，敬请期待 -_-</p>
    </div>
</div>

{include file="../common/footer.tpl"}
</body>
</html>