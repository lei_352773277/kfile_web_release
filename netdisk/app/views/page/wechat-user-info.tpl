<!DOCTYPE HTML>
<html lang="zh"  ng-controller="wechatuserinfoController">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>用户信息</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/wechat.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
    <style type="text/css">

    </style>
</head>
<body>
{literal}
    <div class="loading">
        <loading></loading>
        <span class="loading-txt" ng-bind="loading.tipsText"></span>
    </div>
{/literal}
<div class="userinfo" >
    <div class="title">
        账号信息
    </div>
    <div class="content">

        {if $info_empty==1}
            <div>用户信息获取失败.</div>
        {else}
            <div>
                <label>企业标识：</label>
                <span>{$userinfo['domain_ident']}</span>
            </div>
            <div>
                <label >身份类型：</label>
                {if $userinfo['is_superuser']==1}
                <span>超管</span>
                {elseif $userinfo['is_superuser']!=1 && $userinfo['super_type']==2}
                <span>子超管</span>
                {elseif $userinfo['is_superuser']==0 && $userinfo['super_type']==0}
                <span>普通用户</span>
                {/if}
            </div>
            <div>
                <label >企业名：</label>
                <span>{$userinfo['domain_name']}</span>
            </div>
            <div>
                <label>用户名：</label>
                <span>{$userinfo['user_name']}</span>
            </div>
            <div>
                <label >姓名：</label>
                <span>{$userinfo['staff']['name']}</span>
            </div>
            <div>
                <label>部门：</label>
                <span class="nav-user-dept-style">
                    {$userinfo['staff']['full_name']}
                </span>
            </div>
            <div>
                <label>电话：</label>
                {if $userinfo['staff']['phone']!=''}
                <span>{$userinfo['staff']['phone']}</span>
                {else}
                    <span>空</span>
                {/if}

            </div>
            <div>
                <label>邮箱：</label>
                {if $userinfo['email']!=''}
                    <span>{$userinfo['email']}</span>
                {else}
                    <span>空</span>
                {/if}
            </div>
           <div>
                <label>空间使用：</label>
                <span>{$spaceinfo['usedfilter']}/{$spaceinfo['quotafilter']}</span>
            </div>
            <div class="bind">

                {if $userinfo['wechat_id']}
                    <div>
                        <div class="icon">
                            <span class="icon-suc"></span>
                        </div>
                        <span class="bind-result">已绑定</span>
                    </div>
                {else}
                    <div>
                        <div class="icon">
                            <span class="icon-suc ng-cloak"   ng-if="bind_result"></span>
                            <span class="icon-error ng-cloak" ng-if="!bind_result"></span>
                        </div>
                        <span class="bind-result" ng-bind="bind_msg"></span>
                    </div>
                {/if}
            </div>
        {/if}


    </div>
    <input type="hidden" class="bind-hide" value="{$userinfo['wechat_id']}"/>
</div>
</body>
</html>