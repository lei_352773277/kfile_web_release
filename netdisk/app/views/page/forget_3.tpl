<!DOCTYPE html>
<html lang="zh"  ng-controller="forgetpwdController">
<head>
	<meta charset="utf-8">
	<title>{$paltform['sitename']}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	{literal}
	<style>
		.login-box{font-family:'微软雅黑';margin-top:50px;}
		.downwrap{text-align: center;}
		.downicon{font-size:30px;}
		.ng-cloak{display:none;}
	</style>
	{/literal}
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
    <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
    <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
     <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
	<script>
    	require(['../netdisk/bootstrap'], function(bootstrap) {});
    </script>
</head>
<body ng-cloak class="ng-cloak">

<div id="container">
	<div id="body" class="row">
			<div class="col-lg-4 col-lg-offset-4 col-md-8 col-md-offset-2 login-box">
			<h2 class="">{$paltform['sitename']}</h2>
			<div class="well clearfix">
        	<form class="form-horizontal " name="forgetform" ng-submit="login()">
				    <fieldset>
				        <legend translate="index_login_forgetPwd">忘记密码</legend>
				        <span ng-show="status==1">
				        {if $platform['producttype'] != 1 }
						<div class="form-group" ng-class="{ 'has-error' : forgetform.domainIdent.$invalid && !forgetform.domainIdent.$pristine }">
				            <label for="inputEmail" class="col-lg-1 col-md-1 control-label"></label>
				            <div class="col-lg-8 col-md-8">

				        		<div class="input-group">
								    <span class="input-group-addon">
										<span class="glyphicon glyphicon-globe"></span>
										
										<span translate="forget_filed">企业标识</span>
										
									</span>
								    <input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" name="domainIdent" ng-model="domainIdent" required>
								</div>

				                <p ng-cloak class="ng-cloak help-block"  ng-show="forgetform.domainIdent.$invalid && !forgetform.domainIdent.$pristine" class="help-block" translate="forget_place_identification">
									请输入您的企业标识
								</p>
				            </div>
				        </div>
						{/if}
						<div class="form-group" ng-class="{ 'has-error' : forgetform.user.$invalid && !forgetform.user.$pristine }">
				            <label for="inputEmail" class="col-lg-1 col-md-1 control-label"></label>
				            <div class="col-lg-8 col-md-8">

				                  <div class="input-group">
								    <span class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
										&nbsp;
										<span translate="nav_user_email">邮箱</span>
										&nbsp;
									</span>
								    <input type="email" class="form-control" id="inputGroupSuccess1" aria-describedby="inputGroupSuccess1Status" name="user" ng-model="user" required>
								  </div>

				                <p ng-cloak class="ng-cloak help-block"  ng-show="forgetform.user.$invalid && !forgetform.user.$pristine" class="help-block" translate="forget_error_email">请输入您的登录邮箱</p>
				            </div>
				        </div>

				        <div class="form-group">
				        	<label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
				        	<div class="col-lg-8 col-md-8">
				        		{literal}
									<button type="submit"  ng-disabled="forgetform.$invalid||loging" class="btn btn-primary">{{loginbtn}}</button>
								 {/literal}
								<a href="/"  style="margin-left:50px;" translate="forget_back_login">返回登录</a>
				        	</div>

				        </div>
						{literal}
						<div class="form-group">
							<label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
							<div class="col-lg-8 col-md-8">
								<p ng-cloak class="ng-cloak text-danger"   ng-show="loginErrorCode!=''" class="help-block">{{loginError}}</p>
							</div>
						</div>
				        {/literal}
				    	</span>
				    	
				    	<span ng-show="status==2" ng-cloak class="ng-cloak">
					    	<div class="form-group">
					        	<label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
					        	<div class="col-lg-8 col-md-8">
							    {literal}
					        	<p>
									<span class="glyphicon glyphicon-ok"></span>{{user}}

									<span translate="forget_resetPwd_info_2">您的重置密码邮件已经发往您的邮箱，您需要进入邮箱重置密码</span>
								</p>
					        	{/literal}
					        	<a href="/" translate="forget_back_login">返回登录</a>
					        </div> 
				    	</span>
				    	 
				    </fieldset>
       		</form>
       	</div>
       </div>
    </div>
</div>

</body>
</html>