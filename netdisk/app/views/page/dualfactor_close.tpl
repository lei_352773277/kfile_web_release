<!DOCTYPE html>
<html lang="zh" ng-controller="dualfactorclosepageController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>金山企业云盘-关闭登录双认证</title>
    <meta name="keywords" content="金山企业网盘，网盘，同步盘、一体机，私有云，公有云，协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP、NAS、SaaS" />
    <meta name="description" content="金山企业云盘-企业云存储平台、协作办公工具、文件共享助手" />

    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/netdisk.console.css">
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!--[if lt IE 8]>
    <script src="/scripts/browser-not-supported.js"></script>
    <![endif]-->
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>



</head>
<body id="body" class="root-link">
{include file="../common/header-dualfactor.tpl"}
<div class="container-fluid dualfactor-content">
    <div class="close-content">
            {if $dual_close['code']==0}
            <div class="icon">
                <div class="icon-suc"></div>
            </div>
            <div class="txt">
                <div  class="title" translate="wechat_38">您已关闭登录双认证！</div>
                {if $userinfo['super_type']==0 ||$userinfo['super_type']==2}
                <a class="my-btn my-btn-primary my-btn-lg" href="/dashboard" translate="index_text_3" ></a>
                {elseif $userinfo['super_type']==1}
                <a class="my-btn my-btn-primary my-btn-lg"  href="/account/signup" translate="index_text_3" ></a>
                {/if}
            </div>
            {else}
                <div class="icon">
                    <div class="icon-fail"></div>
                </div>
                <div class="txt" >
                    <div  class="title" >
                        <span translate="wechat_39"></span>
                        <span class="msg">{literal}{{msg}}{/literal}</span>
                    </div>

                </div>
            {/if}
    </div>
</div>
<input type="hidden" id="error_code"  value="{$dual_close['code']}">
</body>
</html>
