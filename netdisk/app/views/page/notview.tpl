<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="utf-8">
	<title>{$paltform['sitename']}</title>
	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	{literal}
	<style>
		.login-box{font-family:'微软雅黑';margin-top:50px;}
	</style>
	{/literal}
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
</head>
<body>

<div id="container">
	<div id="body">
		<div class="col-lg-4 col-lg-offset-4 login-box">
			<div class="well" translate="notview">
				对不起，暂不支持此文件在线预览，我们正在努力改进，敬请期待，谢谢！
       		</div>
       </div>
    </div>
</div>

</body>
</html>