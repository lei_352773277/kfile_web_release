<!DOCTYPE html>
<html lang="zh" ng-controller="resetpwdController">
<head>
    <meta charset="utf-8">
    <title>{$paltform['sitename']}</title>
    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/resetpwd.css">
    {literal}
        <style>
            .login-box {
                font-family: '微软雅黑';
                margin-top: 50px;
            }

            .downwrap {
                text-align: center;
            }

            .downicon {
                font-size: 30px;
            }

            .ng-cloak {
                display: none;
            }
        </style>
    {/literal}
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script src="{$common}/ZeroClipboard.min.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>
<body ng-cloak class="ng-cloak resetpwd">

<div id="container" data-token="{$token}" data-email="{$email}">
    <div id="body">
        <div class="login-box container-resetpwd center-block">
            <h2 class="">{$paltform['sitename']}</h2>
            <div class="well clearfix">
                <form class="form-horizontal " name="resetpwdform">
                    <fieldset>
                        <legend translate="resetPwd">设置密码</legend>
				        <span ng-show="status==1">
				        	
						<div class="form-group"
                             ng-class="{ 'has-error' : resetpwdform.newpwd.$invalid && !resetpwdform.newpwd.$pristine }">
                            <label for="inputEmail" class="col-lg-1 col-md-1 control-label"></label>
                            <div class="col-md-9">

                                <div class="input-group">
                                    {$email}
                                </div>
                            </div>
                        </div>

						<div class="form-group" ng-class="{ 'has-error' : resetpwdform.newpwd.$invalid && !resetpwdform.newpwd.$pristine }">
                            <label for="inputEmail" class="col-md-1 control-label"></label>
                            <div class="col-md-9">

                                <div class="row">
                                    <label class="col-md-3 control-label" translate="resetPwd_new">新密码</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" id="inputGroupSuccess1"
                                               aria-describedby="inputGroupSuccess1Status" name="newpwd" ng-model="newpwd"
                                               required ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)])+$/"
                                               ng-minlength="6" ng-maxlength="20" maxlength="20"
                                               {literal}placeholder="{{'resetPwd_new'|translate}}"{/literal}>
                                        <p ng-cloak class="ng-cloak help-block"
                                           ng-show="resetpwdform.newpwd.$invalid && !resetpwdform.newpwd.$pristine"
                                           translate="comm_err_pwd">密码格式为（6-20位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合）
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>


				        <div class="form-group"
                             ng-class="{ 'has-error' : (resetpwdform.password.$invalid && !resetpwdform.password.$pristine)||(newpwd!=password) }">
                            <label for="inputPassword" class="col-lg-1 col-md-1 control-label"></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <label class="col-md-3 control-label" translate="resetPwd_confirm">确认密码</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" id="inputGroupSuccess1"
                                               aria-describedby="inputGroupSuccess1Status" name="password"
                                               ng-model="password" required
                                               ng-pattern="/^([A-Za-z0-9\!@#$%\^&*\(\)~\-_+=)])+$/"
                                               ng-minlength="6" ng-maxlength="20"  maxlength="20"
                                               {literal}placeholder="{{'resetPwd_confirm'|translate}}"{/literal}>
                                        <p ng-cloak class="ng-cloak help-block"
                                           ng-show="(resetpwdform.password.$invalid && !resetpwdform.password.$pristine)||(newpwd!=password)"
                                           translate="comm_err_pwd_confirm">
                                            密码格式为（6-20位的a-Z，数字，符号!@#$%^&*()~-_+=的任意组合），请保持跟两次密码输入一致
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>


				        <div class="form-group">
                            <label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-3">
                                        {literal}
                                            <button type="button"
                                                    ng-disabled="resetpwdform.$invalid||(newpwd!=password)||loging"
                                                    class="btn btn-primary" ng-click="resetpwd()">{{loginbtn}}
                                            </button>
                                        {/literal}
                                    </div>
                                </div>

                            </div>
                        </div>
                        {literal}
                        <div class="form-group">
                            <label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-3">
                                        <p ng-cloak class="ng-cloak text-danger"
                                           ng-show="loginErrorCode!=''"
                                           class="help-block">{{loginErrorCode|formatErrorText|T}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/literal}
				    	</span>

                        {literal}
				    	<span ng-show="status==2">
					    	<div class="form-group">
                                <label class="col-lg-1 col-md-1 control-label">&nbsp;</label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <p>
                                                <span class="glyphicon glyphicon-ok"></span>
                                                <span translate="resetPwd_success">重置密码操作成功</span>
                                                <a href="{{homelink}}" translate="resetPwd_intoSys">进入系统</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
				    	</span>
                        {/literal}
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>