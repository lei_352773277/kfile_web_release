<!DOCTYPE html>
<html lang="zh" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>金山企业云盘公有云</title>
	<meta name="keywords" content="文件管理、文件协作、文件安全、替代FTP、云存储、云服务" />
	<meta name="description" content="金山企业云盘公有云，只需注册一个账号就能快速享受云服务带来的便利。SAAS模式，按需使用，0运维，省时省力更省钱！" />

	<link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/introduce.css">
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?329b1bc5b5c7aa0d154016d8db24e1ba";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		var PRODUCT = '金山企业云盘公有云';
	</script>
	<script src="{$common}/NotSupport.js"></script>
	<![endif]-->
	{literal}
		<style>

		</style>
	{/literal}
	<script src="{$common}/ZeroClipboard.js"></script>
	{if !$debug}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
		<script src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
	{else}
		<script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>

	{/if}
	<script>
		require(['../netdisk/bootstrap'], function(bootstrap) {});
	</script>
</head>

<body id="body" >
{include file="../common/header.tpl"}
<div class="banner banner-public">
	<div class="w1100">
		<div class="txt">
			<h1>金山企业云盘公有云</h1>
			<h2>0运维，高稳定，高安全</h2>
			<h3>只需注册一个账号就能使用，快速享受云服务带来的便利。<br>SAAS模式，按需使用，省时省力更省钱！</h3>
			<a href="http://www.ksyun.com/register/register?callback=http%3A%2F%2Fpan.ksyun.com%2Faccount%2Fsignup" class="btn-banner" title="免费试用">免费试用</a>
		</div>
	</div>
</div>

<div class="public-intro">
	<div class="public-intro-item w1100">
		<h1>超强的文件管理功能</h1>
		<ul class="public-intro-list public-intro-list-1 clearfix">
			<li>
				<em >
					<span class="icon icon-1"></span>
				</em>
				<h3>文件集中存储管理</h3>
				<p>
					公司文件按需存储到云端，不限文件<br>
					格式，清晰明了；集中管理公司文件，<br>
					防止文件散落各处，防止泄漏公司机密。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-2"></span>
				</em>
				<h3>多终端访问</h3>
				<p>
					全平台支持，支持网页版、客户端、<br>手机、平板，一端上传，多端访问。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-3"></span>
				</em>
				<h3>多版本管理</h3>
				<p>
					记录文件修改痕迹，超过100份的<br>
					历史版本记录，防止文件误修改；<br>
					支持历史版本备注，文件修改了哪<br>里一目了然。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-4"></span>
				</em>
				<h3>在线预览</h3>
				<p>
					多终端支持文件在线预览，支持文档、<br>
					图片、音频等常用格式在线预览，便于<br>
					用户随时随地查看预览。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-5"></span>
				</em>
				<h3>精准的文件检索</h3>
				<p>
					文件全局搜索，匹配模糊查询，精准<br>
					搜到您所需文件，搜索到的文件能够<br>
					直接进行操作，方便快捷。
				</p>
			</li>
		</ul>
	</div>
	<div class="public-intro-item w1100">
		<h1>超快的文件协作模式</h1>
		<ul class="public-intro-list public-intro-list-2 clearfix">
			<li class="text-right" style="    padding-right: 30px;">
				<div class="m-top">
				<h3>权限丰富，灵活控制</h3>
				<p>
					7种权限控制，控制用户对文件的上传、下载、查看、编辑<br>
					及删除等操作，按照部门群组、人员批量设置权限，<br>设置灵活，修改方便。
				</p>
				</div>
				<div class="m-top">
					<h3>内外部共享，轻松分发</h3>
					<p>
						内部设置权限共享即可看到文件，多终端都可访问；<br>
						外部通过外链快速分发合作伙伴，支持设置有效期和<br>密码，管控文件传播范围。
					</p>
				</div>
				<div class="m-top">
					<h3>文件动态，时刻掌控</h3>
					<p>
						文件变化就发送消息通知，通过订阅关注重要文件，<br>
						重要文件一有变化即可收到消息通知，重要文件不再错过。
					</p>
				</div>
			</li>
			<li>
				<img src="{$css}/styles/img/product/intro-public-img-2.png">
			</li>
			<li class="text-left" style="padding-left: 30px;">
				<div class="m-top">
					<h3>文件锁定，避免混乱</h3>
					<p>
						锁定要编辑的文件，正在编辑的过程中防止被其他同事修改，<br>避免文件混乱导致问题。
					</p>
				</div>
				<div class="m-top">
					<h3>Office插件，贴合习惯</h3>
					<p>
						使用Office插件、Outlook插件，本地文档直接存储到云端，<br>邮件中的附件一键到网盘，工作更高效。
					</p>
				</div>
				<div class="m-top">
					<h3>同步盘，更懂你</h3>
					<p>
						直接嵌入本地资源管理器中，本地操作就是云端操作，全面贴合windows操作模式，更懂你。
					</p>
				</div>
			</li>
		</ul>
	</div>

	<div class="public-intro-item w1100">
		<h1>超多的文件安全保证</h1>
		<ul class="public-intro-list public-intro-list-3 clearfix">
			<li>
				<em >
					<span class="icon icon-6"></span>
				</em>
				<h3>依托金山云大平台</h3>
				<p>
					金山云以业内领先的用户体验和<br>
					服务端技术，为用户和企业提供<br>
					国内领军级云服务产品。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-7"></span>
				</em>
				<h3>多重灾备</h3>
				<p>
					对所有存储的数据碎片进行3副本<br>
					备份,三重保障，即使一份数据<br>
					损坏，也不影响服务，数据更安全。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-8"></span>
				</em>
				<h3>多维度加密</h3>
				<p>
					数据存储采用AES256加密算法，<br>
					数据传输采用网银界别SSL加密<br>
					算法，同时还有神秘动态加密<br>
					算法保障。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-9"></span>
				</em>
				<h3>权限管控</h3>
				<p>
					针对每个文件夹，多级权限层层<br>
					设置，可根据工作内容及职位，<br>
					合理设置企业文件的访问以及<br>管理权限。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-10"></span>
				</em>
				<h3>日志审计</h3>
				<p>
					全面记录用户的操作痕迹，并提供<br>
					个人账户登录痕迹，让恶意操作<br>公司文件无所遁形。
				</p>
			</li>
		</ul>
	</div>
	<div class="public-intro-item w1100">
		<h1>超全的企业管理服务</h1>
		<ul class="public-intro-list public-intro-list-4 clearfix">
			<li>
				<em >
					<span class="icon icon-11"></span>
				</em>
				<h3>管理组织结构</h3>
				<p>
					与现实企业中的组织架构保持一致，<br>便于管理企业用户；支持企业用户<br>增、删、改、查，有序管理<br>企业组织结构。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-12"></span>
				</em>
				<h3>离职文件交接</h3>
				<p>
					员工离职，文件不离职，删除员工时<br>
					必须转交企业文件，防止企业数据<br>
					流失，积累企业数据文化知识。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-13"></span>
				</em>
				<h3>空间分配</h3>
				<p>
					为员工分配私密空间，保障员工工作<br>
					私密性；限制员工私密空间，限制<br>
					共享空间，保障企业空间利用最大化。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-14"></span>
				</em>
				<h3>锁定用户</h3>
				<p>
					支持锁定用户，用于用户手机丢失<br>
					紧急锁定及其他安全考虑的用户<br>
					锁定，锁定后的用户无法登录云端。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-15"></span>
				</em>
				<h3>自定义企业信息</h3>
				<p>
					个性化定制，更改企业logo、企业<br>
					名称等信息，变更为您的企业专有<br>云，提升公司形象。
				</p>
			</li>
		</ul>
	</div>
	<div class="public-intro-item w1100 m-b-40">
		<h1>完美替代FTP</h1>
		<ul class="public-intro-list public-intro-list-5 clearfix">
			<li>
				<em >
					<span class="icon icon-16"></span>
				</em>
				<h3>稳定性强</h3>
				<p>
					FTP需要企业准备存储设备，且稳定性<br>
					不能保证，金山企业云盘无需任何基础<br>
					设备配置，更无需专人维护，存储文件<br>
					多重备份，超强稳定性经历数十年验证，<br>
					0事故历史。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-17"></span>
				</em>
				<h3>成本低</h3>
				<p>
					相对FTP需要准备昂贵的存储设备，<br>
					金山企业云盘仅需注册一个账号即可使用，<br>
					也无需专人维护，大大节省IT成本。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-18"></span>
				</em>
				<h3>协同办公</h3>
				<p>
					FTP方案中文件夹的管理及权限控制<br>
					比较简单，很难满足当前企业用户复杂的<br>文件管理需求，金山企业云盘具有的<br>丰富的权限，提供细致的文件和人员<br>管理服务，满足各种企业需求。
				</p>
			</li>
			<li>
				<em >
					<span class="icon icon-19"></span>
				</em>
				<h3>移动办公</h3>
				<p>
					FTP存储系统放置在企业内部，员工在外很难<br>
					访问到企业内部的文件，金山企业云盘能够<br>
					轻松解决该问题，并提供Android端、iPhone端<br>
					及iPad等移动端访问服务，提升企业业绩。
				</p>
			</li>

		</ul>
	</div>

</div>




{include file="../common/footer.tpl"}
{include file="../common/toolbar.tpl"}
</body>
</html>