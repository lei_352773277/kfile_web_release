<!DOCTYPE html>
<html lang="zh" ng-controller="registController">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>金山企业云盘-专注企业服务的网盘云存储和协同办公平台</title>
    <meta name="keywords" content="网盘，同步盘,勒索病毒,协同办公、移动办公，数据存储、文件管理、安全存储、替代FTP，企业云存储，云存储，网络硬盘，网络U盘"/>

    <link rel="stylesheet" type="text/css" href="{$common}/bootstrap-3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$css}/styles/netdisk/regist.css">
    <script>
    </script>
    <!--[if lt IE 9]>
    <script type="text/javascript">
        var PRODUCT = '金山企业云盘-企业云存储';
    </script>
    <script type="text/javascript" src="{$common}/NotSupport.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{$common}/ZeroClipboard.js"></script>
    {if !$debug}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$common}/require.js/2.1.20/require.js"></script>
        <script type="text/javascript" src="{$js}/scripts/netdisk/netdiskConsole.js"></script>
    {else}
        <script data-main="{$js}/scripts/netdisk/main.js" src="{$js}/scripts/vendor/requirejs/require.js"></script>
    {/if}
    <script>
        require(['../netdisk/bootstrap'], function (bootstrap) {});
    </script>
</head>

<body ng-cloak class="ng-cloak">

<div class="header header-other">
    {include file="../common/header-register.tpl"}
</div>

<div class="register">
    <div class="reg-box">
        <h2>您正在注册飞鸟企业云管理员账号</h2>
        <div class="tip"><i>!</i>企业域名作为企业唯一标识，不能更改，请谨慎填写！</div>
        {include file="../common/register-form.tpl"}
    </div>
</div>


{include file="../common/footer.tpl"}
</body>

</html>