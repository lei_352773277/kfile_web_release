<?php

class Shop_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function _get_month_num($date1,$date2){
        $date1_stamp=strtotime($date1);
        $date2_stamp=strtotime($date2);
        list($date_1['y'],$date_1['m'])=explode("-",date('Y-m',$date1_stamp));
        list($date_2['y'],$date_2['m'])=explode("-",date('Y-m',$date2_stamp));
        return ($date_1['y']-$date_2['y'])*12 +$date_2['m']-$date_1['m'];
    }

    function _get_difference_day($date1,$date2) {
        $d1 = strtotime($date1);
        $d2 = strtotime($date2);
        $days = round(($d1-$d2)/3600/24);
        return $days;
    }
   

    //计算产品价格
    private function _get_price($space, $member, $days)
    {
        return $space*0.2*$days+$member*50;
    }

    //根据空间获取赠送的账号数
    private function _get_free_member($space) {
       /* $member = 0;
        if($space>=250) {
            $member = 50;
        } else if($space>=150) {
            $member = 30;
        } else if($space>=100) {
            $member = 20;
        } else if($space>=50) {
            $member = 10;
        } else {
            return 0;//小于50G 就不送了
        }*/

        return floor($space/50)*10;
    }


    /*function get_price($domain_id, $space, $member, $days, $serial) {
        if (API_RET_SUCC === $serial['code']) {

            $order = $serial['data']['order'];
            $expire_time = date('Y-m-d 23:59:59',strtotime($serial['data']['ctime'].' + '.$order['days'].'day'));
            $space_max = $order['space'];
            $user_max = $order['member'];

            $params_space = $space + $space_max;
            if ($params_space<50) {
                $msg = 'total space is less, space+space_max='.$params_space;
                log_message('error',$msg);
                return array(5000001,$msg);
            }
            $free_member = $this->_get_free_member($params_space);
            $params_member =  $member + $user_max;
            $params_days = 0;

            if (strtotime($expire_time) < time()) { //已经过期了
                $overplus_days = 0;
            } else {
                $overplus_days = $this->_get_difference_day($expire_time, date('Y-m-d', time()));//离到期时间还剩多少天
            }

            if (0 == $days) {//只买人跟空间
                if(0 == $overplus_days) {//已经到期了就不能只能买人跟空间了
                    $msg = '已经到期了就不能只能买人跟空间了';
                    log_message('error', $msg);
                    return array(5000002,$msg);
                }

                if (PRODUCT_TRIAL == $order['type']) {//之前是试用产品就不能不买时间了
                    $msg = '之前是试用产品就不能不买时间了';
                    log_message('error',$msg);
                    return array(5000003,$msg);
                }

                $params_use = PRODUCT_USE_CHANGE;//更改配置

                $total = $this->_get_price($space, $member, $overplus_days);//计算剩余的天数需要的钱
                $params_days = intval((strtotime($expire_time) - time()) / 86400);//序列号需要生成的天数

            } else {//购买时间超过当前状态的时间（1年，2年）

                if (PRODUCT_TRIAL == $order['type']) {//试用产品
                    $params_use = PRODUCT_USE_REGULAR;//试用转正式
                } else {
                    $params_use = 2;//续费
                    if($space==0&&$member==0) {
                        $params_use = PRODUCT_USE_RENEW;//续费
                    } else {
                        $params_use = PRODUCT_USE_CHANGE;//试用转正式
                    }
                }
                $overplus_pay =  $this->_get_price($space,0,$overplus_days);//先计算到当前产品的到期时间需要补的钱
                $new_pay_space = $space + $order['space'];//需要把当前的空间加起来计算
                $new_pay_days = $days;
                $new_pay =  $this->_get_price($new_pay_space,$member,$new_pay_days);//从补齐时间到购买的结束时间的计费
                $total = $overplus_pay + $new_pay;
                $params_days = $days;
            }
            $data = array('total'=>$total,'params'=>array('member'=>$params_member,'free_member'=>$free_member,'space'=>$params_space,'days'=>$params_days,'use'=>$params_use));
            return array(0,$data);
        } else {
            $msg = 'domain_id='.$domain_id.' res='.json_encode($serial);
            log_message('error', $msg);
            return array(5000004,$msg);
        }
    }*/

    //创建产品
    function create_kingfile_order($ksyun_token, $domain_id, $domain_ident, $space, $member, $free_member, $days, $price, $use, $base_order_id) {
        $data = "ksyun_token=".$ksyun_token."&domain_id=".$domain_id.'&space='.$space.'&member='.$member.'&free_member='.$free_member;
        $data .= '&days='.$days.'&price='.$price.'&domain_ident='.$domain_ident.'&use='.$use.'&base_order_id='.$base_order_id;
        $serial_res = $this->curl(KINGFILEOP."/api/createorder",$data);
        return json_decode($serial_res,true);
    }

    //获取企业的购买状态
    function get_domainstatus($domain_id, $ksyun_token) {
        log_message("error", 'get_domainstatus:domain_id='.$domain_id.', ksyun_token='.$ksyun_token);
        //$serial_res = $this->curl(KINGFILEOP."/api/domainstatus", "ksyun_token=".$ksyun_token);
        //$serial = json_decode($serial_res,true);
        $serial = json_decode('{
            "code":0,
            "data":{
                "space":30,
                "member":42,
                "free_member":10,
                "ispay": 0,
                "expire_date":"2018-12-28 23:59:59"
            }
        }',true);
        return $serial;
    }
    //计算产品价格
    function get_price($space, $member, $days, $ksyun_token) {
        $data = array(
            "space"=>$space,
            "member"=>$member,
            'days'=>$days,
            'ksyun_token'=>$ksyun_token
        );
        $res = $this->curl(KINGFILEOP."/api/getprice",$data);
        $res = json_decode($res,true);
        return $res;
    }
    //
    function buy($space, $member, $days, $total_money,$ksyun_token) {
        $data = array(
            "space"=>$space,
            "member"=>$member,
            'days'=>$days,
            'total_money'=>$total_money,
            'ksyun_token'=>$ksyun_token
        );
        log_message("error", 'alipay buy='.json_encode($data));
        $res = $this->curl(KINGFILEOP."/api/buy",$data);
        return $res;
    }

    function notify($data) {
        log_message("error", 'alipay notify data='.json_encode($data));
        $res = $this->curl(KINGFILEOP."/api/alipaynotify", $data);
        return $res;
    }

    function get_wechat_login_pay($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid
        ));
        $res = $this->request_netdisk_server("/shop/ddddd", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

}