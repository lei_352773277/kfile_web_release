<?php

/**
 * 日志相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Logger_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function query($token,$user_name="",$share_root="",$log_ids=array(), $start_time, $end_time, $hint=0,$pageMax)
    {
        $post_data = array(
			"token"=>$token,
			"user_name"=>$user_name,
			"log_ids"=>$log_ids,
        
            'order'=>-1,
            'hint'=>$hint,
            'pageMax'=>$pageMax);
        if($share_root!="")
        {
            $post_data["share_root"]=$share_root;
        } 

        if($start_time!="") {
            $post_data['start_time'] = strtotime($start_time);
        }

        if($end_time!="") {
            $post_data['end_time'] =  strtotime($end_time." 23:59:59");
        }


        $res = $this->request_netdisk_server("/logger/query/page/list", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
    
        return $res_data;
    }

}