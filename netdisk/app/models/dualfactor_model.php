<?php

class Dualfactor_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }


    function email_view($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/bind/email/view", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function email_bind($token,$email) {
        $post_data = $this->my_json_encode(array("token" => $token,'email'=>$email));
        $res = $this->request_netdisk_server("/account/dual/factor/email/bind", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function email_send($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/bind/device/token/email/send", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function device_bind_check($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/bind/polling/device/bind/check", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dual_factor_close($token,$banKey) {
        $post_data = $this->my_json_encode(array("token" => $token,"banKey"=>$banKey));
        $res = $this->request_netdisk_server("/account/dual/factor/ban", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function polling_auth_check($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/polling/auth/check", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function ban_email_send($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/ban/email/send", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function qrcode_scene_id($token) {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/dual/factor/auth/qrcode/scene/id", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

}