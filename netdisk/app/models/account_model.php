<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Account_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();

    }

    function info($token)
    {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function signup($domain_data)
    {
        $post_data = $this->my_json_encode($domain_data);
        $res = $this->request_netdisk_server("/account/signup", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function logout($token)
    {
        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server("/account/logout", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function login($login_data)
    {
        $post_data = $this->my_json_encode($login_data);
        $res = $this->request_netdisk_server("/account/login", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function set_password($token, $oldPwd, $userPwd, $confirmPwd)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'oldPwd'=>$oldPwd, 'newPwd' => $userPwd, 'confirmPwd' => $confirmPwd));
        $res = $this->request_netdisk_server("/account/password/set", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function password_reset($token, $userPwd, $confirmPwd)
    {      
        $post_data = $this->my_json_encode(array('token' => $token, 'newPwd' => $userPwd, 'confirmPwd' => $confirmPwd));
        $res = $this->request_netdisk_server("/account/password/reset", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function password_modify($token,$user_id, $newPwd, $userPwd) {
        $post_data = $this->my_json_encode(array('token' => $token, 'user_id' => $user_id, 'newPwd'=>$newPwd, 'confirmPwd' => $userPwd));
        $res = $this->request_netdisk_server("/account/password/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;  
    }


    function password_verify($token,$name,$domainIdent) {
        /*
        $this->load->model('platform_model');
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->get_value('mail_server');
        if(!empty($ret_data)) {
            $email_server = $ret_data[0]['value'];
        }
        $data = "to=".$name."&subject=重置密码&body=中文的link,http://www.ks.com/";
        */
        //$this->curl($email_server,$data);
        $post_data = $this->my_json_encode(array('email' => $name,'domainIdent'=>$domainIdent));
        $res = $this->request_netdisk_server("/account/password/forget", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function account_validate($token, $vtype, $value) {
        $post_data = $this->my_json_encode(array('token'=>$token,'vtype'=>$vtype,'value'=>$value));
        $res = $this->request_netdisk_server("/account/validate", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function account_lock($token, $user_id, $lock) {
        $post_data = $this->my_json_encode(array('token'=>$token,'user_id'=>$user_id,'lock'=>intval($lock)));
        $res = $this->request_netdisk_server("/account/lock", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function account_phone_bind($token, $domainIdent, $user_id, $phone) {
        $post_data = $this->my_json_encode(array('token'=>$token,'userId'=>$user_id,'phone'=>$phone, 'domainIdent'=>$domainIdent));
        $res = $this->request_netdisk_server("/account/phone/bind", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function account_email_bind($token, $domainIdent,  $user_id, $email) {
        $post_data = $this->my_json_encode(array('token'=>$token,'userId'=>$user_id,'email'=>$email,'domainIdent'=>$domainIdent));
        $res = $this->request_netdisk_server("/account/email/bind", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function account_info_unbind($token, $userId, $btype) {
        $post_data = $this->my_json_encode(array('token'=>$token,'userId'=>$userId,'btype'=>$btype));
        $res = $this->request_netdisk_server("/account/info/unbind", $post_data);
        $res_data = json_decode($res, true);
        return $res_data; 
    }

    function refresh_token($token, $deveice_id='') {
        $post_data = $this->my_json_encode(array('token'=>$token,'alternative'=>$deveice_id));
        $res = $this->request_netdisk_server("/account/auth/refresh_token", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    // 获取deviceId
    function get_deviceid($deviceFlag = "KSYUN_WEBSITE") {
        $chars = md5 ( uniqid ( mt_rand (), true ) );
        $deviceId = substr ( $chars, 0, 8 ) . '-';
        $deviceId .= substr ( $chars, 8, 4 ) . '-';
        $deviceId .= substr ( $chars, 12, 4 ) . '-';
        $deviceId .= substr ( $chars, 16, 4 ) . '-';
        $deviceId .= substr ( $chars, 20, 12 );
        return $deviceFlag . "-" . $deviceId;
    }

    //登录账号系统
    function ksyun_login($user, $password) {
        $deviceId = $this->get_deviceid();
        $postData = array('user'=>$user,'password'=>$password,'deviceId'=>$deviceId);
        $res = $this->curl(ACCOUNTSERVER.'/kscLoginJson',json_encode($postData),array('v'=>2,'User-Agent'=>'ekp_web'));
        $res_arr = json_decode($res,true);
        return $res_arr;
    }
    /*function ksyun_login($user, $password,$version=1) {
        $postData = array('user'=>$user,'password'=>$password,'type'=>'root');
        $config = $this->config->item('netdisk.api');
        $version = $version === "" ? $config['default_version'] : $version;
        $http_header=array(
            'Content-Type'=>'Application/json',
            'X-Entry'=>'Application/json',
            'X-Entry-Secret'=>'Application/json',
            'X-Version'=>$version,
            'X-Timestamp'=>date(),
            'X-Client-Ip'=>$this->input->ip_address()
        );
        $res = $this->curl(ACCOUNTSERVER .'/login_tokens',json_encode($postData),$http_header);
        $res_arr = json_decode($res,true);
        return $res_arr;
    }*/

    function getkingfile($ksyun_token) {

        $data = "ksyun_token=".$ksyun_token;
        $res = $this->curl(KINGFILEOP.'/api/getkingfile',$data);
        $res_arr = json_decode($res,true);
        if (API_RET_SUCC == $res_arr['code']) {
            return $res_arr['data'];
        } else {
            return false;
        }
    }
    function getkingfile_bossinfo($user_id) {


        $res = $this->curl('http://profile.inner.sdns.ksyun.com/basic_info/user?user_id='.intval($user_id),'',array('Authorization'=>'Basic '.base64_encode(BASIC_AUTH_USER.":".BASIC_AUTH_PASS)));
        $res_arr = json_decode($res,true);
        log_message('debug', 'getkingfile   profile.inner.sdns.ksyun.com/basic_info/user' . "; get=" . $user_id . "res=" . $res);
        if ('10000' == $res_arr['code']) {
            return $res_arr['data'];
        } else {
            return false;
        }
    }
    function authentication($user_id) {

        $res = $this->curl('http://profile.inner.sdns.ksyun.com/basic_info/user?user_id='.intval($user_id),'',array('Authorization'=>'Basic '.base64_encode(BASIC_AUTH_USER.":".BASIC_AUTH_PASS)));
        $res_arr = json_decode($res,true);
        log_message('debug', 'authentication  profile.inner.sdns.ksyun.com/basic_info/user?user_id=' . "; get=" . $user_id . "res=" . $res);
        if ('10000' == $res_arr['code'] && isset($res_arr['data']) && 0!=$res_arr['data']['type']) {
            return true;
        } else {
            return false;
        }
    }
    function authentication_info($user_id) {

        $res = $this->curl('http://profile.inner.sdns.ksyun.com/basic_info/user?user_id='.intval($user_id),'',array('Authorization'=>'Basic '.base64_encode(BASIC_AUTH_USER.":".BASIC_AUTH_PASS)));
        $res_arr = json_decode($res,true);
        log_message('debug', 'authentication  profile.inner.sdns.ksyun.com/basic_info/user?user_id=' . "; get=" . $user_id . "res=" . $res);
        if ('10000' == $res_arr['code'] && isset($res_arr['data'])) {
            $data_new=array(
                'type'=>$res_arr['data']['type'],
                'identity'=>$res_arr['data']['identity'],
                'person_name'=>isset($res_arr['data']['person_name'])?$res_arr['data']['person_name']:'',
                'company_name'=>isset($res_arr['data']['company_name'])?$res_arr['data']['company_name']:''
            );
            $res_arr['data']=array();
            $res_arr['data']=$data_new;
        }
        return $res_arr;
    }



    /**
     * 初始化kingfile
     */
    function initkingfile($ksyun_token, $domain_name, $domain_ident) {
        $data = "ksyun_token=".$ksyun_token.'&domain_name='.$domain_name.'&domain_ident='.$domain_ident;
        $res = $this->curl(KINGFILEOP.'/api/initkingfile',$data);
        $res_arr = json_decode($res,true);
        return $res_arr;
    }

    /**
    *  检测是否是超管登录
    */
    function  checkIsAdminLogin($domain_ident, $user)
    {
        $data = "domain_ident=".$domain_ident.'&user='.$user;
        $res = $this->curl(KINGFILEOP.'/api/checkisadmin',$data);
        $res_arr = json_decode($res,true);
        return $res_arr;
    }

    /**
     * 获取企业信息
     */
    function getKingfileByIdent($domain_ident) {
        $data = 'domain_ident='.$domain_ident;
        $res = $this->curl(KINGFILEOP.'/api/getKingfileByIdent',$data);
        log_message('debug', 'KINGFILEOP =/api/getKingfileByIdent' . "; get:" . $data . ",res=" . $res);
        $res_arr = json_decode($res,true);
        return $res_arr;
    }


    /*微信公众号二维码*/
    function wechat_qrcode($userId) {
        //获取token
        $access_token=$this->get_access_token(false);
        if(empty($access_token)){
            return array('code'=>900010,'msg'=>'获取微信access_token失败');
        }
        //获取ticket
        $ret_ticket=$this->get_weixin_ticket($access_token,$userId);
        if(!isset($ret_ticket['ticket'])){
            //立即重新获取一次token 获取ticket
            $access_token_again=$this->get_access_token(true);
            if(empty($access_token_again)){
                return array('code'=>900010,'msg'=>'获取微信access_token失败');
            }
            //获取ticket
            $ret_ticket_again=$this->get_weixin_ticket($access_token_again,$userId);
            if(!isset($ret_ticket_again['ticket'])){
                return $ret_ticket_again;
            }else{
                return array('code'=>0,'data'=>$ret_ticket_again['ticket']);
            }
        }else{
            return array('code'=>0,'data'=>$ret_ticket['ticket']);
        }
    }


    private function get_access_token($immediately){
        $weixin_base_url = "https://api.weixin.qq.com/cgi-bin/";
        $appid = "wxba82ee38128b9f25";
        $app_secret = "98d839e36d927428cf632d92899db2b5";

        if($immediately){
            $is_expired=$immediately;
        }else{
            $is_expired=$this->is_expired_access_token();
        }
        if($is_expired){
            //过期 或者指定立即执行 则重新获取并存储
            //获取access_token
            $res = $this->curl($weixin_base_url.'token?grant_type=client_credential&appid='.$appid.'&secret='.$app_secret,'','','','https');
            $res_arr = json_decode($res,true);
            log_message('debug', 'wechat_qrcode  https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential' . "; get=" . "appid=".$appid ."&secret=".$app_secret. "res=" . $res);
            //$res_arr=array('access_token'=>"access_token_test".time());
            if(!isset($res_arr['access_token'])){
                die(json_encode($res_arr));
                exit;
            }
            $set_token=$this->set_db_access_token($res_arr['access_token'],time()+3600);
            if($set_token['access_token']==1 && $set_token['access_token_time']==1){
                return $res_arr['access_token'];
            }else{
                die(json_encode($set_token));
            }

        }else{
            $token= $this->get_value('access_token');
            return $token[0]['value'];
        }

    }

    //微信access_token是否存在 并且是否过期
    private function is_expired_access_token(){
       $access_token=$this->get_db_access_token(array('access_token', 'access_token_time'));
        $token=array();
        foreach($access_token as $value) {
            $token[$value['keyname']] = $value['value'];
        }
        if(!isset($token['access_token'])||!isset($token['access_token_time'])){
            return true;
        }
        if($token['access_token_time']<=0){
            return true;
        }
        if($token['access_token_time']<time()){
            return true;
        }
        return false;
    }

    private  function get_db_access_token($keys) {
        $this->load->database();
        $where = array();
        foreach($keys as $k) {
            $where[] = "keyname='".$k."'";
        }
        $table   = "ec_platform_setting";
        $str_sql  = "select * from {$table} where ".implode(" or ", $where);
        return $this->db->query($str_sql)->result_array();
    }

    private function set_db_access_token($access_token, $access_token_time) {
        $this->load->database();
        $res = $this->get_db_access_token(array('access_token', 'access_token_time'));
        if(empty($res)) {
            $sql = "INSERT INTO ec_platform_setting (keyname, value) VALUES ('access_token', '{$access_token}');";
            $sql2= "INSERT INTO ec_platform_setting (keyname, value) VALUES ('access_token_time', '{$access_token_time}');";
        } else {
            $sql  = "UPDATE ec_platform_setting SET value = '{$access_token}' WHERE keyname='access_token';";
            $sql2=  "UPDATE ec_platform_setting SET value = '{$access_token_time}' WHERE keyname='access_token_time';";
        }
        $result1=$this->db->query($sql);
        $result2=$this->db->query($sql2);
        return array("access_token"=>$result1,"access_token_time"=>$result2);
    }

    private function get_value($keyname){
        $table   = "ec_platform_setting";
        $str_sql  = "select  value  from {$table} where keyname = '{$keyname}' limit 1";
        return $this->db->query($str_sql)->result_array();
    }

    private function get_weixin_ticket($access_token,$userId){

        $postData =json_encode(array(
            "action_name"=>"QR_LIMIT_SCENE",
            "action_info"=>array(
                "scene"=>array(
                    "scene_id"=> $userId
                )
            )
        ));
        $header=array('Content-Type'=>'application/json');
        $weixin_base_url = "https://api.weixin.qq.com/cgi-bin/qrcode/";
        $res = $this->curl($weixin_base_url.'create?access_token='.$access_token,$postData,$header,'','https');
        $res_r=json_decode($res,true);
        //$res_r=array('ticket'=>111);
        return $res_r;
    }

    /**
     * 微信公众号绑定结果
     */
    function query_wechat_office_account_bind($token,$xid) {
        $post_data = $this->my_json_encode(array("token" => $token,"xid"=>$xid));
        $res = $this->request_netdisk_server("/account/wechatquery", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


}