<?php

/**
 * 消息相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Message_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function  message_list($token, $pageMax="100", $hint="0")
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'pageMax'=>$pageMax, 'hint'=>$hint));

        $res = $this->request_netdisk_server("/message/list", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    function  message_pop_list($token, $pageMax=5)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'pageMax'=>$pageMax));
        $res = $this->request_netdisk_server("/message/pop/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  messagesystem_pop_list($token, $cursor=0)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'cursor'=>$cursor));
        $res = $this->request_netdisk_server("/announce/pop/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  messagebroadcast_pop_list($token, $cursor=0)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'cursor'=>$cursor));
        $res = $this->request_netdisk_server("/broadcast/pop/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function  announce_list($token, $pageMax="100", $hint="0")
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'pageMax'=>$pageMax, 'hint'=>$hint));
        $res = $this->request_netdisk_server("/announce/list", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }
    function  get_announce_content($token, $announceId)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'announceId'=>$announceId));
        $res = $this->request_netdisk_server("/announce/read", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }
    function  del_announce($token, $announceId)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'announceId'=>$announceId));
        $res = $this->request_netdisk_server("/announce/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function  broadcast_domain_list($token, $pageMax="100", $hint="0")
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'pageMax'=>$pageMax, 'hint'=>$hint));
        $res = $this->request_netdisk_server("/broadcast/domain/list", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }
    function  broadcast_list($token, $pageMax="100", $hint="0")
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'pageMax'=>$pageMax, 'hint'=>$hint));
        $res = $this->request_netdisk_server("/broadcast/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  get_broadcast_content($token, $broadcastId)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'broadcastId'=>$broadcastId));
        $res = $this->request_netdisk_server("/broadcast/read", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  del_broadcast($token, $broadcastId)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'broadcastId'=>$broadcastId));
        $res = $this->request_netdisk_server("/broadcast/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  del_broadcast_by_admin($token, $broadcastId)
    {
        $post_data = $this->my_json_encode(array('token'=>$token, 'broadcastId'=>$broadcastId));
        $res = $this->request_netdisk_server("/broadcast/withdraw", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  create_broadcast($token, $title,$summary,$content,$issuer)
    {
        $post_data = $this->my_json_encode(array(
            'token'=>$token,
            'title'=>$title,
            'summary'=>$summary,
            'content'=>$content,
            'issuer'=>$issuer,
            ));
        $res = $this->request_netdisk_server("/broadcast/create", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

}