<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Xfile_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function dir_share_create($token, $xid, $name)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'name' => $name));
        $res = $this->request_netdisk_server("/xfile/dir/share/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_create, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_root_create($token, $name, $desc, $quota, $perm)
    {
        $post_data = array("token" => $token, 'name' => $name, 'desc' => $desc, 'perm' => $perm);
        if ($quota > 0) {
            $post_data['quota'] = $quota;
        } else {
            $post_data['quota'] = PBQUOTA;
        }
        $post_data = $this->my_json_encode($post_data);
        $res = $this->request_netdisk_server("/xfile/dir/share/root/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_root_create, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function dir_share_root_modify($token, $xid, $desc, $quota)
    {
        $post_data = array("token" => $token, 'xid' => $xid, 'desc' => $desc);
        if ($quota > 0) {
            $post_data['quota'] = $quota;
        } else {
            $post_data['quota'] = PBQUOTA;
        }
        $post_data = $this->my_json_encode($post_data);
        $res = $this->request_netdisk_server("/xfile/dir/share/root/modify", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_root_modify, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_root_list($token, $sortBy = 1, $order = 1)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'sortBy' => $sortBy, 'order' => $order));
        //$res = $this->request_netdisk_server_v2("/xfile/dir/share/root/list", $post_data);
        //$res_data = json_decode($res, true);
        $res_data = json_decode('{"data":{"result":[{"subscribe":1,"extname":"","used":109894202,"op_version":4056,"quota":1125899906842624,"quota_id":53,"parent_xid":8589934593,"ctime":1510145178,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934605,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"0-pc\u767b\u5f55\u4f18\u5316","name_gbk":"0-pc\u767b\u5f55\u4f18\u5316","mtime":1511855198,"is_share":1},{"subscribe":1,"extname":"","used":1321874303,"op_version":2405,"quota":1125899906842624,"quota_id":17,"parent_xid":8589934593,"ctime":1509436404,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":34359738370,"domain_id":1,"home_xid":8589934593,"user_xid":9,"mime":0,"status":0,"old_user_xid":9,"description":"","role":286331153,"name":"1235","name_gbk":"1235","mtime":1510217276,"is_share":1},{"subscribe":1,"extname":"","used":198783213,"op_version":2778,"quota":1125899906842624,"quota_id":31,"parent_xid":8589934593,"ctime":1509444781,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":64424509442,"domain_id":1,"home_xid":8589934593,"user_xid":51,"mime":0,"status":0,"old_user_xid":51,"description":"","role":286331153,"name":"12PC","name_gbk":"12PC","mtime":1510217222,"is_share":1},{"subscribe":1,"extname":"","used":1807430297,"op_version":3968,"quota":1125899906842624,"quota_id":148,"parent_xid":8589934593,"ctime":1511782201,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":38654707015,"domain_id":1,"home_xid":8589934593,"user_xid":9,"mime":0,"status":0,"old_user_xid":9,"description":"","role":286331153,"name":"7678iuoio","name_gbk":"7678iuoio","mtime":1511782201,"is_share":1},{"subscribe":1,"extname":"","used":121,"op_version":2470,"quota":1125899906842624,"quota_id":70,"parent_xid":8589934593,"ctime":1510303561,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":38654705707,"domain_id":1,"home_xid":8589934593,"user_xid":9,"mime":0,"status":0,"old_user_xid":9,"description":"","role":286331153,"name":"Andriond\u7aef","name_gbk":"Andriond\u7aef","mtime":1510303561,"is_share":1},{"subscribe":1,"extname":"","used":3703836,"op_version":2842,"quota":1125899906842624,"quota_id":68,"parent_xid":8589934593,"ctime":1510297269,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":38654705706,"domain_id":1,"home_xid":8589934593,"user_xid":9,"mime":0,"status":0,"old_user_xid":9,"description":"","role":286331153,"name":"djdjdj","name_gbk":"djdjdj","mtime":1510297269,"is_share":1},{"subscribe":0,"extname":"","used":0,"op_version":4977,"quota":1125899906842624,"quota_id":156,"parent_xid":8589934593,"ctime":1512963269,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589935012,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"dsdsd","name_gbk":"dsdsd","mtime":1512963269,"is_share":1},{"subscribe":0,"extname":"","used":0,"op_version":4180,"quota":1125899906842624,"quota_id":149,"parent_xid":8589934593,"ctime":1512013590,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":55834574881,"domain_id":1,"home_xid":8589934593,"user_xid":13,"mime":0,"status":0,"old_user_xid":13,"description":"","role":286331153,"name":"fwfwf","name_gbk":"fwfwf","mtime":1512013590,"is_share":1},{"subscribe":0,"extname":"","used":3839069222,"op_version":2858,"quota":1125899906842624,"quota_id":65,"parent_xid":8589934593,"ctime":1510218829,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":47244640260,"domain_id":1,"home_xid":8589934593,"user_xid":11,"mime":0,"status":0,"old_user_xid":11,"description":"","role":286331153,"name":"likh_\u9884\u89c8\u8005","name_gbk":"likh_\u9884\u89c8\u8005","mtime":1510218829,"is_share":1},{"subscribe":0,"extname":"","used":250080395,"op_version":4472,"quota":1125899906842624,"quota_id":113,"parent_xid":8589934593,"ctime":1510742732,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":279172874279,"domain_id":1,"home_xid":8589934593,"user_xid":65,"mime":0,"status":0,"old_user_xid":65,"description":"","role":286331153,"name":"PC\u6d4b\u8bd5","name_gbk":"PC\u6d4b\u8bd5","mtime":1510742732,"is_share":1},{"subscribe":0,"extname":"","used":113783273,"op_version":4469,"quota":1125899906842624,"quota_id":147,"parent_xid":8589934593,"ctime":1511778250,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":279172874287,"domain_id":1,"home_xid":8589934593,"user_xid":65,"mime":0,"status":0,"old_user_xid":65,"description":"","role":286331153,"name":"PC\u6d4b\u8bd5\u5b89\u88c5\u5305","name_gbk":"PC\u6d4b\u8bd5\u5b89\u88c5\u5305","mtime":1511778250,"is_share":1},{"subscribe":0,"extname":"","used":1103201,"op_version":4956,"quota":1125899906842624,"quota_id":155,"parent_xid":8589934593,"ctime":1512441811,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934997,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"PC\u7aef\u4f18\u5316","name_gbk":"PC\u7aef\u4f18\u5316","mtime":1512441811,"is_share":1},{"subscribe":0,"extname":"","used":1104164,"op_version":2862,"quota":1125899906842624,"quota_id":142,"parent_xid":8589934593,"ctime":1511172079,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":523986010114,"domain_id":1,"home_xid":8589934593,"user_xid":90,"mime":0,"status":0,"old_user_xid":90,"description":"","role":286331153,"name":"sdsdsds","name_gbk":"sdsdsds","mtime":1511172079,"is_share":1},{"subscribe":0,"extname":"","used":2750634526,"op_version":2993,"quota":1125899906842624,"quota_id":143,"parent_xid":8589934593,"ctime":1511406813,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934870,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"Test","name_gbk":"Test","mtime":1511406813,"is_share":1},{"subscribe":0,"extname":"","used":0,"op_version":4188,"quota":1125899906842624,"quota_id":150,"parent_xid":8589934593,"ctime":1512022923,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":55834574882,"domain_id":1,"home_xid":8589934593,"user_xid":13,"mime":0,"status":0,"old_user_xid":13,"description":"","role":286331153,"name":"wf","name_gbk":"wf","mtime":1512022923,"is_share":1},{"subscribe":1,"extname":"","used":33930569,"op_version":2469,"quota":1125899906842624,"quota_id":63,"parent_xid":8589934593,"ctime":1510211129,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":55834574850,"domain_id":1,"home_xid":8589934593,"user_xid":13,"mime":0,"status":0,"old_user_xid":13,"description":"","role":286331153,"name":"wf1","name_gbk":"wf1","mtime":1510211129,"is_share":1},{"subscribe":0,"extname":"","used":94201,"op_version":1770,"quota":1125899906842624,"quota_id":64,"parent_xid":8589934593,"ctime":1510211141,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":55834574851,"domain_id":1,"home_xid":8589934593,"user_xid":13,"mime":0,"status":0,"old_user_xid":13,"description":"","role":286331153,"name":"wf2","name_gbk":"wf2","mtime":1510211141,"is_share":1},{"subscribe":1,"extname":"","used":4276739,"op_version":4181,"quota":1125899906842624,"quota_id":89,"parent_xid":8589934593,"ctime":1510552544,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":98784247811,"domain_id":1,"home_xid":8589934593,"user_xid":95,"mime":0,"status":0,"old_user_xid":95,"description":"","role":286331153,"name":"wf4","name_gbk":"wf4","mtime":1510552544,"is_share":1},{"subscribe":0,"extname":"","used":18,"op_version":4446,"quota":1125899906842624,"quota_id":151,"parent_xid":8589934593,"ctime":1512378609,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934993,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"zhaolei","name_gbk":"zhaolei","mtime":1512378609,"is_share":1},{"subscribe":0,"extname":"","used":6518037,"op_version":4447,"quota":1125899906842624,"quota_id":152,"parent_xid":8589934593,"ctime":1512378620,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934994,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"zzz","name_gbk":"zzz","mtime":1512378620,"is_share":1},{"subscribe":0,"extname":"","used":9,"op_version":2426,"quota":1125899906842624,"quota_id":69,"parent_xid":8589934593,"ctime":1510297299,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":42949672962,"domain_id":1,"home_xid":8589934593,"user_xid":10,"mime":0,"status":0,"old_user_xid":10,"description":"","role":286331153,"name":"\u54c8\u54c8","name_gbk":"\u54c8\u54c8","mtime":1510297299,"is_share":1},{"subscribe":0,"extname":"","used":142802,"op_version":3069,"quota":1125899906842624,"quota_id":144,"parent_xid":8589934593,"ctime":1511421433,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934886,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"\u5386\u53f2\u7248\u672c\u8fd8\u539f","name_gbk":"\u5386\u53f2\u7248\u672c\u8fd8\u539f","mtime":1511421433,"is_share":1},{"subscribe":1,"extname":"","used":5334043896,"op_version":976,"quota":1125899906842624,"quota_id":50,"parent_xid":8589934593,"ctime":1509609635,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":21474836482,"domain_id":1,"home_xid":8589934593,"user_xid":52,"mime":0,"status":0,"old_user_xid":52,"description":"","role":286331153,"name":"\u53bb\u53bb\u53bb","name_gbk":"\u53bb\u53bb\u53bb","mtime":1509609635,"is_share":1},{"subscribe":0,"extname":"","used":1173624559,"op_version":2843,"quota":1125899906842624,"quota_id":67,"parent_xid":8589934593,"ctime":1510285786,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":38654705705,"domain_id":1,"home_xid":8589934593,"user_xid":9,"mime":0,"status":0,"old_user_xid":9,"description":"","role":286331153,"name":"\u6743\u9650","name_gbk":"\u6743\u9650","mtime":1510285786,"is_share":1},{"subscribe":1,"extname":"","used":1175048,"op_version":2779,"quota":1125899906842624,"quota_id":22,"parent_xid":8589934593,"ctime":1509436937,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":47244640258,"domain_id":1,"home_xid":8589934593,"user_xid":11,"mime":0,"status":0,"old_user_xid":11,"description":"","role":286331153,"name":"\u65b0\u5efa\u6587\u4ef6\u5939","name_gbk":"\u65b0\u5efa\u6587\u4ef6\u5939","mtime":1509436937,"is_share":1},{"subscribe":0,"extname":"","used":350446,"op_version":1024,"quota":1125899906842624,"quota_id":58,"parent_xid":8589934593,"ctime":1510196828,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":219043332098,"domain_id":1,"home_xid":8589934593,"user_xid":51,"mime":0,"status":0,"old_user_xid":51,"description":"","role":286331153,"name":"\u9884\u89c8\u8005\u6d4b\u8bd5","name_gbk":"\u9884\u89c8\u8005\u6d4b\u8bd5","mtime":1510196828,"is_share":1},{"subscribe":1,"extname":"","used":2003112663,"op_version":4440,"quota":107374182400,"quota_id":62,"parent_xid":8589934593,"ctime":1510197872,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":8589934608,"domain_id":1,"home_xid":8589934593,"user_xid":2,"mime":0,"status":0,"old_user_xid":2,"description":"","role":286331153,"name":"\u5f20\u632f\u82b8\u7ba1\u7406\u8005","name_gbk":"\u5f20\u632f\u82b8\u7ba1\u7406\u8005","mtime":1510197872,"is_share":1},{"subscribe":1,"extname":"","used":2159763310,"op_version":4476,"quota":1125899906842624,"quota_id":66,"parent_xid":8589934593,"ctime":1510279024,"xsize":0,"file_version":0,"rsum":"","xtype":1,"sha1":"","xid":227633267864,"domain_id":1,"home_xid":8589934593,"user_xid":94,"mime":0,"status":0,"old_user_xid":94,"description":"","role":286331153,"name":"\u5f20\u632f\u82b8\u6240\u6709\u8005","name_gbk":"\u5f20\u632f\u82b8\u6240\u6709\u8005","mtime":1510279024,"is_share":1}],"has_more":0,"cursor":"eyJodiI6ICJcdTVmMjBcdTYzMmZcdTgyYjhcdTYyNDBcdTY3MDlcdTgwMDUiLCAiaSI6IDIyNzYzMzI2Nzg2NCwgInAiOiA4NTg5OTM0NTkzLCAiYiI6IDEsICJhIjogMCwgIm9mIjogZmFsc2UsICJ0IjogMSwgImQiOiAxLCAib2QiOiBmYWxzZX0="},"code":0}', true);


        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_root_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_root_list_continue($token, $cursor)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'cursor' => $cursor));
        $res = $this->request_netdisk_server_v2("/xfile/dir/share/root/list/continue", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.xfile/dir/share/root/list/continue, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_list($token, $xid, $sortBy = 1, $order = 1)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'sortBy' => $sortBy, 'order' => $order));
        $res = $this->request_netdisk_server_v2("/xfile/dir/share/page/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/dir/share/page/list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_list_continue($token, $cursor)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'cursor' => $cursor));
        $res = $this->request_netdisk_server_v2("/xfile/dir/share/page/list/continue", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.xfile/xfile/dir/share/page/list/continue, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_page_list($token, $xid, $isShare)
    {
        if (0 == $xid) {
            $post_data = $this->my_json_encode(array("token" => $token));
        } else {
            $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid));
        }
        if (1 == $isShare) {
            $res = $this->request_netdisk_server_v2("/xfile/dir/share/dir/page/list", $post_data);
        } else {
            $res = $this->request_netdisk_server_v2("/xfile/dir/cage/dir/page/list", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_page_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_page_list_continue($token, $cursor, $isShare)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'cursor' => $cursor));
        if (1 == $isShare) {
            $res = $this->request_netdisk_server_v2("/xfile/dir/share/dir/page/list/continue", $post_data);
        } else {
            $res = $this->request_netdisk_server_v2("/xfile/dir/cage/dir/page/list/continue", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.xfile/xfile/dir/share(cage)/dir/page/list/continue, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_cage_list($token, $xid, $sortBy = 1, $order = 1)
    {
        if ($xid == 0) {
            $post_data = $this->my_json_encode(array("token" => $token, 'sortBy' => $sortBy, 'order' => $order));
        } else {
            $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'sortBy' => $sortBy, 'order' => $order));
        }
        $res = $this->request_netdisk_server_v2("/xfile/dir/cage/page/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/dir/cage/page/list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_cage_list_continue($token, $cursor)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'cursor' => $cursor));
        $res = $this->request_netdisk_server_v2("/xfile/dir/cage/page/list/continue", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.xfile/xfile/dir/cage/page/list/continue, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_cage_dir_list($token, $xid)
    {

        if ($xid == 0) {
            $post_data = $this->my_json_encode(array("token" => $token));
        } else {
            $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid));
        }
        $res = $this->request_netdisk_server("/xfile/dir/cage/dir/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_cage_dir_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function dir_cage_create($token, $xid, $name)
    {
        if ($xid == 0) {
            $post_data = $this->my_json_encode(array("token" => $token, 'name' => $name));
        } else {
            $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'name' => $name));
        }
        $res = $this->request_netdisk_server("/xfile/dir/cage/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_cage_create, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function dir_cage_delete($token, $xids)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xids));
        $res = $this->request_netdisk_server_v2("/xfile/cage/delete", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_cage_delete, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_cage_delete_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/cage/delete/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/cage/delete/check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_delete($token, $xids)
    {

        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xids));
        $res = $this->request_netdisk_server_v2("/xfile/share/delete", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_delete, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_delete_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/share/delete/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/share/delete/check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;

    }

    function dir_share_role_list($token, $xid)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/dir/share/role/list", $post_data);
        $res_data = json_decode($res, true);
    
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_role_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function dir_share_role_grant($token, $xid, $perm)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'perm' => $perm));
        $res = $this->request_netdisk_server("/xfile/dir/share/role/grant", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_role_grant, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    //deep access
    function dir_share_role_deep_grant($token, $xid, $perm)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid, 'perm' => $perm));
        $res = $this->request_netdisk_server("/xfile/dir/share/role/deep/grant", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.dir_share_role_deep_grant, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function info($token, $xid)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/info", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.info, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function share_archive_batch($token, $xids)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xids' => $xids));
        $res = $this->request_netdisk_server_v2("/xfile/share/batch/archive", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.share_batch_archive, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function share_archive_batch_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/share/batch/archive/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/share/batch/archive/check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function ancestor_list($token, $xid)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/ancestor/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.ancestor_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function share_rename($token, $xid, $name)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'name' => $name, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/share/rename", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.share_rename, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function cage_rename($token, $xid, $name)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'name' => $name, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/cage/rename", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.cage_rename, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function cage_archive_batch($token, $xids)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xids' => $xids));
        $res = $this->request_netdisk_server_v2("/xfile/cage/batch/archive", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.cage_batch_archive, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function cage_archive_batch_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/cage/batch/archive/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/cage/batch/archive/check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function recover($token, $xids, $isShare)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'xid' => $xids));
        if (1 == $isShare) {
            $res = $this->request_netdisk_server_v2("/xfile/share/recover", $post_data);
        } else {
            $res = $this->request_netdisk_server_v2("/xfile/cage/recover", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.recover, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function recover_check($token, $jobId, $isShare)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        if (1 == $isShare) {
            $res = $this->request_netdisk_server_v2("/xfile/share/recover/check", $post_data);
        } else {
            $res = $this->request_netdisk_server_v2("/xfile/cage/recover/check", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.recover_check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function share_archive_page_list($token, $order, $pageMax = 200, $pageIdx = 0, $mtimeMin, $mtimeMax)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'order' => $order,
            'pageMax' => $pageMax,
            'pageIdx' => $pageIdx,
            'mtimeMin' => $mtimeMin,
            'mtimeMax' => $mtimeMax
        ));
        //$res = $this->request_netdisk_server_v2("/xfile/share/archive/page/list", $post_data);
        //$res_data = json_decode($res, true);
        $res_data = json_decode('{"data": [{"parent_xid": 34359738370, "role": 286331153, "xid": 38654705676, "status": 1, "user_xid": 9, "xsize": 721, "name": "123.txt", "xtype": 0, "mtime": 1513237963}, {"parent_xid": 8589934886, "role": 286331153, "xid": 8589934893, "status": 1, "user_xid": 2, "xsize": 24064, "name": "mac\u7aef\u6587\u4ef6\u5939\u4e0b\u8f7d\u6d4b\u8bd5\u7528\u4f8b.xls", "xtype": 0, "mtime": 1513049801}, {"parent_xid": 8589934608, "role": 286331153, "xid": 8589934777, "status": 1, "user_xid": 2, "xsize": 1123, "name": "zhangzhenyun.txt", "xtype": 0, "mtime": 1512615053}, {"parent_xid": 8589934993, "role": 286331153, "xid": 8589934995, "status": 1, "user_xid": 2, "xsize": 9, "name": "2017-12-04 17-13.txt", "xtype": 0, "mtime": 1512378656}, {"parent_xid": 8589934608, "role": 286331153, "xid": 8589934990, "status": 1, "user_xid": 2, "xsize": 1080, "name": "zhangzhenyun.txt", "xtype": 0, "mtime": 1512368977}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640331, "status": 1, "user_xid": 11, "xsize": 11, "name": "2.txt", "xtype": 0, "mtime": 1512113443}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640329, "status": 1, "user_xid": 11, "xsize": 11, "name": "1.txt", "xtype": 0, "mtime": 1512113120}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640328, "status": 1, "user_xid": 11, "xsize": 11, "name": "1.txt", "xtype": 0, "mtime": 1512112999}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707046, "status": 1, "user_xid": 9, "xsize": 12, "name": "2017-12-01 15-16.txt", "xtype": 0, "mtime": 1512112539}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707042, "status": 1, "user_xid": 9, "xsize": 10, "name": "2017-12-01 15-11.txt", "xtype": 0, "mtime": 1512112155}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707040, "status": 1, "user_xid": 9, "xsize": 10, "name": "2017-12-01 15-07.txt", "xtype": 0, "mtime": 1512112060}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707039, "status": 1, "user_xid": 9, "xsize": 8, "name": "2017-12-01 15-07.txt", "xtype": 0, "mtime": 1512111970}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707038, "status": 1, "user_xid": 9, "xsize": 8, "name": "2017-12-01 15-05.txt", "xtype": 0, "mtime": 1512111897}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707037, "status": 1, "user_xid": 9, "xsize": 9, "name": "2017-12-01 15-05.txt", "xtype": 0, "mtime": 1512111827}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707036, "status": 1, "user_xid": 9, "xsize": 9, "name": "2017-12-01 15-05.txt", "xtype": 0, "mtime": 1512111762}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640325, "status": 1, "user_xid": 11, "xsize": 20476, "name": "123.xlsx", "xtype": 0, "mtime": 1512110357}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640324, "status": 1, "user_xid": 11, "xsize": 10239, "name": "123.xlsx", "xtype": 0, "mtime": 1512109989}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640323, "status": 1, "user_xid": 11, "xsize": 10232, "name": "123.xlsx", "xtype": 0, "mtime": 1512109851}, {"parent_xid": 8589934886, "role": 286331153, "xid": 47244640322, "status": 1, "user_xid": 11, "xsize": 10237, "name": "123.xlsx", "xtype": 0, "mtime": 1512109269}, {"parent_xid": 8589934886, "role": 286331153, "xid": 8589934986, "status": 1, "user_xid": 2, "xsize": 10209, "name": "123.xlsx", "xtype": 0, "mtime": 1512109143}, {"parent_xid": 8589934886, "role": 286331153, "xid": 38654707035, "status": 1, "user_xid": 9, "xsize": 10238, "name": "123.xlsx", "xtype": 0, "mtime": 1512100029}, {"parent_xid": 8589934886, "role": 286331153, "xid": 8589934887, "status": 1, "user_xid": 2, "xsize": 13275, "name": "a_12334.docx", "xtype": 0, "mtime": 1512097989}, {"parent_xid": 8589934886, "role": 286331153, "xid": 8589934891, "status": 1, "user_xid": 2, "xsize": 10167, "name": "123.xlsx", "xtype": 0, "mtime": 1512097960}, {"parent_xid": 227633267864, "role": 286331153, "xid": 408021893203, "status": 1, "user_xid": 95, "xsize": 248, "name": "\u65b0\u5efa\u6587\u672c\u6587\u6863.txt", "xtype": 0, "mtime": 1512023610}, {"parent_xid": 8589934945, "role": 286331153, "xid": 8589934974, "status": 1, "user_xid": 2, "xsize": 467, "name": "guota.txt", "xtype": 0, "mtime": 1511957892}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934938, "status": 1, "user_xid": 2, "xsize": 25235944, "name": "20160613\u4e91\u76d8\u5ba3\u4f20\u518c (1) (1) (1).pdf", "xtype": 0, "mtime": 1511926198}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934937, "status": 1, "user_xid": 2, "xsize": 2400678, "name": "\u9879\u76ee\u7ba1\u7406\u8f6f\u6280\u672f\u8bfe\u4ef6---\u7b2c\u4e94\u7ae0 (1).pdf", "xtype": 0, "mtime": 1511926176}, {"parent_xid": 98784247811, "role": 286331153, "xid": 55834574862, "status": 1, "user_xid": 13, "xsize": 12, "name": "2017-11-20 11-38.txt", "xtype": 0, "mtime": 1511852005}, {"parent_xid": 55834574850, "role": 286331153, "xid": 55834574852, "status": 1, "user_xid": 13, "xsize": 37888, "name": "office\u63d2\u4ef6-\u4f01\u4e1a\u7a7a\u95f4.xls", "xtype": 0, "mtime": 1511851294}, {"parent_xid": 55834574850, "role": 286331153, "xid": 55834574860, "status": 1, "user_xid": 13, "xsize": 343928, "name": "kingfile\u6f0f\u6d1e\u8bb0\u5f55.pdf", "xtype": 0, "mtime": 1511851287}, {"parent_xid": 98784247811, "role": 286331153, "xid": 55834574873, "status": 1, "user_xid": 13, "xsize": 561276, "name": "Lighthouse.jpg", "xtype": 0, "mtime": 1511839327}, {"parent_xid": 98784247811, "role": 286331153, "xid": 55834574869, "status": 1, "user_xid": 13, "xsize": 879394, "name": "Chrysanthemum.jpg", "xtype": 0, "mtime": 1511839324}, {"parent_xid": 98784247811, "role": 286331153, "xid": 55834574872, "status": 1, "user_xid": 13, "xsize": 0, "name": "\u65b0\u5efa\u6587\u4ef6\u5939", "xtype": 1, "mtime": 1511838915}, {"parent_xid": 98784247811, "role": 286331153, "xid": 55834574868, "status": 1, "user_xid": 13, "xsize": 0, "name": "wwww", "xtype": 1, "mtime": 1511838907}, {"parent_xid": 279172874287, "role": 286331153, "xid": 279172874289, "status": 1, "user_xid": 65, "xsize": 37922005, "name": "Public_KingFile_NetDisk_2.2.0.36.exe", "xtype": 0, "mtime": 1511838058}, {"parent_xid": 8589934605, "role": 286331153, "xid": 8589934915, "status": 1, "user_xid": 2, "xsize": 142497, "name": "page-1-7-\u7ba1\u7406\u5458\u767b\u5f55-copy.jpg", "xtype": 0, "mtime": 1511836545}, {"parent_xid": 279172874287, "role": 286331153, "xid": 279172874288, "status": 1, "user_xid": 65, "xsize": 37924913, "name": "Public_KingFile_NetDisk_2.2.0.36.exe", "xtype": 0, "mtime": 1511785577}, {"parent_xid": 38654705705, "role": 286331153, "xid": 38654707005, "status": 1, "user_xid": 9, "xsize": 7887152, "name": "\u7f51\u5361\u9a71\u52a8.rar", "xtype": 0, "mtime": 1511781411}, {"parent_xid": 38654705705, "role": 286331153, "xid": 8589934747, "status": 1, "user_xid": 2, "xsize": 2069210, "name": "CheatSheet_1.2.8.zip", "xtype": 0, "mtime": 1511781411}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934907, "status": 1, "user_xid": 2, "xsize": 99328, "name": "\u4f1a\u8bae\u5ba4\u4f7f\u7528\u7ba1\u7406\u529e\u6cd5.wps", "xtype": 0, "mtime": 1511771714}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934901, "status": 1, "user_xid": 2, "xsize": 93184, "name": "\u4eba\u529b\u8d44\u6e90\u91d1\u5c71\u6587\u4ef6(\u63a5\u5f85).wps", "xtype": 0, "mtime": 1511771714}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934900, "status": 1, "user_xid": 2, "xsize": 10240, "name": "\u6d3e\u8f66\u5355.et", "xtype": 0, "mtime": 1511771514}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934904, "status": 1, "user_xid": 2, "xsize": 23040, "name": "Thumbs.db", "xtype": 0, "mtime": 1511771388}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934913, "status": 1, "user_xid": 2, "xsize": 9954304, "name": "OA\u65b0\u5458\u5de5\u57f9\u8bad.dps", "xtype": 0, "mtime": 1511771383}, {"parent_xid": 8589934897, "role": 286331153, "xid": 8589934903, "status": 1, "user_xid": 2, "xsize": 241664, "name": "2003\u5e74MSDN\u5149\u76d8\u76ee\u5f55.et", "xtype": 0, "mtime": 1511771377}, {"parent_xid": 8589934824, "role": 286331153, "xid": 8589934855, "status": 1, "user_xid": 2, "xsize": 552, "name": "PINELUMB.HTM", "xtype": 0, "mtime": 1511770430}, {"parent_xid": 8589934824, "role": 286331153, "xid": 8589934854, "status": 1, "user_xid": 2, "xsize": 529, "name": "PAWPRINT.HTM", "xtype": 0, "mtime": 1511770430}, {"parent_xid": 8589934824, "role": 286331153, "xid": 8589934853, "status": 1, "user_xid": 2, "xsize": 140336, "name": "OSPP.HTM", "xtype": 0, "mtime": 1511770430}, {"parent_xid": 38654705705, "role": 286331153, "xid": 38654707000, "status": 1, "user_xid": 9, "xsize": 173688173, "name": "64\u7ea7\u6587\u4ef6\u5939.rar", "xtype": 0, "mtime": 1511767773}, {"parent_xid": 55834574850, "role": 286331153, "xid": 8589934658, "status": 1, "user_xid": 2, "xsize": 25235944, "name": "20160613\u4e91\u76d8\u5ba3\u4f20\u518c (1) (1).pdf", "xtype": 0, "mtime": 1511067756}], "code": 0}', true); 
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.share_archive_page_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function cage_archive_page_list($token, $order, $pageMax = 200, $pageIdx = 0, $mtimeMin, $mtimeMax)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'order' => $order,
            'pageMax' => $pageMax,
            'pageIdx' => $pageIdx,
            'mtimeMin' => $mtimeMin,
            'mtimeMax' => $mtimeMax
        ));
        $res = $this->request_netdisk_server_v2("/xfile/cage/archive/page/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.cage_archive_page_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function cage_move_batch($token, $xids, $dstXid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'xids' => $xids,
            'dstXid' => $dstXid));
        $res = $this->request_netdisk_server_v2("/xfile/cage/batch/move", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.cage_move_batch, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function cage_move_batch_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/cage/batch/move/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.cage_move_batch_check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function share_move_batch($token, $xids, $dstXid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'xids' => $xids,
            'dstXid' => $dstXid));
        $res = $this->request_netdisk_server_v2("/xfile/share/batch/move", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.share_move_batch, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function share_move_batch_check($token, $jobId)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        $res = $this->request_netdisk_server_v2("/xfile/share/batch/move/check", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.share_move_batch_check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function thumbnail($token, $xid, $width, $height)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'xid' => $xid,
            'width' => $width,
            'height' => $height));
        $res = $this->request_netdisk_server("/xfile/file/thumbnail", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.thumbnail, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function linkcreate($token, $xid, $password, $age, $maxCount)
    {

        if (empty($maxCount)) {
            $post_data = array(
                "token" => $token,
                'xid' => $xid,
                'password' => $password,
                'age' => $age
            );
        } else {
            $post_data = array(
                "token" => $token,
                'xid' => $xid,
                'password' => $password,
                'age' => $age,
                'maxCount' => intval($maxCount)
            );
        }
        $post_data = $this->my_json_encode($post_data);

        $res = $this->request_netdisk_server("/xfile/link/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.linkcreate, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function create_inside_link($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'xid' => $xid
        ));
        $res = $this->request_netdisk_server("/xfile/insidelink/create", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/insidelink/create, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function linkverify($link, $password)
    {
        $post_data = $this->my_json_encode(array(
            "link" => $link,
            'password' => $password));
        $res = $this->request_netdisk_server("/xfile/link/verify", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.linkverify, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function inside_link_verify($token, $link)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "insidelink" => $link
        ));
        $res = $this->request_netdisk_server("/xfile/insidelink/verify", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model./xfile/insidelink/verify, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function linkupdate($token, $linkId, $password, $age)
    {
        log_message('debug', 'model password=' . $password);
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "linkId" => $linkId,
            'password' => $password,
            'age' => $age));
        log_message('debug', $post_data);
        $res = $this->request_netdisk_server("/xfile/link/update", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.linkupdate, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function linkdelete($token, $links)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "links" => $links));
        $res = $this->request_netdisk_server("/xfile/link/delete", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.linkdelete, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function requestlink($link)
    {
        return $res_data = array('code' => 0, 'data' => array('pwd' => '1'));
    }


    function linklist($token)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token));
        $res = $this->request_netdisk_server("/xfile/link/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.linklist, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    /**
     * 订阅文件
     */
    function subscribe($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/subscribe", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.subscribe, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    /**
     * 订阅文件
     */
    function unsubscribe($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/xfile/unsubscribe", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.unsubscribe, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    /**
     * 订阅列表
     */
    function subscribe_list($token)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token));
        $res = $this->request_netdisk_server("/xfile/subscribe/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.subscribe_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    /**
     * 历史版本列表
     */
    function history_list($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid));
        $res = $this->request_netdisk_server("/xfile/file/history/list", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.history_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    /**
     * 备注
     */
    function history_remark($token, $xid, $fileVer, $remark)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid,
            "fileVer" => $fileVer,
            "remark" => $remark
        ));
        $res = $this->request_netdisk_server("/xfile/file/history/remark", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.history_remark, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function history_recover($token, $xid, $fileVer)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid,
            "fileVer" => $fileVer
        ));
        $res = $this->request_netdisk_server("/xfile/file/history/revert", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.history_revert, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    /**
     * 删除被移交的员工个人目录
     */
    function recycle_del_file($token, $xid, $userXid)
    {

        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid,
            "userXid" => $userXid
        ));
        $res = $this->request_netdisk_server("/xfile/recycle/delete", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.recycle_del_file, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


    function level_search($token, $xid, $name)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid,
            "name" => $name
        ));
        $res = $this->request_netdisk_server("/xfile/level/search", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.level_search, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function global_search($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server_v2("/xfile/global/search", $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.global_search, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function file_lock_status($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid
        ));
        $res = $this->request_netdisk_server("/xfile/lock/status", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function file_lock($token, $xid, $status)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            "xid" => $xid,
            "status" => $status
        ));
        $res = $this->request_netdisk_server("/xfile/lock", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function copy_file_batch($token, $xids, $dstXid, $is_share)
    {
        $post_data = $this->my_json_encode(
            array(
                "token" => $token,
                'xids' => $xids,
                'dstXid' => $dstXid
            )
        );
        if ($is_share == 1) {
            $res = $this->request_netdisk_server_v2("/xfile/share/file/batch/copy", $post_data);
        } else {
            if ($dstXid == 0) {
                $post_data = $this->my_json_encode(array(
                    "token" => $token,
                    'xids' => $xids
                ));
            }
            $res = $this->request_netdisk_server_v2("/xfile/cage/file/batch/copy", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.copy_file_batch, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function copy_file_batch_check_check($token, $jobId, $isShare)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'jobId' => $jobId
        ));
        if ($isShare == 1) {
            $res = $this->request_netdisk_server_v2("/xfile/share/file/batch/copy/check", $post_data);
        } else {
            $res = $this->request_netdisk_server_v2("/xfile/cage/file/batch/copy/check", $post_data);
        }
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.copy_file_batch_check, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }

        return $res_data;

    }


    function file_page_list($token, $xid, $sortBy, $order, $is_share)
    {
        $post_data = $this->my_json_encode(array(
            "token" => $token,
            'xid' => $xid,
            'sortBy' => $sortBy,
            'order' => $order
        ));
        if ($xid == 0) {
            $post_data = $this->my_json_encode(array(
                "token" => $token,
                'sortBy' => $sortBy,
                'order' => $order
            ));
        }

        if ($is_share == 0) {
            $addr = "/xfile/dir/cage/file/page/list";
        } else {
            $addr = "/xfile/dir/share/file/page/list";
        }
        $res = $this->request_netdisk_server_v2($addr, $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.file_page_list, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }

    function file_page_list_continue($token, $cursor, $isShare)
    {
        $post_data = $this->my_json_encode(array("token" => $token, 'cursor' => $cursor));
        if ($isShare == 0) {
            $addr = "/xfile/dir/cage/file/page/list/continue";
        } else {
            $addr = "/xfile/dir/share/file/page/list/continue";
        }
        $res = $this->request_netdisk_server_v2($addr, $post_data);
        $res_data = json_decode($res, true);
        if (empty($res_data) || API_RET_SUCC != $res_data['code']) {
            $error_msg = 'fun=xfile_model.xfile/dir/share(cage)/file/page/list/continue, ';
            $error_msg .= 'msg=response error value, params=' . $post_data . ', res=' . $res;
            log_message('error', $error_msg);
        }
        return $res_data;
    }


}