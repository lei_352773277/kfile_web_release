<?php

/**
 * 部门相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Org_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function domain($domain_data)
    {
        $post_data = $this->my_json_encode($domain_data);
        $res = $this->request_netdisk_server("/org/domain/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function domain_info($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/domain/info", $post_data);
        $res_data = json_decode($res, true);
       
        return $res_data;
    }

    function domain_space_info($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/domain/space/info", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    function staff_space_info($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/staff/space/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_all($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/dept/all", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_list($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/dept/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_info($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/dept/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_create($token, $xid, $deptName, $deptDesc, $ldapDn = "")
    {
        $postData = array('token' => $token, 'xid' => $xid, 'deptName' => $deptName, 'deptDesc' => $deptDesc);
        if ("" != $ldapDn) {
            $postData['ldapDn'] = $ldapDn;
        }
        $post_data = $this->my_json_encode($postData);
        $res = $this->request_netdisk_server("/org/dept/create", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function dept_delete($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/dept/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_modify($token, $xid, $deptName, $deptDesc)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid, 'deptDesc' => $deptDesc, 'deptName' => $deptName));
        $res = $this->request_netdisk_server("/org/dept/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function dept_move($token, $xid, $dstXid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid, 'dstXid' => $dstXid));
        $res = $this->request_netdisk_server("/org/dept/move", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function staff_create($token, $deptXid, $userName, $password, $staffName, $quota = "")
    {
        $post_data = array(
            'token' => $token,
            'deptXid' => $deptXid,
            'userName' => $userName,
            'password' => $password,
            'staffName' => $staffName
        );
        if ("" != $quota) {
            $post_data['quota'] = $quota;
        }
        $res = $this->request_netdisk_server("/org/staff/create", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function staff_info($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/staff/info", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    function staff_modify($token, $xid, $staffName, $dstDeptXid, $qutoa = "")
    {
        $post_data = $this->my_json_encode(array(
            'token' => $token,
            'xid' => intval($xid),
            'staffName' => $staffName,
            'dstDeptXid' => $dstDeptXid,
            'quota' => $qutoa));
        $res = $this->request_netdisk_server("/org/staff/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function staff_delete($token, $xid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid));
        $res = $this->request_netdisk_server("/org/staff/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function staff_move($token, $xid, $dstDeptXid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid, 'dstDeptXid' => $dstDeptXid));
        $res = $this->request_netdisk_server("/org/staff/move", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 获取ldap白名单，只有白名单的用户才能登录
     */
    function get_ldap_whitelist()
    {
        return array("code" => 0, "data" => array(
            array("xid" => 1, "name" => "test01", "dn" => "asdf,adfad,adfasd"),
            array("xid" => 2, "name" => "test02", "dn" => "12312,342342,234234"),
            array("xid" => 3, "name" => "test03", "dn" => "12312,554,454,454")
        ));
    }


    /**
     * 企业设置
     */
    function config_setting($token, $domainId, $settings)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId, 'settings' => $settings));
        $res = $this->request_netdisk_server("/org/domain/config/ldap/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 获取设置
     */
    function get_config_setting($token, $domainId)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId));
        $res = $this->request_netdisk_server("/org/domain/config/detail", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    /**
     *获取企业的员工数
     */
    function members_count($token, $domainId)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId));
        $res = $this->request_netdisk_server("/org/domain/members/count", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }


    /**
     * 获取子超管列表
     */
    function manager_list($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/vice/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 获取子超管详情
     */
    function manager_info($token, $xid)
    {
        $post_data = $this->my_json_encode(array(
            'token' => $token,
            'xid' => $xid
        ));
        $res = $this->request_netdisk_server("/org/vice/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 添加子超管
     */
    function manager_create($token, $deptXids, $userName, $password)
    {
        $post_data = array(
            'token' => $token,
            'deptXids' => $deptXids,
            'userName' => $userName,
            'password' => $password
        );
        $res = $this->request_netdisk_server("/org/vice/create", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 修改子超管
     */
    function manager_modify($token, $deptXids, $xid)
    {
        $post_data = array(
            'token' => $token,
            'deptXids' => $deptXids,
            'xid' => $xid
        );
        $res = $this->request_netdisk_server("/org/vice/relation/modify", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 删除子超管
     */
    function manager_del($token, $xid)
    {
        $post_data = array(
            'token' => $token,
            'xid' => $xid
        );
        $res = $this->request_netdisk_server("/org/vice/delete", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 获取待归档用户列表
     */
    function recycle_users_list($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/staff/recycle/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 删除待归档用户
     */
    function recycle_del($token, $xid)
    {
        $post_data = array(
            'token' => $token,
            'xid' => $xid
        );
        $res = $this->request_netdisk_server("/org/staff/recycle/delete", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 移交员工的个人文件给其他人
     */
    function recycle_move($token, $xid, $dstXid)
    {
        $post_data = array(
            'token' => $token,
            'xid' => $xid,
            'dstXid' => $dstXid
        );
        $res = $this->request_netdisk_server("/org/staff/recycle/assign", $this->my_json_encode($post_data));
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /*
    * 设置license
    */
    function config_license_setting($token, $domainId, $license)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId, 'license' => $license));
        $res = $this->request_netdisk_server("/org/domain/config/license/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 移交文件并删除
     */
    function staff_recycle($token, $xid, $dstXid)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'xid' => $xid, 'dstXid' => $dstXid));
        $res = $this->request_netdisk_server("/org/staff/recycle", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 修改用户的空间
     */
    function change_staff_space($token, $xid, $quota)
    {
        $info_ret_data = $this->org_model->staff_info($token, $xid);
        if (API_RET_SUCC != $info_ret_data['code']) {
            return $this->format_error_msg(CODE_STAFF_NOT_FOUND, '获取用户信息失败');
        }

        $space_ret_data = $this->org_model->staff_space_info($token, $xid);
        if (API_RET_SUCC != $space_ret_data['code']) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '获取用户空间信息失败');
        }
        $quota = $quota * 1024 * 1024 * 1024;
        $currSpace = $space_ret_data['data']['used'];
        if ($currSpace > $quota) {
            return $this->format_error_msg(CODE_SPACE_LESS_THAN_CURR, '设置的空间值比当前用户的空间小');
        }

        $dstDeptXid = $info_ret_data['data']['parent_xid'];
        $staffName = $info_ret_data['data']['name'];

        $ret_data = $this->staff_modify($token, $xid, $staffName, $dstDeptXid, $quota);
        return $ret_data;
    }

    /**
     * 详情
     */
    function restriction_detail($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/domain/share/root/xstaff/restriction/detail", $post_data);
        $res_data = json_decode($res, true);
        //$res_data = array("code"=>0,"data"=>array("whitelist"=>array(),"blacklist"=>array(array("user_xid"=>"16"))));
        return $res_data;
    }

    /**
     * 获取指定用户是否有创建共享文件夹的权限
     */
    function getUserPermission($token, $xid)
    {
        $detail = $this->restriction_detail($token);
        if (API_RET_SUCC != $detail['code']) {
            return "allow";
        }
        $currWhiteList = $detail['data']['whitelist'];
        $currBlackList = $detail['data']['blacklist'];
        $blackXidList = $whiteXidList = array();
        $setBlackList = $setWhiteList = False;
        if (!empty($currBlackList)) {
            $setBlackList = True;
            foreach ($currBlackList as $key => $value) {
                $blackXidList[] = $value['xid'];
            }
        }

        if (!empty($currWhiteList)) {
            $setWhiteList = True;
            foreach ($currWhiteList as $k => $v) {
                $whiteXidList[] = $v['xid'];
            }
        }
        if ($setBlackList && !$setWhiteList) {
            if (in_array($xid, $blackXidList)) {
                return "deny";
            } else {
                return "allow";
            }
        } else if (!$setBlackList && $setWhiteList) {
            if (in_array($xid, $whiteXidList)) {
                return "allow";
            } else {
                return "deny";
            }

        } else {
            return "allow";
        }
    }

    /**
     * 修改用户是否可以创建共享文件夹的权限
     */
    function create_share_folder_permission($token, $xid, $option = "allow")
    {
        $detail = $this->restriction_detail($token);
        if (API_RET_SUCC != $detail['code']) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '设置黑白名单格式错误');
        }
        $xid = intval($xid);
        $currWhiteList = $detail['data']['whitelist'];
        $currBlackList = $detail['data']['blacklist'];
        $blackXidList = array();
        $whiteXidList = array();
        $setBlackList = $setWhiteList = False;
        if (!empty($currBlackList)) {
            $setBlackList = True;
            foreach ($currBlackList as $key => $value) {
                $blackXidList[] = intval($value['xid']);
            }
        }

        if (!empty($currWhiteList)) {
            $setWhiteList = True;
            foreach ($currWhiteList as $k => $v) {
                $whiteXidList[] = intval($v['xid']);
            }
        }

        if ($setBlackList && $setWhiteList) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '设置黑白名单格式错误');
        }

        if ($setBlackList && !$setWhiteList) {//企业当前规则是黑名单
            if ("allow" == $option) {
                if (in_array($xid, $blackXidList)) {
                    $key = array_search($xid, $blackXidList);
                    if ($key !== false)
                        array_splice($blackXidList, $key, 1);
                }
            }

            if ("deny" == $option) {
                if (!in_array($xid, $blackXidList)) {
                    $blackXidList[] = $xid;
                }
            }

        } else if (!$setBlackList && $setWhiteList) {//企业当前规则是白名单
            if ("allow" == $option) {
                if (!in_array($xid, $whiteXidList)) {
                    $whiteXidList[] = $xid;
                }
            }

            if ("deny" == $option) {
                if (in_array($xid, $whiteXidList)) {
                    $key = array_search($xid, $whiteXidList);
                    if ($key !== false)
                        array_splice($whiteXidList, $key, 1);
                }
            }
        } else {//企业当前放行所有的用户，则默认采用黑名单
            if ("deny" == $option) {
                $blackXidList[] = $xid;
            }
        }

        $post_data = $this->my_json_encode(array('token' => $token, 'whitelist' => $whiteXidList, 'blacklist' => $blackXidList));
        $res = $this->request_netdisk_server("/org/domain/share/root/xstaff/restriction/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 重置整个企业用户可以创建共享文件夹
     */
    function reset_user_create_folder_permission($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'whitelist' => array(), 'blacklist' => array()));
        $res = $this->request_netdisk_server("/org/domain/share/root/xstaff/restriction/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 设置共享空间
     */
    function config_common_setting($token, $setting)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'settings' => $setting));
        $res = $this->request_netdisk_server("/org/domain/config/common/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 获取ldap的限制名单详情
     */
    function ldap_restriction_detail($token)
    {
        $post_data = $this->my_json_encode(array('token' => $token));
        $res = $this->request_netdisk_server("/org/domain/ldap/user/restriction/detail", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 添加ldap详情
     */
    function add_ldap_restriction($token, $dn)
    {
        $detail = $this->ldap_restriction_detail($token);
        if (empty($detail) || API_RET_SUCC != $detail['code']) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '设置黑白名单格式错误');
        }
        $currWhiteList = $detail['data']['whitelist'];
        $currBlackList = $detail['data']['blacklist'];
        $whiteXidList = array();
        if (in_array($dn, $currWhiteList)) {
            return array("code" => API_RET_SUCC, "data" => array());
        } else {
            foreach ($currWhiteList as $k => $v) {
                $whiteXidList[] = $v['user_dn'];
            }
            $whiteXidList[] = $dn;
            $post_data = $this->my_json_encode(array('token' => $token, 'whitelist' => $whiteXidList, 'blacklist' => array()));
            $res = $this->request_netdisk_server("/org/domain/ldap/user/restriction/setting", $post_data);
            $res_data = json_decode($res, true);
            return $res_data;
        }
    }

    /**
     * 删除ldap详情
     */
    function del_ldap_restriction($token, $dn)
    {
        $detail = $this->ldap_restriction_detail($token);
        if (API_RET_SUCC != $detail['code']) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '设置黑白名单格式错误');
        }
        $currWhiteList = $detail['data']['whitelist'];
        $currWhiteListVal = array();
        foreach ($currWhiteList as $item) {
            $currWhiteListVal[] = $item['user_dn'];
        }

        $currBlackList = $detail['data']['blacklist'];
        $whiteXidList = array();
        if (in_array($dn, $currWhiteListVal)) {
            foreach ($currWhiteList as $k => $v) {
                if ($dn != $v['user_dn']) {
                    $whiteXidList[] = $v['user_dn'];
                }
            }
            $post_data = $this->my_json_encode(array('token' => $token, 'whitelist' => $whiteXidList, 'blacklist' => array()));
            $res = $this->request_netdisk_server("/org/domain/ldap/user/restriction/setting", $post_data);
            $res_data = json_decode($res, true);
            return $res_data;
        } else {
            return array("code" => API_RET_SUCC, "data" => array());
        }
    }

    //批量添加ldap
    function batch_add_ldap_restriction($token, $whitelist)
    {
        $detail = $this->ldap_restriction_detail($token);
        if (empty($detail) || API_RET_SUCC != $detail['code']) {
            return $this->format_error_msg(CODE_SPACE_NOT_FOUND, '设置黑白名单格式错误');
        }
        $currWhiteList = $detail['data']['whitelist'];
        $currBlackList = $detail['data']['blacklist'];

        foreach ($currWhiteList as $k => $v) {
            if (!in_array($v['user_dn'], $whitelist)) {
                $whitelist[] = $v['user_dn'];
            }
        }

        $post_data = $this->my_json_encode(array('token' => $token, 'whitelist' => $whitelist, 'blacklist' => array()));
        $res = $this->request_netdisk_server("/org/domain/ldap/user/restriction/setting", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    /**
     * 导入指定的ldap用户
     */
    function ldapimportuser($ret_data, $selecteds, $synctype)
    {

        $ldap['base'] = $ret_data['data']['ldap_base_dn'];
        // connecting to ldap
        $ldap['user'] = $ret_data['data']['ldap_admin_name'];
        $ldap['pass'] = $ret_data['data']['ldap_admin_pwd'];
        $ldap['conn'] = ldap_connect($ret_data['data']['ldap_server']);
        // connecting to ldap
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);

        $justThese = array("dn");
        $objectClass = $synctype == "1" ? "(objectClass=organizationalUnit)" : "(objectClass=person)";
        $result = @ldap_search($ldap['conn'], $ldap['base'], $objectClass, $justThese) or
        self::res(array('code' => 3025, 'data' => ''));
        $data = ldap_get_entries($ldap['conn'], $result);

        //print_r($selecteds);
        $findRecords = array();
        foreach ($selecteds as $key => $value) {
            $findRecords[] = $value['dn'];
        }

        $filter_records = array();
        foreach ($data as $k => $v) {
            foreach ($findRecords as $item) {
                if ("1" == $synctype) {
                    if (strpos($v['dn'], $item) !== false && $item == $v['dn']) {
                        $filter_records[] = $v;
                        break;
                    }
                } else {
                    if (strpos($v['dn'], $item) !== false) {
                        $filter_records[] = $v;
                        break;
                    }
                }
            }
        }

        $whitelist = array();
        foreach ($filter_records as $k => $v) {
            $whitelist[] = $v['dn'];
        }


        return $whitelist;
        //print_r($filter_records);

        // print_r($data);

        /*
        $node_ref = array();
        $count = count(explode(",", $ldap['base']));

        foreach ($data as $key=>$value) {
            if(is_array($value)) {
                $list = explode(",", $value['dn']);
                $level = count($list) - $count;
                $name_str = array_shift($list);
                $name_str_list = explode("=", $name_str);
                $node_ref[$level][] = array('dn'=>$value['dn'],'name'=>$name_str_list[1],'parent_dn'=>implode(",", $list));
            }
        }
        */

    }


    function mime_distribution($token, $domainId)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId));
        $res = $this->request_netdisk_server("/org/inner/domain/statistics/xfiles/mime/distribution", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    function statistics_share_space($token, $domainId)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId));
        $res = $this->request_netdisk_server("/org/inner/domain/statistics/share/space", $post_data);
        $res_data = json_decode($res, true);
        
        return $res_data;
    }

    function statistics_cage_space($token, $domainId)
    {
        $post_data = $this->my_json_encode(array('token' => $token, 'domainId' => $domainId));
        $res = $this->request_netdisk_server("/org/inner/domain/statistics/cage/space", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }


    function iprule_list($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprule/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprule_create($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprule/create", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprule_modify($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprule/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprule_delete($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprule/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprestriction_list($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprestriction/list", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprestriction_create($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprestriction/create", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprestriction_modify($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprestriction/modify", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function iprestriction_delete($data)
    {
        $post_data = $this->my_json_encode($data);
        $res = $this->request_netdisk_server("/org/domain/iprestriction/delete", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function domain_info_by_domainIdent($token, $domainIdent)
    {
        $post_data = json_encode(array("token" => $token, 'domainIdent' => $domainIdent));
        $res = $this->request_netdisk_server("/org/inner/domain/info", $post_data);
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function report($xid, $password, $reason_type, $file_link, $token)
    {
        $post_data = json_encode(array('xid' => $xid, 'password' => $password, 'reason_type' => $reason_type, 'file_link' => $file_link, "token" => $token));
        $res = $this->request_netdisk_server("/audit/report", $post_data, '', '_audit');
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function audit_check($sha1, $isShare)
    {
        if (1 == $isShare) {
            $post_data = json_encode(array('file_link' => $sha1));
        } else {
            $post_data = json_encode(array('sha1' => $sha1));
        }
        $res = $this->request_netdisk_server("/audit/check", $post_data, '', '_audit');
        $res_data = json_decode($res, true);
        return $res_data;
    }

    function istrial($domain_id)
    {
        $data = 'domain_id=' . $domain_id;
        $res = $this->curl(KINGFILEOP . '/api/istrial', $data);
        log_message('debug', 'KINGFILEOP =/api/istrial' . "; post:" . $data . ",res=" . $res);
        $res_arr = json_decode($res, true);
        return $res_arr;
    }


}