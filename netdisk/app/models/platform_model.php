<?php

/**
 * 账户相关
 * User: qyuan
 * Date: 15-5-19
 * Time: 上午10:04
 */
class Platform_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function exceptKeys($setting) {
        $_exceptKeys = array("smtppass","smtpport");
        foreach($setting as $k=>$v) {
            if (in_array($k, $_exceptKeys)) {
                unset($setting[$k]);
            }
        }
        return $setting;
    }

    function getValues($keys) {
        $where = array();
        foreach($keys as $k) {
            $where[] = "keyname='".$k."'";
        }
        $table   = "ec_platform_setting";
        $str_sql  = "select * from {$table} where ".implode(" or ", $where);
        return $this->db->query($str_sql)->result_array();
    }


    function info()
    {
        $table   = "ec_platform_setting";
        $str_sql  = "select * from {$table}";
        return $this->db->query($str_sql)->result_array();
    }

    function get_value($keyname) 
    {
        $table   = "ec_platform_setting";
        $str_sql  = "select  value  from {$table} where keyname = '{$keyname}' limit 1";
        return $this->db->query($str_sql)->result_array();
    }

    function get_web_server() {
        //$https = $this->get_value('https');
        if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
            $access_ip = $_SERVER['HTTP_X_ORIGIN_HOST'];
        } else {
            $ip_arr = $this->get_value('ip');
            $ip = $ip_arr[0]['value'];
            $wanip_arr = $this->get_value('api_common_http_host');
            $wanip = $wanip_arr[0]['value'];
            $access_ip = trim($wanip)==""?$ip:$wanip;
        }

        if (isset($_SERVER['HTTP_X_ORIGIN_SCHEME'])) {
            $protocol  = $_SERVER['HTTP_X_ORIGIN_SCHEME'];
        } else {
            $protocol  = "http";
        }
        return $protocol."://".$access_ip;
        /*
        if("1" == $https[0]['value']) {
            return "https://".$access_ip;
        } else {
            return "http://".$access_ip;
        }
        */
    }

    function saveFeedBack($domain, $user, $content, $from, $qq) {
        $sql = "INSERT INTO feedback (`domain`,`user`,`content`,`qq`,`from`,`ctime`)VALUES(";
        $sql .= $this->db->escape($domain).",";
        $sql .= $this->db->escape($user).",";
        $sql .= $this->db->escape($content).",";
        $sql .= $this->db->escape($qq).",";
        $sql .= $this->db->escape($from).",";
        $sql .= time().")";
        return  $this->db->query($sql);

    }


    function getLogo($id) {
        $id = intval($id);
        $sql = "SELECT * from domainlogo WHERE domainId =".$id." limit 1";
        return $this->db->query($sql)->row_array();
    }

    function setLogo($id, $logo, $logotype) {
        $res = $this->getLogo($id);
        $logo = base64_encode($logo);
        if(empty($res)) {
            $sql = "INSERT INTO domainlogo (domainId, logo, logotype) VALUES({$id},'{$logo}','{$logotype}')";
        } else {
            $sql = "UPDATE domainlogo SET logo = '{$logo}', logotype='{$logotype}' WHERE domainId=".$id;
        }
        return $this->db->query($sql);
    }







}