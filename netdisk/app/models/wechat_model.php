<?php

class Wechat_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function info($token,$agent)
    {

        $post_data = $this->my_json_encode(array("token" => $token));
        $res = $this->request_netdisk_server_custom("/account/info", $post_data,$agent);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function staff_info($token,$xid,$agent)
    {
        $post_data = $this->my_json_encode(array('token'=>$token,'xid'=>$xid));
        $res = $this->request_netdisk_server_custom("/org/staff/info", $post_data,$agent);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function domain_space_info($token,$agent)
    {
        $post_data = $this->my_json_encode(array('token'=>$token));
        $res = $this->request_netdisk_server_custom("/org/domain/space/info", $post_data,$agent);
        $res_data = json_decode($res, true);
        return $res_data;
    }
    function  staff_space_info($token,$xid,$agent)
    {
        $post_data = $this->my_json_encode(array('token'=>$token,'xid'=>$xid));
        $res = $this->request_netdisk_server_custom("/org/staff/space/info", $post_data,$agent);
        $res_data = json_decode($res, true);
        return $res_data;
    }
}