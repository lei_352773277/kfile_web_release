<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User: qyuan
 * Date: 15-3-19
 * Time: 下午2:33
 */
class MY_Controller extends CI_Controller
{
    protected $account_info = "";
    protected $token = "";
    protected $ksyun_token = "";
    protected $sign = "";

    function __construct()
    {
        parent::__construct();

        $token = $this->input->cookie('netdisk_sid', TRUE);
        $this->token = $token;


        $this->ksyun_token = $this->input->cookie("kscdigest", TRUE);
        if ($this->ksyun_token == '""') {
            $this->ksyun_token = "";
        }

        $className = $this->router->class;
        $methodName = $this->router->method;
        $sign = $className . "_" . $methodName;
        $this->sign = $sign;
        if (!in_array($sign, $this->config->config['anonymous_access_pages'])) {
            if (empty($this->token)) {
                if ($this->input->is_ajax_request()) {
                    self::res(array('code' => 2001, 'msg' => 'Login is required'));
                } else {
                    $loginCode = $this->input->get('logincode');
                    //header("location:/?logincode=".$loginCode);
                    // exit;
                }
            }
        }


        //$res_data = $this->user_model->info($token);
        /*
        $uri = uri_string();
        if(API_RET_SUCC == $res_data['code']) {
            $this->account_info = $res_data;
            $this->token = $token;

            $is_super = $res_data['data']['is_superuser'];
            if (1 != $is_super) {
                if (in_array($uri, array("files", "audit", "department"))) {
                    header("location:/home");
                    exit;
                }
            } else {
                if (in_array($uri, array("home"))) {
                    header("location:/files");
                    exit;
                }
            }
        }
        */
    }

    /**
     * 输出给前端的格式
     * @param $result
     * @param string $data
     */
    protected function response($result, $data = "")
    {
        die(json_encode(array("ret" => $result, 'data' => $data)));
    }

    protected function res($res_data)
    {
        if (empty($res_data)) {
            die(json_encode(array('code' => 99999, 'msg' => 'api response empty')));
        } else {
            die(json_encode($res_data));
        }
        /*
        if(API_RET_SUCC === $res_data['code']) {
            if(isset($res_data['data'])) {
                self::response(API_RES_OK, $res_data['data']);
            } else {
                self::response(API_RES_OK);
            }
        } else {
            self::response($res_data['ret'],$res_data['data']);
        }
        */
    }


    protected function decryption()
    {

        $CI =& get_instance();
        $is_open_encryption = $CI->config->item('open_encryption');
        if (!$is_open_encryption) {
            return;
        }

        $encrypted_string = $this->input->cookie('netdisk_ld', TRUE);
        if (empty($encrypted_string)) {
            die(json_encode(array('code' => 2001)));
        }
        $encrypted_string = @hex2bin($encrypted_string);
        $key = $CI->config->item('encryption_key');
        $plaintext_string = rc4($key, $encrypted_string);
        if (empty($plaintext_string) || strpos($plaintext_string, "@@@") === False) {
            die(json_encode(array('code' => 2001)));
        }

        $arr = explode("@@@", $plaintext_string);

        if (count($arr) == 3) {
            $expire_date = $arr[0];
            if ($this->token != $arr[1]) {
                die(json_encode(array('code' => 2001)));
            }
            if ($expire_date < time()) {
                die(json_encode(array('code' => 900001, 'data' => array())));
            }
        } else {
            die(json_encode(array('code' => 900002, 'data' => array())));
        }
    }

    protected  function checkInputVal($val){
        if(preg_match("/[`*{}\|\"':;\\/?<>,]+/i",$val)){
            die(json_encode(array('code'=>1008,'msg'=>'含有特殊字符，请检查,val='.$val)));
        }
    }


    private function write_token($name,$val){

        $device_id = array(
            'name' => $name,
            'value' => $val,
            'expire' => time() + 99 * 365 * 24 * 3600,
            'prefix' => 'netdisk_user_'
        );
        $this->input->set_cookie($device_id);
    }

    //检查用户身份 //超管和子超管
    protected  function checkuseridentity(){
        if(!isset($_COOKIE['netdisk_user_identity'])) {
            $this->load->model('account_model');
            $ret_data = $this->account_model->info($this->token);
            //超管和子超管有人员管理的权限
            if (!isset($ret_data['code']) ||$ret_data['code']!= API_RET_SUCC) {
                $identity="notadminorsub";
            }else{
                if (!isset($ret_data['data']['is_superuser']) || ($ret_data['data']['is_superuser'] == 0 && $ret_data['data']['super_type'] != 2)) {
                    $identity="notadminorsub";
                } else {
                    $identity="adminorsub";
                }
            }
            $this->write_token("identity", $identity);
        }else{
            $identity=$_COOKIE['netdisk_user_identity'];
        }

        if ($identity=="notadminorsub") {
            die(json_encode(array('code' => 2002,'msg'=>'权限不足')));
        }
    }
    //检查用户身份 是否是超管
    protected  function checkuseridentity_is_admin(){
        if(!isset($_COOKIE['netdisk_user_identityone'])) {
            $this->load->model('account_model');
            $ret_data = $this->account_model->info($this->token);
            //超管权限
            if (!isset($ret_data['code']) ||$ret_data['code']!= API_RET_SUCC) {
                $identity="notadmin";
            }else{
                if(!isset($ret_data['data']['is_superuser']) || $ret_data['data']['is_superuser']!=1){
                    $identity="notadmin";
                } else {
                    $identity="admin";
                }
            }
            $this->write_token("identityone", $identity);

        }else{
            $identity=$_COOKIE['netdisk_user_identityone'];
        }

        if ($identity=="notadmin") {
            die(json_encode(array('code' => 2002,'msg'=>'权限不足')));
        }

    }
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */