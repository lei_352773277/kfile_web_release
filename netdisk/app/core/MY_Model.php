<?php
/**
 * Created by PhpStorm.
 * User: qyuan
 * Date: 15-3-19
 * Time: 下午2:33
 */
class MY_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->config->load('netdisk');
    }

    function format_error_msg($code, $msg) {
        return array('code'=>$code,'msg'=>$msg);
    }

    function my_json_encode($data) {
        /*foreach($data as $key=>$value) {
             if(is_string($value)) {
                $data[$key] = urlencode($value);
             } else{
                $data[$key] = $value;
             }
        }*/
        return  json_encode($data);
    }

    function request_netdisk_server($url,$data = "", $version = '',$other_url='')
    {
        $config = $this->config->item('netdisk.api');

        if(defined("KINGFILE_USER_AGENT") && !defined("KINGFILE_USER_AGENT_hasUse")) {

            $user_agent = KINGFILE_USER_AGENT;
        } else {

            $device_id = $this->input->cookie('netdisk_device_id', TRUE);
            //$user_agent  = $config['user_agent'] . ';' . $device_id;
            $user_agent  = $device_id;
        }


        $this->curl->create($config['server'.$other_url] . $url);

        $this->curl->options(array('timeout'=>180));
        //$this->curl->options(array('timeout'=>6));
        if ($data !== "") {
            $this->curl->post($data);
        }
        $version = $version === "" ? $config['default_version'] : $version;
        $this->curl->http_header("v", $version);
        $this->curl->http_header('User-Agent', $user_agent);
        $this->curl->http_header('X-Real-IP', $this->input->ip_address());
        $this->curl->http_header('Content-Type','application/json');

        $res = $this->curl->execute();
        if (!$res) {
            log_message('error', 'url=' . $config['server'.$other_url] . $url . "; post=" . $data . "; res=" . $res);

        }
       $addr= $this->input->ip_address();

        log_message('debug', 'url=' . $config['server'.$other_url] . $url . "; post=" . $data . ";header="."v=".$version.",device_id=".$user_agent.",add=".$addr."; res=" . $res);

        return $res;
    }

    function request_netdisk_server_v2($url, $data = "", $version = '')
    {
        $config = $this->config->item('netdisk.api');

        if(defined("KINGFILE_USER_AGENT") && !defined("KINGFILE_USER_AGENT_hasUse")) {

            $user_agent = KINGFILE_USER_AGENT;
        } else {

            $device_id = $this->input->cookie('netdisk_device_id', TRUE);
            //$user_agent  = $config['user_agent'] . ';' . $device_id;
            $user_agent  = $device_id;
        }

        $this->curl->create($config['server_v2'] . $url);
        $this->curl->options(array('timeout'=>180));
        if ($data !== "") {
            $this->curl->post($data);
        }
        $version = $version === "" ? $config['default_version'] : $version;
        $this->curl->http_header("v", $version);
        $this->curl->http_header('User-Agent', $user_agent);
        $this->curl->http_header('X-Real-IP', $this->input->ip_address());
        $this->curl->http_header('Content-Type','application/json');

        $res = $this->curl->execute();
        if (!$res) {
            log_message('error', 'url=' . $config['server_v2'] . $url . "; post=" . $data . "; res=" . $res);
        }
        $addr= $this->input->ip_address();
        log_message('debug', 'url=' . $config['server_v2'] . $url . "; post=" . $data . ";header="."v=".$version.",device_id=".$user_agent.",add=".$addr."; res=" . $res);
        return $res;
    }



    function request_netdisk_server_custom($url,$data = "",$user_agent='', $version = '',$other_url='')
    {
        $config = $this->config->item('netdisk.api');
        $this->curl->create($config['server'.$other_url] . $url);
        $this->curl->options(array('timeout'=>180));
        if ($data !== "") {
            $this->curl->post($data);
        }
        $version = $version === "" ? $config['default_version'] : $version;
        $this->curl->http_header("v", $version);
        $this->curl->http_header('User-Agent', $user_agent);
        $this->curl->http_header('X-Real-IP', $this->input->ip_address());
        $this->curl->http_header('Content-Type','application/json');

        $res = $this->curl->execute();
        if (!$res) {
            log_message('error', 'url=' . $config['server'.$other_url] . $url . "; post=" . $data . "; res=" . $res);

        }
        $addr= $this->input->ip_address();

        log_message('debug', 'url=' . $config['server'.$other_url] . $url . "; post=" . $data . ";header="."v=".$version.",device_id=".$user_agent.",add=".$addr."; res=" . $res);

        return $res;
    }


    function curl($url,$data="",$header=array(),$cookie=array(), $https=False) {
        $this->curl->create($url);
        if(!empty($https)) {
            $this->curl->ssl(False);
        }
        if(""!==$data) {
            $this->curl->post($data);
        }
        if(!empty($header)) {
            foreach ($header as $k => $v) {
                $this->curl->http_header($k,$v);
            }
        }
        if (!empty($cookie)) {
            $this->curl->set_cookies($cookie);
        }
        $res = $this->curl->execute();
        if(is_array($data)){
            log_message('debug', 'url=' . $url . "; post=" . json_encode($data) . ";  res=" . $res);
        }else{
            log_message('debug', 'url=' . $url . "; post=" . $data . ";  res=" . $res);
        }
        return $res;
    }



}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */