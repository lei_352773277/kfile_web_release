<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach($ret_data as $value) {
        	$paltform[$value['keyname']] = $value['value'];
        }

        $data['paltform'] = $paltform;
        if($paltform['offline']=='1') {
        	$this->parser->parse("page/offline.tpl",$data);	
        } else {

            if ($this->ksyun_token) {
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                    $data['kingfile'] = $info_res;
                }
            }
            $data['productclass'] = 'class="active"';
            /*$this->parser->parse("page/product.tpl",$data);*/

        }
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */