<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 控制面板
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */

class L extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('xfile_model');
        $this->load->model('platform_model');
    }

    /**
    * 外链查看地址
    */
    public function index()
    {
        $id = $this->input->get('id',TRUE);
        $linkInfo = array('xid'=>12312,'name'=>'钢铁是怎么炼成的.doc','size'=>123123124,'ctime'=>1437562594);
        $locked = true;
        $data = $this->config->config['netdisk.resources'];
        $data['linkInfo'] = $linkInfo;
        $data['locked'] = $locked;
        //$this->parser->parse("page/s.tpl",$data);
    }

    public function s(){
        $link = $this->uri->segment(3);
        $password = $this->input->cookie('netdisk_'.$link, TRUE);

        if(empty($password)) {
            $password = "";
        } else {
            $password = base64_decode($password);
        }

        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->xfile_model->linkverify($link, $password);
        $client_OS=$this->clientOS();
        if (API_RET_SUCC == $ret_data['code']) {
            $token = $ret_data['data']['token'];
            $data['token'] = $token;
            $xid = $ret_data['data']['xid'];
            $file_ret = $this->xfile_model->info($token,$xid);


            if (API_RET_SUCC == $file_ret['code']) {
                $data['linkInfo'] = $file_ret['data'];
                $data['status'] = 0;
                $this->load->model('org_model');

                if(0==$file_ret['data']['xtype']){
                    $sha1=$file_ret['data']['sha1'];
                    $audit_check = $this->org_model->audit_check($sha1,0);
                }else{
                    $file_link='/l/s/'.$link;
                    $audit_check = $this->org_model->audit_check($file_link,1);
                }
                if(isset($audit_check['result']) && $audit_check['result']){
                    $data['status'] = 7;
                }else {

                    if(isset($file_ret['data']['domain_id'])){
                        $istrial = $this->org_model->istrial($file_ret['data']['domain_id']);
                    }
                    if(isset($istrial) && $istrial['code']==API_RET_SUCC && $istrial['data']['istrial']==1){
                        $data['status'] = 8;//提示试用期用户，暂时不提供外链功能
                    }
                    else if ($file_ret['data']['status'] != 0) {
                        $data['status'] = 2;//已经删除
                    } else if (0 == $file_ret['data']['role'] && 1 == $file_ret['data']['is_share']) {
                        $data['status'] = 6;//权限不足
                    } else if ($client_OS == 'windows') {
                        $data['status'] = 3;
                    } else if($client_OS=='Macintosh') {
                        $data['status'] = 4;
                    }else {
                        $data['status'] = 100;
                    }
                }

            } else {
                $data['status'] = 2;
            }
        } else {
            if(LINK_PWD_NOT_MATCH == $ret_data['code']) {
                 $data['status'] = 1;//需要密码
             } else {
                 $data['status'] = 2;//已经删除
             }
        }
        $data['link'] =  $link;
        $data['server'] = $this->config->config['netdisk.api'];
        $web_server = $this->platform_model->get_web_server();
        $data['server']['server'] = $web_server;
        if(1==PRODUCTMODEL){
            $api="/api/xfile";
        }else if(2==PRODUCTMODEL){
            $api="";
        }else{
            $api="";
        }
        $data['server']['api']=$api;
        if(defined("KINGFILE_USER_AGENT") && !defined("KINGFILE_USER_AGENT_hasUse")) {
            $user_agent = KINGFILE_USER_AGENT;
        } else {
            $device_id = $this->input->cookie('netdisk_device_id', TRUE);
            $user_agent  = $device_id;
        }
        $data['agent']=urlencode($user_agent);

        $sitename = $this->platform_model->get_value('sitename');
        $data['title'] = $sitename[0]['value'];
        $data['platform']=$this->get_platform();
        //self::res($data['linkInfo']);
        //$data['fileInfo']=json_encode($data['linkInfo']);
        $this->parser->parse("page/l.tpl",$data);
    }

    public function verfiy()
    {
        $link = $this->input->post('link',TRUE);
        $password = $this->input->post('password',TRUE);
        $ret_data = $this->xfile_model->linkverify($link, $password);
        if (!empty($ret_data) && API_RET_SUCC == $ret_data['code']) {
            //写session
            $data = $this->config->config['netdisk.resources'];
            $cookie = array(
                'name'   => $link,
                'value'  => base64_encode($password),
                'expire' => '86400',
                'path'=>'/',
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($cookie);
            self::res($ret_data);
        } else {
            self::res($ret_data);
        }
    }

    public function i(){
        $link = $this->uri->segment(3);
        $data = $this->config->config['netdisk.resources'];
        //is  login
        $login_user_info=$this->login_user_info();
        if(!$login_user_info){
            if (PRODUCTMODEL==2){
                header("location:/login".'?insidelink='.$link);
                exit;
            }else{
                header("location:/".'?insidelink='.$link);
                exit;
            }
        }
        $ret_data = $this->xfile_model->inside_link_verify($this->token,$link);

        if(!$ret_data||$ret_data['code'] != API_RET_SUCC){
            //内链不存在
            $data['status']=1;
            $this->parser->parse("page/insidelink.tpl",$data);
            return;
        }
       /*$ret_data=array('code'=>0,'data'=>array(
            'inside_link_parent_xid'=>'8589935100',
            'inside_link_xid'=>8589935104
        ));*/

        if(isset($ret_data['data']['inside_link_parent_xid'])){
            $xid=$ret_data['data']['inside_link_parent_xid'];
        }else{
            if(isset($ret_data['data']['inside_link_xid'])){
                $xid=$ret_data['data']['inside_link_xid'];
            }
        }
        $ancestor_list_data= $this->xfile_model->ancestor_list($this->token,$xid);

        if(!$ancestor_list_data || $ancestor_list_data['code']!=API_RET_SUCC){
            //无权限
            $data['status']=2;
            $this->parser->parse("page/insidelink.tpl",$data);
            return;
        }
        $is_share=$ancestor_list_data['data'][0]['is_share'];
        $inside_link_arr=array();
        if($is_share==1){
            array_push($inside_link_arr,"domain");
        }else{
            array_push($inside_link_arr,"person");
        }
        if(isset($ret_data['data']['inside_link_parent_xid'])){
            array_push($inside_link_arr,$ret_data['data']['inside_link_parent_xid'],$ret_data['data']['inside_link_xid']);
        }else{
            if(isset($ret_data['data']['inside_link_xid'])){
                array_push($inside_link_arr,$ret_data['data']['inside_link_xid']);
            }
        }
        //self::res($inside_link_arr);
        //self::res($inside_link_arr);
        //普通用户
        if($login_user_info=='user'){
            header('location:/dashboard#'.implode("/",$inside_link_arr));
        }else{//公有云  超管
            header('location:/account/signup/insidelink/'.implode("-",$inside_link_arr));
        }

    }

    private  function login_user_info(){
        if ($this->token) {
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
               return 'user';
            }
        }else{
            if (PRODUCTMODEL==2 && $this->ksyun_token) {//管理员登录
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                   return 'admin';
                }
            }
        }
        return '';
    }


    public function get_platform(){
        $this->load->model('platform_model');
        $platform = array();
        $allowKey = array(
            'client_windows',
            'client_mac'
        );
        $paltform_ret_data = $this->platform_model->getValues($allowKey);
        foreach ($paltform_ret_data as $value) {
            if (in_array($value['keyname'], $allowKey)) {
                $platform[$value['keyname']] = $value['value'];
            }
        }
        return $platform;
    }

    public function link_file_info(){
        $token=$this->input->post('token',TRUE);
        $xid=$this->input->post('xid',TRUE);
        $file_ret = $this->xfile_model->info($token,$xid);
        self::res($file_ret);
    }

    public function clientOS(){

        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $user_os = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/windows|win32/i', $user_os)) {
                $user_os = 'windows';
            } else if (preg_match('/linux/i', $user_os)) {
                $user_os = 'linux';
            } else if (preg_match('/unix/i', $user_os)) {
                $user_os = 'unix';
            } else if (preg_match('/macintosh|mac os x/i', $user_os)) {
                $user_os = 'Macintosh';
            } else {
                $user_os = 'other';

            }
            //return $user_os = 'Macintosh';
            //$user_os='Macintosh';
            return $user_os;
        }
    }

    /*public function browser(){
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $version= "";
        if(empty($u_agent)){
            return array(
                'name'      => $bname,
                'version'   => $version
            );
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Trident/i',$u_agent))
        {
            // this condition is for IE11
            $bname = 'Internet Explorer';
            $ub = "rv";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        // finally get the correct version number
        // Added "|:"
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/|: ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
            return array(
                'name'      => $bname,
                'version'   => $version
            );
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
        return array(
            'name'      => $bname,
            'version'   => $version
        );


    }*/




}


/* End of file s.php */
/* Location: ./application/controllers/s.php */