<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Org extends MY_Controller {

    private $orgNodes =  array();
    private $orgSelectNodeXid = '';
    private $selectNode = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model('org_model');
        $this->load->model('account_model');
        $this->load->model('xfile_model');
        $this->load->model('platform_model');
    }

    /**
    * 获取企业信息
    */
    function domaininfo() 
    {

        $ret_data = $this->org_model->domain_info($this->token);
    	self::res($ret_data);
    }

    /**
    * 获取企业空间信息
    */
    function domainspace()
    {
        $ret_data = $this->org_model->domain_space_info($this->token);
        self::res($ret_data);
    }

    /**
    * 修改用户的空间
    */
    function changespace()
    {
        self::checkuseridentity();
        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $xid = $this->input->post('xid',TRUE);
        $quota = $this->input->post('quota',TRUE);
        $ret_data = $this->org_model->change_staff_space($this->token, $xid, $quota);
        if(API_RET_SUCC != $ret_data['code']) {
            $ret_data['data']['xid'] = $xid;
        }
        self::res($ret_data);
    }

    /**
    * 设置全局变量设置
    */
    function commsetting()
    {
        $share_root_space_max = $this->input->post('share_root_space_max',TRUE);
        if(empty($share_root_space_max)||intval($share_root_space_max)<0) {
            $share_root_space_max = PBQUOTA;//1PB
        } else {
            $share_root_space_max = $share_root_space_max*1024*1024*1024;
        }
        $setting = array('share_root_space_max'=>$share_root_space_max);
        $ret_data = $this->org_model->config_common_setting($this->token, $setting);
        self::res($ret_data);
    }
    


    /**
    * 获取详细信息
    */
    function restrictiondetail()
    {
        $ret_data = $this->org_model->restriction_detail($this->token);
        self::res($ret_data);
    }

    /**
    * 修改用户的权限
    */
    function changepermission()
    {

        self::checkuseridentity();

        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $xid = $this->input->post('xid',TRUE);
        $permission = $this->input->post('permission',TRUE);
        $permission = empty($permission)?"allow":"deny";
        $ret_data = $this->org_model->create_share_folder_permission($this->token, $xid, $permission);
        if(API_RET_SUCC != $ret_data['code']) {
            $ret_data['data']['xid'] = $xid;
        }
        $ret_data = array('code'=>0,'data'=>array('xid'=>$xid));
        self::res($ret_data);

    }


    /**
    * 获取员工空间信息
    */
    function staffspace()
    {
        $xid = $this->input->get('xid',TRUE);
        if(empty($xid)) {
            self::res(array('code'=>1,'msg'=>'错误的xid'));
        }
        $ret_data = $this->org_model->staff_space_info($this->token,$xid);
        self::res($ret_data); 
    }
    /*
     *
     * 判断是否过期
     * */
    function orglimitdate(){

        $domaininfo=$this->org_model->domain_info($this->token);
        if(empty($domaininfo) ||$domaininfo['code']!=0 ||$domaininfo['data']==null ){
            self::res(array('code'=>2,'msg'=>'获取企业信息失败'));
        }

        $ldapInfo=$this->org_model->get_config_setting($this->token,$domaininfo['data']['domain_id']);
        if(empty($ldapInfo) ||$ldapInfo['code']!=0 ||$ldapInfo['data']==null ){
            self::res(array('code'=>3,'msg'=>'获取ldap企业信息失败'));
        }
        $ldapInfo=$ldapInfo['data'];
        //过期时间
        $expireDate= intval($ldapInfo['limits']['start_time'])+intval($ldapInfo['limits']['expire']);
        //试用期
        //$probation_period=intval($ldapInfo['limits']['start_time'])+intval(3600*24*15);
        $time=time();
        //$istrial = $this->org_model->istrial($domaininfo['data']['domain_id']);
        /*if(isset($istrial) && $istrial['code']==API_RET_SUCC){
            if($istrial['data']['istrial']==1){
                //试用
                self::res(array('code'=>4,'msg'=>'probation period'));
            }else{
                //过期
                if($time>$expireDate){
                    self::res(array('code'=>1,'msg'=>''));
                }
                //未过期
                self::res(array('code'=>0,'data'=>array('expireDate'=>$expireDate,'now time'=>$time)));
            }
        }else{*/
            //过期
            if($time>$expireDate){
                //self::res(array('code'=>1,'msg'=>''));
            }
            //未过期
            self::res(array('code'=>0,'data'=>array('expireDate'=>$expireDate,'now time'=>$time)));
        //}
    }











    /**
    * 保存企业信息
    */
    function domain() 
    {

        //self::checkuseridentity_is_admin();
        $name = $this->input->post('name',TRUE);
        $user_name=$this->input->post('user_name',TRUE);
        $staff_name=$this->input->post('staff_name',TRUE);
        $desc = $this->input->post('desc',TRUE);
        $address = $this->input->post('address',TRUE);
        $phone = $this->input->post('phone',TRUE);
        $fax = $this->input->post('fax',TRUE);
        $corporation = $this->input->post('corporation',TRUE);
        $contact = $this->input->post('contact',TRUE);
        $logo = $this->input->post('logo',TRUE);
        $name = htmlentities ( $name, ENT_QUOTES, "UTF-8" );
        $desc = htmlentities ( $desc, ENT_QUOTES, "UTF-8" );
        $address = htmlentities ( $address, ENT_QUOTES, "UTF-8" );
        $phone = htmlentities ( $phone, ENT_QUOTES, "UTF-8" );
        $fax = htmlentities ( $fax, ENT_QUOTES, "UTF-8" );
        $corporation = htmlentities ( $corporation, ENT_QUOTES, "UTF-8" );
        $contact = htmlentities ( $contact, ENT_QUOTES, "UTF-8" );

        //$account = $this->account_info;

        $domain = array(
            'token'=>$this->token,
            'name' =>$name,
            'user_name'=>$user_name,
            'staff_name'=>$staff_name,
            'description' =>$desc,
            //'phone'=>$phone,
            'contact'=>$phone,
            'address'=>$address,
            'fax'=>$fax,
            'logo'=>$logo,
            'email'=>'',
            'legal'=>$contact
        );
    
        $ret_data = $this->org_model->domain($domain);
        self::res($ret_data);
    }

    function savelogo()
    {
        $name = $this->input->post('name',TRUE);
        $desc = $this->input->post('desc',TRUE);
        $address = $this->input->post('address',TRUE);
        $phone = $this->input->post('phone',TRUE);
        $fax = $this->input->post('fax',TRUE);
        $corporation = $this->input->post('corporation',TRUE);
        $contact = $this->input->post('contact',TRUE);
        $logo = $this->input->post('logo',TRUE);
        $name = htmlentities ( $name, ENT_QUOTES, "UTF-8" );
        $desc = htmlentities ( $desc, ENT_QUOTES, "UTF-8" );
        $address = htmlentities ( $address, ENT_QUOTES, "UTF-8" );
        $phone = htmlentities ( $phone, ENT_QUOTES, "UTF-8" );
        $fax = htmlentities ( $fax, ENT_QUOTES, "UTF-8" );
        $corporation = htmlentities ( $corporation, ENT_QUOTES, "UTF-8" );
        $contact = htmlentities ( $contact, ENT_QUOTES, "UTF-8" );
        $domain = array(
            'token'=>$this->token,
            'name' =>$name,
            'description' =>$desc,
            //'phone'=>$phone,
            'contact'=>$phone,
            'address'=>$address,
            'fax'=>$fax,
            'logo'=>$logo,
            'email'=>'',
            'legal'=>$contact
        );
        $ret_data = $this->org_model->domain($domain);
        self::res($ret_data); 
    }

    /**
    * 获取企业组织结构树,只显示部门
    */
    function orgfulltree()
    {
        $ret_data = $this->org_model->dept_all($this->token, 0);
        $node_list = $ret_data['data'];
        $all_parent = array();
        $dept_parent = array();
        foreach ($node_list as $node_item) {
            $node_item['parent_id'] = $node_item['parent_xid'];
            if ($node_item['parent_xid'] == 0) {
                $root = $node_item;
                $root_copy = $node_item;
            }
            if($node_item['xtype']==1) {
                $dept_parent[$node_item['parent_xid']][] = $node_item;
            }
            $all_parent[$node_item['parent_xid']][] = $node_item;
        }
        $this->getChildrens($root, $dept_parent);//只返回部门
        $this->getChildrens($root_copy, $all_parent);//处理所有节点
        $vice = $this->input->get('vice',TRUE);
        if (!empty($vice)) {
            $ret_info_data = $this->account_model->info($this->token);
            $vice_data = $this->org_model->manager_info($this->token, $ret_info_data['data']['xid']);
            self::res(array('code'=>0,'data'=>array('tree'=>array($root),'nodes'=>$this->orgNodes,'vice_data'=>$vice_data['data'])));
        } else {
            self::res(array('code'=>0,'data'=>array('tree'=>array($root),'nodes'=>$this->orgNodes)));
        }
    }

    private function getChildrens(&$node, $all_parent)
    {
        if(isset($all_parent[$node['xid']])) {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;
            $childrens = $all_parent[$node['xid']];
            foreach ($childrens as $key=>&$value) {
                $value['path'] = $node['path']."";
                $this->getChildrens($value,$all_parent);
            }
            $node['children'] = $childrens;

        } else {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;
            $node['children'] = array();
        }
    }

    /**
    * 组织结构选择树
    */
    function orgselecttree()
    {
        $xid = $this->input->get('xid',TRUE);
        $this->orgSelectNodeXid = $xid;
        $ret_data = $this->org_model->dept_all($this->token, 0);
        $node_list = $ret_data['data'];
        $all_parent = array();
        $dept_parent = array();
        foreach ($node_list as $node_item) {
            $node_item['parent_id'] = $node_item['parent_xid'];
            if ($node_item['parent_xid'] == 0) {
                $root = $node_item;
                $root_copy = $node_item;
            }
            //if($node_item['xtype']==1) {
            $dept_parent[$node_item['parent_xid']][] = $node_item;
            //}
            $all_parent[$node_item['parent_xid']][] = $node_item;
        }
        $this->getSelectChilds($root, $dept_parent);//处理所有节点
        if(!empty($xid)) {
           //$this->selectNode = $this->orgNodes[$xid];
           $ret_role_data = $this->xfile_model->dir_share_role_list($this->token, $xid);
           foreach($ret_role_data['data'] as $k=>$v) {
                if(isset($this->orgNodes[$v['user_xid']])) {
                    $ret_role_data['data'][$k] = $this->orgNodes[$v['user_xid']];
                } else {
                    //超管
                    unset($ret_role_data['data'][$k]);
                }
           }
        } else {
            $ret_role_data['data'] = array();
        }
        $this->getSelectChilds($root_copy, $all_parent,true);//处理所有节点
        
        //self::res(array('code'=>0,'data'=>array('tree'=>array($root),'nodes'=>$this->orgNodes)));
        self::res(array('code'=>0,'data'=>array('tree'=>array($root_copy),'member'=>$ret_role_data['data'])));
    }
    /**
     * 组织结构选择树  只获取子部门和员工
     */
    function orgselecttreeOnlyChildren()
    {
        $xid = $this->input->get('xid',TRUE);
        $this->orgSelectNodeXid = $xid;
        $ret_data = $this->org_model->dept_list($this->token, 0);
        $node_list = $ret_data['data'];
        $all_parent = array();
        $dept_parent = array();
        foreach ($node_list as $node_item) {
            $node_item['parent_id'] = $node_item['parent_xid'];
            if ($node_item['parent_xid'] == 0) {
                $root = $node_item;
                $root_copy = $node_item;
            }
            //if($node_item['xtype']==1) {
            $dept_parent[$node_item['parent_xid']][] = $node_item;
            //}
            $all_parent[$node_item['parent_xid']][] = $node_item;
        }
        $this->getSelectChilds($root, $dept_parent);//处理所有节点
        if(!empty($xid)) {
            //$this->selectNode = $this->orgNodes[$xid];
            $ret_role_data = $this->xfile_model->dir_share_role_list($this->token, $xid);
            foreach($ret_role_data['data'] as $k=>$v) {
                if(isset($this->orgNodes[$v['user_xid']])) {
                    $ret_role_data['data'][$k] = $this->orgNodes[$v['user_xid']];
                } else {
                    //超管
                    unset($ret_role_data['data'][$k]);
                }
            }
        } else {
            $ret_role_data['data'] = array();
        }
        $this->getSelectChilds($root_copy, $all_parent,true);//处理所有节点

        //self::res(array('code'=>0,'data'=>array('tree'=>array($root),'nodes'=>$this->orgNodes)));
        self::res(array('code'=>0,'data'=>array('tree'=>array($root_copy),'member'=>$ret_role_data['data'])));
    }

    private function getDisplayType($path) {
        if(empty($this->orgSelectNodeXid)) {
            return "1";
        } else {
            return "1";
            $node = $this->selectNode;
            if(strpos($path,$node['path'])!==0) {
                if(strpos($node['path'],$path)!==0) {
                    return "-1";
                } else {
                    return "0";
                }
            } else {
                return "1";
            }
        }
    }

    private function getSelectChilds(&$node, $all_parent,$showDisplay=false) 
    {
        if(isset($all_parent[$node['xid']])) {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;
            $childrens = $all_parent[$node['xid']];
            foreach ($childrens as $key=>&$value) {
                $value['path'] = $node['path']."";
                $this->getSelectChilds($value,$all_parent,$showDisplay);
            }
            $node['children'] = $childrens;
            if($showDisplay) {
                //$node['display'] = $this->getDisplayType($node['path']);
            }

        } else {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;
            $node['children'] = array();
            if($showDisplay) {
                //$node['display'] = $this->getDisplayType($node['path']);
            }
        }
    }


    /**
    * 添加员工
    */
    function adduser(){

        //验证是否超管和自超管
        self::checkuseridentity();

        $password =trim($this->input->post('password',TRUE));
        $user =trim($this->input->post('user',TRUE));
        $staffName = trim($this->input->post('staffName',TRUE));
        $phone = trim($this->input->post('phone',TRUE));
        $email = trim($this->input->post('email',TRUE));
        $quota = $this->input->post('quota',TRUE);
        $quota = $quota*1024*1024*1024;
        $deptXid = $this->input->post('deptXid',TRUE);
        $permission = $this->input->post('permission',TRUE);//是否可以创建共享文件夹

        self::checkInputVal($password);
        self::checkInputVal($user);
        self::checkInputVal($staffName);
        self::checkInputVal($phone);
        self::checkInputVal($email);
        self::checkInputVal($quota);
        self::checkInputVal($deptXid);
        self::checkInputVal($permission);


        $ret_data = $this->org_model->staff_create($this->token,
            $deptXid,$user,$password,$staffName,$quota);

        if (API_RET_SUCC == $ret_data['code']) {
            $xid = $ret_data['data']['user_id'];
            $ret_info_data = $this->account_model->info($this->token);
            $info_bind=array();
            if(""!=$email) {
                $ret_data_email=$this->account_model->account_email_bind($this->token,$ret_info_data['data']['domain_ident'],$xid,$email);
                if(API_RET_SUCC!=$ret_data_email['code']){
                    $info_bind['email']=$ret_data_email;
                }
            }
            if(""!=$phone) {
                $ret_data_phone=$this->account_model->account_phone_bind($this->token,$ret_info_data['data']['domain_ident'],$xid,$phone);
                if(API_RET_SUCC!=$ret_data_phone['code']){
                    $info_bind['phone']=$ret_data_phone;
                }
            }
            if(1 == intval($permission)) {
                $ret_data_permission=$this->org_model->create_share_folder_permission($this->token, $ret_data['data']['xid'],"deny");
                if(API_RET_SUCC!=$ret_data_permission['code']){
                    $info_bind['permission']=$ret_data_permission;
                }
            }
            if($info_bind){
                $ret_data['info_bind']=$info_bind;
            }
        }

        self::res($ret_data);
    }

    /**
    * 删除员工
    */
    /*function deleteuser()
    {
        $xids = $this->input->post('xids');
        $dstXid = $this->input->post('dstXid');
        $userId=$this->input->post('user_id');
        $isOk = true;
        foreach($xids as $xid) {
            //$ret_data = $this->org_model->staff_delete($this->token,$xid);
            $lock_data = $this->account_model->account_lock($this->token,$userId, 1);
            if(API_RET_SUCC !== $lock_data['code']) {
                $isOk = false;
                continue;
            }
            $ret_data  = $this->org_model->staff_recycle($this->token, $xid, $dstXid);
            if(API_RET_SUCC !== $ret_data['code']) {
                $isOk = false;
            }
        }
        self::res(array('code'=>$isOk?API_RET_SUCC:3013,"data"=>""));
    }*/
    function deleteuser()
    {
        $xid = $this->input->post('xid',TRUE);
        $dstXid = $this->input->post('dstXid',TRUE);
        $userId=$this->input->post('user_id',TRUE);
        $isOk = false;
        //$ret_data = $this->org_model->staff_delete($this->token,$xid);
        $lock_data = $this->account_model->account_lock($this->token,$userId, 1);
        if(API_RET_SUCC == $lock_data['code']) {
            $ret_data  = $this->org_model->staff_recycle($this->token, $xid, $dstXid);
            if(API_RET_SUCC !== $ret_data['code']) {
                self::res(array('code'=>3013,"data"=>""));
            }else{
                self::res(array('code'=>API_RET_SUCC,"data"=>""));
            }

        }else{
            self::res(array('code'=>3013,"data"=>""));
        }

    }

    /**
    * 获取员工信息
    */
    function userinfo()
    {
        $xid = $this->input->get('xid',TRUE);
        $ret_data = $this->org_model->staff_info($this->token, $xid);
        if(API_RET_SUCC == $ret_data['code']) {
            $permission = $this->org_model->getUserPermission($this->token, $ret_data['data']['xid']);
            $ret_data['data']['createfolder'] = $permission=="deny"?"1":"0";
        }
        self::res($ret_data);
    }

    /**
    * 编辑员工信息
    */
    function edituser()
    {
        $xid = $this->input->post('xid',TRUE);
        $staffName = trim($this->input->post('staffName',TRUE));
        $dstDeptXid = $this->input->post('dstDeptXid',TRUE);
        $phone = trim($this->input->post('phone',TRUE));
        $qutoa = $this->input->post('qutoa',TRUE);
        $qutoa = $qutoa*1024*1024*1024;
        $permission = $this->input->post('permission',TRUE);
        $ret_data = $this->org_model->staff_modify($this->token,$xid,$staffName,$dstDeptXid,$qutoa);

        if(API_RET_SUCC == $ret_data['code']) {
            $password = trim($this->input->post('password',TRUE));
            $user_id = $this->input->post('user_id',TRUE);
            if(!empty($password)) {
                $ret_password_data = $this->account_model->password_modify($this->token,$user_id,$password, $password);
                $ret_data['data']['ret_password'] = $ret_password_data;
            }

            $ret_info_data = $this->account_model->info($this->token);

            if ("-1" != $phone && "" != $phone) {
                $ret_phone_data = $this->account_model->account_phone_bind($this->token,$ret_info_data['data']['domain_ident'], $user_id, $phone);
                $ret_data['data']['ret_phone'] = $ret_phone_data;
            }
            if(""==$phone){
                $ret_phone_data = $this->account_model->account_info_unbind($this->token,$user_id, 2);
                $ret_data['data']['ret_phone'] = $ret_phone_data;
            }
            $email = $this->input->post('email',TRUE);

            if("-1" != $email && "" != $email) {
                $ret_email_data = $this->account_model->account_email_bind($this->token, $ret_info_data['data']['domain_ident'], $user_id, $email);
                $ret_data['data']['ret_email'] = $ret_email_data;
            }
            if(""==$email){
                $ret_email_data = $this->account_model->account_info_unbind($this->token,$user_id, 1);
                $ret_data['data']['ret_email'] = $ret_email_data;
            }

            $status = $this->input->post('status',TRUE);
            if ("-1" != $status) {
                $ret_lock_data = $this->account_model->account_lock($this->token,$user_id,$status);
                $ret_data['data']['ret_lock'] = $ret_lock_data;
            }

            if ("-1" != $permission) {
                $permission = empty($permission)?"allow":"deny";
                $ret_permission_data = $this->org_model->create_share_folder_permission($this->token, $xid, $permission);
                $ret_data['data']['ret_permission'] = $ret_permission_data;
            }

        }
        self::res($ret_data);
    }

    //锁定 解锁用户
    function user_lock(){
        $status = $this->input->post('status',TRUE);
        $user_id = $this->input->post('user_id',TRUE);
        $ret_lock_data = $this->account_model->account_lock($this->token,$user_id,$status);
        self::res($ret_lock_data);
    }

    /*
    * 移动用户部门
    */
    function moveuser()
    {

        //验证是否超管和子超管
        self::checkuseridentity();

        $xids = $this->input->post('xid',TRUE);
        $dstDeptXid = $this->input->post('dstDeptXid',TRUE);
        foreach($xids as $xid) {
            $ret_data = $this->org_model->staff_move($this->token,$xid,$dstDeptXid);
        }
        self::res($ret_data);
    }


    /**
    * 添加部门
    */
    function adddept()
    {

        //验证是否超管和子超管
        self::checkuseridentity();

        $parent_id = $this->input->post('parent_id',TRUE);
        $deptName = $this->input->post('name',TRUE);
        $deptDesc = $this->input->post('desc',TRUE);
        $ret_data = $this->org_model->dept_create($this->token,$parent_id,$deptName,$deptDesc);
        self::res($ret_data);   
    }


    /**
    * 编辑部门
    */
    function editdept()
    {

        //验证是否超管和子超管
        self::checkuseridentity();

        $xid = $this->input->post('xid',TRUE);
        $name = $this->input->post('name',TRUE);
        $desc = $this->input->post('desc',TRUE);
        $dstXid = $this->input->post('dstXid',TRUE);
        $ret_data = $this->org_model->dept_modify($this->token, $xid, $name, $desc);
        if($dstXid!='-1') {
            $ret_data_move = $this->org_model->dept_move($this->token, $xid, $dstXid);
            $ret_data['move_result']=$ret_data_move;
        }
        self::res($ret_data);
    }

    /**
    * 获取部门信息
    */
    function deptinfo()
    {
        $xid = $this->input->get('xid',TRUE);
        $ret_data = $this->org_model->dept_info($this->token, $xid);
        self::res($ret_data);
    }

    function deptlist()
    {
        $xid = $this->input->get('xid',TRUE);
        $xid = empty($xid)?0:intval($xid);
        $ret_data = $this->org_model->dept_list($this->token, $xid);
        self::res($ret_data);
    }

    /**
    * 删除部门信息
    */
    function deletedept()
    {
        //验证是否超管和子超管
        self::checkuseridentity();

        $dept_id = $this->input->post('dept_id',TRUE);
        $ret_data = $this->org_model->dept_delete($this->token, intval($dept_id));
        self::res($ret_data);
    }

    function importview()
    {

        //验证是否超管和自超管
        self::checkuseridentity();


       $config['upload_path'] = 'upload';
       $config['allowed_types'] = 'csv';
       $config['encrypt_name'] = TRUE;
       $config['remove_spaces'] = TRUE;
       $this->load->library('upload', $config);
       $this->upload->initialize($config);
       if ( ! $this->upload->do_upload('file')){
            self::res(array('code'=>'1','msg'=>'解析文件出错，请上传正确的模板文件（CSV格式，记录数不能为空）！')); 
       } else {
            $data = array('upload_data' => $this->upload->data());
            $this->load->library('csvreader');
            $csvData = $this->csvreader->parse_file($data['upload_data']['full_path']); 
            if(count($csvData)>900) {
                self::res(array('code'=>'2','msg'=>'单次导入最大支持900条记录，您可以分多次导入！'));
            }else if(0==count($csvData)) {
                unlink($config['upload_path'].'/'.$data['upload_data']['file_name']);
                self::res(array('code'=>'3','msg'=>'记录数不能为空'));
            }
           //print_r($csvData);exit;
            self::res(array('code'=>0,'data'=>array('filedata'=>$csvData,'filename'=>$data['upload_data']['file_name'])));
            //print_r($csvData);
       }
    }


    private function convToUtf8($str) {
        if( mb_detect_encoding($str,"UTF-8, ISO-8859-1, GBK")!="UTF-8" ) {//判断是否不是UTF-8编码，如果不是UTF-8编码，则转换为UTF-8编码
            return  iconv("gbk","utf-8",$str);
        } else {
            return $str;
        }
    }

    function downtemplate()
    {
        $xid = $this->input->get('xid',TRUE);
        $ret_data = $this->org_model->dept_info($this->token, $xid);
        if(API_RET_SUCC == $ret_data['code']) {
            $name = $ret_data['data']['name'];
            $data = file_get_contents('imgs/template.csv');
            $name = iconv("utf-8","gbk",$name);
            $fmt = str_replace('DEPT', $name, $data);
            $this->load->helper('download');
            $this->load->helper('string');
            force_download('批量导入模板.csv', $fmt);
        }
    }

    function importorg()
    {

        //验证是否超管和自超管
        self::checkuseridentity();

        $name = $this->input->post('name',TRUE);
        $userInfo=$this->input->post('userInfo',TRUE);
        $this->load->library('csvreader');
        $csvData = $this->csvreader->parse_file('upload/'.$name);
        if($csvData) {
            $ret_data = $this->org_model->dept_all($this->token, 0);
            $node_list = $ret_data['data'];
            $all_parent = array();
            $dept_parent = array();
            foreach ($node_list as $node_item) {
                $node_item['parent_id'] = $node_item['parent_xid'];
                if ($node_item['parent_xid'] == 0) {
                    $root = $node_item;
                    $root_copy = $node_item;
                }
                if($node_item['xtype']==1) {
                    $dept_parent[$node_item['parent_xid']][] = $node_item;
                }
                $all_parent[$node_item['parent_xid']][] = $node_item;
            }
            //$this->getChildrens($root, $dept_parent);//只返回部门
            $this->getImportChildrens($root_copy, $all_parent);//处理所有节点
            $allNodes = $this->orgNodes;
            $map = array();
            $result = array('total'=>0,'success'=>0,'error'=>0,'errorlist'=>array());
            $this->load->helper('email');
            $ret_info_data = $this->account_model->info($this->token);
            $value=$userInfo;

            $isNameExist=$this->isNameExist($allNodes,$value);
            if($isNameExist){//用户名已存在
                $map[$value['name']] = $isNameExist['xid'];
                self::res(array('code'=>'5001','data'=>$value));
                exit;
            }

            $isParentExist=$this->isParentExist($allNodes,$value);
            if($isParentExist){//上级存在
                $parent_id = $isParentExist['xid'];
                $name = $value['name'];
                if($value['type']=='部门') {
                    $ret_data = $this->org_model->dept_create($this->token,$parent_id,$name,'');
                    if(API_RET_SUCC == $ret_data['code']) {
                        $allNodes[$name] = array('xid'=>$ret_data['data']['xid'],'name'=>$name,'parent_id'=>$parent_id);
                    }
                } else {
                    $user = $value['userName'];
                    $password = $value['password'];
                    $email = $value['email'];
                    $phone = $value['phone'];
                    $quota = $value['space']*1024*1024*1024;


                    if(!preg_match('/^[\x{4e00}-\x{9fa5}A-Za-z0-9_\.\s]+$/u',$name)) {
                        self::res(array('code'=>'5002','data'=>$value));
                    }

                    if(empty($user) || !preg_match('/([A-Za-z0-9])+$/',$user)) {
                        self::res(array('code'=>'5003','data'=>$value));
                    }

                    if(!empty($email)&&!valid_email($email) || strlen($email)>30 ) {
                        self::res(array('code'=>'5004','data'=>$value));
                    }

                    if(empty($password) || strlen($password) >20 || strlen($password) < 6|| !preg_match('/^([A-Za-z0-9-`!@#$%^&*\(\)~_+=\[\]:\'.?|])+$/',$password)) {
                        self::res(array('code'=>'5005','data'=>$value));
                    }

                    if(!empty($phone)&&!preg_match('/^([0-9\-])+$/', $phone)) {
                        self::res(array('code'=>'5006','data'=>$value));
                    }

                    if(empty($quota)||!preg_match('/\d+/',$quota)||($value['space']*1)>30) {
                        self::res(array('code'=>'5007','data'=>$value));
                    }
                    $ret_data = $this->org_model->staff_create($this->token,$parent_id,$user,$password,$name,$quota);
                    if(API_RET_SUCC == $ret_data['code']) {
                        $result['success'] = $result['success'] + 1;
                        $xid = $ret_data['data']['user_id'];
                        try{
                            if(!empty($email)) {
                                $this->account_model->account_email_bind($this->token,$ret_info_data['data']['domain_ident'],$xid,$email);
                            }
                            if(!empty($phone)) {
                                $this->account_model->account_phone_bind($this->token,$ret_info_data['data']['domain_ident'],$xid,$phone);
                            }
                        }catch (Exception $e) {
                            //echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }
                        self::res(array('code'=>0,'data'=>$value));
                    } else {
                        self::res(array('code'=>$ret_data['code'],'data'=>$value));
                    }
                }
            }else{
                //上级部门 不存在
                self::res(array('code'=>'5008','data'=>$value));
            }
        }
    }
    private function isNameExist($all_node,$value)
    {
        foreach($all_node as $key=>$node){
            if($node['xtype']==0 && ($node['name']==$value['name'] ||$node['user_name']==$value['userName'])){
                return $node;
                exit;
            }
        }
        return false;
    }
    private function isParentExist($all_node,$value)
    {
        foreach($all_node as $key=>$node){
            if($node['xtype']==1 && $node['name']==$value['parent']){
                return $node;
                exit;
            }
        }
        return false;
    }
    private function getImportChildrens(&$node, $all_parent)
    {

        if(isset($all_parent[$node['xid']])) {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;

            $childrens = $all_parent[$node['xid']];
            foreach ($childrens as $key=>&$value) {
                $value['path'] = $node['path']."";
                $this->getImportChildrens($value,$all_parent);
            }
            $node['children'] = $childrens;

        } else {
            if(isset($node['path'])) {
                $node['path'] = $node['path']."_".$node['xid'];
            } else {
                $node['path'] = $node['xid']."";
            }
            $this->orgNodes[$node['xid']] = $node;
            $node['children'] = array();
        }

    }



    function uploadlogo()
    {
       $config['upload_path'] = IMG_PATH;
       $config['allowed_types'] = 'png|jpg|gif|jpeg';
       $config['encrypt_name'] = TRUE;
       $config['remove_spaces'] = TRUE;
       $config['max_width'] = 150;
       $config['max_height'] = 80;
       $this->load->library('upload', $config);
       $this->upload->initialize($config);
       if ( ! $this->upload->do_upload('file')){
            self::res(array('code'=>'1','msg'=> $this->upload->display_errors())); 
       } else {

            $data = $this->upload->data();
            $web_server = $this->platform_model->get_web_server();
            $filename = $data['file_name'];
            if(2 == PRODUCTMODEL) {
                $domain = $this->org_model->domain_info($this->token);
                $domain_id = $domain['data']['domain_id'];
                $this->uploadlogo2db(IMG_PATH."/".$filename, $data['file_type'], $domain_id);
                self::res(array('code'=>0,'data'=>"/org/viewlogo/".$domain_id."/".time().$data['image_type']));
            } else {
                //self::res(array('code'=>0,'data'=>"/".IMG_PATH."/".$filename));
                self::res(array('code'=>0,'data'=>"/org/viewlogo/".$data['raw_name']."/".time().$data['image_type']));
            }
       }
    }

    function viewlogo()
    {
        $key = $this->uri->segment(3);
        $ext = $this->uri->segment(4);
        if(empty($key)) {
            die('no');
        } else {
            if(2 == PRODUCTMODEL) {
                $loginInfo = $this->platform_model->getLogo($key);
                header('Content-type: '.$loginInfo['logotype']);
                echo base64_decode($loginInfo['logo']);
            } else {
                header("location:/".IMG_PATH."/".$key.".".$ext);
            }
        }
    }

    private function uploadlogo2db($img, $fileType, $domain_id) {
        $logoContents = file_get_contents($img);
        $this->load->model("platform_model");
        $this->platform_model->setLogo($domain_id, $logoContents, $fileType);
    }

    /**
     * 企业设置
     */
    function configSetting()
    {
        $domainId = $this->input->post('domainId',TRUE);
        $ldap_server = $this->input->post('ldap_server',TRUE);
        $ldap_admin_name = $this->input->post('ldap_admin_name',TRUE);
        $ldap_admin_pwd = $this->input->post('ldap_admin_pwd',TRUE);
        $ldap_base_dn = $this->input->post('ldap_base_dn',TRUE);
        $ldap_account_field=$this->input->post('ldap_account_field',TRUE);
        $ldap_name_field =  $this->input->post('ldap_name_field',TRUE);
        $ldap_account_field =  empty($ldap_account_field)?"sAMAccountName":$ldap_account_field;
        $ldap_name_field = empty($ldap_name_field)?"sAMAccountName":$ldap_name_field;

        $settings = array('ldap_server'=>$ldap_server, 
            'ldap_admin_name'=>$ldap_admin_name, 
            'ldap_admin_pwd'=>$ldap_admin_pwd, 
            'ldap_base_dn'=>$ldap_base_dn,
            'ldap_account_field'=>$ldap_account_field,
            'ldap_name_field'=>$ldap_name_field);
        $ret_data = $this->org_model->config_setting($this->token,$domainId,$settings);
        self::res($ret_data);
    }

    function getConfigSetting()
    {

        $domainId = $this->input->get('domainId',TRUE);
        $ret_data = $this->org_model->get_config_setting($this->token,$domainId);
        self::res($ret_data); 
    }

    /**
    * 检测配置的ldap是否可用
    */
    function checkldap()
    {
        $domainId = $this->input->get('domainId',TRUE);
        $ret_data = $this->org_model->get_config_setting($this->token,$domainId);
        //print_r($ret_data);
        if(function_exists('ldap_connect')) {
            $ldap['user'] = $ret_data['data']['ldap_admin_name'];
            $ldap['pass'] = $ret_data['data']['ldap_admin_pwd'];
            $ldap['base'] = $ret_data['data']['ldap_base_dn'];
            if(empty($ldap['base'])){
                self::res(array('code'=>3025,'msg'=>'配置BASE DN是否正确'));
            }

            //strtolower( $ldap['base']);
            $ldap['conn'] = ldap_connect( $ret_data['data']['ldap_server'])
            or self::res(array('code'=>3026,'data'=>'','msg'=>'链接ldap失败'.$ldap['host']));

            $ldap['bind'] = @ldap_bind( $ldap['conn'], $ldap['user'], $ldap['pass'] );
            if($ldap['bind']) {
                self::res(array('code'=>API_RET_SUCC,'data'=>''));
            } else {
                self::res(array('code'=>3028,'msg'=>'链接失败'));
            }
        } else {
            self::res(array('code'=>3027,'data'=>'','msg'=>'没有开启ldap模块'));
        }
    }
    /**
     * 获取组织结构
     */
    function viewldaporg()
    {
        //$ldap['user'] = 'kingsoft';
        //$ldap['pass'] = 'Ksc1234567';
        //$ldap['host'] = '123.59.14.251';
        //$ldap['port'] = 389;
        $rer_info_data = $this->account_model->info($this->token);
        $domainId = $rer_info_data['data']['domain_id'];
        $ret_data = $this->org_model->get_config_setting($this->token,$domainId);
        $ldap['base'] =$ret_data['data']['ldap_base_dn'];
        // connecting to ldap
        $ldap['user'] = $ret_data['data']['ldap_admin_name'];
        if(empty($ldap['base'])){
            self::res(array('code'=>3025,'msg'=>'配置BASE DN是否正确'));
        }
        $ldap['pass'] = $ret_data['data']['ldap_admin_pwd'];
        $ldap['conn'] = ldap_connect( $ret_data['data']['ldap_server']);
        //$ldap['conn'] = ldap_connect( $ldap['host'], $ldap['port']);
        ldap_set_option($ldap['conn'],LDAP_OPT_REFERRALS,0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap['bind'] = @ldap_bind( $ldap['conn'], $ldap['user'], $ldap['pass'] )  or 
        self::res(array('code'=>3028,'data'=>'','msg'=>'链接ldap失败'.$ldap['host'])); ;

        $justThese = array("dn");
        $result = @ldap_search($ldap['conn'], $ldap['base'], "(objectClass=organizationalUnit)",$justThese) or
        self::res(array('code'=>3029,'data'=>''));
        $data = ldap_get_entries($ldap['conn'], $result);

        $node_ref = array();
        $count = count(explode(",", $ldap['base']));
        foreach ($data as $key=>$value) {
            if(is_array($value)) {
                $list = explode(",", $value['dn']);
                $level = count($list) - $count;
                $name_str = array_shift($list);
                $name_str_list = explode("=", $name_str);
                $node_ref[$value['dn']] = array('dn'=>$value['dn'],'name'=>$name_str_list[1],'parent_dn'=>implode(",", $list));
            }
        }

        $root = array('parent_dn'=>"0","name"=>$ldap['base'],"dn"=>$ldap['base']);
        $all_parent = array();
        foreach($node_ref as $node) {
            $all_parent[$node['parent_dn']][] = $node;
        }
        $all_parent["0"][] = $root;
        $this->getLdapChildrens($root, $all_parent);
        if(!isset($root['children'])) {
            self::res(array('code'=>3029,'组织结构为空,检查配置BASE DN是否正确','data'=>''));
        } else {
            self::res(array('code'=>API_RET_SUCC,'data'=>$root));
        }
    }

    private function getLdapChildrens(&$node, $all_parent)
    {
        if(isset($all_parent[$node['dn']])) {
            $childrens = $all_parent[$node['dn']];
            foreach ($childrens as $key=>&$value) {
                $this->getLdapChildrens($value,$all_parent);
            }
            $node['children'] = $childrens;
        } else {
            $node['children'] = array();
        }
    }

    function syncldap()
    {
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->get_config_setting($this->token,$domainId);
        $ldap['base'] = $ret_data['data']['ldap_base_dn'];
        // connecting to ldap
        $ldap['user'] = $ret_data['data']['ldap_admin_name'];
        $ldap['pass'] = $ret_data['data']['ldap_admin_pwd'];
        $ldap['conn'] = ldap_connect( $ret_data['data']['ldap_server']);
        // connecting to ldap
        //$ldap['conn'] = ldap_connect( $ldap['host'], $ldap['port']);
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS,0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap['bind'] = ldap_bind( $ldap['conn'], $ldap['user'], $ldap['pass'] );

        $justThese = array("dn");
        $result = @ldap_search($ldap['conn'], $ldap['base'], "(objectClass=organizationalUnit)",$justThese) or
        self::res(array('code'=>3025,'data'=>''));
        $data = ldap_get_entries($ldap['conn'], $result);


        $selecteds = $this->input->post('selecteds',TRUE);
        //$selectedLevel = count(explode(",", $selecteds['dn']));
        if(empty($selecteds)) {
             self::res(array('code'=>REQUEST_PARAMS_ERROR,'data'=>array('syncnum'=>0)));
        }

        $node_ref = array();
        $count = count(explode(",", $ldap['base']));
        foreach ($data as $key=>$value) {
            if(is_array($value)) {
                $list = explode(",", $value['dn']);
                $level = count($list) - $count;
                $name_str = array_shift($list);
                $name_str_list = explode("=", $name_str);
                $node_ref[$level][] = array('dn'=>$value['dn'],'name'=>$name_str_list[1],'parent_dn'=>implode(",", $list));
            }
        }

        $first_dept = $ret_info_data['data']['first_dept'];
        //print_r("first_dept=".$first_dept);
        if(!empty($selecteds)) {
            $name2id[$selecteds['dn']] = $first_dept;
        } else {
            $name2id[$ldap['base']] = $first_dept;
        }
        //print_r("name2id=".$name2id[$ldap['base']]);
        $i = 0;
        foreach($node_ref as $key=>$value) {
            foreach($value as $v) {
                //print_r("v=".$v['parent_dn']);
                if(strpos($v['dn'], $selecteds['dn'])!==false) {
                    if(isset($name2id[$v['parent_dn']])) {
                        $parent_id = $name2id[$v['parent_dn']];
                    }
                    if($v['dn'] == $selecteds['dn']) {
                       $parent_id = $name2id[$v['dn']];
                    }

                    if(!empty($parent_id)) {
                        $ret_data = $this->org_model->dept_create($this->token,$parent_id,$v['name'],'',$v['dn']);
                        if(API_RET_SUCC == $ret_data['code']) {
                          //$allNodes[$name] = array('xid'=>$ret_data['data']['xid'],'name'=>$name,'parent_id'=>$parent_id);
                          $name2id[$v['dn']] = $ret_data['data']['xid'];
                          $i++;
                        }
                    }
                    
                }
            }
        }

        self::res(array('code'=>API_RET_SUCC,'data'=>array('syncnum'=>$i)));
    }
    /**
     * 获取子超管列表
     */
    function managers(){
        $ret_data = $this->org_model->manager_list($this->token);
        self::res($ret_data);
    }
    /**
     * 获取子超管详情
     */
    function managerinfo(){
        $xid = $this->input->post('xid',TRUE);
        $f_xid=intval($xid);
        $ret_data = $this->org_model->manager_info($this->token, $f_xid);
        self::res($ret_data);
    }
    /**
     * 添加子超管
     */
    function addmanager()
    {
        $password = $this->input->post('password',TRUE);
        $userName = $this->input->post('userName',TRUE);
        $email = $this->input->post('email',TRUE);
        $deptXids = $this->input->post('deptXids',TRUE);
        $f_deptXids = array();
        foreach($deptXids as $value) {
            array_push($f_deptXids,intval($value));
        }

        $ret_data = $this->org_model->manager_create($this->token,
            $f_deptXids,$userName,$password);
        //bind email
        if (API_RET_SUCC == $ret_data['code']) {
            $xid = $ret_data['data']['user_id'];
            $ret_info_data = $this->account_model->info($this->token);
            if(""!=$email) {
                $ret_email_data=$this->account_model->account_email_bind($this->token,$ret_info_data['data']['domain_ident'],$xid,$email);
                $ret_data['data']['ret_email'] = $ret_email_data;
            }
            self::res($ret_data);

        }else{
            self::res(array('code'=>$ret_data['code']));
        }


    }
    /**
     * 修改子超管
     */
    function modifymanager()
    {
        $xid = $this->input->post('xid',TRUE);
        $deptXids = $this->input->post('deptXids',TRUE);
        $f_xid = intval($xid);
        $f_deptXids = array();
        foreach($deptXids as $value) {
            array_push($f_deptXids,intval($value));
        }

        $ret_data = $this->org_model->manager_modify($this->token, $f_deptXids,$f_xid);
        //echo $ret_data;

        //修改密码
        if(API_RET_SUCC == $ret_data['code']) {
            $password = $this->input->post('password',TRUE);
            $user_id = $this->input->post('user_id',TRUE);
            if(!empty($password)) {
                $ret_password_data = $this->account_model->password_modify($this->token,$user_id,$password, $password);
                $ret_data['data']['ret_password'] = $ret_password_data;
            }
        }



        self::res($ret_data);
    }
    /**
     * 删除子超管
     */
    function delmanager()
    {
        $xid = $this->input->post('xid',TRUE);
        $f_xid=intval($xid);
        $ret_data = $this->org_model->manager_del($this->token, $f_xid);
        self::res($ret_data);
    }

    /**
     * 获取待归档用户列表
     */
    function recycleusers()
    {
        $ret_data = $this->org_model->recycle_users_list($this->token);
        self::res($ret_data);
    }
    /**
     * 删除待归档用户
     */
    function deleterecycleuser()
    {
        $xid = $this->input->post('xid',TRUE);
        $f_xid=intval($xid);
        $ret_data = $this->org_model->recycle_del($this->token, $f_xid);
        self::res($ret_data);
    }
    /**
     * 转交待归档用户
     */
    function moverecycleuser()
    {
        $xid = $this->input->post('xid',TRUE);
        $dstXid = $this->input->post('dstXid',TRUE);
        $f_xid=intval($xid);
        $f_dstXid=intval($dstXid);
        $ret_data = $this->org_model->recycle_move($this->token, $f_xid,$f_dstXid);
        self::res($ret_data);
    }

    /**
    * 保存license
    */
    function savelicense() {
       $license = $this->input->post('license',TRUE);
       $ret_info_data = $this->account_model->info($this->token);
       $domainId = $ret_info_data['data']['domain_id'];
       $ret_data = $this->org_model->config_license_setting($this->token,$domainId,$license);    
       self::res($ret_data);
    }

    /**
    *获取员工数
    */
    function memberscount() {
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->members_count($this->token, $domainId);
        self::res($ret_data);
    }

    /**
    * ldap白名单
    */
    function ldapwhitelist() {

        self::checkuseridentity_is_admin();

        $ret_data = $this->org_model->ldap_restriction_detail($this->token);
        self::res($ret_data);
    }

    /**
    * 导入白名单用户
    */
    function ldapimportuser() {
        $selecteds = $this->input->post('selecteds',TRUE);
        $synctype = $this->input->post('synctype',TRUE);
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->get_config_setting($this->token,$domainId);
        $whitelist = $this->org_model->ldapimportuser($ret_data, $selecteds,$synctype);
        $result = $this->org_model->batch_add_ldap_restriction($this->token, $whitelist);
        self::res($result);
    }

    /**
    * 删除用户
    */
    function ldapwhitelistdeluser() {

        self::checkuseridentity_is_admin();

        $dn = $this->input->post('dn',TRUE);
        $ret_data = $this->org_model->del_ldap_restriction($this->token, $dn);
        self::res($ret_data);
    }

    /**
    * 添加用户
    */
    function ldapwhitelistadduser() {

        self::checkuseridentity_is_admin();

        $dn = $this->input->post('dn',TRUE);
        $ret_data = $this->org_model->add_ldap_restriction($this->token, $dn);
        self::res($ret_data);
    }

    public function mime_distribution(){

        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->mime_distribution($this->token, $domainId);
        self::res($ret_data);
    }

    public function statistics_share_space(){
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->statistics_share_space($this->token, $domainId);
        self::res($ret_data);
    }
    public function statistics_cage_space(){
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        $ret_data = $this->org_model->statistics_cage_space($this->token, $domainId);
        self::res($ret_data);
    }

    public function export_statistics_csv(){
        $type = $this->input->get('type',TRUE);

        if('files'==$type){
            $file_name_h='文档数量统计';
            $str = "图片,文档,视频,音频,其他\n";
        }
        if('share'==$type){
            $file_name_h='公司文件容量使用统计';
            $str = "公司文件根目录,使用大小,限制大小,统计时间\n";
        }
        if('cage'==$type){
            $file_name_h='个人文件容量使用统计';
            $str = "个人文件使用者,使用大小,限制大小,统计时间\n";
        }
        $ret_info_data = $this->account_model->info($this->token);
        $domainId = $ret_info_data['data']['domain_id'];
        if('files'==$type){
            $ret_data = $this->org_model->mime_distribution($this->token, $domainId);
        }
        if('share'==$type){
            $ret_data = $this->org_model->statistics_share_space($this->token, $domainId);
        }
        if('cage'==$type){
            $ret_data = $this->org_model->statistics_cage_space($this->token, $domainId);
        }

        if(!empty($ret_data) && !empty($ret_data['data'])){
            $export_data = $ret_data['data'];
        }
        //导出的标题对应
        $str = iconv('utf-8','gb2312',$str);

        $str .=$this->filter_export_data($export_data,$type);

        //print_r($str);exit;

        //设置文件名
        $filename =iconv('utf-8','gb2312',$file_name_h.date('Y-m-d H:i:s',time()).'.csv');
        $this->export_csv($filename,$str); //导出

    }
    public  function filter_export_data($export_data,$type){
        $str='';
        if('files'==$type){
            $other=0;
            $pic=0;
            $doc=0;
            $video=0;
            $voice=0;
            foreach($export_data as $key=>$value){
                if($key==-1 ||$key==0){
                    $other+=$value;
                }
                if($key==1){
                    $pic+=$value;
                }
                if($key==2){
                    $doc+=$value;
                }
                if($key==3){
                    $voice+=$value;
                }
                if($key==4){
                    $video+=$value;
                }
            }
            $str .= $pic.",".$doc.",".$video.",".$voice.",".$other."\n"; //用引文逗号分开
        }
        if('share'==$type){
            foreach($export_data as $key=>$value){
                $name = iconv('utf-8','gb2312',strval($value['name'])); //中文转码
                $used = $this->readablizeBytes_quota($value['used']);
                $limit = $this->readablizeBytes_quota($value['quota']);
                $time=date('Y-m-d H:i:s',$value['mtime']);
                $str .= $name.",".$used.",".$limit.",".$time."\n"; //用引文逗号分开
            }
        }
        if('cage'==$type){
            foreach($export_data as $key=>$value){
                if($value['used']==0){
                    continue;
                }
                $name = iconv('utf-8','gb2312',strval($value['staff_name'])); //中文转码
                $used = $this->readablizeBytes_quota($value['used']);
                $limit = $this->readablizeBytes_quota($value['quota']);
                $time=date('Y-m-d H:i:s',$value['mtime']);
                $str .= $name.",".$used.",".$limit.",".$time."\n"; //用引文逗号分开
            }
        }
        return $str;

    }
    public function export_csv($filename,$data) {
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=".$filename);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        echo $data;
    }

    private  function readablizeBytes_quota($bytes){

        if($bytes>=1125899906842600) {
            return "--";
        }else if(0==$bytes){
            return  "0B";
        } else {
            $s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
            $e = floor(log($bytes) / log(1024));
            //echo $s[$e].'---';
            return sprintf("%.2f",$bytes / pow(1024, $e))." ".$s[$e];
        }
    }
    public function iprule_list(){
        $data=array(
            "token"=>$this->token
        );
        $ret_data = $this->org_model->iprule_list($data);
        if(!isset($ret_data['data'])){
            self::res($ret_data);
        }
        //获取规则和成员对应列表
        $ret_data_list=$this->org_model->iprestriction_list(array("token"=>$this->token));
        if(!isset($ret_data_list['data']) || empty($ret_data_list['data'])){
            self::res($ret_data);
        }
        foreach ($ret_data['data'] as &$node){
            $node['min_ip_str']=$this->long2Ip($node['min_ip']);
            $node['max_ip_str']=$this->long2Ip($node['max_ip']);
            $node['obj']=array();
            $node['obj_staff_name']=array();
            foreach ($ret_data_list['data'] as $node_item){
                if($node_item['rule_id']==$node['rule_id']){
                    array_push($node['obj'],$node_item);
                    array_push($node['obj_staff_name'],$node_item['xstaff_name']);
                }
            }
        }
        self::res($ret_data);

    }
    public  function iprule_create(){

        //是否超管
        self::checkuseridentity_is_admin();

        $name = $this->input->post('name',TRUE);
        $minIp =$this->input->post('minIp',TRUE);
        $minIp=$this->ipToInt($minIp);
        $maxIp= $this->input->post('maxIp',TRUE);
        $maxIp=$this->ipToInt($maxIp);
        $rule_desc=$this->input->post('rule_desc',TRUE);
        $data=array(
            "name"=>$name,
            "minIp"=>$minIp,
            "maxIp"=>$maxIp,
            "desc"=>$rule_desc,
            "token"=>$this->token
        );
        $ret_data = $this->org_model->iprule_create($data);
        self::res($ret_data);
    }
    public  function iprule_modify(){
        $ruleId= $this->input->post('ruleId',TRUE);
        $name = $this->input->post('name',TRUE);
        $minIp =$this->input->post('minIp',TRUE);
        $minIp=$this->ipToInt($minIp);
        $maxIp= $this->input->post('maxIp',TRUE);
        $maxIp=$this->ipToInt($maxIp);
        $rule_desc=$this->input->post('rule_desc',TRUE);
        $data=array(
            "ruleId"=>$ruleId,
            "name"=>$name,
            "minIp"=>$minIp,
            "maxIp"=>$maxIp,
            "desc"=>$rule_desc,
            "token"=>$this->token
        );
        $ret_data = $this->org_model->iprule_modify($data);
        self::res($ret_data);
    }

    public function iprule_delete(){
        $rule_id= $this->input->post('rule_id',TRUE);
        $iprestriction_list= $this->input->post('obj',TRUE);
        $rule_id_arr=array($rule_id);
        $data=array(
            "ruleIds"=>$rule_id_arr,
            "token"=>$this->token
        );
        if(empty($iprestriction_list)){
            $ret_data = $this->org_model->iprule_delete($data);
            self::res($ret_data);
        }
        $ruleIds=array();
        foreach ($iprestriction_list as $node){
            array_push($ruleIds,$node['rule_id']);
        }
        $ruleIds=array_unique($ruleIds);
        $ret_data_node = $this->org_model->iprestriction_delete(array("ruleIds"=>$ruleIds,"token"=>$this->token));
        if(API_RET_SUCC!=$ret_data_node['code']){
            $ret_data_node['remark']="删除对象时失败";
            self::res($ret_data_node);
        }
        $ret_data = $this->org_model->iprule_delete($data);
        self::res($ret_data);

    }
    public function iprestriction_create(){
        $ruleId= $this->input->post('ruleId',TRUE);
        $xids=$this->input->post('xids',TRUE);
        $data=array(
            "ruleId"=>$ruleId,
            "xids"=>$xids,
            "token"=>$this->token
        );
        $ret_data = $this->org_model->iprestriction_create($data);
        self::res($ret_data);
    }
    public  function iprestriction_modify(){
        $ruleId= $this->input->post('ruleId',TRUE);
        $xids=$this->input->post('xids',TRUE);
        $data=array(
            "ruleId"=>$ruleId,
            "xids"=>$xids,
            "token"=>$this->token
        );
        $ret_data = $this->org_model->iprestriction_modify($data);
        self::res($ret_data);
    }

    function  ipToInt($ip){
        return bindec(decbin(ip2long($ip)));
    }

    function long2Ip($long){
        return long2ip($long);
    }


    function getStartIp($ip = null, $mask = null){
        if(is_null($ip) || is_null($mask)) return false;
        $m   =   intval($mask);
        if   (   $m   <   0   ||   $m   >   32   ) return  'error   submask ';
        if(substr_count($ip,'.')){
            $ip = ip2long($ip);
        }else{
            if(!is_numeric($ip))return false;
        }
        $m_dec=str_repeat("1", $m).str_repeat("0",32-$m);
        //return bindec(decbin($nw | (~$mask)));
    }

    function getEndIP($ip = null, $mask = null,$returnNum = false){
        if(is_null($ip) || is_null($mask)) return false;
        $m   =   intval($mask);
        if   (   $m   <   0   ||   $m   >   32   ) return  'error   submask ';
        if(substr_count($ip,'.')){
            $ip = ip2long($ip);
        }else{
            if(!is_numeric($ip))return false;
        }
        if(substr_count($mask,'.')){
            $mask = ip2long($mask);
        }else{
            $mask = 0xffffffff << (32 - $mask);
        }
        $nw = ($ip & $mask);
        if($returnNum) return bindec(decbin($nw | (~$mask)));
        return   long2ip($nw | (~$mask));
    }

    function report(){

        $xid=$this->input->post('xid',TRUE);
        $reason_type= $this->input->post('reason_type',TRUE);
        $file_link= $this->input->post('file_link',TRUE);
        $token= $this->input->post('token',TRUE);
        $link=explode('/',$file_link);
        if(empty($link)){
            $password = "";
        }else{
            $password = $this->input->cookie('netdisk_'.$link[3], TRUE);
        }
        if(empty($password)) {
            $password = "";
        } else {
            $password = base64_decode($password);
        }
        $ret_data = $this->org_model->report($xid,$password,$reason_type,$file_link,$token);
        self::res($ret_data);
    }





}

/* End of file org.php */
/* Location: ./application/controllers/org.php */