<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Softcenter extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data = $this->config->config['netdisk.resources'];
		$allowKey = array(
			'web_crt',
			'web_crt_help',
			'offline',
			'client_windows',
			'client_android',
			'client_iphone',
			'client_windows_sync',
			'client_win_sync',
			'client_mac',
			'client_office',
			'sitename',
			'sendemailurl',
			'https');
		$ret_data = $this->platform_model->getValues($allowKey);
		$paltform = array();
		foreach($ret_data as $value) {
			$paltform[$value['keyname']] = $value['value'];
		}
        //$ret_data = $this->platform_model->info();


        $data['paltform'] = $paltform;
        if($paltform['offline']=='1') {
        	$this->parser->parse("page/softcenter.tpl",$data);
        } else {

            $this->parser->parse("page/softcenter.tpl",$data);

        }
	}

	public function  cv()
    {
        $data = array();
        $this->parser->parse("page/cv.tpl",$data);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */