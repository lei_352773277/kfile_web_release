<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 控制面板
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */
class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }

    private function getIndexData()
    {
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach ($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        $data['paltform'] = $paltform;
        return $data;
    }

    public function index()
    {
        if (!empty($this->token)) {
            $data = $this->getIndexData();
            $this->load->model('org_model');
            $domain_info = $this->org_model->domain_info($this->token);
            if (!empty($domain_info) && 0 === $domain_info['code']) {
                $configInfo = $this->org_model->get_config_setting($this->token, $domain_info['data']['domain_id']);
                $configInfo = $configInfo['data'];
                $expireDate = intval($configInfo['limits']['start_time']) + intval($configInfo['limits']['expire']);
                $encrypted_string = $this->my_encryption($expireDate . "@@@" . $this->token . '@@@' . time());
                $this->write_token($encrypted_string);
            }

            $this->parser->parse("page/dashboard.tpl", $data);

        } else {
            header("location:" . base_url());
        }
    }


    function my_encryption($value)
    {
        $CI =& get_instance();
        $key = $CI->config->item('encryption_key');
        $encrypted_string = rc4($key, $value);
        return bin2hex($encrypted_string);

    }

    private function write_token($value)
    {

        $this->load->helper('cookie');
        $cookie_ld = $this->input->cookie('netdisk_ld', TRUE);
        if (!empty($cookie_ld)) {
            delete_cookie('netdisk_ld');
        }
        $cookie = array(
            'name' => 'ld',
            'value' => $value,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($cookie);
    }


    function pc()
    {
        $data = $this->getIndexData();
        $this->parser->parse("page/dashboard.tpl", $data);
    }

    function m()
    {
        $data = $this->getIndexData();
        $this->parser->parse("page/dashboard_mobile.tpl", $data);
    }

    function get_device_type()
    {
        //全部变成小写字母
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        $type = 'other';
        //分别进行判断
        if (strpos($agent, 'iphone')) {
            $type = 'ios';
        }
        if (strpos($agent, 'ipad')) {
            $type = 'ipad';
        }

        if (strpos($agent, 'android')) {
            $type = 'android';
        }
        if (strpos($agent, 'micromessenger') && strpos($agent, 'android')) {
            $type = 'wechatAndroid';
        }
        return $type;
    }

}


/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */