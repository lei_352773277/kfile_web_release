<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 消息列表
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */

class Message extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');
    }

    public function getlist()
    {
        $this->load->helper('cookie');
        $data = $this->config->config['netdisk.resources'];
        //delete_cookie('netdisk_msg',$data['cookie_domain']);
        delete_cookie('netdisk_msg');
        $hint = $this->input->post('hint',TRUE);
        $hint = empty($hint)?0:$hint;
        $ret_data = $this->message_model->message_list($this->token,50,$hint);
        //print_r($ret_data);

        self::res($ret_data);
    }

    public function poplist()
    {
        session_write_close();
        $ret_data=array();

        if(empty($this->token)){
            $ret_data['code']=2001;
            self::res($ret_data);
        }

        //文件动态的消息
        $this->load->helper('cookie');
        $msgnum = $this->input->cookie('netdisk_msg', TRUE);
        $msgnum = empty($msgnum)?0:$msgnum;
        $ret_data_file = $this->message_model->message_pop_list($this->token);
        if (API_RET_SUCC == $ret_data_file['code']) {
            $ret_data_file['msgnum'] = $msgnum+count($ret_data_file['data']);
            if ($ret_data_file['msgnum']>0) {
                $this->write_cookie("msg",$ret_data_file['msgnum']);
            }
        }
        $ret_data['file']=$ret_data_file;

        //系统公告新消息
        $msg_announce_num = $this->input->cookie('netdisk_announce', TRUE);
        $msg_announce_num = empty($msg_announce_num)?0:$msg_announce_num;
        $announce_cursor=$this->input->cookie('netdisk_announce_cursor', TRUE);
        if(empty($announce_cursor)){
            //获取最新一条系统公告的信息
            $last_announce_info = $this->message_model->announce_list($this->token,1,0);
            if(isset($last_announce_info) && $last_announce_info['code']==API_RET_SUCC && isset($last_announce_info['data'][0])){
                $announce_cursor=$last_announce_info['data'][0]['announce_id'];
            }
        }
        $pop_list_announce = $this->message_model->messagesystem_pop_list($this->token,intval($announce_cursor));
        if(isset($pop_list_announce) && API_RET_SUCC==$pop_list_announce['code'] ){
            if(isset($pop_list_announce['data'][0])){
                $announce_cursor=$pop_list_announce['data'][0]['announce_id'];
            }
            $pop_list_announce['msgnum'] = $msg_announce_num+count($pop_list_announce['data']);
            if($pop_list_announce['msgnum']>=$msg_announce_num && $pop_list_announce['msgnum']>0){
                $this->write_cookie("announce",$pop_list_announce['msgnum']);
            }
        }
        $this->write_cookie("announce_cursor",$announce_cursor);
        $ret_data['announce']=$pop_list_announce;

        //企业公告新消息
        $msg_broadcast_num = $this->input->cookie('netdisk_broadcast', TRUE);
        $msg_broadcast_num = empty($msg_broadcast_num)?0:$msg_broadcast_num;
        $broadcast_cursor=$this->input->cookie('netdisk_broadcast_cursor', TRUE);
        if(empty($broadcast_cursor)){
            //获取最新一条公告的信息
            $last_broadcast_info = $this->message_model->broadcast_list($this->token,1,0);
            if(isset($last_broadcast_info) && $last_broadcast_info['code']==API_RET_SUCC && isset($last_broadcast_info['data'][0])){
                $broadcast_cursor=$last_broadcast_info['data'][0]['broadcast_id'];
            }
        }
        $pop_list_broadcast = $this->message_model->messagebroadcast_pop_list($this->token,intval($broadcast_cursor));
        if(isset($pop_list_broadcast) && API_RET_SUCC==$pop_list_broadcast['code'] ){
            if(isset($pop_list_broadcast['data'][0])){
                $broadcast_cursor=$pop_list_broadcast['data'][0]['broadcast_id'];
            }
            $pop_list_broadcast['msgnum'] = $msg_broadcast_num+count($pop_list_broadcast['data']);
            if($pop_list_broadcast['msgnum']>=$msg_broadcast_num && $pop_list_broadcast['msgnum']>0){
                $this->write_cookie("announce",$pop_list_broadcast['msgnum']);
            }
        }
        $this->write_cookie("broadcast_cursor",$broadcast_cursor);
        $ret_data['broadcast']=$pop_list_broadcast;



        self::res($ret_data);
    }


    private function write_cookie($name,$value){
        $this->load->helper('cookie');
        if(isset($_COOKIE['netdisk_'.$name])){
            delete_cookie('netdisk_'.$name);
        }
        $cookie = array(
            'name'   => $name,
            'value'  => $value,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($cookie);
    }

    //获取系统公告列表
    public function get_announce_list()
    {
        $this->load->helper('cookie');
        delete_cookie('netdisk_announce');
        $hint = $this->input->post('hint',TRUE);
        $hint = empty($hint)?0:$hint;
        $pageMax = $this->input->post('pageMax',TRUE);
        $ret_data = $this->message_model->announce_list($this->token,$pageMax,$hint);
        self::res($ret_data);
    }

    //获取系统公告的内容
    public function get_announce_content()
    {
        $announceId = $this->input->post('announceId',TRUE);
        $ret_data = $this->message_model->get_announce_content($this->token,$announceId);
        self::res($ret_data);
    }

    //删除系统公告
    public  function del_announce(){
        $announceId = $this->input->post('announceId',TRUE);
        $ret_data = $this->message_model->del_announce($this->token,$announceId);
        self::res($ret_data);
    }

    //获取企业公告列表
    public function get_broadcast_list()
    {
        $this->load->helper('cookie');
        delete_cookie('netdisk_broadcast');
        $hint = $this->input->post('hint',TRUE);
        $hint = empty($hint)?0:$hint;
        $pageMax = $this->input->post('pageMax',TRUE);

        $this->load->model('account_model');
        $user_info = $this->account_model->info($this->token);
        if(!empty($user_info) && $user_info['code']==API_RET_SUCC){
            if($user_info['data']['is_superuser']==1){
                $is_admin=true;
            }else{
                $is_admin=false;
            }
        }
        if($is_admin){
            $ret_data = $this->message_model->broadcast_domain_list($this->token,$pageMax,$hint);
        }else{
            $ret_data = $this->message_model->broadcast_list($this->token,$pageMax,$hint);
        }

        self::res($ret_data);
    }

    public  function get_broadcast_content(){
        $id = $this->input->post('broadcastId',TRUE);
        $ret_data = $this->message_model->get_broadcast_content($this->token,$id);
        self::res($ret_data);
    }
    //删除公司公告
    public  function del_broadcast(){
        $id = $this->input->post('broadcastId',TRUE);
        $ret_data = $this->message_model->del_broadcast($this->token,$id);
        self::res($ret_data);
    }
    //删除公司公告
    public  function del_broadcast_by_admin(){
        $id = $this->input->post('broadcastId',TRUE);
        $ret_data = $this->message_model->del_broadcast_by_admin($this->token,$id);
        self::res($ret_data);
    }
    //发布公司公告
    public  function create_broadcast(){
        $title = $this->input->post('title',TRUE);
        $summary = $this->input->post('summary',TRUE);
        $content = $this->input->post('content',TRUE);
        $issuer = $this->input->post('issuer',TRUE);
        $ret_data = $this->message_model->create_broadcast($this->token,$title,$summary,$content,$issuer);
        self::res($ret_data);
    }

    
}


/* End of file message.php */
/* Location: ./application/controllers/message.php */