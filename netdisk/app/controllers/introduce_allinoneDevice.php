<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Introduce_allinoneDevice extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode',TRUE);
        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
            exit;
        }
		$ret_data = $this->platform_model->info();
		$paltform = array();
		foreach($ret_data as $value) {
			$paltform[$value['keyname']] = $value['value'];
		}

		$data['paltform'] = $paltform;
        if($paltform['offline']=='1') {
        	$this->parser->parse("page/introduce_allinoneDevice.tpl",$data);
        } else {
			$data['productclass'] = 'class="active"';
            $login_info=$this->get_login_info();
            if(!empty($login_info)){
                $data['userInfo']=$login_info['userInfo'];
            }
            $this->parser->parse("page/introduce_allinoneDevice.tpl",$data);

        }
	}

    private  function get_login_info(){
        $data='';
        if ($this->token) {//肯定是已经登录了
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
                $data['userInfo'] = $info_res['data'];
            }
        }else{
            //没有登录 从金山云官网跳转
            if ($this->ksyun_token) {//管理员登录
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                    $data['userInfo'] = array();
                    $data['userInfo']['user_name'] = $info_res['ksyun_user']['email'];
                    $data['userInfo']['super_type'] = 1;
                }
            }
        }
        return $data;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */