<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('shop_model');
        $this->load->model('platform_model');
        $this->load->model('account_model');
        $this->load->model('org_model');
    }

    //购买页面
	public function index()
	{

        $data = $this->config->config['netdisk.resources'];

        $logincode = $this->input->get('logincode',TRUE);
        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
            exit;
        }


        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        $data['paltform'] = $paltform;
        if($paltform['offline']=='1') {
            $this->parser->parse("page/offline.tpl",$data);
        }else{
            $data['shopclass'] = 'class="active"';
            $login_info=$this->get_login_info();
            if(!empty($login_info)){
                $data['userInfo']=$login_info['userInfo'];
            }
            $this->parser->parse("page/shop.tpl",$data);
        }


	}

    //获取账号的状态
    public function getstatus()
    {
        $ret = $this->account_model->info($this->token);
        $domain_id = $ret['data']['domain_id'];
        if(!empty($domain_id)) {
            //$ksyun_token = $this->ksyun_token;
            $serial = $this->shop_model->get_domainstatus($domain_id,$this->token);
            return self::res($serial);
        } else {
            return self::res(array('code'=>LOGIC_ERROR,"msg"=>"获取状态失败"));
        }
    }

    //获取
    //这的参数是购买的参数
    public  function getprice()
    {
        $space = $this->input->post('space',TRUE);
        $member = $this->input->post('member',TRUE);
        $days = $this->input->post('days',TRUE);
        $ksyun_token = $this->ksyun_token;
        //$ksyun_token="111";
        $ret = $this->shop_model->get_price($space, $member, $days, $ksyun_token);
        return self::res($ret);
    }
    /*public  function getprice()
    {
        $space = $this->input->post('space');
        $member = $this->input->post('member');
        $days = $this->input->post('days');
        $ret = $this->account_model->info($this->token);
        $domain_id = $ret['data']['domain_id'];
        $ksyun_token = $this->ksyun_token;
        $serial = $this->shop_model->get_domainstatus($domain_id, $ksyun_token);
        list($result,$total_money) = $this->shop_model->get_price($domain_id, $space, $member, $days, $serial);
        if(0===$result) {
            return self::res(array('code'=>API_RET_SUCC,'data'=>$total_money));
        } else {
            return self::res(array('code'=>$result,'data'=>"","msg"=>$total_money));
        }
    }*/

    //购买
    public function buy(){
        $space = $this->input->post('space',TRUE);//存储空间，单位是G
        $member = $this->input->post('member',TRUE);//人数，单位是个
        $days = $this->input->post('days',TRUE);//购买时间，单位是月
        $total_money = $this->input->post('total_money',TRUE);//总价钱
        $ksyun_token = $this->ksyun_token;
        header("Content-type: text/html; charset=utf-8");

        $ret = $this->shop_model->buy($space, $member, $days, $total_money,$ksyun_token);
        echo $ret;
    }
    //支付宝异步通知
    public function alipayreturn()
    {

        $data = $this->config->config['netdisk.resources'];
        $_GET['isJump'] = '1';
        $ret = $this->shop_model->notify($_GET);

        $ret_arr = json_decode($ret,true);
        $data['orderResult']=$ret_arr['code'];
        if(0 == $data['orderResult']) {
            $data['orderInfo']=$ret_arr['data'];
        } else {
            $data['orderInfo']= array('id'=>$this->input->get('out_trade_no',TRUE));
            $data['errmsg'] = $ret_arr['msg'];
        }
        $this->parser->parse("page/chargeResult.tpl",$data);
    }

    public function alipaynotify()
    {
        $data = $_POST;
        $ret = $this->shop_model->notify($data);
        echo $ret;
    }

   /* public function buy()
    {
        $space = $this->input->post('space');//存储空间，单位是G
        $member = $this->input->post('member');//人数，单位是个
        $days = $this->input->post('days');//购买时间，单位是月
        $total_money = $this->input->post('total_money');//总价钱
        $ksyun_token = $this->ksyun_token;

        $ret = $this->account_model->info($this->token);
        $domain_id = $ret['data']['domain_id'];
        $domain_ident = $ret['data']['domain_ident'];
        if (intval($days)<0) {
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'错误的购买时长'));
        } else {
            if(!in_array($days, array("0","365","730","1095"))) {
                self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'错误的购买天数'));
            }
        }
        if (intval($member)<0) {
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'错误的购买人数'));
        }
        if(intval($space)<0) {
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'错误的购买空间值'));
        }
        //取当前企业的产品状态
        $serial = $this->shop_model->get_domainstatus($domain_id, $ksyun_token);
        if(API_RET_SUCC!==$serial['code']) {
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'获取企业当前状态失败,code='.$serial['code']));
        }

        $expire_time = date('Y-m-d 23:59:59',strtotime($serial['data']['ctime'].' + '.$serial['data']['order']['days'].'day'));

        if(($serial['data']['order']['space']+$space) < 50) {
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>'空间50G起售'));
        }
        list($result,$product_price) = $this->shop_model->get_price($domain_id, $space, $member, $days, $serial);
        if(0!==$result) {
            self::res(array('code'=>$result,'data'=>"","msg"=>$product_price));
        }

        if(!$product_price) {
           self::res(array('code'=>LOGIC_ERROR, "msg"=>"获取产品价格失败")); 
        }

        $price = $product_price['total'];//产品的实际价格
        if(floatval($price) !== floatval($total_money)) {
            log_message('error', 'price='.$price.'&total_money='.$total_money);
            self::res(array('code'=>REQUEST_PARAMS_ERROR,'data'=>array('total_money'=>$total_money,'price'=>$price),'msg'=>'金额计算不正确'));
        }

        //余额不足 需要判断
        if(floatval($serial['data']['balance']) < floatval($price)) {
            self::res(array('code'=>LOGIC_ERROR, 'data'=>array('balance'=>$serial['data']['balance'],'price'=>$price),"msg"=>"您余额不足，请充值后再购买")); 
        }

        $params = $product_price['params'];
        $base_order_id = $serial['data']['order_id'];
        $product_res = $this->shop_model->create_kingfile_order($ksyun_token, $domain_id, $domain_ident, 
                $params['space'], $params['member'], $params['free_member'], $params['days'], $price, $params['use'], $base_order_id);//创建产品
        self::res($product_res);
    }*/

    private  function get_login_info(){
        $data='';
        if ($this->token) {//肯定是已经登录了
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
                $data['userInfo'] = $info_res['data'];
            }
        }else{
            //没有登录 从金山云官网跳转
            if ($this->ksyun_token) {//管理员登录
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                    $data['userInfo'] = array();
                    $data['userInfo']['user_name'] = $info_res['ksyun_user']['email'];
                    $data['userInfo']['super_type'] = 1;
                }
            }
        }
        return $data;
    }


    public  function wechat_login_pay()
    {
        $xid = $this->input->get('xid',TRUE);
        $ksyun_token = $this->token;
        $ret = $this->shop_model->get_wechat_login_pay($xid, $ksyun_token);
        return self::res($ret);
    }

}

/* End of file shop.php */
/* Location: ./application/controllers/shop.php */