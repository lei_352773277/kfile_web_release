<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('org_model');
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode', TRUE);

        if (!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl", $data);
        } else {
            $allowKey = array(
                'web_crt',
                'web_crt_help',
                'offline',
                'sitename',
                'sendemailurl',
                'https');
            $ret_data = $this->platform_model->getValues($allowKey);
            $paltform = array();
            foreach ($ret_data as $value) {
                $paltform[$value['keyname']] = $value['value'];
            }
            if (!isset($paltform['web_crt'])) {
                $paltform['web_crt'] = '/crt/kingfile.crt';
            }
            if (!isset($paltform['web_crt_help'])) {
                $paltform['web_crt_help'] = '/crt/kingfile.doc';
            }
            $data['paltform'] = $paltform;
            $data['platform']['producttype'] = PRODUCTTYPE;
            $data['platform']['default_domain_name'] = DEFAULT_DOMAIN_NAME;
            if ($paltform['offline'] == '1') {
                $this->parser->parse("page/offline.tpl", $data);
            } else {
                if ($this->token) {
                    /*$this->load->model('account_model');
                    $info_res = $this->account_model->info($this->token);
                    if (API_RET_SUCC == $info_res['code']) {
                        $data['userInfo'] = $info_res['data'];
                    }*/
                }
                $data['indexclass'] = 'class="active"';
                if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
                    $host = $_SERVER['HTTP_X_ORIGIN_HOST'];
                } else {
                    $host = $_SERVER['REMOTE_ADDR'];
                }
                if ($paltform['https'] == 1) {
                    $data['host'] = "https://" . $host;
                } else {
                    $data['host'] = "http://" . $host;
                }
                $device_type = $this->get_device_type();
                if ($device_type == "ios") {
                    $data['device_download_addr'] = $paltform['client_iphone'];
                } else if ($device_type == "android") {
                    $data['device_download_addr'] = $paltform['client_android'];
                } else {
                    $data['device_download_addr'] = "other";
                }
                //$this->parser->parse("page/initkingfile.tpl",$data);
                $this->parser->parse("page/index.tpl",$data);

            }

        }
    }


    public function download()
    {
        $this->load->library('user_agent');
        $mobile = $this->agent->mobile;
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach ($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        if (!empty($mobile)) {
            $mobile = strval($mobile);
            $iPhone = stripos($mobile, 'iPhone');
            $iPad = stripos($mobile, 'iPad');
            if ($iPhone !== false) {
                header("location:" . $paltform['client_iphone']);
            } else if ($iPad !== false) {
                header("location:" . $paltform['client_iphone']);
            } else {
                header("location:" . $paltform['client_android']);
            }
        } else {
            header("location:" . $paltform['client_android']);
        }
    }

    function cons()
    {
        $ret_data = $this->org_model->domain_info($this->token);

        $ret_data = array('code' => API_RET_SUCC, 'data' => array(
            'domain_id' => 1,
            'ident' => 'test'
        ));

        //是否新用户 没有购买过
        if ($ret_data['code'] == API_RET_SUCC) {
            $domain_id = $ret_data['data']['domain_id'];
            if (!empty($domain_id) && !empty($this->ksyun_token)) {
                $ksyun_token = $this->ksyun_token;
                $this->load->model('shop_model');
                $serial = $this->shop_model->get_domainstatus($domain_id, $ksyun_token);
                if ($serial['code'] == API_RET_SUCC) {
                    $ispay = $serial['data']['ispay'];
                }
            }
        }
        //$ispay = 1;
        if (isset($ispay) && $ispay == 1) {
            $discountPackages = "[{\"space\":0,\"member\":0,\"days\":360},{\"space\":0,\"member\":0,\"days\":180}]";
        } else {
            $discountPackages = "[{\"space\":0,\"member\":0,\"days\":360},{\"space\":0,\"member\":0,\"days\":180},{\"space\":0,\"member\":0,\"days\":30}]";
        }

        if ($ret_data['code'] == API_RET_SUCC && ($ret_data['data']['ident'] == 'bjiff' || $ret_data['data']['ident'] == 'ceshi001')) {
            echo "define({
                'MAXVIEWDOC':30* 1024 * 1024,
                'MAXVIEWIMG':30* 1024 * 1024,
                'imgType':['jpg','jpeg','gif','bmp','png'],
                'custom_pagefilelist':true,
                'download_bypc_min':20* 1024 * 1024,
                'download_bypc_immediately':300* 1024 * 1024,
                'docType':['html','txt','doc','docx','ppt','pptx','xls','xlsx','pdf','mp4','mp3'],
                'discountPackages':$discountPackages
                })";
        } else {
            echo "define({
                    'MAXVIEWDOC':30* 1024 * 1024,
                    'MAXVIEWIMG':30* 1024 * 1024,
                    'imgType':['jpg','jpeg','gif','bmp','png'],
                    'download_bypc_min':20* 1024 * 1024,
                    'download_bypc_immediately':300* 1024 * 1024,
                    'docType':['html','txt','doc','docx','ppt','pptx','xls','xlsx','pdf','mp4','mp3'],
                    'discountPackages':$discountPackages
                })";
        }


    }

    function conss()
    {
        $ret_data = $this->org_model->domain_info($this->token);
        //是否新用户 没有购买过
        if ($ret_data['code'] == API_RET_SUCC) {
            $domain_id = $ret_data['data']['domain_id'];
            if (!empty($domain_id) && !empty($this->ksyun_token)) {
                $ksyun_token = $this->ksyun_token;
                $this->load->model('shop_model');
                $serial = $this->shop_model->get_domainstatus($domain_id, $ksyun_token);
                if ($serial['code'] == API_RET_SUCC) {
                    $ispay = $serial['data']['ispay'];
                }
            }
        }
        //$ispay = 1;
        if (isset($ispay) && $ispay == 1) {
            $discountPackages = array();
            $discountPackages[0] = array(
                "space" => 0,
                "member" => 0,
                "days" => 360
            );
            $discountPackages[1] = array(
                "space" => 0,
                "member" => 0,
                "days" => 180
            );
        } else {
            $discountPackages = array();
            $discountPackages[0] = array(
                "space" => 0,
                "member" => 0,
                "days" => 360
            );
            $discountPackages[1] = array(
                "space" => 0,
                "member" => 0,
                "days" => 180
            );
            $discountPackages[2] = array(
                "space" => 0,
                "member" => 0,
                "days" => 30
            );
        }

        if ($ret_data['code'] == API_RET_SUCC && ($ret_data['data']['ident'] == 'bjiff' || $ret_data['data']['ident'] == 'ceshi001')) {
            $data = array(
                'MAXVIEWDOC' => 30 * 1024 * 1024,
                'MAXVIEWIMG' => 30 * 1024 * 1024,
                'imgType' => array('jpg', 'jpeg', 'gif', 'bmp', 'png'),
                'custom_pagefilelist' => true,
                'download_bypc_min' => 20 * 1024 * 1024,
                'download_bypc_immediately' => 300 * 1024 * 1024,
                'docType' => array('html', 'txt', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf', 'mp4', 'mp3'),
                'discountPackages' => $discountPackages
            );
        } else {
            $data = array(
                'MAXVIEWDOC' => 30 * 1024 * 1024,
                'MAXVIEWIMG' => 30 * 1024 * 1024,
                'imgType' => array('jpg', 'jpeg', 'gif', 'bmp', 'png'),
                'download_bypc_min' => 20 * 1024 * 1024,
                'download_bypc_immediately' => 300 * 1024 * 1024,
                'docType' => array('html', 'txt', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf', 'mp4', 'mp3'),
                'discountPackages' => $discountPackages
            );
        }
        self::res($data);


    }

    function con()
    {

        echo "define({
                'MAXVIEWDOC':30* 1024 * 1024,
                'MAXVIEWIMG':30* 1024 * 1024,
                'imgType':['jpg','jpeg','gif','bmp','png'],
                'custom_pagefilelist':true,
                'download_bypc_min':20* 1024 * 1024,
                'download_bypc_immediately':300* 1024 * 1024,
                'docType':['html','txt','doc','docx','ppt','pptx','xls','xlsx','pdf','mp4','mp3']
                
                })";

        exit;
        $ret_data = $this->org_model->domain_info($this->token);
        $ret_data = array('code' => API_RET_SUCC, 'data' => array(
            'domain_id' => 1,
            'ident' => 'test'
        ));

        //是否新用户 没有购买过
        if ($ret_data['code'] == API_RET_SUCC) {
            $domain_id = $ret_data['data']['domain_id'];
            if (!empty($domain_id) && !empty($this->ksyun_token)) {
                $ksyun_token = $this->ksyun_token;
                $this->load->model('shop_model');
                $serial = $this->shop_model->get_domainstatus($domain_id, $ksyun_token);
                if ($serial['code'] == API_RET_SUCC) {
                    $ispay = $serial['data']['ispay'];
                }
            }
        }
        //$ispay = 1;
        if (isset($ispay) && $ispay == 1) {
            $discountPackages = "[{\"space\":0,\"member\":0,\"days\":360},{\"space\":0,\"member\":0,\"days\":180}]";
        } else {
            $discountPackages = "[{\"space\":0,\"member\":0,\"days\":360},{\"space\":0,\"member\":0,\"days\":180},{\"space\":0,\"member\":0,\"days\":30}]";
        }

        if ($ret_data['code'] == API_RET_SUCC && ($ret_data['data']['ident'] == 'bjiff' || $ret_data['data']['ident'] == 'ceshi001')) {
            echo "define({
                'MAXVIEWDOC':30* 1024 * 1024,
                'MAXVIEWIMG':30* 1024 * 1024,
                'imgType':['jpg','jpeg','gif','bmp','png'],
                'custom_pagefilelist':true,
                'download_bypc_min':20* 1024 * 1024,
                'download_bypc_immediately':300* 1024 * 1024,
                'docType':['html','txt','doc','docx','ppt','pptx','xls','xlsx','pdf','mp4','mp3'],
                'discountPackages':$discountPackages
                })";
        } else {
            echo "define({
                    'MAXVIEWDOC':30* 1024 * 1024,
                    'MAXVIEWIMG':30* 1024 * 1024,
                    'imgType':['jpg','jpeg','gif','bmp','png'],
                    'download_bypc_min':20* 1024 * 1024,
                    'download_bypc_immediately':300* 1024 * 1024,
                    'docType':['html','txt','doc','docx','ppt','pptx','xls','xlsx','pdf','mp4','mp3'],
                    'discountPackages':$discountPackages
                })";
        }


    }

    function get_device_type()
    {
        //全部变成小写字母
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $type = 'other';
        //分别进行判断

        if (strpos($agent, 'iphone') || strpos($agent, 'ipad')) {
            $type = 'ios';
        }
        if (strpos($agent, 'android')) {
            $type = 'android';
        }
        if (strpos($agent, 'micromessenger') && strpos($agent, 'android')) {
            $type = 'wechatAndroid';
        }
        return $type;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */