<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('org_model');
        $this->load->model('account_model');
    }

    /**
     * 获取登陆的用户信息
     */
    function info()
    {
        $ret_data = $this->account_model->info($this->token);

        $this->load->model('platform_model');
        $platform = array();
        $allowKey = array(
            'media_server',
            "web_server",
            'api_server',
            'client_windows',
            'client_windows_sync',
            'client_win_sync',
            'helpurl',
            'https',
            'ip',
            'api_common_http_host',
            'logo');
        $paltform_ret_data = $this->platform_model->getValues($allowKey);
        foreach ($paltform_ret_data as $value) {
            if (in_array($value['keyname'], $allowKey)) {
                $platform[$value['keyname']] = $value['value'];
            }
        }

        $ip = $platform['ip'];
        $wanip = $platform['api_common_http_host'];
        $access_ip = trim($wanip) == "" ? $ip : $wanip;
        $platform['productmodel'] = PRODUCTMODEL;
        if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
            $access_ip = $_SERVER['HTTP_X_ORIGIN_HOST'];
        }

        //user agent
        if (defined("KINGFILE_USER_AGENT") && !defined("KINGFILE_USER_AGENT_hasUse")) {
            $user_agent = KINGFILE_USER_AGENT;
        } else {
            $device_id = $this->input->cookie('netdisk_device_id', TRUE);
            $user_agent = $device_id;
        }
        $platform['agent'] = $user_agent;

        /*if (1 == PRODUCTMODEL) {
            $platform['api_server'] = '//' . $access_ip . "api";
            $platform['web_server'] = '//' . $access_ip . "";
            $platform['media_server'] = '//' . $access_ip . "/olc";
        }*/
        $platform['api_server'] = '//' . $access_ip . "";
        $platform['web_server'] = '//' . $access_ip . "";
        $platform['media_server'] = '//' . $access_ip . "/olc";

        $platform['wechat_user_info'] = "/weichat/account/user/info";
        $platform['default_sharefolder_quota'] = 1024;
        $ret_data['data']['platform'] = $platform;
        if ($ret_data['code'] == API_RET_SUCC && $ret_data['data'] && $ret_data['data']['xid']) {
            $staff_info = $this->org_model->staff_info($this->token, $ret_data['data']['xid']);
            //$permission = $this->org_model->getUserPermission($this->token, $ret_data['data']['xid']);
            if($staff_info && isset($staff_info['data'])){
                $ret_data['data']['staff'] = $staff_info['data'];
                //$ret_data['data']['staff']['createfolder'] = $permission == "deny" ? "1" : "0";
            }
        }
        $ret_data['data']['token'] = $this->token;

        $ret_data['data']['resources'] = $this->config->config['netdisk.resources'];

        //$ret_data['data']['device_id'] = $this->input->cookie('netdisk_device_id', TRUE);
        self::res($ret_data);
    }

    /**
     * 从客户端跳转过来
     */
    function sso()
    {
        $id = $this->uri->segment(3);
        $token = html_entity_decode($id);
        $pc_user_agent = $this->uri->segment(4);
        $client_device_id = html_entity_decode($pc_user_agent);
        $client_device_id = urldecode($client_device_id);
        define("KINGFILE_USER_AGENT", $client_device_id);

        $config = $this->config->item('netdisk.api');
        $this->load->helper('string');
        $web_device_id = $config['user_agent'] . ';' . random_string('alnum', 16);

        $res = $this->account_model->refresh_token($token, $web_device_id);
        if (API_RET_SUCC != $res['code']) {
            header('location:/?error_token');
            exit;
            /*$this->write_token_state("archive");
            header('location:/dashboard');
            exit;*/

        }

        $device_id = array(
            'name' => 'device_id',
            'value' => $web_device_id,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($device_id);

        $_COOKIE['netdisk_device_id'] = $web_device_id;


        $token = $res['data']['token'];

        define("KINGFILE_USER_AGENT_hasUse", 1);
        $ret_data = $this->account_model->info($token);
        if (API_RET_SUCC == $ret_data['code']) {
            //$data = $this->config->config['netdisk.resources'];
            $cookie = array(
                'name' => 'sid',
                'value' => $token,
                'expire' => '86500',
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($cookie);

            $segment5 = $this->uri->segment(5);
            if ($segment5 && $segment5 == "feedback") {
                header('location:/feedback?from=' . KINGFILE_USER_AGENT);
                exit;
            } else if ($segment5 && $segment5 != "feedback") {
                $this->write_token_state($segment5);
            }
            header('location:/dashboard');
        } else {
            header('location:/?error');
        }
    }

    /*
     * 获取客户端要跳转网盘的字段
     * */
    function getFromClientGo()
    {

        if (!empty($_COOKIE["netdisk_state"])) {
            $state = $_COOKIE["netdisk_state"];
            self::res(array('code' => 0, 'state' => $state));
        } else {
            self::res(array('code' => 1, 'state' => "/"));
        }

    }

    /**
     * 登陆
     */
    function login()
    {

        if ($this->form_validation->run() == FALSE) {
            $msg = validation_errors();
            return self::res(array('code' => REQUEST_PARAMS_ERROR, 'msg' => $msg));
        }
        $domainIdent = $this->input->post('domainIdent', TRUE);
        $userName = $this->input->post('user', TRUE);
        $userPwd = $this->input->post('password', TRUE);
        $remember = $this->input->post('remember', TRUE);

        //$this->load->helper('email');
        //if (valid_email($userName)) {
            //$loginType = 1;
        //} else {
            //$loginType = 0;
        //}

        $loginType = 0;
        //获取浏览器的 device_id 如果没有就生成新的
        $this->write_user_agent();

        $post_data = array("loginType" => $loginType, "loginTag" => $userName, "userPwd" => $userPwd, 'domainIdent' => $domainIdent);
        $res_data = $this->account_model->login($post_data);


        if (ACCOUNTLOCK == $res_data['code']) {
            self::res($res_data);
        }

        if (API_RET_SUCC != $res_data['code'] && $loginType == 0 && preg_match("/^\d{11,}$/", $userName)) {
            $post_data['loginType'] = 2;
            $res_data = $this->account_model->login($post_data);
            if (ACCOUNTLOCK == $res_data['code']) {
                self::res($res_data);
            }
        }

        if (!empty($remember)) {
            $life = 60 * 60 * 24 * 1;//一天内cookie有效
        }

        /*if (2 == PRODUCTMODEL && 1 == $loginType && API_RET_SUCC != $res_data['code']) {
            $res_check_result = $this->account_model->checkIsAdminLogin($domainIdent, $userName);
            if (API_RET_SUCC == $res_check_result['code']) {
                self::res(array('code' => 5000005, 'msg' => ''));
            }
        }*/

        //写入
        $this->write_token_indent($domainIdent);
        $this->write_token($res_data);
        self::res($res_data);
    }

    //定义user agent
    private function write_user_agent()
    {
        $config = $this->config->item('netdisk.api');
        $this->load->helper('cookie');
        if (isset($_COOKIE['netdisk_device_id'])) {
            if (!defined("KINGFILE_USER_AGENT")) {
                define("KINGFILE_USER_AGENT", $_COOKIE['netdisk_device_id']);
            }
        } else {
            if (!defined("KINGFILE_USER_AGENT")) {
                $this->load->helper('string');
                define("KINGFILE_USER_AGENT", $config['user_agent'] . ';' . random_string('alnum', 16));
            }
        }
        $this->write_token_deviceId();
    }

    /*
     * 写cookie
     * */
    private function write_token_deviceId()
    {
        $this->load->helper('cookie');
        if (!isset($_COOKIE['netdisk_device_id'])) {
            $device_id = array(
                'name' => 'device_id',
                'value' => KINGFILE_USER_AGENT,
                'expire' => time() + 99 * 365 * 24 * 3600,
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($device_id);
        }
    }

    private function write_token($res_data)
    {
        $this->load->helper('cookie');

        if (API_RET_SUCC == $res_data['code']) {
            if (isset($_COOKIE['netdisk_sid'])) {
                delete_cookie("netdisk_sid");
            }
            $cookie = array(
                'name' => 'sid',
                'value' => $res_data['data']['token'],
                'expire' => '86500',
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($cookie);
        }
    }

    /*
     * logo定制的
     * */
    private function write_token_indent($domainIdent)
    {
        $this->load->helper('cookie');
        if (isset($_COOKIE['netdisk_indent'])) {
            delete_cookie("netdisk_indent");
        }
        $indent = array(
            'name' => 'indent',
            'value' => $domainIdent,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($indent);
    }

    /*
     * 客户端跳转的字段
     * */
    private function write_token_state($state)
    {
        $state = array(
            'name' => 'state',
            'value' => $state,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($state);
    }


    function out()
    {
        echo "<script>window.location.href=http://pan.ksyun.com/account/signup</script>";
        //$user_id='73405404';
        //$boss_info=$this->account_model->getkingfile_bossinfo($user_id);
        //echo ($boss_info['name']);
    }


    /**
     * 注册表单
     */
    function signup()
    {
        $ksyun_token = $this->ksyun_token;

        if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
            $host = $_SERVER['HTTP_X_ORIGIN_HOST'];
            $host = "http://" . $host;
        } else {
            $host = HOST;
        }

        $from = $this->uri->segment(3);

        if (empty($ksyun_token)) {
            if ("ksyun" == $from) {
                header("location:http://pan.ksyun.com");
            } else {
                header("location:http://www.ksyun.com/user/login?callback=" . urlencode($host . "/account/signup"));
            }
            exit;
        } else {
            $kingfile_res = $this->account_model->getkingfile($ksyun_token);

            if (is_array($kingfile_res)) {
                $kingfile = $kingfile_res['kingfile'];
                if (!empty($kingfile)) {//已经开通过了

                    //cookie  device_id
                    $this->write_user_agent();

                    $post_data = $this->create_admin_post_data($kingfile);
                    $res_data = $this->account_model->login($post_data);
                    if (API_RET_SUCC == $res_data['code']) {
                        $this->write_token_indent($kingfile['domain_ident']);
                        //未开通双因子认证的超管登录 写cookie
                        if (isset($res_data['data']['token_type']) && $res_data['data']['token_type'] != 'DualFactor') {
                            $this->write_token($res_data);
                        }
                    } else {
                        $this->log_self('error', 'signup', $post_data, $res_data);
                    }

                    if ($from == "charge") {
                        header("location:" . HOST . "/dashboard#/charge");
                        exit;
                    }
                    if ($from == 'insidelink') {
                        $inside_link_path = $this->uri->segment(4);
                        $path = implode("/", explode("-", $inside_link_path));
                        header("location:" . HOST . "/dashboard#/" . $path);
                        exit;
                    }
                    header("location:" . HOST . "/dashboard");
                    exit;

                } else {
                    $data = $this->config->config['netdisk.resources'];
                    $data['ksyun_user'] = $kingfile_res['ksyun_user'];
                    //$data['ksyun_user']['company_name']="111";
                    $user_id = $data['ksyun_user']['id'];
                    $boss_info = $this->account_model->getkingfile_bossinfo($user_id);
                    if (isset($boss_info) && isset($boss_info['company_name'])) {
                        $data['ksyun_user']['company_name'] = $boss_info['company_name'];
                    } else {
                        $data['ksyun_user']['company_name'] = '';
                    }

                    $this->parser->parse("page/initkingfile.tpl", $data);
                }
            } else {
                if ("ksyun" == $from) {
                    header("location:http://pan.ksyun.com");
                } else {
                    header("location:http://www.ksyun.com/user/login?callback=" . urlencode($host . "/account/signup"));
                }
                exit;
            }
        }
    }

    /**
     * 获取注册用户的信息
     */

    function ksyuninfo()
    {
        $ksyun_token = $this->ksyun_token;
        if (empty($ksyun_token)) {
            self::res(array('code'=>900008,'msg'=>'ksyun token 不存在'));
        } else {
            $kingfile_res = $this->account_model->getkingfile($ksyun_token);
            if (is_array($kingfile_res)) {
                $kingfile = $kingfile_res['kingfile'];
                if (!empty($kingfile)) {//已经开通过了
                    $this->write_user_agent();
                    $post_data = $this->create_admin_post_data($kingfile);
                    $res_data = $this->account_model->login($post_data);
                    if (API_RET_SUCC == $res_data['code']) {
                        $this->write_token_indent($kingfile['domain_ident']);
                        //未开通双因子认证的超管登录 写cookie
                        if (isset($res_data['data']['token_type']) && $res_data['data']['token_type'] != 'DualFactor') {
                            $this->write_token($res_data);
                        }
                    } else {
                        $this->log_self('error', 'signup', $post_data, $res_data);
                        self::res(array('code'=>900009,'msg'=>'登陆失败'));
                    }
                    self::res(array('code'=>API_RET_SUCC,'data'=>array('has_config'=>0),'msg'=>'已配置企业信息'));
                    exit;

                } else {
                    $data = $this->config->config['netdisk.resources'];
                    $data['ksyun_user'] = $kingfile_res['ksyun_user'];
                    self::res(array('code'=>API_RET_SUCC,'data'=>array('has_config'=>1,'username'=>$kingfile_res['ksyun_user']['username'])));
                }
            } else {
                self::res(array('code'=>900007,'msg'=>'获取企业配置信息失败'));
                exit;
            }
        }
    }

    //超管 双因子认证登陆
    function dualfactoradminlogin()
    {
        /*$res_data=array('code'=>0,"data"=>array('token_type'=>'DualFactor',"token"=>"0460132b64a762336e6eac5ae87719dd-2621404756"));
        $this->write_user_agent();
        $this->write_token_indent("111");
        $this->write_token($res_data);
        self::res($res_data);
        exit;*/

        $ksyun_token = $this->ksyun_token;//金山云的token
        if (!empty($ksyun_token)) {
            $kingfile_res = $this->account_model->getkingfile($ksyun_token);
            if (is_array($kingfile_res)) {
                $kingfile = $kingfile_res['kingfile'];
                if (!empty($kingfile)) {
                    $this->write_user_agent();
                    $post_data = $this->create_admin_post_data($kingfile);
                    $res_data = $this->account_model->login($post_data);
                    if (API_RET_SUCC == $res_data['code']) {
                        $this->write_token_indent($kingfile['domain_ident']);
                        $this->write_token($res_data);//超管登录
                        if ($res_data['data']['token_type'] == 'DualFactor') {
                            self::res($res_data);
                        }
                    }
                }
            }
        }
    }


    //客户端用金山云token登录云盘
    function ksyunlogin()
    {
        $ksyun_token = $this->input->post('token', TRUE);//金山云的token
        $client_agent = $this->input->post('agent', TRUE);//客户端的agent
        if (empty($ksyun_token) || empty($client_agent)) {
            self::res(array('code' => 810000, 'msg' => '缺少必要的参数'));
        }

        $kingfile_res = $this->account_model->getkingfile($ksyun_token);
        if (!is_array($kingfile_res)) {//错误的token

            $this->log_self('error', 'ksyunlogin', array('ksyun_token' => $ksyun_token, 'client_agent' => $client_agent));
            self::res(array('code' => 810001, 'msg' => '错误的token'));
        }

        $kingfile = $kingfile_res['kingfile'];
        if (empty($kingfile)) {//没有开通过了
            $this->log_self('error', 'ksyunlogin', array('ksyun_token' => $ksyun_token, 'client_agent' => $client_agent));

            self::res(array('code' => 810002, 'msg' => '该账号暂未开通金山企业云盘'));
        }

        define("KINGFILE_USER_AGENT", $client_agent);

        $post_data = $this->create_admin_post_data($kingfile);
        $res_data = $this->account_model->login($post_data);
        if (API_RET_SUCC == $res_data['code']) {
            self::res($res_data);
        } else {
            $this->log_self('error', 'ksyunlogin', $post_data, $res_data);
            self::res(array('code' => 810003, 'msg' => '登录失败'));
        }
    }

    //客户端用金山云token开通kingfile
    function ksyuninitkingfile()
    {
        $domain_ident = $this->input->post('domain_ident', TRUE);
        $domain_name = $this->input->post('domain_name', TRUE);
        $ksyun_token = $this->input->post('token', TRUE);
        $client_agent = $this->input->post('agent', TRUE);//客户端的agent

        if (empty($ksyun_token) || empty($domain_ident) || empty($domain_name) || empty($client_agent)) {
            self::res(array('code' => 810000, 'msg' => '缺少必要的参数'));
        }

        if (!preg_match("/^([A-Za-z0-9-_.])+$/", $domain_ident)) {
            self::res(array('code' => 810004, 'msg' => '错误的domain_ident'));
        }

        $ret_arr = $this->account_model->initkingfile($ksyun_token, $domain_name, $domain_ident);
        if (API_RET_SUCC === $ret_arr['code']) {
            $kingfile = $ret_arr['data'];
            if ($domain_ident != $kingfile['domain_ident']) {
                self::res(array('code' => 810004, 'msg' => '错误的domain_ident'));
            }

            define("KINGFILE_USER_AGENT", $client_agent);

            $post_data = $this->create_admin_post_data($kingfile);
            $res_data = $this->account_model->login($post_data);
            if (API_RET_SUCC === $res_data['code']) {
                self::res($res_data);
            } else {

                $this->log_self('error', 'login kingfile error', $post_data, $res_data);

                self::res(array('code' => 810005, 'msg' => '开通kingfile失败'));
            }
        } else {
            self::res($ret_arr);
        }
    }


    /**
     * 初始化kingfile账号
     */
    function initkingfile()
    {
        $domain_ident = $this->input->post('domain_ident', TRUE);
        $domain_name = $this->input->post('domain_name', TRUE);
        $ksyun_token = $this->ksyun_token;
        if (isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
            $host = $_SERVER['HTTP_X_ORIGIN_HOST'];
            $host = "http://" . $host;
        } else {
            $host = HOST;
        }

        if (empty($ksyun_token)) {
            header("location:http://www.ksyun.com/user/login?callback=" . urlencode($host . "/account/signup"));
            exit;
        } else {
            if (empty($domain_name) || empty($domain_ident)) {
                self::res(array('code' => 'initkingfile_error_1'));
            }

            $ret_arr = $this->account_model->initkingfile($ksyun_token, $domain_name, $domain_ident);
            if (API_RET_SUCC === $ret_arr['code']) {
                $kingfile = $ret_arr['data'];
                if ($domain_ident != $kingfile['domain_ident']) {
                    self::res(array('code' => 'initkingfile_error_2', 'msg' => '错误的企业标识'));
                }

                //$config = $this->config->item('netdisk.api');
                //$this->load->helper('string');
                //define("KINGFILE_USER_AGENT", $config['user_agent'] . ';' . random_string('alnum', 16));

                //获取浏览器的 device_id 如果没有就生成新的
                $config = $this->config->item('netdisk.api');
                if (isset($_COOKIE['netdisk_device_id'])) {
                    define("KINGFILE_USER_AGENT", $_COOKIE['netdisk_device_id']);
                } else {
                    $this->load->helper('string');
                    define("KINGFILE_USER_AGENT", $config['user_agent'] . ';' . random_string('alnum', 16));
                }
                $this->write_token_deviceId();


                $post_data = $this->create_admin_post_data($kingfile);
                $res_data = $this->account_model->login($post_data);
                if (API_RET_SUCC === $res_data['code']) {
                    $this->write_token($res_data);//超管登录
                    self::res($res_data);
                } else {

                    $this->log_self('error', 'login kingfile error', $post_data, $res_data);

                    if (isset($res_data['code'])) {
                        $ret_code = $res_data['code'];
                    } else {
                        $ret_code = "initkingfile_error_3";
                    }
                    self::res(array('code' => $ret_code, 'data' => array(), 'msg' => '开通失败，原因：登录超管失败'));
                }
            } else {
                self::res($ret_arr);
            }
        }
    }


    /**
     * 退出登陆
     */
    function logout()
    {
        $this->load->helper('cookie');
        $data = $this->config->config['netdisk.resources'];
        $res_data = $this->account_model->logout($this->token);
        //delete_cookie('netdisk_sid',$data['cookie_domain']);
        delete_cookie('netdisk_ssid');
        delete_cookie('netdisk_sid');
        //delete_cookie('netdisk_device_id');
        delete_cookie('kscdigest', '.ksyun.com');
        delete_cookie('netdisk_state');
        delete_cookie('netdisk_indent');
        delete_cookie('netdisk_ld');
        if(isset($_COOKIE['netdisk_user_identity'])){
            delete_cookie('netdisk_user_identity');
        }
        if(isset($_COOKIE['netdisk_user_identityone'])){
            delete_cookie('netdisk_user_identityone');
        }

        //header("location:/");
        if ($this->input->is_ajax_request()) {
            self::res(array('code' => 0, 'data' => array()));
        } else {
            if (PRODUCTMODEL == 2) {
                $from = $this->input->cookie('netdisk_indent', TRUE);
                if (isset($from) && in_array($from, array("novogene"))) {
                    delete_cookie('netdisk_indent');
                    header("location:/login/c/$from");
                } else {
                    header("location:/login");
                }
            } else {
                header("location:/");
            }
            exit;
        }
    }

    /**
     * 修改密码
     */
    function changepwd()
    {
        if ($this->form_validation->run() == FALSE) {
            $msg = validation_errors();
            return self::res(array('code' => REQUEST_PARAMS_ERROR, 'msg' => $msg));
        }
        $userpwd = $this->input->post('userpwd', TRUE);
        $newpwd = $this->input->post('password', TRUE);
        $renewpwd = $this->input->post('confirmpwd', TRUE);
        if (empty ($userpwd) || strlen($userpwd) < 6) {
            self::response("userpwdError");
        }
        if (empty ($newpwd) || strlen($newpwd) < 6) {
            self::response("newpwdError");
        }
        if ($newpwd !== $renewpwd) {
            self::response("twoPwdNotSame");
        }
        $ret_data = $this->account_model->set_password($this->token, $userpwd, $newpwd, $renewpwd);
        self::res($ret_data);
    }


    /**
     * 从邮件跳入，密码输入框
     */
    function verify()
    {
        //$username = "19669216@qq.com";
        //往平台发送验证码
        //$ret_data = $this->account_model->password_verify($this->token, $username);
        //self::res($ret_data);
        $token = $this->input->get('token', TRUE);
        $email = $this->input->get('email', TRUE);
        $data = $this->config->config['netdisk.resources'];
        $this->load->model('platform_model');
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach ($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        $data['paltform'] = $paltform;
        $data['token'] = $token;
        $data['email'] = $email;
        if ($paltform['offline'] == '1') {
            $this->parser->parse("page/offline.tpl", $data);
        } else {
            $data['base_url'] = base_url();
            if (2 == PRODUCTMODEL) {
                $this->parser->parse("page/resetpwdform.tpl", $data);
            } else {
                $this->parser->parse("page/resetpwdform_3.tpl", $data);
            }
        }
    }

    /**
     * 重置密码逻辑
     */
    function resetpwd()
    {
        if ($this->form_validation->run() == FALSE) {
            $msg = validation_errors();
            return self::res(array('code' => REQUEST_PARAMS_ERROR, 'msg' => $msg));
        }
        $token = $this->input->post('token', TRUE);
        $userPwd = $this->input->post('newpwd', TRUE);
        $confirmPwd = $this->input->post('password', TRUE);
        if ($userPwd != $confirmPwd) {
            self::res(array('code' => 1, 'msg' => '两次密码输入不一致'));
        } else {
            $ret = $this->account_model->password_reset($token, $userPwd, $confirmPwd);
            self::res($ret);
        }
    }


    /**
     * 忘记密码，输入框
     */
    function forget()
    {
        $data = $this->config->config['netdisk.resources'];
        $this->load->model('platform_model');
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach ($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        $data['paltform'] = $paltform;
        $data['platform']['producttype'] = PRODUCTTYPE;
        if ($paltform['offline'] == '1') {
            $this->parser->parse("page/offline.tpl", $data);
        } else {
            $data['base_url'] = base_url();
            if (2 == PRODUCTMODEL) {
                $this->parser->parse("page/forget.tpl", $data);
            } else {
                $this->parser->parse("page/forget_3.tpl", $data);
            }

        }
    }

    /**
     * 发送邮件接口
     */
    function forgetpwd()
    {
        if ($this->form_validation->run() == FALSE) {
            $msg = validation_errors();
            return self::res(array('code' => REQUEST_PARAMS_ERROR, 'msg' => $msg));
        }
        $domainIdent = $this->input->post('domainIdent', TRUE);
        $email = $this->input->post('email', TRUE);
        $this->load->helper('email');
        if (valid_email($email)) {
            if (PRODUCTTYPE == 1) {
                $domainIdent = trim(file_get_contents(DEFAULT_DOMAIN_NAME));
            }
            $ret = $this->account_model->password_verify($this->token, $email, $domainIdent);
            //self::res(array('code'=>0,'data'=>array('email'=>$email)));
            $ret['data']['email'] = $email;
            self::res($ret);
        } else {
            self::res(array('code' => 9998, 'msg' => "错误的email格式"));
        }
    }

    /**
     * 验证账号的有效性
     */
    function vaildate()
    {
        $vtype = $this->input->post('vtype', TRUE);
        $value = $this->input->post('value', TRUE);
        if ($vtype == 2 && $value == "") {
            self::res(array('code' => 0, 'data' => array()));
        }
        if (intval($vtype) == 1) {
            if ($value == "") {
                self::res(array('code' => 0, 'data' => array()));
            }

            $this->load->helper('email');
            if (!valid_email($value)) {
                self::res(array('code' => 9998, 'msg' => '错误的email格式'));
            }
        }
        $value = trim($value);
        $ret_data = $this->account_model->account_validate($this->token, $vtype, $value);
        self::res($ret_data);
    }


    function get_device_type()
    {
        //全部变成小写字母
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $type = 'other';
        //分别进行判断
        if (strpos($agent, 'iphone')) {
            $type = 'ios';
        }
        if (strpos($agent, 'ipad')) {
            $type = 'ipad';
        }

        if (strpos($agent, 'android')) {
            $type = 'android';
        }
        return $type;
    }

    //用key进行登录
    function loginbyident()
    {
        $client_agent = $this->input->post('agent', TRUE);
        $ident = $this->input->post('ident', TRUE);
        $sign = $this->input->post('sign', TRUE);
        $identMap = array('novogene' => 'novogeneKingFile_453');
        if (!empty($ident) && isset($identMap[$ident]) && $sign == md5($client_agent . $ident . $identMap[$ident])) {
            $ret = $this->account_model->getKingfileByIdent($ident);
            $kingfile = $ret['data'];
            define("KINGFILE_USER_AGENT", $client_agent);
            $post_data = $this->create_admin_post_data($kingfile);
            $res_data = $this->account_model->login($post_data);
            if (API_RET_SUCC == $res_data['code']) {
                self::res($res_data);
            } else {
                $this->log_self('error', 'loginbyident', $post_data, $res_data);
                self::res(array('code' => 810003, 'msg' => '登录失败'));
            }
        } else {
            self::res(array('code' => 810003, 'msg' => '登录失败,错误的签名'));
        }
    }

    //是否实名认证
    function authentication()
    {

        $ksyun_token = $this->ksyun_token;
        if (empty($ksyun_token)) {
            $this->log_self('error', 'authentication', array('ksyun_token' => $ksyun_token), array('code' => 810000, 'msg' => 'token缺少'));
            self::res(array('code' => 810000, 'msg' => 'token缺少'));
        }
        $this->log_self('debug', 'authentication', array('ksyun_token' => $ksyun_token));
        $kingfile_res = $this->account_model->getkingfile($ksyun_token);
        if (!is_array($kingfile_res)) {//错误的token
            $this->log_self('error', 'authentication', array('ksyun_token' => $ksyun_token), "");
            self::res(array('code' => 810001, 'msg' => '错误的token'));
        }
        if (!isset($kingfile_res['kingfile'])) {
            $this->log_self('error', 'authentication', array('ksyun_token' => $ksyun_token), "");
            self::res(array('code' => 810002, 'msg' => '该账号暂未开通金山企业云盘'));
        }
        $kingfile = $kingfile_res['kingfile'];
        $id = $kingfile['ksyun_userid'];
        $is_authentication = $this->account_model->authentication($id);

        if ($is_authentication || $id == '73402307' || $id == '73405227'||$id=='188'||$id=='2000014974') {
            self::res(array('code' =>API_RET_SUCC , 'msg' => '已实名认证,id='.$id.',is_authentication='.$is_authentication));
        }else{
            self::res(array('code' =>900005 , 'msg' => '未实名认证'));
        }
    }

    //客户端 获取用户基本信息  替换passport新接口
    function basicinfo()
    {
        $ksyun_token = $this->input->post('token', TRUE);
        $this->log_self('debug', 'authentication_from_client', array('ksyun_token' => $ksyun_token));
        if (empty($ksyun_token)) {
            self::res(array('code' => 2001, 'msg' => 'token缺少'));
        }
        $kingfile_res = $this->account_model->getkingfile($ksyun_token);
        if (!is_array($kingfile_res)) {//错误的token
            $this->log_self('error', 'authentication_from_client', array('ksyun_token' => $ksyun_token));
            self::res(array('code' => 810001, 'msg' => '错误的token'));
        }
        if (!isset($kingfile_res['kingfile'])) {
            $this->log_self('error', 'authentication_from_client', array('ksyun_token' => $ksyun_token), array('msg' => '该账号暂未开通金山企业云盘'));
            self::res(array('code' => 810002, 'msg' => '该账号暂未开通金山企业云盘'));
        }
        $kingfile = $kingfile_res['kingfile'];
        $id = $kingfile['ksyun_userid'];
        $basic_info = $this->account_model->authentication_info($id);
        if ( $id == '2000014974') {
            $basic_info['data']['type'] = 2;
        }
        self::res($basic_info);
    }

    private function create_admin_post_data($kingfile)
    {
        $post_data = array(
            "loginType" => 0,
            "loginTag" => empty($kingfile['ksyun_username']) ? $kingfile['ksyun_email'] : $kingfile['ksyun_username'],
            "userPwd" => md5("kingfile" . $kingfile['ksyun_userid'] . $kingfile['domain_key']),
            "domainIdent" => $kingfile['domain_ident']
        );
        return $post_data;
    }

    private function log_self($type, $name, $param, $res = '')
    {

        $param = ' post=' . json_encode($param, true);
        $res = " res=" . json_encode($res, true);
        if ('error' == $type) {
            log_message('error', "Log Self " . $name . $param . $res);
        }
        if ('debug' == $type) {
            log_message('debug', "Log Self " . $name . $param . $res);
        }

    }

    function wechat_qrcode()
    {
        $userId = $this->input->post('userId', true);
        $userId = intval($userId);
        $ret_data = $this->account_model->wechat_qrcode($userId);
        self::res($ret_data);
    }


}

/* End of file account.php */
/* Location: ./application/controllers/account.php */
