<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 控制面板
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */

class File extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('xfile_model');
        $this->load->model('account_model');
        $this->load->model('platform_model');
    }

    private function mysort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
        return $arrays;
    }


    /*
     * rc4加密算法
     * $pwd 密钥
     * $data 要加密的数据
    */
    private function rc4 ($pwd, $data){
        $key[] ="";
        $box[] ="";

        $pwd_length = strlen($pwd);
        $data_length = strlen($data);

        for ($i = 0; $i < 256; $i++)
        {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }

        for ($j = $i = 0; $i < 256; $i++)
        {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        $cipher='';
        for ($a = $j = $i = 0; $i < $data_length; $i++)
        {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;

            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;

            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ $k);
        }

        return $cipher;
    }

    /**
    * 列出根共享目录
    */
    public function rootlist()
    {


        self::decryption();

        $pageIdx = $this->input->get('pageIdx',TRUE);
         $sortBy = $this->input->get('sortBy',TRUE);
         $order =$this->input->get('order',TRUE);

        if(empty($pageIdx)) {
            $pageIdx = 0;
        }
        if(empty($sortBy)) {
            $sortBy = 1;
        } else {
            $map = array('name'=>1,'size'=>2,'date'=>3);
            $sortBy = $map[$sortBy];
        }
        if(empty($order)) {
            $order = 1;
        } else {
            $orderMap = array('desc'=>-1,'asc'=>1);
            $order = $orderMap[$order];
        }
        //$run_time = new runtime();
        //$run_time->start();
        $ret_data = $this->xfile_model->dir_share_root_list($this->token,$sortBy,$order, $pageIdx);
         if( API_RET_SUCC == $ret_data['code']) {

             foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
             }
         }
        //$run_time->stop();
        //$time=$run_time->spent()."ms";
        //$ret_data['time']=$time;
        self::res($ret_data);
    }


    public function rootlistcontinue()
    {
        $cursor = $this->input->get('cursor',TRUE);
        $ret_data = $this->xfile_model->dir_share_root_list_continue($this->token,$cursor);
        if( API_RET_SUCC == $ret_data['code']) {
            foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
            }
        }
        self::res($ret_data);
    }


    /**
    * 列出文件夹下的所有文件
    */
    public function  dirsharelist()
    {
        $file_id = $this->input->get('xid',TRUE);
        $sortBy = $this->input->get('sortBy',TRUE);
        $order =$this->input->get('order',TRUE);

      
        if(empty($sortBy)) {
            $sortBy = 1;
        } else {
            $map = array('name'=>1,'size'=>2,'date'=>3);
            $sortBy = $map[$sortBy];
        }
        if(empty($order)) {
            $order = 1;
        } else {
            $orderMap = array('desc'=>-1,'asc'=>1);
            $order = $orderMap[$order];
        }

        $ret_data = $this->xfile_model->dir_share_list($this->token, $file_id,$sortBy,$order);
        if (API_RET_SUCC == $ret_data['code']  && isset($ret_data['data']['result']) && is_array($ret_data['data']['result'])) {
            foreach($ret_data['data']['result'] as &$folder) {
                if(isset($folder['locked']) && $folder['locked']==1){
                    //已被锁定
                    $ret_lock_status = $this->xfile_model->file_lock_status($this->token, $folder['xid']);
                    if($ret_lock_status['code']==API_RET_SUCC){
                        $folder['lock_info']=$ret_lock_status['data'];
                    }
                }
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
            }
        }
        self::res($ret_data);
    }

    public function dirsharelistcontinue()
    {
        $cursor = $this->input->get('cursor',TRUE);
        $ret_data = $this->xfile_model->dir_share_list_continue($this->token,$cursor);
        if (API_RET_SUCC == $ret_data['code']  && isset($ret_data['data']['result']) && is_array($ret_data['data']['result'])) {
            foreach($ret_data['data']['result'] as &$folder) {
                if(isset($folder['locked']) && $folder['locked']==1){
                    //已被锁定
                    $ret_lock_status = $this->xfile_model->file_lock_status($this->token, $folder['xid']);
                    if($ret_lock_status['code']==API_RET_SUCC){
                        $folder['lock_info']=$ret_lock_status['data'];
                    }
                }
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
            }
        }
        self::res($ret_data);
    }

    /*
     * 获取目录列表
     * */

    public function dir_page_list()
    {
        $file_id = $this->input->post('xid',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        $ret_data = $this->xfile_model->dir_page_list($this->token, $file_id,$isShare);
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data']['result'])) {
            foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
            }
        }
        self::res($ret_data);
    }
    /*
     * 获取目录列表 continue
     * */
    public function dir_page_list_continue(){
        $cursor = $this->input->get('cursor',TRUE);
        $isShare = $this->input->get('isShare',TRUE);
        $ret_data = $this->xfile_model->dir_page_list_continue($this->token, $cursor,$isShare);
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data']['result'])) {
            foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
            }
        }
        self::res($ret_data);
    }


    /**
    * 创建根共享文件夹
    */
    public function createsharefolder()
    {

        self::decryption();

        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $name = $this->input->post('name',TRUE);
        $desc = $this->input->post('desc',TRUE);
        $members = $this->input->post('members',TRUE);//'[{"member_id":"1","type":"2","power":"3"},]'
        $f_members = json_decode($members,true);
        if ($f_members && is_array($f_members)) {
            foreach($f_members as &$member) {
                $member['user_xid'] = intval($member['user_xid']);
                $member['role'] = intval($member['role']);
            }
        }

        $space = abs($this->input->post('space',TRUE) * 1024 * 1024 * 1024);
        //$desc = htmlentities ( $desc, ENT_QUOTES, "UTF-8" );
        //$name = htmlentities ( $name, ENT_QUOTES, "UTF-8" );
        if (empty ($name)) {
            self::response ( "nameisempty" );
        }
        $ret_data = $this->xfile_model->dir_share_root_create($this->token,$name,$desc,$space,$f_members);
        self::res($ret_data);
    }

    /**
    * 创建文件夹
    */
    public function createfolder()
    {
        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $parentId = $this->input->post('parentId',TRUE);
        //$parentId = intval($parentId);
        $parentId = $parentId == "-1" ? 0 : $parentId;
        $name = $this->input->post('name',TRUE);
        $iscageroot = $this->input->post('iscageroot',TRUE);
        //self::res(array('code'=>'2001','data'=>0));
        //跟目录
        if(1==$iscageroot) {
            $ret_data = $this->xfile_model->dir_cage_create($this->token, $parentId, $name);
        } else {//非根目录
            $info_data = $this->xfile_model->info($this->token, $parentId);
            if(API_RET_SUCC==$info_data['code']) {
                if($info_data['data']['is_share']==0) {
                    $ret_data = $this->xfile_model->dir_cage_create($this->token, $parentId, $name);
                } else {
                    $ret_data = $this->xfile_model->dir_share_create($this->token, $parentId, $name);
                }
            }else{
                self::response("error_parentId");
            }
        }
        self::res($ret_data);
    }

    /**
    * 编辑权限
    */
    public function overwriterole()
    {
        $xid = $this->input->post('xid',TRUE);
        $members = $this->input->post('members',TRUE);//'[{"member_id":"1","type":"2","power":"3"},]'
        $depth=$this->input->post('deep',TRUE);
        $operators = json_decode($members,true);
        $operators_ids = array();
        if ($operators && is_array($operators)) {
            foreach($operators as &$member) {
                $member['user_xid'] = intval($member['user_xid']);
                $member['role'] = intval($member['role']);
                array_push($operators_ids,$member['user_xid']);
            }
        }

        $perm =  array();

        $ret_data = $this->xfile_model->dir_share_role_list($this->token,$xid);
        $user_info  = $this->account_model->info($this->token);

        $f_members  = array();
        $before = array();
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data'])) {
            foreach ( $ret_data ['data'] as $key => $value ) {//处理删掉的情况
                if(intval($value['role'])!=ROLE_GOD && $user_info['data']['xid'] != $value['user_xid']) {
                    if(in_array($value['user_xid'],$operators_ids)) {
                        $f_members[$value['user_xid']] = $value['role'];
                        array_push($before,$value['user_xid'].$value['role']);
                    } else {
                        $f_members[$value['user_xid']] = ROLE_NOTHING;
                        array_push($before,$value['user_xid'].ROLE_NOTHING);
                    }
                }
            }
        }


        $after = array();
        foreach($operators as $item) {//处理新增的
            if(intval($item['role'])!=ROLE_GOD && $user_info['data']['xid'] != $item['user_xid']) {
                $user_xid = $item['user_xid'];
                $role = intval($item['role']);
                $f_members[$user_xid] = $role;
                array_push($after,$item['user_xid'].$item['role']);
            }
        }
        foreach($f_members as $key=>$value) {
            $perm_item = array();
            $perm_item['user_xid'] = $key;
            $perm_item['role'] = $value;
            $perm[] = $perm_item;
        }
        sort($before);
        sort($after);
        if (implode(".",$before) === implode(".", $after)) {
            //self::res(array('code'=>API_RET_SUCC,'data'=>array()));
        }

        //deep grant
        if($depth==0){

            $ret_data = $this->xfile_model->dir_share_role_deep_grant($this->token,$xid,$perm);
        } else if($depth==1){
            $ret_data = $this->xfile_model->dir_share_role_grant($this->token,$xid,$perm);
        }
        $ret_data['data'] = array('id'=>$xid);
        self::res($ret_data);
    }

    /**
    * 修改共享根目录
    */
    public function editShareRoot()
    {
        if ($this->form_validation->run() == FALSE) {
           $msg = validation_errors();
           return self::res(array('code'=>REQUEST_PARAMS_ERROR,'msg'=>$msg));
        }
        $xid = $this->input->post('xid',TRUE);
        $name = $this->input->post('name',TRUE);
        $quota = $this->input->post('quota',TRUE);
        $desc = $this->input->post('desc',TRUE);
        if("-1" == $quota) {
            $quota = 0;
        }
        $quota = abs($quota* 1024 * 1024 * 1024);

        $info_ret = $this->xfile_model->info($this->token, $xid);
        if (API_RET_SUCC == $info_ret['code']) {

            if(strcmp($name,$info_ret['data']['name'])!=0)
            {
                $rename_ret = $this->xfile_model->share_rename($this->token,$xid,$name);
            } else {
                $rename_ret = array('code'=>API_RET_SUCC);
            }
            if(API_RET_SUCC == $rename_ret['code'] && ($desc != $info_ret['data']['description'] || $quota != $info_ret['data']['quota'])) {
                //if ($quota < $info_ret['data']['used']) {
                //    self::res(array('code'=>QUOTA))
                //}
                $ret_data = $this->xfile_model->dir_share_root_modify($this->token, $xid, $desc, $quota);
                self::res($ret_data);
            } else {
                self::res($rename_ret);
            }
        }

    }


    /**
    * 重命名
    */
    public function rename()
    {
        $file_id = $this->input->post('xid',TRUE);
        $name = $this->input->post('name',TRUE);

        self::checkInputVal($name);

        $info_data = $this->xfile_model->info($this->token, $file_id);
        if(API_RET_SUCC==$info_data['code']) {
            $file_id = 0+$file_id;
            if($info_data['data']['is_share']==0) {
                $ret_data = $this->xfile_model->cage_rename($this->token, $file_id, $name);
            } else {
                $ret_data = $this->xfile_model->share_rename($this->token, $file_id, $name);
            }
        }else{
            self::response("error_file_id");
        }
        self::res($ret_data);
    }

    private function  get_son_node($list,$parent_id) {
        foreach($list as $value) {
            if($value['parent_xid'] == $parent_id) {
                return $value;
            }
        }
    }

    /**
    * 获取祖先
    */
    public function ancestorlist()
    {
        $fileId = $this->input->get('xid',TRUE);
        //$fileId = $fileId;
        $is_cage_space = false;
        $ret_data = $this->xfile_model->ancestor_list($this->token, $fileId);
        //unset($ret_data['data']['0']);
        //$ret_data['data'] = array_shift($ret_data['data']);
        if(API_RET_SUCC!=$ret_data['code']) {
            self::res($ret_data);
        }
        foreach ($ret_data['data'] as &$file) {
            $file['parent_id'] = $file['parent_xid'];
            if($file['is_share'] ==0) {
                $is_cage_space = true;
            }
        }
        $parent_id = 0;
        $map = array();
        while ($node = $this->get_son_node($ret_data['data'], $parent_id)) {
            $map[] = $node;
            $parent_id = $node['xid'];
        }
        $data['code'] = $ret_data['code'];
        $data['data'] = $map;
        array_shift($data['data']);

        //$data['data'] = array_reverse($map);
        if($is_cage_space) {
           // array_push($data['data'],array('name'=>'个人空间','xid'=>'-1'));
           //         $info_data = $this->xfile_model->info($this->token, $file_id);
         //print_r($info_data);
         //exit;
        }
        self::res($data);
    }

    /**
    * 删除文件（夹）
    */
    public function archive()
    {
        set_time_limit(0);

        $file_ids = $this->input->post('xids',TRUE);
        $xids_len = count($file_ids);
        if($xids_len==0){
            selt::res(array('code'=>3));
        }

        $file_id=$file_ids[0];
        //print_r($file_id);

        $info_data = $this->xfile_model->info($this->token, $file_id);
        if(API_RET_SUCC==$info_data['code']) {
            if($info_data['data']['is_share']==1) {
                $ret_data = $this->xfile_model->share_archive_batch($this->token, $file_ids);
            } else {
                $ret_data = $this->xfile_model->cage_archive_batch($this->token, $file_ids);
            }
        }else{
            usleep(100000);
            self::response("error_file_id");
        }
        //usleep(100000);
        self::res($ret_data);
    }
    /**
     * 查询删除文件进度
     * */

    public  function archive_check(){
        $jobId = $this->input->post('jobId',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        if(1==$isShare){
            $ret_data = $this->xfile_model->share_archive_batch_check($this->token, $jobId);
        }else{
            $ret_data = $this->xfile_model->cage_archive_batch_check($this->token, $jobId);
        }
        self::res($ret_data);
    }




    /**
    * 从回收站彻底删除
    */
    public function delete()
    {
        $file_ids = $this->input->post('xids',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        $xids_len = count($file_ids);
        if($xids_len==0){
            selt::res(array('code'=>3));
        }
        $xid=$file_ids[0];
        $info_data = $this->xfile_model->info($this->token, $xid);
        if(API_RET_SUCC==$info_data['code']) {
            if($info_data['data']['is_share']==0) {
                $ret_data = $this->xfile_model->dir_cage_delete($this->token, $xid);
            } else {
                $ret_data = $this->xfile_model->dir_share_delete($this->token, $xid);
            }
        }else{
            usleep(100000);
            self::response("error_file_id");
        }

        self::res($ret_data);
    }


    /**
     * 查询回收站彻底删除进度
     * */

    public  function deletecheck(){
        $jobId = $this->input->post('jobId',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        if(1==$isShare){
            $ret_data = $this->xfile_model->dir_share_delete_check($this->token, $jobId);
        }else{
            $ret_data = $this->xfile_model->dir_cage_delete_check($this->token, $jobId);
        }
        self::res($ret_data);
    }


    /**
    * 恢复文件（夹）
    */
    public function recover(){
        $file_ids = $this->input->post('xids',TRUE);
        $xids_len = count($file_ids);
        if($xids_len==0){
            selt::res(array('code'=>3));
        }
        $xid=$file_ids[0];
        $info_data = $this->xfile_model->info($this->token, $xid);
        //foreach($file_ids as $file_id) {
            if(API_RET_SUCC==$info_data['code']) {
                $isShare=$info_data['data']['is_share'];
                $ret_data = $this->xfile_model->recover($this->token, $xid,$isShare);
            }else{
                usleep(100000);
                self::response("error_file_id");
            }
        //}
        usleep(100000);
        self::res($ret_data);
    }

    /*
     *查询 恢复文件（夹）进度
     * */
    function recover_check(){
        $jobId = $this->input->post('jobId',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        $ret_data = $this->xfile_model->recover_check($this->token, $jobId,$isShare);
        self::res($ret_data);
    }

    /**
    * 移动文件
    */
    public function move()
    {
        set_time_limit(0);
        $dstXid = $this->input->post('dstXid',TRUE);
        $xids = $this->input->post('xids',TRUE);
        $xids_len = count($xids);
        if($xids_len==0){
            selt::res(array('code'=>3));
        }
        if(0 === $dstXid) {
            $ret_data = $this->xfile_model->cage_move_batch($this->token, $xids, $dstXid);
        }else {
            $xid=$xids[0];
            $info_data = $this->xfile_model->info($this->token, $xid);
            if (API_RET_SUCC == $info_data['code']) {
                if ($info_data['data']['is_share'] == 0) {
                    $ret_data = $this->xfile_model->cage_move_batch($this->token, $xids, $dstXid);
                } else {
                    $ret_data = $this->xfile_model->share_move_batch($this->token, $xids, $dstXid);
                }

            } else {
                usleep(100000);
                //self::res($info_data);
                self::response("error_file_id");
            }
        }

        usleep(100000);
        self::res($ret_data);
    }

    /**
     * 查询批量接口的进度
     * */

    public  function move_check(){
        $jobId = $this->input->post('jobId',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        if(1==$isShare){
            $ret_data = $this->xfile_model->share_move_batch_check($this->token, $jobId);
        }else{
            $ret_data = $this->xfile_model->cage_move_batch_check($this->token, $jobId);
        }
        self::res($ret_data);
    }



    /**
    * 列出个人文件的子文目录信息
    */
    public function dircagelist()
    {
        $file_id = $this->input->get('xid',TRUE);
        $sortBy = $this->input->get('sortBy',TRUE);
        $order = $this->input->get('order',TRUE);
        if(empty($sortBy)) {
            $sortBy = 1;
        } else {
            $map = array('name'=>1,'size'=>2,'date'=>3);
            $sortBy = $map[$sortBy];
        }
        if(empty($order)) {
            $order = 1;
        } else {
            $orderMap = array('desc'=>-1,'asc'=>1);
            $order = $orderMap[$order];
        }
        $ret_data = $this->xfile_model->dir_cage_list($this->token, $file_id,$sortBy,$order);

        if (API_RET_SUCC == $ret_data['code']) {
            foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
                $folder['role'] = ROLE_GOD;
            }

        }
        self::res($ret_data);
    }

    public function dircagelistcontinue(){
        $cursor = $this->input->get('cursor',TRUE);
        $ret_data = $this->xfile_model->dir_cage_list_continue($this->token,$cursor);
        if (API_RET_SUCC == $ret_data['code']) {
            foreach($ret_data['data']['result'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
                $folder['role'] = ROLE_GOD;
            }

        }
        self::res($ret_data);
    }

    /**
    * 列出个人文件的子目录里的目录
    */
    public function dircagedirlist()
    {
        $file_id = $this->input->get('xid',TRUE);
        $ret_data = $this->xfile_model->dir_cage_dir_list($this->token, $file_id);
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data'])) {
            foreach($ret_data['data'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
                $folder['role'] = ROLE_GOD;
            }
        }
        self::res($ret_data);
    }

    /**
    * 获取文件夹的共享成员
    */
    public function rolelist()
    {
        $xid = $this->input->get('xid',TRUE);
        $ret_data = $this->xfile_model->dir_share_role_list($this->token, $xid);
        self::res($ret_data);
    }

    /**
    * 企业回收站
    */
    public function archivelist()
    {
        $order = $this->input->get('order',TRUE);
        $pageMax = $this->input->get('pageMax',TRUE);
        $pageIdx = $this->input->get('pageIdx',TRUE);
        if(empty($order)) {
            $ordrer = -1;
        }
        if(empty($pageMax)) {
           $pageMax = 50;
        } else {
            $pageMax = intval($pageMax);
        }
        if(empty($pageIdx)) {
            $pageIdx = 0;
        } else {
            $pageIdx = intval($pageIdx);
        }
        $mtimeMin = $this->input->get('mtime_min',TRUE);
        if(empty($mtimeMin) ||is_null($mtimeMin)) {
            $mtimeMin = 0;
        }else{
            $mtimeMin = strtotime($mtimeMin);
        }

        $mtimeMax = $this->input->get('mtime_max',TRUE);
        if(empty($mtimeMax)||is_null($mtimeMax)) {
            $mtimeMax = 2147483647;
        }else{
            $mtimeMax =  strtotime($mtimeMax." 23:59:59");
        }
        $ret_data = $this->xfile_model->share_archive_page_list($this->token,$order,$pageMax,$pageIdx,$mtimeMin,$mtimeMax);
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data'])) {
            foreach($ret_data['data'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
            }
        }
        self::res($ret_data);
    }

    /**
    * 个人回收站
    */
    public function cagearchivelist()
    {
        $order = $this->input->get('order',TRUE);
        $pageMax = $this->input->get('pageMax',TRUE);
        $pageIdx = $this->input->get('pageIdx',TRUE);
        if(empty($order)) {
            $ordrer = -1;
        }
        if(empty($pageMax)) {
           $pageMax = 50;
        } else {
            $pageMax = intval($pageMax);
        }
        if(empty($pageIdx)) {
            $pageIdx = 0;
        } else {
            $pageIdx = intval($pageIdx);
        }
        $mtimeMin = $this->input->get('mtime_min',TRUE);
        if(empty($mtimeMin) ||is_null($mtimeMin)) {
            $mtimeMin = 0;
        }else{
            $mtimeMin = strtotime($mtimeMin);
        }

        $mtimeMax = $this->input->get('mtime_max',TRUE);
        if(empty($mtimeMax)||is_null($mtimeMax)) {
            $mtimeMax = 2147483647;
        }else{
            $mtimeMax =  strtotime($mtimeMax." 23:59:59");
        }

        $ret_data = $this->xfile_model->cage_archive_page_list($this->token,$order,$pageMax,$pageIdx,$mtimeMin,$mtimeMax);
        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data'])) {
            foreach($ret_data['data'] as &$folder) {
                $folder['parent_id'] = $folder['parent_xid'];
                $folder['fmt_mtime']  = date('Y-m-d H:i:s',$folder['mtime']);
            }
        }
        self::res($ret_data);
    }

    /**
    * 链接列表
    */
    public function linklist()
    {

        self::decryption();

        $order = $this->input->get('order',TRUE);
        $pageMax = $this->input->get('pageMax',TRUE);
        $hint = $this->input->get('hint',TRUE);
        if(empty($order)) {
            $ordrer = -1;
        }
        if(empty($pageMax)) {
           $pageMax = 200;
        }
        if(empty($hint)) {
            $hint = 0;
        }
        $web_server = $this->platform_model->get_web_server();
        $ret_data = $this->xfile_model->linklist($this->token);
        if($ret_data && isset($ret_data['data']) && is_array($ret_data['data'])) {
            foreach($ret_data['data'] as &$item) {
                $item['name']  = $item['xfile_name'];
                if(empty($item['xtype'])){
                    $item['xtype']=0;
                }
                $item['expired'] = $item['ctime'] + $item['age'];
                $item['full_link'] =  $web_server."/l/s/".$item['link'];
            }
        }
        self::res($ret_data);
    }

    public function  get_web_server(){
        $web_server = $this->platform_model->get_web_server();
        echo $web_server;
    }

    /**
    * 删除链接
    */
    public function deletelink()
    {

        $links = $this->input->post('links',TRUE);
        $fmt_link = array();
        $i = 0;
        $count = count($links);
        foreach($links as $value) {
            array_push($fmt_link, intval($value));
            $i++;
            if(($count<=50 && $i==$count) || ($count>50 &&$i==50)) {
                $ret_data = $this->xfile_model->linkdelete($this->token,$fmt_link);
                $count = $count - count($fmt_link);
                $fmt_link = array();
                $i = 0;
            }
        }
        usleep(100000);
        self::res($ret_data);
    }

    /**
    * 发布链接
    */
    public function linkpublish()
    {
        $xid = $this->input->post('xid',true);
        $password = $this->input->post('password',true);
        $expired = $this->input->post('expired',true);
        $maxCount = $this->input->post('maxCount',true);
        if (!empty($expired)) {
            $age = $expired*86400;
        } else {
            $age = 365*86400;
        }
        $ret_data_info = $this->xfile_model->info($this->token,$xid);
        if(empty($ret_data_info) ||$ret_data_info['code']!=API_RET_SUCC){
            self::res(array('code'=>1005,'msg'=>'无权限'));
            exit;
        }
        if(isset($ret_data_info) && isset($ret_data_info['code'])){
            if($ret_data_info['data']['role']==ROLE_INVISIBLE && $ret_data_info['data']['is_share']==1){
                self::res(array('code'=>1005,'msg'=>'无权限'));
                exit;
            }
        }
        $ret_data = $this->xfile_model->linkcreate($this->token,$xid,$password,$age,$maxCount);
        self::res($ret_data);
    }

    /**
     * 发布内链
     */
    public function insidelinkpublish()
    {
        $xid = $this->input->post('xid',true);
        $ret_data = $this->xfile_model->create_inside_link($this->token,$xid);
        self::res($ret_data);
    }

    /**
    * 更新链接信息
    */
    public function updatelink()
    {
        $xid = $this->input->post('xid',true);
        $ret_data_info = $this->xfile_model->info($this->token,$xid);
        if(empty($ret_data_info) ||$ret_data_info['code']!=API_RET_SUCC){
            self::res(array('code'=>1005,'msg'=>'无权限'));
            exit;
        }
        if(isset($ret_data_info) && isset($ret_data_info['code'])){
            if($ret_data_info['data']['role']==ROLE_INVISIBLE && $ret_data_info['data']['is_share']==1){
                self::res(array('code'=>1005,'msg'=>'无权限'));
                exit;
            }
        }
        $linkId = $this->input->post('linkId',true);
        $password = $this->input->post('password',true);
        $expired = $this->input->post('expired',true);
        $ctime = $this->input->post('ctime',true);
        if (!empty($expired)) {
            if(strtotime($expired." ".date('H:i',$ctime)) > time()) {
                $age = strtotime($expired." ".date('H:i',$ctime)) - $ctime;
            } else {
                $age = strtotime($expired." 23:59:59") - $ctime;
            }
            if($age<=0) {
                self::res(array('code'=>1,'msg'=>'错误的日期格式'));
            }
        } else {
            $age = 365*86400;
        }
        log_message('debug','password ='.$password);


        $ret_data = $this->xfile_model->linkupdate($this->token,$linkId, $password, $age);
        self::res($ret_data);
    }

    /**
    * 获取文件信息
    */
    public function info()
    {
        $xid = $this->input->get('xid',true);
        $ret_data = $this->xfile_model->info($this->token,$xid);
        $ret_data['data']['parent_id'] = $ret_data['data']['parent_xid'];
        self::res($ret_data);
    }

    /**
    * 订阅消息
    */
    public function subscribe()
    {
        $xid = $this->input->post('xid',true);
        $ret_data = $this->xfile_model->subscribe($this->token, $xid);
        self::res($ret_data);
    }

    /**
    * 取消订阅消息
    */
    public function unsubscribe()
    {
        $xid = $this->input->post('xid',true);
        $ret_data = $this->xfile_model->unsubscribe($this->token, $xid);
        self::res($ret_data);
    }


    /**
    * 取消订阅消息
    */
    public function subscribelist()
    {
        $ret_data = $this->xfile_model->subscribe_list($this->token);
        self::res($ret_data);
    }

    public function notview()
    {
        $this->load->model('platform_model');
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach($ret_data as $value) {
            $paltform[$value['keyname']] = $value['value'];
        }
        $data['paltform'] = $paltform;
        $this->parser->parse("page/notview.tpl",$data);
    }

    /**
     * 文件历史版本
     *
     */
    function history()
    {
        $xid = $this->input->post('xid',TRUE);
        $ret_data = $this->xfile_model->history_list($this->token,$xid);
        self::res($ret_data);
    }


    /**
     * 历史版本 备注
     */
    public function remark()
    {
        $xid = $this->input->post('xid',TRUE);
        $fileVer = $this->input->post('fileVer',TRUE);
        $remark = $this->input->post('remark',TRUE);
        $ret_data = $this->xfile_model->history_remark($this->token, $xid,$fileVer,$remark);

        self::res($ret_data);
    }
    /**
     * 历史版本 还原
     */
    public function history_recover()
    {
        $xid = $this->input->post('xid',TRUE);
        $fileVer = $this->input->post('fileVer',TRUE);
        $ret_data = $this->xfile_model->history_recover($this->token, $xid,$fileVer);
        self::res($ret_data);
    }

    /**
     * 删除被移交的员工个人目录
     */
    public function deleterecyclefile()
    {

        $xid = $this->input->post('xid',TRUE);
        $userXid = $this->input->post('userXid',TRUE);

        $ret_data = $this->xfile_model->recycle_del_file($this->token, $xid,$userXid);

        usleep(100000);
        self::res($ret_data);
    }

    public function levelsearch()
    {
        $xid = $this->input->post('xid',TRUE);
        $name = $this->input->post('name',TRUE);
        $ret_data = $this->xfile_model->level_search($this->token, $xid, $name);
        self::res($ret_data);
    }
    public function globalsearch()
    {

        self::decryption();

        $name = $this->input->get('name',TRUE);
        //$name=preg_replace("/\/|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\.|\/|\;|\'|\`|\-|\=|\\\|\|/"," ",$name);
        $isShare = $this->input->get('isShare',TRUE);
        $start=$this->input->get('start',TRUE);
        $offset=$this->input->get('offset',TRUE);
        $mime=$this->input->get('mime',TRUE);
        $order=$this->input->get('order',TRUE);
        $sortBy=$this->input->get('sortBy',TRUE);
        $data=array(
            'token'=>$this->token,
            'name'=>$name,
            'isShare'=>intval($isShare),
            'start'=>intval($start),
            'offset'=>intval($offset),
            'xmime'=>intval($mime),
            'order'=>intval($order),
            'sortBy'=>intval($sortBy)
        );
        $ret_data = $this->xfile_model->global_search($data);

        if ($ret_data && isset($ret_data['data']) && is_array($ret_data['data']['records'])) {
            $ret_data['data']['records']=$this->add_file_lock_status($ret_data['data']['records']);
        }
        self::res($ret_data);
    }

    private function  add_file_lock_status($files){
        foreach($files as &$folder) {
            if(isset($folder['locked']) && $folder['locked']==1){
                //已被锁定
                $ret_lock_status = $this->xfile_model->file_lock_status($this->token, $folder['xid']);
                if($ret_lock_status['code']==API_RET_SUCC){
                    $folder['lock_info']=$ret_lock_status['data'];
                }
            }
        }
        return $files;
    }
    public function fileLockStatus()
    {
        $xid = $this->input->get('xid',TRUE);
        $ret_data = $this->xfile_model->file_lock_status($this->token, $xid);
        self::res($ret_data);
    }
    public function lock()
    {
        $xid = $this->input->get('xid',TRUE);
        $status = $this->input->get('status',TRUE);
        $ret_data = $this->xfile_model->file_lock($this->token, $xid, $status);
        self::res($ret_data);
    }

    public function copy()
    {
        set_time_limit(0);
        $dstXid = $this->input->post('dstXid',TRUE);
        $xids = $this->input->post('xids',TRUE);
        $is_share = $this->input->post('isShare',TRUE);
        $xids_len = count($xids);
        if($xids_len==0){
            selt::res(array('code'=>3));
        }
        $ret_data = $this->xfile_model->copy_file_batch($this->token, $xids, $dstXid,$is_share);
        //usleep(100000);
        self::res($ret_data);
    }

    public  function copy_check(){

        $jobId = $this->input->post('jobId',TRUE);
        $isShare = $this->input->post('isShare',TRUE);
        $ret_data = $this->xfile_model->copy_file_batch_check_check($this->token, $jobId,$isShare);
        self::res($ret_data);
    }

    public function file_page_list(){

        $xid = $this->input->post('xid',TRUE);
        $sortBy = $this->input->post('sortBy',TRUE);
        $order =$this->input->post('order',TRUE);
        $isShare=$this->input->post('isShare',TRUE);
        $ret_data = $this->xfile_model->file_page_list($this->token,$xid,$sortBy,$order,$isShare);
        if(API_RET_SUCC == $ret_data['code'] && $ret_data['data']){
            $ret_data['data']['result']=$this->add_file_lock_status($ret_data['data']['result']);
            self::res($ret_data);
        }else{
            self::res($ret_data);
        }
    }
    public function file_page_list_continue(){
        $cursor = $this->input->get('cursor',TRUE);
        $isShare=$this->input->get('isShare',TRUE);
        $ret_data = $this->xfile_model->file_page_list_continue($this->token,$cursor,$isShare);
        if (API_RET_SUCC == $ret_data['code'] && $ret_data['data']['result'] && count($ret_data['data']['result'])>0 ) {
            $ret_data['data']['result']=$this->add_file_lock_status($ret_data['data']['result']);
            self::res($ret_data);
        }
        self::res($ret_data);
    }





}
/* End of file file.php */
/* Location: ./application/controllers/file.php */
