<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('account_model');
        //print_r($this);
        //$d = $this->router->method;
        //if("eyeac" ==$d) {
            //$this->eyeac();
       // }
       // exit;
    }


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode',TRUE);

        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
        } else {
            $allowKey = array(
                'web_crt',
                'web_crt_help',
                'sitename',
                'offline',
                'sendemailurl',
                'https');
            $ret_data = $this->platform_model->getValues($allowKey);
            $paltform = array();
            foreach($ret_data as $value) {
                $paltform[$value['keyname']] = $value['value'];
            }
            if(!isset($paltform['web_crt'])) {
                $paltform['web_crt'] = '/crt/kingfile.crt';
            }
            if(!isset($paltform['web_crt_help'])) {
                $paltform['web_crt_help'] = '/crt/kingfile.doc';
            }
            $data['paltform'] = $paltform;
            $data['platform']['producttype'] = PRODUCTTYPE;
            $data['platform']['default_domain_name'] = DEFAULT_DOMAIN_NAME;

            if($paltform['offline']=='1') {
                $this->parser->parse("page/offline.tpl",$data);
            } else {
                /*if ($this->token) {//已经登录了
                    $this->load->model('account_model');
                    $info_res = $this->account_model->info($this->token);
                    if(API_RET_SUCC == $info_res['code']) {
                        $data['userInfo'] = $info_res['data'];
                    }
                }*/
                $data['loginclass'] = 'class="active"';
                if(isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
                    $host=$_SERVER['HTTP_X_ORIGIN_HOST'];
                } else {
                    $host = $_SERVER['REMOTE_ADDR'];
                }
                if($paltform['https']==1){
                    $data['host']="https://".$host;
                }else{
                    $data['host']="http://".$host;
                }
                //echo base_url();
                $this->parser->parse("page/login.tpl",$data);
                //$this->parser->parse("page/initkingfile.tpl",$data);
            }

        }
    }

    //定制logo的企业
    public  function  c(){
        $from = $this->uri->segment(3);
        $this->login_custom($from);
    }
    //眼科医院的定制
    public function eyeac(){
        $from = $this->uri->segment(2);
        $this->login_custom($from);
    }


    public function login_custom($from){
        //定制logo的企业
        //$this->create_qr_png();
        /*if('eyeac'==$from){
            header("Content-type: text/html; charset=utf-8");
            echo "<script>alert('暂时无法访问！');</script>";
            exit;
        }*/

        $customize=array(
            "ceshi001"=>array("pc_addr"=>"ceshi001"),
            "kingfile"=>array("pc_addr"=>"1111"),
            "novogene"=>array(
                "pc_addr"=>"https://kss.ksyun.com/ecloud/Customize/novogene/novogene_2.1.0.71.exe",
                "mac_addr"=>"https://kss.ksyun.com/ecloud/Customize/novogene/Novogene_Netdisk_v1.0.8.dmg",
                "android_addr"=>"https://kss.ksyun.com/ecloud/Customize/novogene/novogene_v2.0.0.apk",
                "ios_addr"=>"itms-services://?action=download-manifest&url=https://kss.ksyun.com/ecloud/Customize/novogene/novogene_v2.6.4.plist",
                "client_more"=>'',
                "info"=>array(
                    'ident'=>'',
                    'txt'=>'',
                    'qrcode'=>''
                )
            ),
            "eyeac"=>array(
                "pc_addr"=>"https://kss.ksyun.com/ecloud/public/client/Public_KingFile_NetDisk_2.2.0.7.exe",
                "mac_addr"=>"https://kss.ksyun.com/ecloud/public/Mac/kingfile_mac_v1.0.5.3.dmg",
                "android_addr"=>"",
                "ios_addr"=>"",
                "client_more"=>'https://pan.ksyun.com/downloadclient',
                "info"=>array(
                    'ident'=>'',
                    'txt'=>'温州医科大学附属眼视光医院成立于1998年，2009年经省卫生厅批准增挂“浙江省眼科医院”，设有温州总院、杭州院区、之江院区等三个院区，是目前浙江省唯一一家三级甲等眼科专科医院。经过近二十年的发展，医院形成了集医疗、教学、科研、产业、公益、推广为一体的眼视光体系。',
                    'qrcode'=>''
                )
            )
        );
        $info_duc=array();
        if(empty($from) || !array_key_exists($from,$customize)) {
            header("location:/");
            exit;
        }


        $data = $this->config->config['netdisk.resources'];
        $data['platform']=array();
        $data['platform']['producttype'] = PRODUCTTYPE;
        $data['platform']['default_domain_name'] = DEFAULT_DOMAIN_NAME;
        $this->load->helper('url');
        $data['base_url'] = base_url();

        if ($this->token) {//肯定是已经登录了
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
                $data['userInfo'] = $info_res['data'];
            }
        }else{
            //没有登录 从金山云官网跳转
            if ($this->ksyun_token) {//管理员登录
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                    $data['userInfo'] = array();
                    $data['userInfo']['user_name'] = $info_res['ksyun_user']['email'];
                    $data['userInfo']['super_type'] = 1;
                }
            }
        }


        $this->load->model('org_model');
        if(isset($_SERVER['KFILE_LOCAL_LOGIN_KEY'])){
            $token=$_SERVER['KFILE_LOCAL_LOGIN_KEY'];
        }else{
            $token='';
        }
        $info=$this->org_model->domain_info_by_domainIdent($token, $from);
        if($info['code']==API_RET_SUCC){
            $data['logo']=$info['data']['logo'];
        }else{
            $data['logo']='';
        }
        if($from=='eyeac'){
            $data['classtype']='login-page-customize-2';
        }else{
            $data['classtype']='';
        }
        //$data['logo']="111";
        //echo $info['data']['logo'].'q';
        $data['paltform']['from']=$from;
        $data['paltform']['client_windows']=$customize[$from]['pc_addr'];
        $data['paltform']['client_mac']=$customize[$from]['mac_addr'];
        $data['paltform']['client_android']=$customize[$from]['android_addr'];
        $data['paltform']['client_ios']=$customize[$from]['ios_addr'];
        $data['paltform']['info']=$customize[$from]['info'];
        $data['paltform']['client_more']=$customize[$from]['client_more'];
        $this->parser->parse("page/login_customize.tpl",$data);
    }

    public  function create_qr_png(){

        require_once 'phpqrcode.php';
        $errorLevel = "L";
        $size = "4";
        $url="http://www.lpcblog.com/";
        echo (QRcode::png($url, false, $errorLevel, $size));
    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */