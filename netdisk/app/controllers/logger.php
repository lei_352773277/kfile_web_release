<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 控制面板
 * User: qyuan
 * Date: 15-5-19
 * Time: 下午7:48
 */

class Logger extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('logger_model');
    }

    /**
    * 列出根共享目录
    */
    public function query()
    {

        self::checkuseridentity_is_admin();

        $user_name = $this->input->post('user_name',TRUE);
        if(empty($user_name)) {
            $user_name = "";
        }
        $share_root = $this->input->post('share_root',TRUE);
        if(empty($share_root)) {
            $share_root = "";
        }
        $log_ids = $this->input->post('log_ids',TRUE);
        if(empty($log_ids)) {
            $log_ids =  array();
        }

        $hint = $this->input->post('hint',TRUE);
        if(empty($hint)) {
            $hint = 0;
        }

        $startTime = $this->input->post('start_time',TRUE);

        if(empty($startTime)) {
            $startTime = "";
        }
        $endTime = $this->input->post('end_time',TRUE);
        if(empty($endTime)) {
            $endTime = "";
        }
        $pageMax = $this->input->post('pageMax',TRUE);
        if(empty($pageMax)) {
            $pageMax = 50;
        }


        $ret_data = $this->logger_model->query($this->token,$user_name,$share_root,$log_ids, $startTime,$endTime,$hint,$pageMax);
         //var_dump($ret_data);
         /*
         $ret_data = array("code"=>0,"data"=>array(
            array("user_id"=>6,
                "ctime"=>date("Y-m-d H:i",1436941425),
                "log_id"=>201,
                "xfile_name"=>"",
                "arg"=>"{\"xid\": 13, \"name\": \"\\u653f\\u4f01\\u4e8b\\u4e1a\\u90e8\"}",
                "device"=>0,
                "ip_addr"=>178286363,
                "user_name"=>"888@qq.com",
                "domain_id"=>3)
            ));
        */
         self::res($ret_data);
    }


    public function xfilelist()
    {
        $ret_data = $this->logger_model->logger_xfilelist($this->token);
        self::res($ret_data);
    }


    public function export_logger_csv(){

        $type = $this->input->get('type',TRUE);
        if('operate'==$type){
            $file_name_h='操作日志';
            $str = "成员名,共享文件夹,操作,具体操作,时间,IP,设备\n";

            $user_name = $this->input->get('user_name',TRUE);
            if(empty($user_name)) {
                $user_name = "";
            }
            $share_root = $this->input->get('share_root',TRUE);
            if(empty($share_root)) {
                $share_root = "";
            }
            $log_ids = $this->input->get('log_ids',TRUE);
            if(empty($log_ids)) {
                $log_ids =  array();
            }

            $hint = $this->input->get('hint',TRUE);
            if(empty($hint)) {
                $hint = 0;
            }
            $startTime = $this->input->get('start_time',TRUE);
            if(empty($startTime)) {
                $startTime = "";
            }
            $endTime = $this->input->get('end_time',TRUE);
            if(empty($endTime)) {
                $endTime = "";
            } else {

            }
            $pageMax = $this->input->get('pageMax',TRUE);
            if(empty($pageMax)) {
                $pageMax = 255;
            }


            $ret_data = $this->logger_model->query($this->token,$user_name,$share_root,$log_ids, $startTime,$endTime,$hint,$pageMax);
        }


        if(!empty($ret_data) && !empty($ret_data['data'])){
            $export_data = $ret_data['data'];
        }
        //导出的标题对应
        $str = iconv('utf-8','gb2312',$str);

        $str .=$this->filter_export_data($export_data,$type);

        //print_r($str);exit;

        //设置文件名
        $filename =iconv('utf-8','gb2312',$file_name_h.date('Y-m-d H:i:s',time()).'.csv');
        $this->export_csv($filename,$str); //导出

    }

    public  function filter_export_data($export_data,$type){
        $str='';
        if('operate'==$type){
            foreach($export_data as &$value){
                if($value['share_root']!=''){
                    $share_root=$value['share_root'];
                }else{
                    $share_root="-";
                }
                $logaction=$this->fileter_logaction($value['log_id']);
                $arg_json=json_encode($value['log_id']);
                $time=date('Y-m-d H:i:s',$value['ctime']);
                $str .= $value['user_name'].",".$share_root.",".$logaction.",".$arg_json.",".$time.",".$value['ip_addr'].",".$value['device_type']."\n"; //用引文逗号分开
            }
        }
        return $str;

    }
    public function fileter_logaction($log_id){
        return $log_id;
    }
    public function export_csv($filename,$data) {
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=".$filename);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        echo $data;
    }

    
}


/* End of file logger.php */
/* Location: ./application/controllers/logger.php */