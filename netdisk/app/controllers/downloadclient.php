<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downloadclient extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('account_model');
    }

	public function index()
	{
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->get_value('sitename');
        $this->load->model('platform_model');
        $platform = array();
        $allowKey = array(
            //'client_windows_sync',
            'client_win_sync',
            'client_download_info'
        );
        $paltform_ret_data = $this->platform_model->getValues($allowKey);
        foreach ($paltform_ret_data as $value) {
            if (in_array($value['keyname'], $allowKey)) {
                $platform[$value['keyname']] = $value['value'];
            }
        }
        $platform['sitename'] = $ret_data[0]['value'];
        $data['paltform'] = $platform;
        $data['downloadclientclass'] = 'class="active"';

        if (!empty($platform['client_download_info']))
        {
            $info =  json_decode($platform['client_download_info'],true);
            $fmtInfo = array();
            if(is_array($info)) {
                foreach($info as $k=>$v) {
                    $key = substr($k, 7);
                    $key = str_replace("windows", "win", $key);

                    if (strpos($key, "_msg")!==False) {
                        $fmtKey = str_replace("_msg", "_update", $key);
                        $vList = explode("\\n", $v);
                        $fmtInfo[$fmtKey] = $vList;
                    } else {
                        $fmtInfo[$key] = $v;
                    }
                }
                $data['client_info'] = $fmtInfo;
            }
        }

        if (empty($data['client_info'])) {
            $data['client_info']=array(
                'win_sync_version'=>"2.0.1.26",
                'win_sync_size'=>"7.82M",
                'win_sync_update'=>array(
                    "1.全新的同步盘上线",
                    "2.支持双向同步",
                    "3.支持分享外链/查看共享等操作",
                    "4.支持过滤同步文件/文件夹"

                )
            );
        }
        //$data['showheader'] = PRODUCTMODEL==2?true:false;

        //$login_info=$this->get_login_info();
        if(!empty($login_info)){
            //$data['userInfo']=$login_info['userInfo'];
        }

        $this->parser->parse("page/downloadclient.tpl",$data);
	}

    private  function get_login_info(){
        $data='';
        if ($this->token) {//肯定是已经登录了
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
                $data['userInfo'] = $info_res['data'];
            }
        }else{
            //没有登录 从金山云官网跳转
            if ($this->ksyun_token) {
                $this->load->model('account_model');
                $info_res = $this->account_model->getkingfile($this->ksyun_token);
                if($info_res) {
                    $data['userInfo'] = array();
                    $data['userInfo']['user_name'] = $info_res['ksyun_user']['email'];
                    $data['userInfo']['super_type'] = 1;
                }
            }
        }
        return $data;
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */