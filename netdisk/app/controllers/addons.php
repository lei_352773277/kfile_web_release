<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addons extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
    }

	public function index()
	{
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->info();
        $paltform = array();
        foreach($ret_data as $value) {
        	$paltform[$value['keyname']] = $value['value'];
        }
        $paltform['producttype'] = PRODUCTTYPE;
        $paltform['productmodel'] = PRODUCTMODEL;

        $list = $this->input->get('list');
        $list = intval($list);
        $data['list'] = $list;

        $source = $this->input->get('source');
        $source = intval($source);
        $data['source'] = $source;

        $data['paltform'] = $paltform;
        if (empty($this->token)) {
            $this->parser->parse("addons/login.tpl",$data); 
        } else {
            $this->load->model('account_model');
            $info_res = $this->account_model->info($this->token);
            if(API_RET_SUCC == $info_res['code']) {
                $this->load->model('org_model');
                $domain_info=$this->org_model->domain_info($this->token);
                if(!empty($domain_info) && 0===$domain_info['code'] ){
                    $configInfo=$this->org_model->get_config_setting($this->token,$domain_info['data']['domain_id']);
                    $configInfo=$configInfo['data'];
                    $expireDate= intval($configInfo['limits']['start_time'])+intval($configInfo['limits']['expire']);
                    $encrypted_string=$this->my_encryption($expireDate."@@@".$this->token.'@@@'.time());
                    $this->write_token($encrypted_string);
                }
                $this->parser->parse("addons/dashboard.tpl",$data); 
            } else {
                $this->parser->parse("addons/login.tpl",$data); 
            }
        }
	}

    function my_encryption($value){
        $CI =& get_instance();
        $key = $CI->config->item('encryption_key');
        $encrypted_string=rc4($key,$value);
        return bin2hex($encrypted_string);

    }

    private function write_token($value){

        $this->load->helper('cookie');
        $cookie_ld=$this->input->cookie('netdisk_ld', TRUE);
        if(!empty($cookie_ld)){
            delete_cookie('netdisk_ld');
        }
        $cookie = array(
            'name' => 'ld',
            'value' => $value,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($cookie);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */