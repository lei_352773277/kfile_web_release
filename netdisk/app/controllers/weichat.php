<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Weichat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('org_model');
        $this->load->model('account_model');
        $this->load->model('wechat_model');
    }

    public  function account(){
        $token = $this->input->get('token',true);
        $token = html_entity_decode($token);
        $agent = $this->input->get('agent',true);
        $agent = html_entity_decode($agent);
        $client_device_id = urldecode($agent);
        //define("KINGFILE_USER_AGENT", $client_device_id);
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->wechat_model->info($token,$client_device_id);
        if(API_RET_SUCC!=$ret_data['code']){
            $data['info_empty']=1;
        }else{
            if ($ret_data['code'] == API_RET_SUCC && $ret_data['data'] && $ret_data['data']['xid']) {
                $staff_info = $this->wechat_model->staff_info($token, $ret_data['data']['xid'],$client_device_id);
                $ret_data['data']['staff'] = $staff_info['data'];
            }
            $ret_data['data']['token'] = $token;
            $ret_data['data']['agent'] = $client_device_id;
            //$ret_data['data']['token'] = $token;
            $ret_data['data']['resources'] = $this->config->config['netdisk.resources'];
            //$ret_data['data']['wechat_id']=1;
            $data['userinfo']=$ret_data['data'];
            //space
            if($ret_data['data'] && $ret_data['data']['is_superuser']==1){
                $space_data = $this->wechat_model->domain_space_info($token,$client_device_id);
            }else{

                $xid = $ret_data['data']['xid'];
                if(empty($xid)) {
                    $data['info_space_error']=1;
                }
                $space_data = $this->wechat_model->staff_space_info($token,$xid,$client_device_id);
            }
            if(API_RET_SUCC==$space_data['code']){
                $space_data['data']['usedfilter']=$this->readablizeBytesWithZero($space_data['data']['used']);
                $space_data['data']['quotafilter']=$this->readablizeBytesWithZero($space_data['data']['quota']);
                $data['spaceinfo']=$space_data['data'];

            }
        }


        $this->parser->parse("page/wechat-user-info.tpl",$data);
    }


    /**
     * 获取登陆的用户信息
     */
    function info()
    {
        $token=$this->input->post("token",true);
        $agent=$this->input->post("agent",true);
        $ret_data = $this->wechat_model->info($token,$agent);
        self::res($ret_data);
    }

    private  function readablizeBytesWithZero($bytes){
        if(0 == $bytes) {
            return '0 KB';
        } else {
            $s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
            $e = floor(log($bytes) / log(1024));
            return number_format(($bytes / pow(1024, $e)),2) ." " .$s[$e];
        }
    }
}

/* End of file account.php */
/* Location: ./application/controllers/account.php */
