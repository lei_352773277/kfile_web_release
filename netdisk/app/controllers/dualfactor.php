<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dualfactor extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('dualfactor_model');
        $this->load->model('account_model');
    }


    public  function email_view(){
        $ret_data = $this->dualfactor_model->email_view($this->token);
        self::res($ret_data);
    }

    public  function email_bind(){
        $email=$this->input->post('email',true);
        $this->load->helper('email');
        if(!empty($email)&&!valid_email($email) ) {
            self::res(array('code'=>'5004','data'=>$email));
        }
        $ret_data = $this->dualfactor_model->email_bind($this->token,$email);
        self::res($ret_data);
    }
    public  function email_send(){
        $ret_data = $this->dualfactor_model->email_send($this->token);
        self::res($ret_data);
    }

    /*双因子认证 是否绑定*/
    public function device_bind_check(){
        $ret_data = $this->dualfactor_model->device_bind_check($this->token);
        //sleep(3);
        //$ret_data['code']=0;
        self::res($ret_data);
    }

    /*双因子认证开启与关闭*/
    public  function dual_factor_close(){
        $banKey=$this->input->post('banKey',true);
        $ret_data = $this->dualfactor_model->dual_factor_close($this->token,$banKey);
        self::res($ret_data);
    }

    //关闭
    public  function close(){
        $token = $this->input->get('token', TRUE);
        $agent = $this->input->get('agent', TRUE);
        $code = $this->input->get('code', TRUE);
        $data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode',TRUE);

        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
            exit;
        }
        $this->write_user_agent($agent);
        $this->write_token($token);

        $userinfo = $this->account_model->info($token);
        if(isset($userinfo) && isset($userinfo['code']) && $userinfo['code']==API_RET_SUCC){
            $data['userinfo']=$userinfo['data'];
        }

        $ret_data_close = $this->dualfactor_model->dual_factor_close($token,$code);
        if(isset($ret_data_close)){
            $data['dual_close']=$ret_data_close;
        }
        $this->parser->parse("page/dualfactor_close.tpl",$data);
    }

    //定义user agent
    private function write_user_agent($agent)
    {
        $this->load->helper('cookie');
        if (isset($_COOKIE['netdisk_device_id'])) {
            delete_cookie('netdisk_device_id');
            if (!defined("KINGFILE_USER_AGENT")) {
                define("KINGFILE_USER_AGENT", $agent);
            }
        } else {
            if (!defined("KINGFILE_USER_AGENT")) {
                define("KINGFILE_USER_AGENT", $agent);
            }
        }
        $this->write_token_deviceId($agent);
    }
    /*
    * 写cookie
    * */
    private function write_token_deviceId($agent)
    {
        $this->load->helper('cookie');
        $device_id = array(
            'name' => 'device_id',
            'value' => $agent,
            'expire' => time() + 99 * 365 * 24 * 3600,
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($device_id);
    }
    private function write_token($token)
    {
        $this->load->helper('cookie');

        if (isset($_COOKIE['netdisk_sid'])) {
            delete_cookie("netdisk_sid");
        }
        $cookie = array(
            'name' => 'sid',
            'value' => $token,
            'expire' => '86500',
            'prefix' => 'netdisk_'
        );
        $this->input->set_cookie($cookie);

    }

    /*双因子认证 扫码登录 查询*/
    public  function polling_auth_check(){
        $ret_data = $this->dualfactor_model->polling_auth_check($this->token);
        //$ret_data=array('code'=>0,'data'=>array('token'=>"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJpc3MiOiJsb2dpbiIsImV4cCI6MTUxMDE1MTYwOCwiaXNhIjoxNTA5NTQ2ODA4LCJkZXZpY2UiOiJXRUItSWdvTHVqaHh4YzFJQXJqayJ9.Bl5YOLGbaa6Tdyvz8FYnX0GvF5j9YpCvnC4XVrso5No"));
        if(isset($ret_data) && $ret_data['code']==API_RET_SUCC){
            //将token 写入cookie
            $this->overwrite_token($ret_data);
        }
        self::res($ret_data);
    }

    private function overwrite_token($res_data)
    {
        $this->load->helper('cookie');
        if (API_RET_SUCC == $res_data['code']) {
            if(isset($_COOKIE['netdisk_sid'])){
                delete_cookie("netdisk_sid");
            }
            $cookie = array(
                'name' => 'sid',
                'value' => $res_data['data']['token'],
                'expire' => '86500',
                'prefix' => 'netdisk_'
            );
            $this->input->set_cookie($cookie);
        }
    }

    /*双因子认证 发送关闭邮件*/
    public  function ban_email_send(){
        $ret_data = $this->dualfactor_model->ban_email_send($this->token);
        self::res($ret_data);
    }

    /*双因子认证 登陆二维码的字符串*/
    public  function qrcode_scene_id(){
        $ret_data = $this->dualfactor_model->qrcode_scene_id($this->token);
        self::res($ret_data);
    }








}

/* End of file account.php */
/* Location: ./application/controllers/account.php */
