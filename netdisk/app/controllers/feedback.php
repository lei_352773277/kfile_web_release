<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('account_model');
    }

	public function index()
	{
        $data = $this->config->config['netdisk.resources'];
        $ret_data = $this->platform_model->get_value('sitename');
        $paltform = array();
        $paltform['sitename'] = $ret_data[0]['value'];
        $data['paltform'] = $paltform;
        $data['feedbackclass'] = 'class="active"';
        $from = $this->input->get('from',TRUE);
        $data['from'] = $from;
        $data['fromMobile']= 0;
        if(2==PRODUCTMODEL) {
            $this->parser->parse("page/feedback.tpl",$data);
        } else {
            $this->parser->parse("page/feedback_3.tpl", $data);
        }
    }


    //保存移动端的反馈
    public function commit()
    {
        $from = $this->input->post('from', TRUE);
        $content = $this->input->post('content', TRUE);
        $qq = $this->input->post('qq', TRUE);
        $domain = $this->input->post('domain', TRUE);
        $user = $this->input->post('user', TRUE);
        $sign = $this->input->post('sign', TRUE);

        log_message("debug",'feedback=>'."post data:".'domain='.$domain.',user='.$user.',content='.$content.',qq='.$qq.',sign='.$sign);

        if (empty($domain) || empty($user) || empty($content) || empty($qq) || empty($sign)) {
            self::res(array('code' => 1, 'msg' => "数据有为空"));
            exit;
        }
        //计算签名
        if ($sign !== md5("kingfile" . $domain . $user)) {
            self::res(array('code' => 2, 'msg' => "sign error", "sign" => md5("kingfile" . $domain . $user), "post data sign" => $sign));
            exit;
        }
        $sql_row = $this->platform_model->saveFeedBack($domain, $user, $content, $from, $qq);
        if($sql_row){
            self::res(array('code' => 0, 'msg' => "success"));
        }else{
            self::res(array('code' => 3, 'msg' => "database fail"));
        }

    }
        

    //保存反馈结果
    public function save()
    {
        $from = $this->input->post('from', TRUE);
        $content = $this->input->post('content', TRUE);
        $qq = $this->input->post('qq', TRUE);
        log_message("debug",'feedback=>'.'content='.$content.',qq='.$qq);
        if (empty($from) || $from == "web" || strpos($from, "kingsoft-ecloud-pc") !== false) {
            $this->save_web();
            exit;
        }
    }

    private function save_web(){
        $ret_data = $this->account_model->info($this->token);
        if ($ret_data['code'] === API_RET_SUCC) {
            $content = $this->input->post('content', TRUE);
            $qq = $this->input->post('qq', TRUE);
            if (empty($content) || trim($content) == "") {
                header("Content-type: text/html; charset=utf-8");
                echo "<script>alert('请输入反馈内容');history.go(-1);</script>";
                exit;
            }

            $qq = intval($qq);
            $domain = $ret_data['data']['domain_ident'];
            $user = $ret_data['data']['email'];
            if(empty($user)) {
                $user = $ret_data['data']['user_name'];
            }
            $from = $this->input->post('from',TRUE);
            if(empty($from)) {
                $from = "web";
            }
            $this->platform_model->saveFeedBack($domain, $user, $content, $from, $qq);
            header("Content-type: text/html; charset=utf-8");
            echo "<script>alert('提交反馈成功');location.href='/feedback';</script>";
        } else {
            header("location:/?nologin");
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */