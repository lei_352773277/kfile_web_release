<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('platform_model');
        $this->load->model('account_model');
    }


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode',TRUE);

        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
        } else {
            $allowKey = array(
                'web_crt',
                'offline',
                'https');
            $ret_data = $this->platform_model->getValues($allowKey);
            $paltform = array();
            foreach($ret_data as $value) {
                $paltform[$value['keyname']] = $value['value'];
            }
            if(!isset($paltform['web_crt'])) {
                $paltform['web_crt'] = '/crt/kingfile.crt';
            }
            $data['paltform'] = $paltform;
            if($paltform['offline']=='1') {
                $this->parser->parse("page/offline.tpl",$data);
            } else {

                if(isset($_SERVER['HTTP_X_ORIGIN_HOST'])) {
                    $host=$_SERVER['HTTP_X_ORIGIN_HOST'];
                } else {
                    $host = $_SERVER['REMOTE_ADDR'];
                }
                if($paltform['https']==1){
                    $data['host']="https://".$host;
                }else{
                    $data['host']="http://".$host;
                }
                $this->parser->parse("page/regist.tpl",$data);
                //$this->parser->parse("page/initkingfile.tpl",$data);
            }

        }
    }

    public function result()
    {
        $data = $this->config->config['netdisk.resources'];
        $logincode = $this->input->get('logincode',TRUE);

        if(!empty($logincode)) {
            $data['msg'] = "验证失败，您暂没权限登录该系统";
            $this->parser->parse("page/notice.tpl",$data);
        } else {
            $this->parser->parse("page/register_result.tpl",$data);
        }
    }







}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */