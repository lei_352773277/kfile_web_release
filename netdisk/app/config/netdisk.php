<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: qyuan
 * Date: 15-3-19
 * Time: 上午11:34
 */

//1 private, 2 public , 3 mix
//$_SERVER['HTTP_X_ORIGIN_SCHEME']='http';
if(isset($_SERVER['KFILE_TARGET_ENV'])) {
    if($_SERVER['KFILE_TARGET_ENV'] = "allInOne") {
        define("PRODUCTMODEL",3);
    } elseif ($_SERVER['KFILE_TARGET_ENV'] = "public") {
        define("PRODUCTMODEL",3);
    } elseif ($_SERVER['KFILE_TARGET_ENV'] = "private") {
        define("PRODUCTMODEL",3);
    } else {
        define("PRODUCTMODEL",3);
    }
} else {
    define("PRODUCTMODEL",3);
}

//1 only none, 2 more 
if (isset($_SERVER['KFILE_SINGLE_DOMAIN_MODE'])) {
    if ($_SERVER['KFILE_SINGLE_DOMAIN_MODE'] == 0) {
        define("PRODUCTTYPE",1);
    } else {
        define("PRODUCTTYPE",2);
    }
} else {
   define("PRODUCTTYPE",2);
}

$_SERVER['KFILE_LOCAL_LOGIN_KEY'] = 'Sy_GkrFdfb-X6t3D-rFiUqngsE8Bl_U0';
define('API_SUPER_TOKEN','wangpan@kingsoft.com');//api super token
//define('DEFAULT_DOMAIN_NAME','kingsoft.com');
//define('DEFAULT_DOMAIN_NAME','E:\code\test-243.txt');
//define('DEFAULT_DOMAIN_NAME','E:\code\test-107.txt');
//define('DEFAULT_DOMAIN_NAME','E:\code\test-180.txt');
//define('DEFAULT_DOMAIN_NAME','E:\code\test-9.txt');
//define('DEFAULT_DOMAIN_NAME','E:\code\test-188.txt');
define('DEFAULT_DOMAIN_NAME','/users/weifeng/ksyun/test-110.txt');
define("IMG_PATH","imgs/logo");//保存img的地方
define("KINGFILECONSOLE","http://console.com");//kingfile的console
define("KINGFILEOP","http://op.pan.ksyun.com:9999");//kingfile的console
define("KINGFILEBOSS","http://api.boss.dc.ksyun.com:8099");//kingfile的boss信息获取
//define("ACCOUNTSERVER","http://10.0.3.161/xsvr");
//define("ACCOUNTSERVER","http://10.160.111.76");
define("HOST","http://pan.interncode.com");
define("HOS","http://pan.interncode.com");
$config['netdisk.api'] = array(



    //'server' => 'http://123.59.14.9/api/v1',
    //'server' => 'http://192.168.140.166/api/v1',

    //'server' => 'http://10.69.35.188/api/v1',
    //'server' => 'http://192.168.140.166/api/v1',



    //'server' => 'http://192.168.8.103:481/api/v1',

    //'server' => 'http://192.168.8.103/api/v1',
    'server' => 'http://pan.interncode.com/api/v1',
    'server_v2' => 'http://pan.interncode.com/api/v2',
    'server_audit' => 'http://pan.interncode.com:5555',


    //'server' => 'http://120.92.10.180/api/v1',
    'default_version' => 2,
    //'user_agent' => 'ekp_web'
    'user_agent' =>'flyer-ecloud-web'
);


//开发环境
$config['netdisk.resources'] = array(
    'css' => 'http://localhost:3000',
    'img' => 'static/',
    'js' => 'http://localhost:3000',
    'common' =>'/dist/common',
    'debug' => false,
    'title' => '云盘',
    'cookie_domain'=>'.ks.com'
);

if (1==PRODUCTMODEL) {
    $config['anonymous_access_pages'] = array(
        "account_sso",
        "account_login",
        "account_verify",
        "account_resetpwd",
        "account_forget",
        "account_forgetpwd",
        "account_vaildate",
        "account_loginbyident",
        "account_ksyuninfo",
        "weichat_account",
        "dualfactor_close",
        "l_s",
        //"l_i",
        "l_verfiy",
        "softcenter_index",
        "softcenter_cv",
        "welcome_index",
        "welcome_download",
        "welcome_test",
        "addons_index",
        "addons_test",
        "login_index",
        "login_c",
        "register_index",
        "register_result"
        //"login_eyeac"
    );
} else {
    $config['anonymous_access_pages'] = array(
        "account_sso",
        "account_login",
        "account_verify",
        "account_resetpwd",
        "account_forget",
        "account_signup",
        "account_logout",
        "account_initkingfile",
        "account_forgetpwd",
        "account_vaildate",
        "account_ksyunlogin",
        "account_ksyuninitkingfile",
        "account_loginbyident",
        "account_test",
        "account_ksyuninfo",
        "weichat_account",
        "dualfactor_close",

        //"account_out",
        "l_s",
        //"l_i",
        "l_verfiy",
        "welcome_index",
        "welcome_download",
        "welcome_test",
        "welcome_cons",
        "addons_index",
        "addons_test",
        "shop_index",
        "shop_alipayreturn",
        "shop_alipaynotify",
        "shop_buy",
        "agent_index",
        //"product_index",
        "feedback_index",
        "feedback_save",
        "softcenter_index",
        "softcenter_cv",
        "introduce_public_index",
        "introduce_private_index",
        "introduce_allinoneDevice_index",
        "login_index",
        "login_c",
        //"login_eyeac",
        //"platform_config",
        "usercase_industry",
        "downloadclient_index",
        "register_index",
        "register_result"
    );
}


/*
//生产环境
$config['netdisk.resources'] = array(
    'css' => '/dist/49',
    'img' => 'static/',
    'js' => '/dist/49',
    'common' =>'/dist/common',
    'debug' => false,
    'title' => '金山云盘',
    'cookie_domain'=>'123.59.14.9'
);
*/