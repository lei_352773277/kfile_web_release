<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');




define('API_RET_SUCC',0);
define('LINK_PWD_NOT_MATCH',4063);
define('ACCOUNTLOCK', 2009);

/*权限*/
define('PERM_CODE_VIEW',0x00000001);
define('PERM_CODE_READ',0x00000010);
define('PERM_CODE_EDIT',0x00000100);
define('PERM_CODE_DELETE',0x00001000);
define('PERM_CODE_GRANT',0x00010000);

define('ROLE_NOTHING',-1); //-1
define('ROLE_INVISIBLE',0); //0
define('ROLE_VIEWER',PERM_CODE_VIEW|PERM_CODE_READ); //17
define('ROLE_EDITOR',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT);//273
define('ROLE_MANIPULATOR',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT|PERM_CODE_DELETE);//4369
define('ROLE_ADMIN',PERM_CODE_VIEW|PERM_CODE_READ|PERM_CODE_EDIT|PERM_CODE_DELETE|PERM_CODE_GRANT); //69905
define('ROLE_GOD',0x11111111);//286331153



/*
 * 定义CURL的常量
 */
define("CURL_POST", 'post');
define("CURL_PUT", 'put');
define("CURL_DELETE", 'delete');


/*front response code*/
define('API_RES_OK',10000);
define('NOT_LOGIN','noLogin');
define('API_RES_EMPTY','API_RES_EMPTY');
define('LOGIC_ERROR','LOGIC_ERROR');
define('REQUEST_PARAMS_ERROR','REQUEST_PARAMS_ERROR');
define('SIGN_ERROR','SIGN_ERROR');
define("LOGIN_FAILE",10001);

//"试用":1 ,"支付宝":2,"线下支付":3,"代理商":4
define("PRODUCT_TRIAL",1);//试用产品
define("PRODUCT_ALIPAY",2);//在线产品
define("PRODUCT_SALES",3);//销售
define("PRODUCT_AGENT",4);//代理商

//1:直接购买 2:续费 3:更配 4:转正 5:延期 6:套餐购买
define("PRODUCT_USE_BUY",1);
define("PRODUCT_USE_RENEW",2);
define("PRODUCT_USE_CHANGE",3);
define("PRODUCT_USE_REGULAR",4);
define("PRODUCT_USE_DELAY",5);
define("PRODUCT_USE_SET",6);


/*oauth2 client*/
define("CLIENT_ID","client_id");
define("CLIENT_SECRET","secret_1");

define("CODE_STAFF_NOT_FOUND", 4000001);
define("CODE_SPACE_NOT_FOUND", 4000101);
define("CODE_SPACE_LESS_THAN_CURR", 40000102);

define("PBQUOTA", 1125899906842624);//32位的系统下会溢出

/*Basic Authentication */
define("BASIC_AUTH_USER","kingfile");
define("BASIC_AUTH_PASS","5e6065aedf1b4ebd22a976965c646cd4");





/* End of file constants.php */
/* Location: ./application/config/constants.php */