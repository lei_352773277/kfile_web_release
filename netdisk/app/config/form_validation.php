<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array(
    'org/changespace' => array(
        array(
            'field' => 'xid',
            'label' => 'xid',
            'rules' => 'required'
        ),
        array(
            'field' => 'quota',
            'label' => '空间',
            'rules' => 'required'
        )
    ),
    'org/changepermission' => array(
        array(
            'field' => 'xid',
            'label' => 'xid',
            'rules' => 'required'
        ),
        array(
            'field' => 'permission',
            'label' => 'permission',
            'rules' => 'required'
        )
    ),
    'account/login' => array(
        array(
            'field' => 'user',
            'label' => 'user',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'password',
            'rules' => 'required'
        )
    ),
    'account/changepwd' => array(
        array(
            'field' => 'userpwd',
            'label' => 'userpwd',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'password',
            'rules' => 'required'
        ),
        array(
            'field' => 'confirmpwd',
            'label' => 'confirmpwd',
            'rules' => 'required'
        )
    ),
    'account/resetpwd' => array(
        array(
            'field' => 'token',
            'label' => 'token',
            'rules' => 'required'
        ),
        array(
            'field' => 'newpwd',
            'label' => 'newpwd',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'password',
            'rules' => 'required'
        )
    ),
    'account/forgetpwd' => array(
        array(
            'field' => 'email',
            'label' => 'email',
            'rules' => 'required'
        )
    ),
    'file/createsharefolder' => array(
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ),
        array(
            'field' => 'members',
            'label' => 'members',
            'rules' => 'required'
        )
    ),
    'file/createsharefolder' => array(
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ),
        array(
            'field' => 'members',
            'label' => 'members',
            'rules' => 'required'
        )
    ),
    'file/createfolder' => array(
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        )
    ),
    'file/editShareRoot' => array(
        array(
            'field' => 'xid',
            'label' => 'xid',
            'rules' => 'required'
        ),
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required'
        ),
        array(
            'field' => 'quota',
            'label' => 'quota',
            'rules' => 'required'
        )
    )
                          
);
