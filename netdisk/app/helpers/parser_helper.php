<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author      Dwayne Charrington
 * @copyright  2014 Dwayne Charrington and Github contributors
 * @link           http://ilikekillnerds.com
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @version     2.0
 */
/**
 * Theme URL
 *
 * A helper function for getting the current theme URL
 * in a web friendly format.
 *
 * @param string $location
 * @return mixed
 */
function theme_url($location = '')
{
    $CI =& get_instance();
    return $CI->parser->theme_url($location);
}
/**
 * CSS
 *
 * A helper function for getting the current theme CSS embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('css') )
{
    function css($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->css($file, $attributes);
    }
}
/**
 * JS
 *
 * A helper function for getting the current theme JS embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('js') )
{
    function js($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->js($file, $attributes);
    }
}
/**
 * IMG
 *
 * A helper function for getting the current theme IMG embed code
 * in a web friendly format
 *
 * @param $file
 * @param $attributes
 */
if ( ! function_exists('themes_img') )
{
    function themes_img($file, $attributes = array())
    {
        $CI =& get_instance();
        echo $CI->parser->img($file, $attributes);
    }
}
/**
 * Session
 *
 * A helper function for getting a session variable alias of $this->session->userdata($name)
 *
 * @param $name
 */
if ( ! function_exists('userdata') )
{
    function userdata($name)
    {
        $CI =& get_instance();
        return $CI->session->userdata($name);
    }
}
/**
 * Uri segment
 *
 * A helper function for getting a uri segment alias of $this->uri->segment(n)
 *
 * @param $segnum
 */
if ( ! function_exists('uriseg') )
{
    function uriseg($segnum)
    {
        $CI =& get_instance();
        return $CI->uri->segment($segnum);
    }
}
/**
 * Flashdata
 *
 * A helper function for getting a flash message alias of $this->session->flashdata($name)
 *
 * @param $name
 */
if ( ! function_exists('flashdata') )
{
    function flashdata($name)
    {
        $CI =& get_instance();
        return $CI->session->flashdata($name);
    }
}


if ( !function_exists( 'hex2bin' ) ) {
    function hex2bin( $str ) {
        $sbin = "";
        $len = strlen( $str );
        for ( $i = 0; $i < $len; $i += 2 ) {
            $sbin .= pack( "H*", substr( $str, $i, 2 ) );
        }
        return $sbin;
    }
}


/*
     * rc4加密算法
     * $pwd 密钥
     * $data 要加密的数据
    */
if ( !function_exists( 'rc4' ) ) {
    function rc4 ($pwd, $data)
    {
        $key[] ="";
        $box[] ="";

        $pwd_length = strlen($pwd);
        $data_length = strlen($data);

        for ($i = 0; $i < 256; $i++)
        {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }

        for ($j = $i = 0; $i < 256; $i++)
        {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        $cipher='';
        for ($a = $j = $i = 0; $i < $data_length; $i++)
        {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;

            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;

            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ $k);
        }

        return $cipher;
    }
}
